# -*- coding: utf-8 -*-
# pns 07.2017

from lib_for_DMFT_NRG import *

p={}

#  DMFT NRG program files
p['name_input_file'] = 'DMFT_INPUT_1_band'
p['name_output_file'] = 'DMFT_out'

# program files for critical temperature calculation
p['prog_calc_Tc'] = 'DMFT_Bethe_Tc_mu.run'
p['name_input_file_Tc'] = 'DMFT_input_Tc.dat'
p['name_output_file_Tc'] = 'DMFT_out_Tc.dat'

# program for search critical temperature using mu and T
p['prog_Tc_interpol'] = 'Tc_muT_interpol.run'
p['prog_Tc_interpol_input'] = 'DMFT_input_mu_T_interpol.dat'

# program search lambda
p['Prog_search_lambda'] = 'Prog_search_lambda.run'
p['Prog_search_lambda_out'] = 'NRG_T_out.dat'

p['Emax'] = 5

calc_Tc_all_dirs_in_root( **p )
