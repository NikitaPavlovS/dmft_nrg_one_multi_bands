# -*- coding: utf-8 -*-
# pns 02.2018

import os
import glob
import numpy as np
#from multiprocessing import Pool
from subprocess import Popen, PIPE
import re
#--------------------------------------------------------------------
#--------------------------------------------------------------------
def createFolder(path):
    if not os.path.exists(path):
        os.makedirs(path)

def copyFile(pathFrom, pathTo):
    with open(pathFrom, 'rb') as fileFrom, open(pathTo, 'wb') as fileTo:
        fileTo.write(fileFrom.read())


# input file for Tc program
def inputFile_Tc(mu, temperature, U, Delta):
    data = '''\
          mu                      T            U (with sign)   Delta (Sigma_imp=Delta^2*Gii)    number Gauss intervals
 %.16fd0   %.16fd0   %fd0          %fd0                     100
 ''' % (mu, temperature, U, Delta)
    with open(name_input_file_Tc, 'w') as f:
        f.write(data)


# input file for run DMFT(NRG) program
def inputFile(U, it, it_after_tolerance, new, temperature, precision_of_T, variable_mu,
              start_from_read_mu, mu, occupancy, occupancy_tolerance, part_of_dmu, iter_each_change_mu, mesh):
    data = '''\
DMFT PARAMETERS:
     Value of Coloumb interaction                               |     %Fd0
     number of DMFT iterations                                  |     %d
     number of DMFT iterations after tolerance satisfaction     |     %d
     new calculation: 1 - new DMFT, 0 - start form old Sigma    |     %d
TEMPERATURE:
     temperature                                                |     %.16fd0
     tolerance of total energy convergence                      |     1.0D-3
FOR SEARCH LAMBDA:
     precision to search border of N                            |     1.0D-10
     precision to search temperature                            |     1.0D-%d
BAND:
     What is model used: Bethe (1), FCC (2), from DOS(0)        |     1
     half bandwidth                                             |     0.5d0
     small image shift in Green function                        |     1.0D-10
     parameter of disoder: Delta                                |     0.d0
MAGNETIC PROPERTIES:
     Number of spin: 1 - paramagnetic calc, 2 - magnetic calc   |     1
     Vaule of magnetic field in eV                              |     0.d0
NRG PARAMETERS:
     Lambda                                                     |     2.d0
     number of NRG states kept                                  |     500
     Particle Hole Symmetry                                     |     N
     Keep NRG mesh                                              |     0
     type of output                                             |     11
CHEMICAL POTENTIAL:
     Variable (T) or Const (F)                                  |     %s
     Use this value of mu for first iter (T), else mu=mu+U/2 (F)|     %s
     vaule of start Chemical Potential                          |     %.16fd0
     What is total occupancy needed in result of calculation ?  |     %fd0
     tolerance of occupancy convergence                         |     1.0D-%d
     What is value of part_of_dmu? (mu=mu+part_of_dmu*delta_mu) |     %fd0
     number of iter at change mu (2 -- even iter change mu)     |     %d
     tolerance of search mu                                     |     1.0D-12
ENERGY MESH:
     Exponential (T) or Linear (F)                              |     T
     if previous F: Maximum Energy in eV (integer value)        |     5
     1/(step of energy mesh)                                    |     %d
     Value of minimum energy for exp meshEmin_exp_mesh          |     1.0D-4
''' % (U, it, it_after_tolerance, new, temperature, precision_of_T, variable_mu, start_from_read_mu,
       mu, occupancy, occupancy_tolerance, part_of_dmu, iter_each_change_mu, mesh)
    with open(name_input_file, 'w') as f:
        f.write(data)


# script for run DMFT(NRG) program
def runer():
    data = '''\
%s > NRG_out 2> NRG_sys
rm -f *.sve''' % prog_DMFT
    with open(name_script_file, 'w') as f:
        f.write(data)

# extract Tc from output
def extract_Tc():
    error = [0]
    Tc = [-1]
    search_err = 0
    if (os.path.exists(name_output_file_Tc)):
      with open(name_output_file_Tc, 'r') as f:
        outfile = f.read().split('\n')
        Tc = re.findall('([-\s]\d+[.]\d+\S+\d+\Z)', outfile[0])
        if (Tc == [-1]):
           search_err = 1
    else:
        print '----- Error in xtract_Tc. File  "%s"  does not exist -----' % name_output_file_Tc
        error[0] = 1
    if (search_err==1):
        print '----- Error in extract_Tc. Tc not found in file %s -----' % name_output_file_Tc
        error[0] = 1
    return( float(Tc[0]), error[0] )


# find self consistently mu
def find_results_mu_t():
    error = [0]
    temper_s=['-1.0']
    mu_s=['0.0']
    search_err = [1,0]
    if (os.path.exists(name_output_file)):
      with open(name_output_file, 'r') as f:
        outfile = f.read().split('\n')
        lines = outfile[-27:]
        for x in lines[::-1]:
          if x.startswith('      Lattice problem mu'):
            mu_s = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
            if (mu_s==['0.0']):
                search_err[0] = 1
            else:
                search_err[0] = 0
      with open(name_input_file, 'r') as f:
        outfile = f.read().split('\n')
        temper_s = re.findall('([-\s]\d+[.]\d+)', outfile[6])
        if (temper_s==['-1.0']):
            search_err[1] = 1
    else:
         print '----- Error in find_results_mu_t. File  "%s"  does not exist-----' % name_output_file
         error[0] = 1
    if ((search_err[0]==1) or (search_err[1]==1)):
         print '----- Error in find_results_mu_t. mu or temperature not found-----'
         error[0] = 1
    return( float(temper_s[0]), float(mu_s[0]), error[0] )


# find self consistently occupancy
def find_results_U_n():
    error = [0]
    U_s=['0.0']
    n_s=['0.0']
    search_err = [1,0]
    if (os.path.exists(name_output_file)):
      with open(name_output_file, 'r') as f:
        outfile = f.read().split('\n')
        lines = outfile[-27:]
        for x in lines[::-1]:
          if x.startswith('     Total occupancy ='):
            n_s = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
            if (n_s==['0.0']):
                search_err[0] = 1
            else:
                search_err[0] = 0
      with open(name_input_file, 'r') as f:
        outfile = f.read().split('\n')
        U_s = re.findall('([-\s]\d+[.]\d+)', outfile[1])
        if (U_s==['0.0']):
            search_err[1] = 1
    else:
         print '----- Error in find_results_U_n. File  "%s"  does not exist-----' % name_output_file
         error[0] = 1
    if ((search_err[0]==1) or (search_err[1]==1)):
         print '----- Error in find_results_U_n. U or n not found-----'
         error[0] = 1
    return( float(U_s[0]), float(n_s[0]), error[0] )


# Calculation of critical temperature
def calc_Tc(path):
    error = [0]
    Tc_found = 0
    temperature = 0.0

    print '  Calculation of Tc:'
    # search mu and temperature
    os.chdir(curdir)
    os.chdir(path)
    temper_f, mu_f, error[0] = find_results_mu_t()
    U_f, n_f, error[0] = find_results_U_n()

    if ( error[0] == 0):
      # calculation of new Tc
      inputFile_Tc(mu_f, temper_f, U_f, Delta)
      p = Popen(prog_calc_Tc, shell=True, stdout=PIPE, stderr=PIPE)
      p.wait()
      temperature, error[0] = extract_Tc()
      
      # print results  mu  T  Tc to file and display
      file_name_mu_T_Tc = 'DMFT_mu_T_Tc_U%s.dat'%U_f
      with open(os.path.join(curdir,file_name_mu_T_Tc), 'a') as f:
           f.write( ' %.16f   %.16f   %.16f\n' % ( mu_f, temper_f, temperature ) )
      print ' %.16f   %.16f   %.16f' % ( mu_f, temper_f, temperature )

      if ( temperature < 1e-20 ):
          temperature = 0.9*temper_f
          print '     Critical temperature is zero'
          print '     T is taken %.16f' % temperature
          print '     U = %f  n = %f  T = %.16f  mu = %.16f   Tc = 0.0' % ( U_f, n_f, temper_f, mu_f )
      else:
          print '     New temperature is %.16f' %temperature
          print '     U = %f  n = %f  T = %.16f  mu = %.16f   Tc = %.16f' % ( U_f, n_f, temper_f, mu_f, temperature )
          if (abs(temper_f - temperature) < 0.001*temperature):
              print '+++++ Critical temperature is found with precision=0.001. Tc = %.16f' %( temperature )
              Tc_found = 1

    os.chdir(curdir)
    return(temperature, mu_f, Tc_found, error[0])


# check convergence of DMFT(NRG) calculation
def check_convergence(path_check):
    error = [0]
    converged=0
    search_err = 1
    t_not_found = 0

    print '  Check convergence:'
    os.chdir(curdir)
    os.chdir(path_check)
    if (os.path.exists(name_output_file)):
      with open(name_output_file, 'r') as f:
        outfile = f.read().split('\n')
        for x in outfile:
          if  x.startswith('  ERROR'):
              error[0] = 1
              search_err = 0
              t_not_found = 1
              print '----- NRG temperature not found -----'
        if (error[0]==0):
          lines = outfile[-27:]
          for x in lines[::-1]:
            if x.startswith(' DMFT loop for T'):
              converged=1
            # for compare current and last calculation
            elif x.startswith(' Occupancy convergence ='):
              tmp = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
              occupancy_convergence.append( float(tmp[0]) )
              # print '     occupancy convergence = %e' %  float(tmp[0])
              search_err = 0
    else:
        print '----- Error in check_convergence. File  "%s"  does not exist -----' % name_output_file
        error[0] = 1
    if (search_err == 1):
        print '----- Error in check_convergence. Occupancy convergence not found in results -----'
        error[0] = 1
    os.chdir(curdir)
    return(converged, error[0], t_not_found)


# new DMFT(NRG) calculation with input parameters
def DMFT_new(path_start, temperature):
    print '  new DMFT:'
    os.chdir(curdir)
    dos_file_start = sorted(glob.glob('%s/DOS_G*' % path_start))
    if (len(dos_file_start)==0):
       print '     Start DMFT'
       createFolder(path_start)
       os.chdir(path_start)
       runer()
       inputFile(U, it, it_after_tolerance, 1, temperature, precision_of_T, variable_mu, start_from_read_mu,
                 mu_start, occupancy, occupancy_tolerance, part_of_dmu, iter_each_change_mu, mesh)
       p = Popen('bash %s'%name_script_file, shell=True, stdout=PIPE, stderr=PIPE)
       p.wait()
       os.chdir(curdir)
    else:
      print '----- Warning in DMFT_new.  "DOS files"  exist -----'


# continue DMFT(NRG) calculation with T and mu from output
def DMFT_continue(path_continue):
  error = [0]
  os.chdir(curdir)
  if os.path.exists(path_continue):
    os.chdir(path_continue)
    dos_files = sorted(glob.glob('DOS_G*'))
    N_dos_files = len(dos_files)
    if (N_dos_files>0):
        #--------------------------------------------------------------------
        # search mu and temperature
        temper_f, mu_f, error[0] = find_results_mu_t()

        #--------------------------------------------------------------------
        # save old file to new dir "path_to_old_data"
        path_to_old_data='001_%s' %N_dos_files
        if os.path.exists(path_to_old_data):
           if not os.path.exists(path_to_old_data+'_2'):
              path_to_old_data = path_to_old_data+'_2'
           else:
              if not os.path.exists(path_to_old_data+'_3'):
                path_to_old_data = path_to_old_data+'_3'
              else:
                if not os.path.exists(path_to_old_data+'_4'):
                   path_to_old_data = path_to_old_data+'_4'
                else:
                   if not os.path.exists(path_to_old_data+'_5'):
                      path_to_old_data = path_to_old_data+'_5'
        os.makedirs(path_to_old_data)
        for name_file in files_to_save:
           if os.path.exists(name_file):
              copyFile(name_file,os.path.join(path_to_old_data,name_file))
              os.remove(name_file)

        copyFile(dos_files[-1],'sig.dat')
        index=0
        for files in dos_files:
            index=index+1
            if (N_dos_files-index)<5: # save only last 4 files
               copyFile( files, os.path.join(path_to_old_data,files) )
            os.remove(files)

        #--------------------------------------------------------------------
        # write new input file
        inputFile(U, it, it_after_tolerance, 0, temper_f, precision_of_T, variable_mu, start_from_read_mu,
                  mu_f, occupancy, occupancy_tolerance, part_of_dmu, iter_each_change_mu, mesh)
        runer()

        #--------------------------------------------------------------------
        # run new calc
        p = Popen('bash %s'%name_script_file, shell=True, stdout=PIPE, stderr=PIPE)
        p.wait()
        #--------------------------------------------------------------------
    else:
        print '----- Error in DMFT_continue.  "DOS files"  do not exist -----'
        error[0] = 1
    os.chdir(curdir)

  return(error[0])


# continue DMFT(NRG) calculation with T and mu from output 5 iters and check convergence
def run_DMFT(path,temperature):
    error = [0]
    occupancy_convergence = []
    #--------------------------------------------------------------------------------------
    print ''
    print '----------------------------------------'
    print 'Temperature is %.16f' %temperature
    DMFT_new(path, temperature)
    #--------------------------------------------------------------------------------------
    DMFT_conveged, error[0], t_not_found = check_convergence(path)
    if (t_not_found==0):
     for iter in range(1,5):
      if (error[0]==0):
        if ( DMFT_conveged == 0 ):
            print ' Start DMFT continue # %d' % iter
            error[0] = DMFT_continue(path)
            DMFT_conveged, error[0], t_not_found  = check_convergence(path)
        else:
            break

        # if in DMFT_continue occupancy convergence greater then in previous DMFT calculation
        if ( occupancy_convergence[iter] > occupancy_convergence[iter-1] ):
            print '!!!! DMFT calculation does not conveged, occupancy convergence of DMFT cycles:'
            print occupancy_convergence
            break

     if ( (DMFT_conveged==1) ):
        print '+++++ DMFT is conveged for T = %.16f'%temperature
     else:
        print '----- DMFT calculation does not conveged for T = %.16f'%temperature

    return( DMFT_conveged, error[0], t_not_found )


if __name__ == '__main__':
    #--------------------------------------------------------------------
    # input parameters
    U_start = -1.0
    U_end = -1.05
    dU = -0.1
    Delta = 0.0
    occupancy = 0.5
    it = 100
    it_after_tolerance = 10
    mesh = 100

    temperature_start = 0.0952935218811035
    precision_of_T = 2

    variable_mu = 'T'
    start_from_read_mu = 'F'
    mu_start = -0.494812223907229
    part_of_dmu = 0.1
    iter_each_change_mu = 10

    occupancy_tolerance = 4


    #--------------------------------------------------------------------
    prog_DMFT = 'DMFT_1_band_9.run'
    name_script_file = 'DMFT_RUN.sh'
    name_input_file = 'DMFT_INPUT_1_band'
    name_output_file = 'DMFT_out'
    files_to_save = [name_input_file,name_output_file,'NRG_out',name_script_file,'sig.dat']

    prog_calc_Tc = 'DMFT_Bethe_Tc_mu.run'
    name_input_file_Tc = 'DMFT_input_Tc.dat'
    name_output_file_Tc = 'DMFT_out_Tc.dat'
    #--------------------------------------------------------------------
    curdir = os.getcwd()
    temperature_now = temperature_start
    mu_now = mu_start
    occupancy_convergence = []
    errors = [0]

    #--------------------------------------------------------------------
    # search dirs
    paths_in_dir = os.listdir(curdir)
    path_to_dirs = []
    for path in paths_in_dir:
       if os.path.isdir(path):
          path_to_dirs.append(path)
    #--------------------------------------------------------------------
    # delete DOS files. Save only 10 last iterations
    for path in sorted(path_to_dirs):
        print ''
        print 'path %s' % path
        temperature_now, mu_start, Tc_found, errors[0] = calc_Tc(path)