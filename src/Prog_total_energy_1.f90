﻿PROGRAM Prog_total_energy_1

  use module_energy
  use module_impurity_1
  use module_parameters

implicit none

double precision, allocatable :: resig(:), imsig(:), reG(:), imG(:)

double precision, allocatable :: temp_array(:)
double precision	            :: tmpd
integer                       :: i, is
integer                       :: nmax_temp

character*50                 	:: tmpstring
character*100               	:: char_str

!=============================================================================
   ! READ input DMFT(NRG) parameters
   read(10,'(A50)')tmpstring
   read(10,'(A70,D22.15)') char_str, t                 !     temperature
   !MAGNETIC PROPERTIES!----------------------------------------------------------------------------------------------
   read(10,'(A50)') tmpstring
   read(10,'(A70,I1)') char_str,  nspin                !     1 - paramagnetic calc, 2 - magnetic calc
   read(10,'(A70,D22.15)') char_str, H_field           !     Magnetic field (in eV)
   !CHEMICAL POTENTIAL!-----------------------------------------------------------------------------------------------
   read(10,'(A70,D22.15)') char_str, mu                !     vaule of Chemical Potential
   ! READ ENERGY MESH!------------------------------------------------------------------------------------------------
   read(10,'(A50)') tmpstring
   read(10,'(A70,L1)') char_str, exp_mesh              !     Exponential (T) or Linear (F) mesh
   read(10,'(A70,I2)') char_str, Emax                  !     Maximum Energy in eV (integer value)
   read(10,'(A70,I6)') char_str, factor_of_step        !     1/(energy step) (nmax=Emax*factor_of_step)
   read(10,'(A70,D22.15)') char_str, Emin_exp_mesh     !     Value of minimum energy for exp mesh

   !=============================================================================
   ! Create energy mesh and determine value of nmax
   nmax_temp = 100000
   allocate(temp_array(-nmax_temp:nmax_temp))
   Call Energy_mesh(temp_array, nmax_temp)
   allocate(om(-nmax:nmax))
   DO i=-nmax,nmax
      om(i) = temp_array(i)
   END DO
   deallocate(temp_array)

   !=============================================================================
   allocate(G(1:nspin,-nmax:nmax))
   allocate(sig(1:nspin,-nmax:nmax))
   allocate(dos(1:nspin,-nmax:nmax))
   allocate(totalDOS(-nmax:nmax))
   allocate(reG(1:nspin))
   allocate(imG(1:nspin))
   allocate(resig(1:nspin))
   allocate(imsig(1:nspin))
   allocate(occupancy_d(1:nspin))
   !=============================================================================

   open(output_file,file='Total_energy_new.dat',form='FORMATTED')

   open(10,file='Input_GS.dat',form='FORMATTED')
   if (nspin==1) then
      is = 1
      do i=-nmax,nmax
         read(10,*)om(i), dos(is, i), reG(is), imG(is), resig(is), imsig(is)
            G(is,i)=dcmplx(reG(is),imG(is))
            sig(is,i)=dcmplx(resig(is),imsig(is))
         end do
      else
         do i=-nmax,nmax
            read(10,*)om(i), totalDOS(i), dos(1,i), dos(2,i), (reG(is), imG(is), resig(is), imsig(is), is=1, nspin)
            do is=1, nspin
               G(is,i)=dcmplx(reG(is),imG(is))
               sig(is,i)=dcmplx(resig(is),imsig(is))
            end do
      end do
   end if
   close(10)

   call occupancy_and_mu_search_1
   !=============================================================================
   ! Calculation of Total Energy
   call total_energy_1

   write(output_file,*) '   temp         mom         occup         mu         totalEn:'
   write(output_file,'(4(2x,f10.6),2x,f15.10)')t, Magnetic_moment, occupancy_total,  mu, totalEn
   write(output_file,*) '   temp         totalEn            Ekin            Epot:'
   write(output_file,'(2x,f10.6,3(2x,f15.10))')t, totalEn, Kin, Pot
   write(output_file,*) '   temp         mom         mu        occup_tot      occ_up      occ_down:'
   write(output_file,'(6(2x,f10.6))')t, Magnetic_moment, mu, occupancy_total, occupancy_d(1), occupancy_d(2)
   close(output_file)

   deallocate(G,om)
   deallocate(sig)
   deallocate(reG,imG,resig,imsig)
   deallocate(dos, totalDOS, occupancy_d)

END PROGRAM Prog_total_energy_1
