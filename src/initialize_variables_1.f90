subroutine initialize_variables_1
  use module_energy
  use module_impurity_1
  use module_parameters
  use module_nrg_variable
implicit none

double precision, allocatable :: temp_array(:)
integer                       :: nmax_temp
integer                       :: i, is

double precision              :: omsig, tmpd
double precision, allocatable :: resig(:), imsig(:), reG(:), imG(:)

   !=============================================================================
   ! Create energy mesh and determine value of nmax
   nmax_temp = 100000
   allocate(temp_array(-nmax_temp:nmax_temp))
   Call Energy_mesh(temp_array, nmax_temp)
   allocate(om(-nmax:nmax))
   DO i=-nmax,nmax
      om(i) = temp_array(i)
   END DO
   deallocate(temp_array)
   !=============================================================================
   allocate(G(1:nspin,-nmax:nmax))
   if (model.eq.1) then
      allocate(G_prev(1:nspin,-nmax:nmax))
      G_prev(:,:)=dcmplx(0.d0,0.d0)
   end if
   allocate(sig(1:nspin,-nmax:nmax))
   allocate(dd(1:nspin,-nmax:nmax))
   allocate(dos(1:nspin,-nmax:nmax))
   allocate(totalDOS(-nmax:nmax))

   allocate(occupancy_d(1:nspin))
   allocate(dos_0(-nmax:nmax))
   !=============================================================================
   if (new_calc.eq.0) then
      allocate(resig(1:nspin))
      allocate(imsig(1:nspin))
      allocate(reG(1:nspin))
      allocate(imG(1:nspin))

      open(10,file='sig.dat',form='FORMATTED')
      write(output_file,*)'Started new calculation with old Sigma:'
      if (nspin==1) then
         is = 1
         do i=-nmax,nmax
            read(10,*)omsig, dos(is, i), reG(is), imG(is), resig(is), imsig(is)
            sig(is,i)=dcmplx(resig(is),imsig(is))
            G(is,i)=dcmplx(reG(is),imG(is))
            dd(is,i) = dimag(sig(is,i)+1.d0/G(is,i))
            write(output_file,'(x, 3(2x, e15.8))') omsig, dreal(sig(is, i)), dimag(sig(is,i))
         end do
      else
         do i=-nmax,nmax
            read(10,*)omsig, totalDOS(i), dos(1,i), dos(2,i), (reG(is), imG(is), resig(is), imsig(is), is=1, nspin)
            do is=1, nspin
               sig(is,i)=dcmplx(resig(is),imsig(is))
               G(is,i)=dcmplx(reG(is),imG(is))
               dd(is,i) = dimag(sig(is,i)+1.d0/G(is,i))
            end do
            write(output_file,'(x, 5(2x, e15.8))') omsig, (dreal(sig(is, i)), dimag(sig(is,i)), is=1,nspin)
         end do
      end if
      close(10)
      deallocate(resig,imsig,reG, imG)
   end if

end subroutine initialize_variables_1

subroutine deallocate_variables_1
  use module_energy
  use module_impurity_1
implicit none
   if (allocated(om))          deallocate(om)
   if (allocated(G))           deallocate(G)
   if (allocated(sig))         deallocate(sig)
   if (allocated(dd))          deallocate(dd)
   if (allocated(G_prev))      deallocate(G_prev)
   if (allocated(dos))         deallocate(dos)
   if (allocated(dos_0))       deallocate(dos_0)
   if (allocated(totalDOS))    deallocate(totalDOS)
   if (allocated(occupancy_d)) deallocate(occupancy_d)
end subroutine deallocate_variables_1