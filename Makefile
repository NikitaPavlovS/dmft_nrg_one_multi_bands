include make.arch

DIRS =  src_entropy  src_kk  src_multi  src_one  src_share

LIBOBJ = 
LIB_DEPS = 

PROGRAMS = lda_dmft

#   Defines variable MAKE if it is not defined
ifeq ($(MAKE),)
  MAKE := make
endif

################################################################################
#
lda_dmft: ldadmftlib $(LIB_DEPS)
	$(FC) -o $@ $(LIBOBJ) $(LFLAGS)
#
all: $(PROGRAMS) 
#
clean:
	$(MAKE) -C src clean
#
distclean:
	for d in $(DIRS);            \
	do                           \
	   $(MAKE) -C $$d distclean; \
	done
	- $(RM) *.mod *.a *.o */*.o
#
ldadmftlib:
	$(MAKE) -C src
#
green_function: ldadmftlib $(LIB_DEPS)
	$(FC) -o $@ Tools/green_function.f90 src/libldadmft.a $(LIBOBJ) $(LFLAGS)
	strip $@
	- $(RM) *__genmod.*