SUBROUTINE Bands_in_directions_and_Bare_Fermi_surface_fraction

  use energy_module
  use hamiltonian_module_1band
  use parameters_module

IMPLICIT NONE

!============================================
! INTERNAL variables
complex*16,       allocatable :: Hamilton
!--------------------------------------------------------------------------------
integer                       :: i, j, l, m           
double precision              :: dk             ! k step
double precision, PARAMETER   :: kMin=0.d0
double precision              :: kMax
integer,          PARAMETER   :: MaxK=300
double precision, allocatable :: kxa(:), kya(:), kza(:) ! mesh of k points
double precision              :: kx, ky, kz         ! variable for symmetric directions in Brilloun zone
!--------------------------------------------------------------------------------
! for bare Fermi surface
logical                       :: EigenValues_Fermi
double precision              :: tempd_value_previous, tempd_value_now
double precision              :: tempdouble
double precision              :: a_left(1:2), b_right(1:2), c_medium(1:2)
!======================================================================================
ALLOCATE(Hamilton)
ALLOCATE(kxa(0:MaxK))
ALLOCATE(kya(0:MaxK))
ALLOCATE(kza(0:MaxK))
!======================================================================================
kMax=pi

IF (Bands_plot) THEN
   !make kx and ky mesh
   dk=(kMax - kMin)/MaxK
   DO i = 0, MaxK
      kxa(i)= kMin +dk*i
      kya(i)= kxa(i)
      kza(i)= kxa(i)
   END DO
   !==================
   OPEN(Unit = 98, FILE = 'OUT_BANDS.dat', FORM='FORMATTED')
   !-----------------------------------------------------------------------------------------------------------------------------
   !direction (0,0,0) -- (pi,0,0)
   DO i = 0, MaxK
      kx=kxa(i)
      ky=0.d0
      kz=0.d0
      CALL Hamilton_one_band_in_kx_ky_kz_mu( kx, ky, kz, mu, Hamilton)
      WRITE(98, '(2x,10(2x,F15.7))') 300.d0*kx/pi, dreal(Hamilton)
   END DO!direction (0,0,0) -- (pi,0,0)
   WRITE(98,*)
   !==============================================================================================================
   !direction (pi,0,0) -- (pi,pi,0)
   DO i = 0, MaxK
      kx=kMax
      ky=kya(i)
      kz=0.d0
      CALL Hamilton_one_band_in_kx_ky_kz_mu( kx, ky, kz, mu, Hamilton)
      WRITE(98, '(2x,10(2x,F15.7))') 300.d0*(kMax + ky)/pi, dreal(Hamilton)
   END DO !direction (pi,0,0) -- (pi,pi,0)
   WRITE(98,*)
   !==============================================================================================================
   !direction (pi,pi,0) -- (pi,pi,pi)
   DO i = 0, MaxK
      kx=kMax
      ky=kMax
      kz=kza(i)
      CALL Hamilton_one_band_in_kx_ky_kz_mu( kx, ky, kz, mu, Hamilton)
      WRITE(98, '(2x,10(2x,F15.7))') 300.d0*(2*kMax + kz)/pi, dreal(Hamilton)
   END DO !direction (pi,pi,0) -- (pi,pi,pi)
   WRITE(98,*)
   !==============================================================================================================
   !direction (pi,pi,pi) -- (0,0,0)
   DO i = MaxK, 0, -1
      kx= kxa(i)
      ky= kya(i)
      kz= kza(i)
      CALL Hamilton_one_band_in_kx_ky_kz_mu( kx, ky, kz, mu, Hamilton)
      WRITE(98, '(2x,10(2x,F15.7))') 300.d0*(4*kMax - kx)/pi, dreal(Hamilton)
   END DO !direction (pi,pi) -- (0,0)
   WRITE(98,*)
   CLOSE(98)
END IF !Bands_plot
!======================================================================================
!======================================================================================
IF (Fermi_Surface_without_Interaction) THEN
    tempdouble = 0.d0
    dk=(kMax - kMin)/MaxK
   ! to file OUT_Fermi_surface_bare.dat write bare Fermi surface
   ! search by diagonal half divided methods
   OPEN(Unit = 900, file='OUT_Fermi_surface_bare.dat', form='FORMATTED')
   DO i = 0, 2*MaxK
      j=0
      DO WHILE ((j.eq.0).or.(.NOT.((abs(kMax-ky)<1E-5).or.(abs(kMax-kx)<1E-5))))
         IF (i<MaxK) THEN
            kx= kMin +dk*j
            ky= kMax +dk*(j-i)
            j=j+1
         ELSE
            kx= kMin +dk*((i-MaxK)+j)
            ky= kMin +dk*j
            j=j+1
         END IF
         b_right(1) = kx
         b_right(2) = ky
         write(901,'(2x,2(2x,F15.8))') kx, ky

         CALL Hamilton_one_band_in_kx_ky_kz_mu( kx, ky, mu, Hamilton)
         IF (j.eq.0) THEN
            tempd_value_previous = dreal(Hamilton)
            a_left(1) = kx
            a_left(2) = ky
            IF (abs(tempd_value_previous)<1E-6) THEN
                write(900,'(2x,3(2x,F15.8))') kx, ky, tempdouble
            END IF
         ELSE
            tempd_value_now = dreal(Hamilton)
            IF (tempd_value_previous*tempd_value_now<0) THEN
               IF (1) THEN
               EigenValues_Fermi=0

               DO WHILE (.NOT.EigenValues_Fermi)
                  c_medium(1) = (a_left(1) + b_right(1))/2.d0
                  c_medium(2) = (a_left(2) + b_right(2))/2.d0
                  CALL Hamilton_one_band_in_kx_ky_kz_mu( c_medium(1), c_medium(2), mu, Hamilton)
                  IF (tempd_value_previous*dreal(Hamilton)<0) THEN
                     b_right(1)=c_medium(1)
                     b_right(2)=c_medium(2)
                     !tempd_value_now = dreal(Hamilton)
                  ELSE
                     a_left(1)=c_medium(1)
                     a_left(2)=c_medium(2)
                     tempd_value_previous = dreal(Hamilton)
                  END IF
                  IF (abs(dreal(Hamilton))<1E-6) THEN
                     EigenValues_Fermi=1
                     write(900,'(2x,3(2x,F15.8))') c_medium(1), c_medium(2), tempdouble
                  END IF
               END DO
               !------------------------------------------------------------------------------------------------------------------
               ELSE
                  IF (tempd_value_now<tempd_value_previous) THEN
                     write(900,'(2x,3(2x,F15.8))') b_right(1), b_right(2), tempdouble
                  ELSE
                     write(900,'(2x,3(2x,F15.8))') a_left(1), a_left(2), tempdouble
                  END IF
               END IF
               !------------------------------------------------------------------------------------------------------------------
            END IF
            tempd_value_previous = tempd_value_now
         END IF
         a_left(1) = kx
         a_left(2) = ky
      END DO
   END DO
   CLOSE(900)
END IF
!========================================================================================================================================

!stop 'stop in Bands_in_symmetric_direction END'

deallocate( Hamilton, kxa, kya, kza)

END SUBROUTINE Bands_in_directions_and_Bare_Fermi_surface_fraction