#-*- coding:utf-8 -*-
###############################################################################
import re
import glob
import os
import numpy as np
###############################################################################

def find_results_Cv(b):
    data_b = np.array([[], []])
    for path in b:
        f=open(path, 'r')
        lines = f.readlines()
        arrayList = []
        arrayList.extend(lines[0].split())

        temper=[]
        temper=np.append(temper,float(arrayList[0]))
        Cv=[]
        Cv=np.append(Cv,float(arrayList[1]))
        data_b = np.append(data_b, [temper, Cv], axis=1)
        f.close()

    q = np.argsort(data_b[0])
    data_b = [[data_b[i][x] for x in q] for i in range(2)]
    data_b = [[float(i) for i in data_b[x]] for x in range(2)]
    return data_b


if __name__ == '__main__':
    files_to_remove = ['Input_GS.dat']

    data_file = 'dataON_Cv.dat'
    hfield = 'on'
    path_to_dir=os.path.join(os.getcwd(),'hfield_%s' %hfield )
    if not os.path.isfile(data_file):
       d_on = sorted(glob.glob('*/*%s/heat_capacity.dat' %hfield ))
       dataON = find_results_Cv(d_on)
       np.savetxt(data_file, np.transpose(dataON))
       d_remove = sorted(glob.glob('*/*%s' %hfield ))
       for path in d_remove:
         for file_name in files_to_remove:
           all_path=os.path.join(path,file_name)
           if os.path.isfile(all_path):
              os.remove(all_path)

    data_file = 'dataOFF_Cv.dat'
    hfield = 'off'
    path_to_dir=os.path.join(os.getcwd(),'hfield_%s' %hfield )
    if not os.path.isfile(data_file):
       d_on = sorted(glob.glob('*/*%s/heat_capacity.dat' %hfield ))
       dataON = find_results_Cv(d_on)
       np.savetxt(data_file, np.transpose(dataON))
       d_remove = sorted(glob.glob('*/*%s' %hfield ))
       for path in d_remove:
         for file_name in files_to_remove:
           all_path=os.path.join(path,file_name)
           if os.path.isfile(all_path):
              os.remove(all_path)