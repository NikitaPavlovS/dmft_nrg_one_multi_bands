! Simple Fortran program to demonstrate the use of the NRG-Fortran interface

program NRG_multi_band_magnetic

  use energy_module
  use hamiltonian_module_nband
  use parameters_module
  use impurity_module

implicit none

! This is the maximal size of the fields, it must be set to a value
! accomodating the possible size of the fields. The value should be OK,
! but a test is necessary for actual applications

!=========================================================================================================================================
Call read_input_nband
!=========================================================================================================================================
Call initialize_variables
!=========================================================================================================================================
!stop 'stop init'

OPEN(Unit = 91, FILE = 'OUT_Integ', FORM='FORMATTED')
!=========================================================================================================================================
Call DMFT_loop_multi_band
!==========================================================================================================================================
Call after_DMFT_loop_multi_band
!==========================================================================================================================================
close(91)
!==========================================================================================================================================
IF (Write_HK) THEN
   Call Write_Hamiltonian_HK
END IF
!==========================================================================================================================================

deallocate( dos, om, totalDOS, sig, dd)

OPEN(Unit = 991, FILE = 'end_of_calc', FORM='FORMATTED')
close(991)

end program NRG_multi_band_magnetic
