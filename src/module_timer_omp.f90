module module_timer_omp
  use module_parameters, only : output_file
!$ use omp_lib ! if using openMP ! pns
  implicit none
  private
  public add_timer,start_timer,stop_timer
  
  type, public :: t_timer
    real :: total_time = 0.
    real :: tmp_time   = 0.
    double precision :: total_time_omp = 0.d0 ! pns
    double precision :: tmp_time_omp   = 0.d0 ! pns
    character,     pointer :: name(:) => null()
    type(t_timer), pointer :: next => null()
  end type t_timer

  type(t_timer), pointer :: timer => null()
  type(t_timer), pointer :: last_timer => null() 

  contains
  
  !------------------------------------
  subroutine add_timer( ptr_timer,tname )
    implicit none
    character(len=*), intent(in   ) :: tname
    type(t_timer), pointer :: ptr_timer
    integer :: ln,i

    if( associated(ptr_timer) )  return
    if( .not.associated(timer) )then
       allocate(timer)
       ptr_timer=>timer
     else
       allocate(ptr_timer)
       last_timer%next=>ptr_timer
    end if
    
    ln = len(tname)
    allocate(ptr_timer%name(ln))
    
    forall( i = 1:ln ) ptr_timer%name(i) = tname(i:i)
    last_timer=>ptr_timer
  end subroutine add_timer
  
  !------------------------------------
  subroutine start_timer( tm )
    implicit none
    type(t_timer),pointer::tm

    if( .not.associated(tm) )  return
    call cpu_time(tm%tmp_time)
!$  tm%tmp_time_omp = omp_get_wtime() ! pns
  end subroutine start_timer
  
  !------------------------------------
  subroutine stop_timer( tm )
    implicit none
    integer :: max_len_name
    type(t_timer),pointer:: tm
    real    :: t
    logical :: omp_true=.false. ! pns

    if( .not.associated(tm) )  return
    call cpu_time(t)
    tm%total_time = t - tm%tmp_time
!$  omp_true = .true. ! pns
!$  tm%total_time_omp = omp_get_wtime() ! pns
!$  tm%total_time_omp = tm%total_time_omp - tm%tmp_time_omp ! pns
    if (omp_true) then ! pns
       tm%total_time = real(tm%total_time_omp) ! pns
    end if ! pns

    max_len_name=getMaxLenName()
    write(output_file,*)
    call show( tm,max_len_name )
    write(output_file,*)
  end subroutine stop_timer

  
  !------------------------------------
  subroutine show( tm,max_len_name )
    implicit none
    integer, intent(in   ) :: max_len_name
    type(t_timer), pointer :: tm
    
    integer t_min,len_name,i
    real t_sec
    character(1)      :: name(40) = ' '
    character(len=40) :: nname
    equivalence (nname,name)

    if(.not.associated(tm)) return
    len_name=size(tm%name)
    forall(i=1:len_name) name(i)=tm%name(i)
    forall(i=len_name+1:max_len_name) name(i)=' '
    t_min=int(tm%total_time/60.)
    t_sec=tm%total_time-real(t_min*60)
    if(t_min>0) then
      write(output_file,998) ' ',nname,t_min,' min ',t_sec,' sec '
     else
      write(output_file,999) ' ',nname,t_sec,' sec '
    endif
998 format(a,a,I5,a,f6.2,a)
999 format(a,a,f6.2,a)

  end subroutine show

  !--------------------------------------
  function getMaxLenName() result(maxlen)
    implicit none
    integer maxlen
    type(t_timer),pointer:: cur_tm
    if(.not.associated(timer)) &
         return
    maxlen=0
    cur_tm=>timer
    do while(associated(cur_tm))
       if(maxlen<size(cur_tm%name)) &
            maxlen=size(cur_tm%name)
       cur_tm=>cur_tm%next
    end do    
  end function getMaxLenName
  
end module module_timer_omp
