SUBROUTINE green_function_1_models
  use module_energy,     only : nmax
  use module_impurity_1
  use module_parameters, only : pi, H_field
implicit none

integer    :: i, is

complex*16 :: green_function_DOS, green_function_Bethe, green_function_FCC
integer    :: points_z_0(1:2)

   if (nspin.eq.1) then
      H_field = 0.d0
   end if

   select case (model)
   !-------------------------------------------------
   case(0) ! using density of states representation
      do is=1, nspin
         do i=-nmax,nmax
            G(is,i) = green_function_DOS(is,i)
         end do
      end do
   !-------------------------------------------------
   case(1) ! infinite-dimensional Bethe latice
      do is=1, nspin
         do i=-nmax,nmax
            G(is,i) = green_function_Bethe(is,i)
            G_prev(is,i) = G(is,i)
         end do
      end do
   !-------------------------------------------------
   case(2) ! infinite-dimensional FCC latice
      do is=1, nspin
         do i=-nmax,nmax
            G(is,i) = green_function_FCC(is,i)
         end do
      end do
   !-------------------------------------------------
   case default
      stop 'error when select the model for green function'
   end select
   !-------------------------------------------------
   totalDOS(:)=0.d0
   do is=1, nspin
      do i=-nmax,nmax
         dos(is,i) = -1.d0/pi*dimag(G(is,i))
         totalDOS(i) = totalDOS(i)+dos(is,i)
         dd(is,i) = dimag(sig(is,i)+1.d0/G(is,i))
      end do
   end do

   if (nspin.eq.1) then
      totalDOS(:) = 2.d0*totalDOS(:) 
   end if

END SUBROUTINE green_function_1_models




!===========================================================================================
! Green function is calculated using density of states representation
FUNCTION green_function_DOS(spin_index, E_index)
  use module_energy,     only : om, mu, nmax
  use module_impurity_1
  use module_parameters, only : pi, H_field
implicit none

complex*16          :: green_function_DOS
integer, intent(in) :: spin_index, E_index
! Local variables
integer             :: j
complex*16          :: z
double precision    :: b, c
complex*16          :: Gtmp

  Gtmp = dcmplx(0.d0,0.d0)
  z = dcmplx(om(E_index)+mu,delta) - (sig(spin_index,E_index) - (1.5d0 - spin_index)* dcmplx(H_field,0.d0) )
  do j=-nmax,nmax
     c = ( dos_0(j+1) - dos_0(j) )/( om(j+1)-om(j) )
     b = dos_0(j) - c*om(j)
     Gtmp = Gtmp - c*(om(j+1)-om(j)) - (b + c*z) * cdlog(1.d0 - (om(j+1)-om(j))/(z - om(j)))
  end do
  green_function_DOS = Gtmp

END FUNCTION green_function_DOS
!===========================================================================================

!===========================================================================================
! Green function is calculated for infinite-dimensional FCC latice
FUNCTION green_function_FCC(spin_index, E_index)
  use module_energy,     only : om, mu
  use module_impurity_1
  use module_parameters, only : pi, H_field
  use module_use_libcerf
implicit none

complex*16          :: Green_function_FCC
integer, intent(in) :: spin_index, E_index
! Local variables
complex*16       :: z

  z = dcmplx(om(E_index)+mu,delta) - ( sig(spin_index,E_index) - (1.5d0 - spin_index)* dcmplx(H_field,0.d0) )

    ! DOS by Igoshev                  1/sqrt(2 pi x)       exp(-x/2)
  ! Green_function_of_E_FCC = cdsqrt(0.5d0*pi/z) * cdexp(-0.5d0*z) * (  -dcmplx(0.d0,1.d0) + cErfi_F( cdsqrt(0.5d0*z) ) )
    ! DOS FCC shifted by -1/sqrt(2):  1/sqrt(pi x sqrt(2)) exp(-sqrt(2)x/2)
  green_function_FCC = cdsqrt(pi/(z*dsqrt(2.d0))) * cdexp(-z/(dsqrt(2.d0))) * (  -dcmplx(0.d0,1.d0) + cErfi_F( cdsqrt(z/(dsqrt(2.d0))) ) ) 

END FUNCTION green_function_FCC
!===========================================================================================

!===========================================================================================
! Green function is calculated for infinite-dimensional Bethe latice
FUNCTION green_function_Bethe(spin_index, E_index)
  use module_energy,     only : om, mu
  use module_impurity_1
  use module_parameters, only : H_field, D, imp
implicit none
! in ! om      : real frequencies
! in ! Sigma  : Self-energy
! in ! mu     : Chemical potential
! in ! D      : half band width
! in ! imp    : disorder
! in ! G_prev : local Green Function of previous iter
complex*16          :: green_function_Bethe
integer, intent(in) :: spin_index, E_index
! Local variables
complex*16          :: a, Et

   Et = om(E_index) + mu - sig(spin_index,E_index) - imp*imp*G_prev(spin_index,E_index) + dcmplx(0.d0,delta) - (1.5d0 - spin_index)* dcmplx(H_field,0.d0)
   a = ( Et + sqrt(Et*Et - D*D) )/D
   IF (dimag(a).GT.0.) a = 1/a
   green_function_Bethe = 2.d0*a/D

END FUNCTION green_function_Bethe
!===========================================================================================


!===========================================================================================
! subroutine searches the energy point where z equals zero
SUBROUTINE search_point_where_z_0(points_z_0)
  use module_energy,     only : om, mu, nmax
  use module_impurity_1
  use module_parameters, only : H_field
implicit none

integer, intent(out) :: points_z_0(1:2)
! Local variables
integer              :: is, E_index
complex*16           :: z
double precision     :: z_0(1:2)

  points_z_0=0
  do is=1, nspin
    E_index = -nmax
    points_z_0(is)=E_index
    z_0(is)=dreal(dcmplx(om(E_index)+mu,delta) - ( sig(is,E_index) - (1.5d0 - is)* dcmplx(H_field,0.d0) ))
    do E_index=(-nmax+1),nmax
       z = dcmplx(om(E_index)+mu,delta) - ( sig(is,E_index) - (1.5d0 - is)* dcmplx(H_field,0.d0) )
       if ( abs(dreal(z))<abs(z_0(is)) ) then
         z_0(is)=dreal(z)
         points_z_0(is)=E_index
       end if
    end do
  end do

END SUBROUTINE search_point_where_z_0
!===========================================================================================
