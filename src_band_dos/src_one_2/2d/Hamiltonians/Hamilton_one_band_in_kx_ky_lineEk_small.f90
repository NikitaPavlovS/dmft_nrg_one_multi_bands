SUBROUTINE Hamilton_one_band_in_kx_ky(  kx, ky, Hamilton)

  USE hamiltonian_module_1band
  IMPLICIT NONE

double precision,    intent(in) :: kx, ky
complex*16,         intent(out) :: Hamilton
!===============================================================

      Hamilton = dcmplx( (kx + ky)/6.d0 , 0.d0)


END SUBROUTINE Hamilton_one_band_in_kx_ky
