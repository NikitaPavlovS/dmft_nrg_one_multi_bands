# -*- coding: utf-8 -*-
# pns 06.2017

import os
import glob
import numpy as np
#from multiprocessing import Pool
from subprocess import Popen, PIPE
import re
#--------------------------------------------------------------------
#--------------------------------------------------------------------
def createFolder(path):
    if not os.path.exists(path):
        os.makedirs(path)

def copyFile(pathFrom, pathTo):
    with open(pathFrom, 'rb') as fileFrom, open(pathTo, 'wb') as fileTo:
        fileTo.write(fileFrom.read())


# input file for Tc program
def inputFile_Tc(mu, temperature, U, Delta, name_input_file_Tc):
    data = '''\
          mu                      T            U (with sign)   Delta (Sigma_imp=Delta^2*Gii)    number Gauss intervals
 %.16fd0   %.16fd0   %fd0          %fd0                     100
 ''' % (mu, temperature, U, Delta)
    with open(name_input_file_Tc, 'w') as f:
        f.write(data)


# input file for run DMFT(NRG) program
def inputFile( new, temperature, mu, **params_kw):
    data = '''\
DMFT PARAMETERS:
     Value of Coloumb interaction                               |     %Fd0
     number of DMFT iterations                                  |     %d
     number of DMFT iterations after tolerance satisfaction     |     %d
     new calculation: 1 - new DMFT, 0 - start form old Sigma    |     %d
TEMPERATURE:
     temperature                                                |     %.16fd0
     tolerance of total energy convergence                      |     1.0D-3
FOR SEARCH LAMBDA:
     precision to search border of N                            |     1.0D-10
     precision to search temperature                            |     1.0D-%d
BAND:
     What is model used: Bethe (1), FCC (2), from DOS(0)        |     1
     half bandwidth                                             |     0.5d0
     small image shift in Green function                        |     1.0D-10
     parameter of disoder: Delta                                |     0.d0
MAGNETIC PROPERTIES:
     Number of spin: 1 - paramagnetic calc, 2 - magnetic calc   |     1
     Vaule of magnetic field in eV                              |     0.d0
NRG PARAMETERS:
     Lambda                                                     |     2.d0
     number of NRG states kept                                  |     500
     Particle Hole Symmetry                                     |     N
     Keep NRG mesh                                              |     0
     type of output                                             |     11
CHEMICAL POTENTIAL:
     Variable (T) or Const (F)                                  |     %s
     Use this value of mu for first iter (T), else mu=mu+U/2 (F)|     %s
     vaule of start Chemical Potential                          |     %.16fd0
     What is total occupancy needed in result of calculation ?  |     %fd0
     tolerance of occupancy convergence                         |     1.0D-%d
     What is value of part_of_dmu? (mu=mu+part_of_dmu*delta_mu) |     %fd0
     number of iter at change mu (2 -- even iter change mu)     |     %d
     tolerance of search mu                                     |     1.0D-12
ENERGY MESH:
     Exponential (T) or Linear (F)                              |     T
     if previous F: Maximum Energy in eV (integer value)        |     5
     1/(step of energy mesh)                                    |     %d
     Value of minimum energy for exp meshEmin_exp_mesh          |     1.0D-4
''' % (params_kw['U'],  params_kw['it'],  params_kw['it_after_tolerance'],  new,  temperature,
       params_kw['precision_of_T'],  params_kw['variable_mu'],  params_kw['start_from_read_mu'],
       mu,  params_kw['occupancy'],  params_kw['occupancy_tolerance'],  params_kw['part_of_dmu'],
       params_kw['iter_each_change_mu'],  params_kw['mesh'])
    with open(params_kw['name_input_file'], 'w') as f:
        f.write(data)


# script for run DMFT(NRG) program
def runer(name_script_file, prog_DMFT):
    data = '''\
%s > NRG_out 2> NRG_sys
rm -f *.sve''' % prog_DMFT
    with open(name_script_file, 'w') as f:
        f.write(data)

# extract Tc from output
def extract_Tc(name_output_file_Tc):
    error = [0]
    Tc = [-1]
    search_err = 0
    if (os.path.exists(name_output_file_Tc)):
      with open(name_output_file_Tc, 'r') as f:
        outfile = f.read().split('\n')
        Tc = re.findall('([-\s]\d+[.]\d+\S+\d+\Z)', outfile[0])
        if (Tc == [-1]):
           search_err = 1
    else:
        print '----- Error in extract_Tc. File  "%s"  does not exist -----' % name_output_file_Tc
        error[0] = 1
    if (search_err==1):
        print '----- Error in extract_Tc. Tc not found in file %s -----' % name_output_file_Tc
        error[0] = 1
    return( float(Tc[0]), error[0] )


# find self consistently mu
def find_results_mu_t(name_input_file, name_output_file):
    error = [0]
    temper_s=['-1.0']
    mu_s=['0.0']
    search_err = [1,0]
    if (os.path.exists(name_output_file)):
      with open(name_output_file, 'r') as f:
        outfile = f.read().split('\n')
        lines = outfile[-27:]
        for x in lines[::-1]:
          if x.startswith('      Lattice problem mu'):
            mu_s = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
            if (mu_s==['0.0']):
                search_err[0] = 1
            else:
                search_err[0] = 0
      with open(name_input_file, 'r') as f:
        outfile = f.read().split('\n')
        temper_s = re.findall('([-\s]\d+[.]\d+)', outfile[6])
        if (temper_s==['-1.0']):
            search_err[1] = 1
    else:
         print '----- Error in find_results_mu_t. File  "%s"  does not exist-----' % name_output_file
         error[0] = 1
    if ((search_err[0]==1) or (search_err[1]==1)):
         print '----- Error in find_results_mu_t. mu or temperature not found-----'
         error[0] = 1
    return( float(temper_s[0]), float(mu_s[0]), error[0] )


# Calculation of critical temperature
def calc_Tc(path, **params_kw):
    error = [0]
    Tc_found = 0

    print '  Calculation of Tc:'
    # search mu and temperature
    os.chdir(curdir)
    os.chdir(path)
    temper_f, mu_f, error[0] = find_results_mu_t(name_input_file=params_kw['name_input_file'],
                                                 name_output_file=params_kw['name_output_file'])

    if ( error[0] == 0):
      # calculation of new Tc
      inputFile_Tc(mu=mu_f, temperature=temper_f, U=params_kw['U'],Delta=params_kw['Delta'],
                   name_input_file_Tc=params_kw['name_input_file_Tc'])
      p = Popen(params_kw['prog_calc_Tc'], shell=True, stdout=PIPE, stderr=PIPE)
      p.wait()
      temperature, error[0] = extract_Tc(params_kw['name_output_file_Tc'])
      if ( temperature < 1e-20 ):
          temperature = 0.9*temper_f
          print '     Critical temperature is zero'
          print '     T is taken %.16f' % temperature
          print '     U = %f  T = %.16f  mu = %.16f   Tc = 0.0' % ( params_kw['U'], temper_f, mu_f )
      else:
          print '     New temperature is %.16f' %temperature
          print '     U = %f  T = %.16f  mu = %.16f   Tc = %.16f' % ( params_kw['U'], temper_f, mu_f, temperature )
          if (abs(temper_f - temperature) < 0.001*temperature):
              print '+++++ Critical temperature is found with precision=0.001. Tc = %.16f' %( temperature )
              Tc_found = 1

    os.chdir(curdir)
    return(temperature, mu_f, Tc_found, error[0])


# check convergence of DMFT(NRG) calculation
def check_convergence(path_check, **params_kw):
    error = [0]
    converged=0
    search_err = 1
    t_not_found = 0

    print '  Check convergence:'
    os.chdir(curdir)
    os.chdir(path_check)
    if (os.path.exists(params_kw['name_output_file'])):
      with open(params_kw['name_output_file'], 'r') as f:
        outfile = f.read().split('\n')
        for x in outfile:
          if  x.startswith('  ERROR'):
              error[0] = 1
              search_err = 0
              t_not_found = 1
              print '----- NRG temperature not found -----'
        if (error[0]==0):
          lines = outfile[-27:]
          for x in lines[::-1]:
            if x.startswith(' DMFT loop for T'):
              converged=1
            # for compare current and last calculation
            elif x.startswith(' Occupancy convergence ='):
              tmp = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
              params_kw['occupancy_convergence'].append( float(tmp[0]) )
              # print '     occupancy convergence = %e' %  float(tmp[0])
              search_err = 0
    else:
        print '----- Error in check_convergence. File  "%s"  does not exist -----' % params_kw['name_output_file']
        error[0] = 1
    if (search_err == 1):
        print '----- Error in check_convergence. Occupancy convergence not found in results -----'
        error[0] = 1
    os.chdir(curdir)
    return(converged, error[0], t_not_found)


# new DMFT(NRG) calculation with input parameters
def DMFT_new(path_start, temperature, **params_kw):
    print '  new DMFT:'
    os.chdir(curdir)
    dos_file_start = sorted(glob.glob('%s/DOS_G*' % path_start))
    if (len(dos_file_start)==0):
       print '     Start DMFT'
       createFolder(path_start)
       os.chdir(path_start)
       runer(params_kw['name_script_file'], params_kw['prog_DMFT'])
       inputFile( new=1, temperature=temperature, mu=params_kw['mu_start'], **params_kw )
       p = Popen('bash %s'%params_kw['name_script_file'], shell=True, stdout=PIPE, stderr=PIPE)
       p.wait()
       os.chdir(curdir)
    else:
      print '----- Warning in DMFT_new.  "DOS files"  exist -----'


# continue DMFT(NRG) calculation with T and mu from output
def DMFT_continue(path_continue, **params_kw):
  error = [0]
  os.chdir(curdir)
  if os.path.exists(path_continue):
    os.chdir(path_continue)
    dos_files = sorted(glob.glob('DOS_G*'))
    N_dos_files = len(dos_files)
    if (N_dos_files>0):
        #--------------------------------------------------------------------
        # search mu and temperature
        temper_f, mu_f, error[0] = find_results_mu_t(name_input_file=params_kw['name_input_file'],
                                                     name_output_file=params_kw['name_output_file'])

        #--------------------------------------------------------------------
        # save old file to new dir "path_to_old_data"
        path_to_old_data='001_%s' %N_dos_files
        if os.path.exists(path_to_old_data):
           if not os.path.exists(path_to_old_data+'_2'):
              path_to_old_data = path_to_old_data+'_2'
           else:
              if not os.path.exists(path_to_old_data+'_3'):
                path_to_old_data = path_to_old_data+'_3'
              else:
                if not os.path.exists(path_to_old_data+'_4'):
                   path_to_old_data = path_to_old_data+'_4'
                else:
                   if not os.path.exists(path_to_old_data+'_5'):
                      path_to_old_data = path_to_old_data+'_5'
        os.makedirs(path_to_old_data)
        for name_file in params_kw['files_to_save']:
           if os.path.exists(name_file):
              copyFile(name_file,os.path.join(path_to_old_data,name_file))
              os.remove(name_file)

        copyFile(dos_files[-1],'sig.dat')
        index=0
        for files in dos_files:
            index=index+1
            if (N_dos_files-index)<5: # save only last 4 files
               copyFile( files, os.path.join(path_to_old_data,files) )
            os.remove(files)

        #--------------------------------------------------------------------
        # write new input file
        inputFile( new=0, temperature=temper_f, mu=mu_f, **params_kw )
        runer(params_kw['name_script_file'], params_kw['prog_DMFT'])

        #--------------------------------------------------------------------
        # run new calc
        p = Popen('bash %s'%params_kw['name_script_file'], shell=True, stdout=PIPE, stderr=PIPE)
        p.wait()
        #--------------------------------------------------------------------
    else:
        print '----- Error in DMFT_continue.  "DOS files"  do not exist -----'
        error[0] = 1
    os.chdir(curdir)

  return(error[0])


# continue DMFT(NRG) calculation with T and mu from output 5 iters and check convergence
def run_DMFT(path, temperature, **params_kw):
    error = [0]
    params_kw['occupancy_convergence'] = []
    #--------------------------------------------------------------------------------------
    print ''
    print '----------------------------------------'
    print 'Temperature is %.16f' %temperature
    DMFT_new(path, temperature, **params_kw)
    #--------------------------------------------------------------------------------------
    DMFT_conveged, error[0], t_not_found = check_convergence(path, **params_kw)
    if (t_not_found==0):
     for iter in range(1,5):
      if (error[0]==0):
        if ( DMFT_conveged == 0 ):
            print ' Start DMFT continue # %d' % iter
            error[0] = DMFT_continue(path, **params_kw)
            DMFT_conveged, error[0], t_not_found  = check_convergence(path, **params_kw)
        else:
            break

        # if in DMFT_continue occupancy convergence greater then in previous DMFT calculation
        if ( params_kw['occupancy_convergence'][iter] > params_kw['occupancy_convergence'][iter-1] ):
            print '!!!! DMFT calculation does not conveged, occupancy convergence of DMFT cycles:'
            print params_kw['occupancy_convergence']
            break

     if ( (DMFT_conveged==1) ):
        print '+++++ DMFT is conveged for T = %.16f'%temperature
     else:
        print '----- DMFT calculation does not conveged for T = %.16f'%temperature

    return( DMFT_conveged, error[0], t_not_found )


if __name__ == '__main__':
    #--------------------------------------------------------------------
    # input parameters
    p={}
    U_start = -0.9
    U_end = -1.15
    dU = -0.1
    p['Delta'] = 0.0
    p['occupancy'] = 0.5
    p['it'] = 100
    p['it_after_tolerance'] = 10
    p['mesh'] = 100

    temperature_start = 0.1
    p['precision_of_T'] = 2

    p['variable_mu'] = 'T'
    p['start_from_read_mu'] = 'F'
    p['mu_start'] = -0.494812223907229
    p['part_of_dmu'] = 0.1
    p['iter_each_change_mu'] = 10

    p['occupancy_tolerance'] = 4
    #--------------------------------------------------------------------
    p['prog_DMFT'] = 'DMFT_1_band_9.run'
    p['name_script_file'] = 'DMFT_RUN.sh'
    p['name_input_file'] = 'DMFT_INPUT_1_band'
    p['name_output_file'] = 'DMFT_out'
    p['files_to_save'] = [p['name_input_file'],p['name_output_file'],'NRG_out',p['name_script_file'],'sig.dat']

    p['prog_calc_Tc'] = 'DMFT_Bethe_Tc_mu.run'
    p['name_input_file_Tc'] = 'DMFT_input_Tc.dat'
    p['name_output_file_Tc'] = 'DMFT_out_Tc.dat'
    #--------------------------------------------------------------------
    curdir = os.getcwd()
    temperature_now = temperature_start
    errors = [0]

    U_array = np.arange(U_start, U_end, dU)

    for U in U_array:
        p['U'] = U
        print '=================================================='
        print '=====     START DMFT calculation for U = %f' % p['U']
        errors[0] = 0

        #--------------------------------------------------------------------------------------
        path_now = 'U%s_n%s_T%s' % (p['U'], p['occupancy'], temperature_now)
        DMFT_conveged, errors[0], t_not_found = run_DMFT( path_now, temperature_now, **p )
        if (errors[0]==0):
            temperature_now, params_kw['mu_start'], Tc_found, errors[0] = calc_Tc( path_now, **p )
        print ''
        #--------------------------------------------------------------------------------------
    
        if (t_not_found==0):
          # Start temperature cycle
          Tc_found = 0
          if ( (DMFT_conveged==1) and (errors[0]==0) ):
            DMFT_conveged_T = 1
            for T_iter in range(1,6):
              if ( (DMFT_conveged_T==1) and (errors[0]==0) and (Tc_found==0) ):
                  path_now = 'U%s_n%s_T%s' % (p['U'], p['occupancy'], temperature_now)
                  DMFT_conveged_T, errors[0], t_not_found = run_DMFT( path_now, temperature_now, **p )
                  if (errors[0]==0):
                      temperature_now, params_kw['mu_start'], Tc_found, errors[0] = calc_Tc( path_now, **p )
                      print ''
          if (errors[0]==0):
            os.chdir(curdir)
            os.chdir(path_now)
            temper_f, mu_f, errors[0] = find_results_mu_t(name_input_file=p['name_input_file'],name_output_file=p['name_output_file'])
            os.chdir(curdir)
            print '+++++ for U = %f.  Is Tc found ? Answer: %d.  mu = %.16f  Tc = %.16f' % ( p['U'], Tc_found, mu_f, temperature_now )

        if (t_not_found==1):
            temperature_1 = 0.9*temperature_now
            temperature_2 = 1.1*temperature_now
            #--------------------------------------------------------------------------------------
            errors[0] = 0
            path_now = 'U%s_n%s_T%s' % (p['U'], p['occupancy'], temperature_1)
            DMFT_conveged, errors[0], t_not_found = run_DMFT( path_now, temperature_1, **p )
            if (errors[0]==0):
                temperature, mu_s, Tc_found, errors[0] = calc_Tc( path_now, **p )
            print ''
            #--------------------------------------------------------------------------------------
            errors[0] = 0
            path_now = 'U%s_n%s_T%s' % (p['U'], p['occupancy'], temperature_2)
            DMFT_conveged, errors[0], t_not_found = run_DMFT( path_now, temperature_2, **p )
            if (errors[0]==0):
                temperature, mu_s, Tc_found, errors[0] = calc_Tc( path_now, **p )
            print ''
            #--------------------------------------------------------------------------------------

        print ''
        print '======     END calculation for U = %f' % p['U']
        print '=================================================='
        print ''
        print ''