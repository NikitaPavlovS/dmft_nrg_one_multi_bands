! Simple Fortran program to demonstrate the use of the NRG-Fortran interface

program NRG_one_band_magnetic

  use energy_module
  use hamiltonian_module_1band
  use parameters_module
  use impurity_module

implicit none

! This is the maximal size of the fields, it must be set to a value
! accomodating the possible size of the fields. The value should be OK,
! but a test is necessary for actual applications

!=========================================================================================================================================
Call read_input_1band
!=========================================================================================================================================
Call initialize_variables_1band
!=========================================================================================================================================
!stop 'stop init'

OPEN(Unit = 91, FILE = 'OUT_Integ', FORM='FORMATTED')
!=========================================================================================================================================
Call DMFT_loop_one_band
!==========================================================================================================================================
Call after_DMFT_loop_one_band
!==========================================================================================================================================
close(91)

deallocate(dos,dos_0, om, totalDOS, sig, dd)

end program NRG_one_band_magnetic
