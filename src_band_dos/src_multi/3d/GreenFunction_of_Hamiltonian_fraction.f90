SUBROUTINE GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, Sigma, GreenF)
use hamiltonian_module_Nband
IMPLICIT NONE
! EXTERNAL variables
double precision,  intent(in) :: E
double precision,  intent(in) :: mu
complex*16,        intent(in) :: Hamilton( 1:ndim, 1:ndim)
complex*16,        intent(in) :: Sigma( 1:ndim)
complex*16,       intent(out) :: GreenF( 1:ndim, 1:ndim)    ! Green function Matrix
!==================================================
! INTERNAL variables
complex*16,       allocatable :: WORK_2(:)
integer,          allocatable :: IPIV(:)
integer                       :: INFO

integer                       :: j, m
!======================================================================================

allocate(IPIV(1:ndim))
allocate(WORK_2(1:2*ndim))
!===================================================
DO m=1, ndim
    GreenF(m,m) = dcmplx( E + mu, delta) - Hamilton( m, m ) - Sigma(m)
END DO
DO j=1, ndim-1
DO m=j+1, ndim
      GreenF(j,m) = - Hamilton( j, m )
      GreenF(m,j) = - Hamilton( m, j )
END DO
END DO
!===================================================
! finding inverse matrix
call zgetrf( ndim, ndim, GreenF, ndim, IPIV, INFO )
call zgetri( ndim, GreenF, ndim, IPIV, WORK_2, 2*ndim, INFO )

!======================================================================================

END SUBROUTINE GreenFunction_of_Hamiltonian_fraction