! 2D ()
program Tc_DMFTmu_f

      implicit none
!-------------------------------------------------     
      integer                       :: nr, ios
      integer                       :: v,j,m,p,next
      double precision, allocatable :: TT(:),muT(:)
	  double precision, allocatable :: spcoefmu(:),tmp(:)
      integer,          parameter   :: ngp=50         ! number of Gaussian points
      double precision              :: x(ngp),w(ngp)  ! Gaussian points, weights
      double precision              :: T,Tc,dT
	  double precision              :: U,Dp,Dm
      double precision              :: xc,xs,s,s1
	  double precision				:: Tp,Tm,xx,xm,xt
	  double precision				:: aa,bb
	  double precision              :: mu,mup,mum
	  double precision              :: Delta,sq
	  double precision              :: th,func
      double precision, parameter   :: pi=3.14159265358979
      character*8                   :: tmpstring
      character*256                 :: infname
      character*64                  :: string_1, string_2

      logical                       :: seach_Tc  ! pns
      double precision              :: h_field   ! pns
      integer                       :: tmp_int   ! pns
!=================================================================================================

    !-------------------------------------------------
    open(11,file='DMFT_input_mu_T_interpol.dat', form='formatted', iostat=ios, status='old', action='read')
    if (ios==0) then
		   read(11,*) tmpstring
		   read(11,*) U, Delta, h_field, p, next
	     close(11)
    else
       stop ' For mu_T_interpolation program input file "DMFT_input_mu_T_interpol.dat" is needed '
    end if
	  sq = DSQRT( 1.d0 + 16.d0 *Delta *Delta)
    !-------------------------------------------------

    !---Read input data from file -----------------
    open(88, file='DMFT_input_mu_T.dat', form='formatted', iostat=ios, status='old', action='read')
    if (ios/=0) then
       stop ' For mu_T_interpolation program input file "DMFT_input_mu_T.dat" is needed '
    end if
    v = 1
    do
       read(88,'(A10)',iostat=ios) string_1
       if( ios /= 0 ) exit
       tmp_int= len( trim(adjustl(string_1)) )
       if (tmp_int>3) then
          v = v + 1
       end if
    end do
    nr = v - 1

    !write(*,*) 'Number of points', nr
    ! Temporary allocate
	  allocate(muT(nr))
    allocate(TT(nr))
    allocate(spcoefmu(nr))

    rewind(88)
      do m=1,nr
        read(88,*)muT(m),TT(m)
      enddo
    close(88)
	  Dm = TT(1)
	  Dp = TT(nr)
    !---Read input data from file -----------------

    !---> Make spline coefficients
    allocate(tmp(nr))
    call spline(TT,muT,nr,1.d30,1.d30,spcoefmu,tmp)
    deallocate(tmp)
	  
	  T = (Dm+Dp) *0.5d0
	  IF (next.EQ.1) Dm=0.d0
	  IF (next.EQ.2) Dp=10.d0

    call Gaussian_points( x(1:50), w(1:50) )

    ! opening of output file
    write(string_1,'(f5.2)') U
    write(string_2,'(f5.3)') Delta
	  infname='DMFT_out_mu_T_interpol_U'//trim(adjustl(string_1))//'_d'//trim(adjustl(string_2))//'.dat'
	  open(1,file=Trim(infname))

  !=================================================
  seach_Tc = .true.
  ! cycle for search Tc
  DO WHILE (seach_Tc)

    ! interpolation of data: mu T
    IF (nr.EQ.2) THEN
	    mu = muT(1)   + (muT(2) - muT(1)) * (T - TT(1)) / (TT(2) - TT(1))
	  ELSE
	    call splint(TT,muT,spcoefmu,nr,T,mu)
	  ENDIF

!  Deff=D*DSQRT(1+4(Delta/D)^2); U/2D, mu/2D, Tc/2D
		mu = 2.d0 * mu/sq
	  
!---------------------U(T)=>T(U)----------------------------
	  Tp=10.0
	  Tm=0.0
      Tc=Tp
	  xc=1./p
!-----------------------------------------------------------

20   s=0.0
		xs=-1.-xc
    DO v=1,p
      xs=xs+2.*xc
	    DO j=1,50
        xx=xs+xc*x(j)

		    xm=xx-mu
		    s = s + w(j) * DSQRT(1.d0-xx*xx) *0.5d0*( th(0.5d0*(xm-h_field)/Tc) + th(0.5d0*(xm+h_field)/Tc) )/xm  
 	    END DO
	    DO j=1,50
        xx=xs-xc*x(j)

		    xm=xx-mu
		    s = s + w(j) *DSQRT(1.d0-xx*xx) *0.5d0*( th(0.5d0*(xm-h_field)/Tc) + th(0.5d0*(xm+h_field)/Tc) ) /xm  
 	    END DO
	  END DO

! U-->2U/sq
	  s = 2.d0 * abs(U) * s * xc/pi/sq
	   
	  IF (s.LT.1.0) THEN
	    Tp = Tc
	    Tc = (Tp+Tm)*0.5d0
	  ELSE
	    Tm = Tc
	    Tc = (Tp+Tm) * 0.5d0
	  END IF
	  IF (Tc.LT.1.0E-15) THEN
		  Tc=0.d0
	    GOTO 30
		END IF

	  IF (ABS(Tp-Tm)/Tc > 0.00001) GOTO 20

30   Tc = Tc *sq *0.5d0
!-------------------------------------------------
	  IF (T.GT.Tc) THEN
	    Dp = T
	    T = (Dp + Dm) *0.5d0
		  write(1,*) mu*sq*0.5d0, T, Tc
	  ELSE
	    Dm = T
	    T = (Dp + Dm) *0.5d0
		  write(1,*) mu*sq*0.5d0, T, Tc
	  ENDIF

    IF (T.LT.1.d-15) seach_Tc = .false.
	  IF (ABS((Dp-Dm)/T) < 0.0001) seach_Tc = .false.

  END DO  ! while  cycle for search Tc
  !=================================================


    write(*,*)
	  deallocate(TT)
    deallocate(muT,spcoefmu)

end program Tc_DMFTmu_f




!--------------Th---------------------------------		
!		contains
double precision function th(y)
		double precision y,ep,em

		if (ABS(y)>30.) then
		  th = ABS(y)/y
		else
		  ep = exp(y)
		  em = exp(-y)
		  th = (ep - em)/(ep + em)
		end if
end function th

!--------------th(y)-y/ch(y)ch(y)---------------------------------		
!		contains
double precision function func(y)
		double precision y,ep,em,th1,ch

		if (ABS(y)>30.) then
		  func=ABS(y)/y
		else
		  ep = exp(y)
		  em = 1.d0/ep
		  ch = (ep + em) *0.5d0
		  th1 = (ep - em)/(ep + em)
		  func = th1-y/ch/ch
		end if
end function func


!=================================================
subroutine spline( x,y,n,yp1,ypn,y2,u )
!|----------------------------------------------------------------------------|
!|                                                                            |
!|             This subrutine make spline coefficients                        |
!|                                                                            |
!|  Inputs :                                                                  |
!|i    x - set of points where function is defined                            |
!|i    y - set of function value                                              |
!|i    yp1,ypn - value of left end right second derivative of function        |
!|i    n - number of points from which spline coefficients are made           |
!|  Outputs :                                                                 |
!|o    y2 - spline coefficients                                               |
!|r Remarks                                                                   |
!|r    u - auxiliary dimenshion                                               |
!|                                                                            |
!|----------------------------------------------------------------------------|
      implicit none
      integer          :: n,i,k
      double precision :: x(n),y(n),y2(n),u(n)
      double precision :: p,qn,sig,un,yp1,ypn

      if( yp1.gt..99d30 )then
        y2(1) = 0.d0
        u(1)  = 0.d0
      else
        y2(1) = -5.d-1
        u(1)  = ( 3.d0 / ( x(2)-x(1) ) )*( (y(2)-y(1) ) /&
                ( x(2)-x(1) ) - yp1 )
      endif

      do i = 2,n-1
        sig = ( x(i)-x(i-1) ) / ( x(i+1)-x(i-1) )
        p   = sig*y2(i-1) + 2.d0
        y2(i) = ( sig-1.d0 ) / p
        u(i)  = ( 6.d0*( ( y(i+1)-y(i) ) / ( x(i+1)-x(i) ) -&
                ( y(i)-y(i-1) ) / ( x(i)-x(i-1) ) ) /&
                ( x(i+1)-x(i-1) ) - sig*u(i-1) ) / p
      enddo

      if( ypn.gt..99d30 )then
        qn = 0.d0
        un = 0.d0
       else
        qn = 5.d-1
        un = ( 3.d0 / ( x(n)-x(n-1) ) )*( ypn - ( y(n)-y(n-1) ) /&
             ( x(n)-x(n-1) ) )
      endif

      y2(n) = ( un - qn*u(n-1) ) / ( qn*y2(n-1) + 1.d0 )

      do k = n-1,1,-1
        y2(k) = y2(k)*y2(k+1) + u(k)
      enddo

end subroutine spline


!=================================================
subroutine splint( xa,ya,y2a,n,x,y )
!|----------------------------------------------------------------------------|
!|                                                                            |
!|             This subrutine make spline for real function                   |
!|                                                                            |
!|  Inputs :                                                                  |
!|i    xa - set of points where function is defined                           |
!|i    ya - set of function value                                             |
!|i    y2a - spline coefficients                                              |
!|i    n - number of points from which spline coefficients are made           |
!|i    x - point where is necessary to calculate new value of function        |
!|  Outputs :                                                                 |
!|o    y - value of function in point x                                       |
!|                                                                            |
!|----------------------------------------------------------------------------|
      implicit none

      integer          :: n,k,khi,klo
	  double precision :: xa(n)
	  double precision :: y2a(n),ya(n)
	  double precision :: a,b,x,h
	  double precision :: y

      klo=1
      khi=n

1     if( khi-klo.gt.1 )then
        k = ( khi + klo ) *0.5d0
        if( xa(k).gt.x )then
          khi = k
         else
          klo = k
        endif
        goto 1
      endif

      h = xa(khi) - xa(klo)
      if( h.eq.0.d0 ) stop 'bad xa input in splint'

      a = ( xa(khi) - x ) / h
      b = ( x - xa(klo) ) / h


      y = a*ya(klo) + b*ya(khi) + ( ( a**3.d0-a )*y2a(klo) +&
          ( b**3.d0-b )*y2a(khi) )*(h**2) / 6.d0

end subroutine splint




!=================================================
subroutine Gaussian_points(x,w)
 implicit none

 double precision, intent(inout) :: x(1:50), w(1:50)
! Gaussian points and weights table ------------
      x(1)= 1.562898442154299E-002
      w(1)= 3.125542345386337E-002
      x(2)= 4.687168242159154E-002
      w(2)= 3.122488425484935E-002
      x(3)= 7.806858281343654E-002
      w(3)= 3.116383569620989E-002
      x(4)= 1.091892035800610E-001
      w(4)= 3.107233742756650E-002
      x(5)= 1.402031372361140E-001
      w(5)= 3.095047885049099E-002
      x(6)= 1.710800805386033E-001
      w(6)= 3.079837903115258E-002
      x(7)= 2.017898640957359E-001
      w(7)= 3.061618658398045E-002
      x(8)= 2.323024818449740E-001
      w(8)= 3.040407952645482E-002
      x(9)= 2.625881203715034E-001
      w(9)= 3.016226510516913E-002
      x(10)=2.926171880384719E-001
      w(10)=2.989097959333285E-002
      x(11)=3.223603439005291E-001
      w(11)=2.959048805991265E-002
      x(12)=3.517885263724217E-001
      w(12)=2.926108411063827E-002
      x(13)=3.808729816246298E-001
      w(13)=2.890308960112521E-002
      x(14)=4.095852916783015E-001
      w(14)=2.851685432239509E-002
      x(15)=4.378974021720317E-001
      w(15)=2.810275565910117E-002
      x(16)=4.657816497733581E-001
      w(16)=2.766119822079239E-002
      x(17)=4.932107892081911E-001
      w(17)=2.719261344657690E-002
      x(18)=5.201580198817632E-001
      w(18)=2.669745918357095E-002
      x(19)=5.465970120650944E-001
      w(19)=2.617621923954569E-002
      x(20)=5.725019326213813E-001
      w(20)=2.562940291020812E-002
      x(21)=5.978474702471789E-001
      w(21)=2.505754448157957E-002
      x(22)=6.226088602037079E-001
      w(22)=2.446120270795706E-002
      x(23)=6.467619085141294E-001
      w(23)=2.384096026596818E-002
      x(24)=6.702830156031411E-001
      w(24)=2.319742318525411E-002
      x(25)=6.931491993558020E-001
      w(25)=2.253122025633627E-002
      x(26)=7.153381175730565E-001
      w(26)=2.184300241624740E-002
      x(27)=7.368280898020207E-001
      w(27)=2.113344211252765E-002
      x(28)=7.575981185197073E-001
      w(28)=2.040323264620945E-002
      x(29)=7.776279096494956E-001
      w(29)=1.965308749443529E-002
      x(30)=7.968978923903145E-001
      w(30)=1.888373961337491E-002
      x(31)=8.153892383391763E-001
      w(31)=1.809594072212811E-002
      x(32)=8.330838798884008E-001
      w(32)=1.729046056832359E-002
      x(33)=8.499645278795913E-001
      w(33)=1.646808617614519E-002
      x(34)=8.660146884971647E-001
      w(34)=1.562962107754599E-002
      x(35)=8.812186793850185E-001
      w(35)=1.477588452744131E-002
      x(36)=8.955616449707269E-001
      w(36)=1.390771070371879E-002
      x(37)=9.090295709825296E-001
      w(37)=1.302594789297155E-002
      x(38)=9.216092981453339E-001
      w(38)=1.213145766297949E-002
      x(39)=9.332885350430795E-001
      w(39)=1.122511402318599E-002
      x(40)=9.440558701362559E-001
      w(40)=1.030780257486898E-002
      x(41)=9.539007829254917E-001
      w(41)=9.380419653694464E-003
      x(42)=9.628136542558154E-001
      w(42)=8.443871469669000E-003
      x(43)=9.707857757637063E-001
      w(43)=7.499073255464720E-003
      x(44)=9.778093584869184E-001
      w(44)=6.546948450845286E-003
      x(45)=9.838775407060570E-001
      w(45)=5.588428003865505E-003
      x(46)=9.889843952429918E-001
      w(46)=4.624450063422116E-003
      x(47)=9.931249370374435E-001
      w(47)=3.655961201326339E-003
      x(48)=9.962951347331251E-001
      w(48)=2.683925371553477E-003
      x(49)=9.984919506395959E-001
      w(49)=1.709392653517994E-003
      x(50)=9.997137267734412E-001
      w(50)=7.346344905057997E-004
! end Gaussian points and weights table ------------

end subroutine Gaussian_points