module module_integration
   implicit none
 
   public integration_simpson, integration_trapezoidal, Gaussian_mesh_with_weights
   private Gaussian_points_and_weights

! public variables
   integer, save,    public  :: ngp=50         ! number of Gaussian points
! private variables
   double precision, private :: xg(50)         ! Gaussian points
   double precision, private :: wg(50)         ! Gaussian weights


Contains


double precision function integration_simpson( nmin, nmax, x, fx )
   ! mesh (x) must have odd points
   implicit none

   integer, intent(in)          :: nmin
   integer, intent(in)          :: nmax
   double precision, intent(in) :: x(nmin:nmax), fx(nmin:nmax)
   double precision             :: dx1, dx2, y0, y1, y2
   double precision             :: integral
   integer                      :: k, k_max

   integral=0.d0
   if (nmin == -nmax) then
      k_max = nmax
   else
      k_max = int(0.5*(nmax-nmin))
   end if
   do k=1,k_max
      dx1=x(nmin+2*k-1)-x(nmin+2*k-2)
      dx2=x(nmin+2*k)-x(nmin+2*k-1)
      y0=fx(nmin+2*k-2)
      y1=fx(nmin+2*k-1)
      y2=fx(nmin+2*k)
      integral = integral +((dx1+dx2)/(6*dx1*dx2))*(y0*(2*dx1-dx2)*dx2+(y1*(dx1+dx2)**2)+y2*(2*dx2-dx1)*dx1)
   end do

   integration_simpson = integral

end function integration_simpson


double precision function integration_trapezoidal( nmin, nmax, x, fx )
   implicit none

   integer, intent(in)          :: nmin
   integer, intent(in)          :: nmax
   double precision, intent(in) :: x(nmin:nmax), fx(nmin:nmax)
   double precision             :: integral,dx
   integer                      :: i

   integral = 0.d0
   do i=nmin,nmax-1
      dx = (x(i+1)-x(i))
      integral = integral + 0.5d0*(fx(i)+fx(i+1))*dx
   end do
   integration_trapezoidal = integral

end function  integration_trapezoidal


subroutine  Gaussian_mesh_with_weights( x_min, x_max, ni, x, wx )
   implicit none

   double precision, intent(in) :: x_max,x_min
   integer,          intent(in) :: ni
   double precision,intent(out) :: x(1:2*ni*ngp), wx(1:2*ni*ngp)
   double precision             :: dx_ni,xs
   integer                      :: ix,jx,lx

   Call Gaussian_points_and_weights
   dx_ni = (x_max-x_min)/(2.d0*ni)
   xs = x_min - dx_ni
   DO lx = 0, ni-1
      xs = xs + 2.d0*dx_ni
      DO ix = 1, ngp
         jx = (2*lx+1)*ngp+1-ix
         x(jx) = xs - dx_ni*xg(ix)
         wx(jx) = wg(ix)

         jx = (2*lx+1)*ngp+ix
         x(jx) = xs + dx_ni*xg(ix)
         wx(jx) = wg(ix)
      ENDDO ! ix
   ENDDO ! lx
end subroutine Gaussian_mesh_with_weights


subroutine Gaussian_points_and_weights
   implicit none
   ! for Gaussian integration
   ! Gaussian points and weights table
   ! xg(ngp)	     ! Gaussian points
   ! wg(ngp)	     ! Gaussian weights
   !----------------------------------
   xg(1)= 1.562898442154299E-002
   wg(1)= 3.125542345386337E-002
   xg(2)= 4.687168242159154E-002
   wg(2)= 3.122488425484935E-002
   xg(3)= 7.806858281343654E-002
   wg(3)= 3.116383569620989E-002
   xg(4)= 1.091892035800610E-001
   wg(4)= 3.107233742756650E-002
   xg(5)= 1.402031372361140E-001
   wg(5)= 3.095047885049099E-002
   xg(6)= 1.710800805386033E-001
   wg(6)= 3.079837903115258E-002
   xg(7)= 2.017898640957359E-001
   wg(7)= 3.061618658398045E-002
   xg(8)= 2.323024818449740E-001
   wg(8)= 3.040407952645482E-002
   xg(9)= 2.625881203715034E-001
   wg(9)= 3.016226510516913E-002
   xg(10)=2.926171880384719E-001
   wg(10)=2.989097959333285E-002
   xg(11)=3.223603439005291E-001
   wg(11)=2.959048805991265E-002
   xg(12)=3.517885263724217E-001
   wg(12)=2.926108411063827E-002
   xg(13)=3.808729816246298E-001
   wg(13)=2.890308960112521E-002
   xg(14)=4.095852916783015E-001
   wg(14)=2.851685432239509E-002
   xg(15)=4.378974021720317E-001
   wg(15)=2.810275565910117E-002
   xg(16)=4.657816497733581E-001
   wg(16)=2.766119822079239E-002
   xg(17)=4.932107892081911E-001
   wg(17)=2.719261344657690E-002
   xg(18)=5.201580198817632E-001
   wg(18)=2.669745918357095E-002
   xg(19)=5.465970120650944E-001
   wg(19)=2.617621923954569E-002
   xg(20)=5.725019326213813E-001
   wg(20)=2.562940291020812E-002
   xg(21)=5.978474702471789E-001
   wg(21)=2.505754448157957E-002
   xg(22)=6.226088602037079E-001
   wg(22)=2.446120270795706E-002
   xg(23)=6.467619085141294E-001
   wg(23)=2.384096026596818E-002
   xg(24)=6.702830156031411E-001
   wg(24)=2.319742318525411E-002
   xg(25)=6.931491993558020E-001
   wg(25)=2.253122025633627E-002
   xg(26)=7.153381175730565E-001
   wg(26)=2.184300241624740E-002
   xg(27)=7.368280898020207E-001
   wg(27)=2.113344211252765E-002
   xg(28)=7.575981185197073E-001
   wg(28)=2.040323264620945E-002
   xg(29)=7.776279096494956E-001
   wg(29)=1.965308749443529E-002
   xg(30)=7.968978923903145E-001
   wg(30)=1.888373961337491E-002
   xg(31)=8.153892383391763E-001
   wg(31)=1.809594072212811E-002
   xg(32)=8.330838798884008E-001
   wg(32)=1.729046056832359E-002
   xg(33)=8.499645278795913E-001
   wg(33)=1.646808617614519E-002
   xg(34)=8.660146884971647E-001
   wg(34)=1.562962107754599E-002
   xg(35)=8.812186793850185E-001
   wg(35)=1.477588452744131E-002
   xg(36)=8.955616449707269E-001
   wg(36)=1.390771070371879E-002
   xg(37)=9.090295709825296E-001
   wg(37)=1.302594789297155E-002
   xg(38)=9.216092981453339E-001
   wg(38)=1.213145766297949E-002
   xg(39)=9.332885350430795E-001
   wg(39)=1.122511402318599E-002
   xg(40)=9.440558701362559E-001
   wg(40)=1.030780257486898E-002
   xg(41)=9.539007829254917E-001
   wg(41)=9.380419653694464E-003
   xg(42)=9.628136542558154E-001
   wg(42)=8.443871469669000E-003
   xg(43)=9.707857757637063E-001
   wg(43)=7.499073255464720E-003
   xg(44)=9.778093584869184E-001
   wg(44)=6.546948450845286E-003
   xg(45)=9.838775407060570E-001
   wg(45)=5.588428003865505E-003
   xg(46)=9.889843952429918E-001
   wg(46)=4.624450063422116E-003
   xg(47)=9.931249370374435E-001
   wg(47)=3.655961201326339E-003
   xg(48)=9.962951347331251E-001
   wg(48)=2.683925371553477E-003
   xg(49)=9.984919506395959E-001
   wg(49)=1.709392653517994E-003
   xg(50)=9.997137267734412E-001
   wg(50)=7.346344905057997E-004
end subroutine gaussian_points_and_weights


end module module_integration
  
