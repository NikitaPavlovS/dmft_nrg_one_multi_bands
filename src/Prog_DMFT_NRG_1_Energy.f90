﻿PROGRAM DMFT_NRG_1_band_energy
  use module_energy
  use module_impurity_1
  use module_parameters
  use module_nrg_variable
  use module_check_convergence
  use module_timer_omp
implicit none

double precision          :: point_double_1, point_double_2
integer                   :: i, j, is
integer	                 :: nmax_temp
logical                   :: dmft_loop_true = .false.

character*100             :: filename
character*100             :: fj,fi
character*100             :: filename_dos='DOS_G_Sigma_T_'

double precision          :: nM           ! capacity of electron states
double precision          :: Entropy_inf  ! Entropy at infinity

double precision          :: search_lambda
logical                   :: t_found
double precision          :: mu_nrg
! Timer
type(t_timer), pointer    :: total_time=>null(), iter_time=>null()
! OpenMP
!integer(kind = OMP_integer_kind) :: ID
!integer(kind = OMP_integer_kind) :: proc_num
!integer(kind = OMP_lock_kind)    :: lck
!========================================================================================================================
call add_timer( total_time,'    Program total time:' )
call start_timer( total_time )
! READ input parameters
OPEN(Unit = output_file, FILE = 'DMFT_out', FORM='FORMATTED')
Call read_input_1

! Search value of Lambda
if (t>1E-20) then
   lam = search_lambda(t, t_found)
else
   ! for t=0
   t_found = .true.
end if

if (t_found) then
   write(*,'(A5,f20.15)') ' T = ', t
   write(*,*)'Lambda is ', lam

   Call initialize_variables_1
   ! this file for NRG program, which read this file and use for partition function calculation.
   ! Because NRG use scaling energy mesh by Emax.

   point_double_1 = 0.d0
   point_double_2 = 0.d0
   !=============================================================================
   ! First call to NRG with iflag<0. Inititialize internal parameters like scale,
   ! actual temperature, etc. Will return the actually used t.
   SELECT CASE (nspin)
   CASE (1)
   	   Call nrg_nspin(P,SU2_sim,H_field,0.5d0,U,t,om,nspin,dd(1,:),point_double_1,occupancy_nrg,sig(1,:),point_double_2,nmax,lam,nkeep,-1,iover)
   CASE (2)
   	   Call nrg_nspin(P,SU2_sim,H_field,0.5d0,U,t,om,nspin,dd(1,:),dd(2,:),occupancy_nrg,sig(1,:),sig(2,:),nmax,lam,nkeep,-1,iover)
   CASE DEFAULT
   	   stop 'error in spin value'
   END SELECT
   !=============================================================================

   if ( new_calc ) then ! For first iter
      if (model.eq.2) then ! for FCC model
        if (start_from_read_mu) then
           mu = mu + 1.d0/sqrt(2.d0)
        else
           mu = 1.d0/sqrt(2.d0)
        end if
      end if
      write(output_file,*)
      write(output_file,*) '     Before DMFT loop'
      write(output_file,*) '     Lattice problem mu=',mu

      ! Calculation of bare Green function
      G(:,:)=dcmplx(0.d0,0.d0)
      sig(:,:)=dcmplx(0.d0,0.d0)
      Call green_function_1_models

      ! write bare green function and dos
      write(fi, '(f8.5)') t
      filename = trim(adjustl(filename_dos))//trim(adjustl(fi))//'.100'
      open(Unit = 100, FILE = filename, FORM='FORMATTED')
      if (nspin==1) then
         is = 1
         do i=-nmax,nmax
            write(100, '(x, 7(2x, e15.8))') om(i), dos(is, i), dreal(G(is, i)), dimag(G(is,i))
         end do 
      else
         do i=-nmax,nmax
            write(100,'(x,20(2x,e15.8))') om(i), totalDOS(i), dos(1,i), dos(2,i), (dreal(G(is,i)), dimag(G(is,i)), is=1, nspin)
         end do
      end if
      close(100+iter)

      ! Search for the chemical potential
      Call occupancy_and_mu_search_1
      write(output_file,*)
      if ((.not.start_from_read_mu) .or. variable_mu) then
         ! Adding Hartree term (U/2) for first iter
         mu = mu + 0.5d0*U
         ! G is recalculated with new Sigma and mu
         Call green_function_1_models
      end if
   end if

   ! Initialize DMFT and convergence parameters
   if (NDMFT>0) then 
      iter = 0
      dmft_loop_true=.true.
   else
      dmft_loop_true=.false.
   end if
   !=============================================================================
   DO WHILE (dmft_loop_true) ! DMFT loop
      Call add_timer( iter_time,' Iteration time:' )
      Call start_timer( iter_time )
      iter = iter+1

      write(*,*)
      write(*,*)
      write(*,*) '----------------------------------------------------------------'
      write(*,*) '     DMFT loop, iter # ', iter,', T =', t
      write(*,*) '----------------------------------------------------------------'
      write(*,*)
      write(output_file,*)
      write(output_file,*) '----------------------------------------------------------------'
      write(output_file,*) '     DMFT loop, iteration # ',iter
      write(output_file,*) '     Lattice problem mu =',mu
      write(output_file,*) '----------------------------------------------------------------'

      !--------------------------------------------------------------------------------------------------------
      !Do an NRG run
      if (model.eq.2) then
          mu_nrg = mu - 1.d0/sqrt(2.d0) ! for FCC ! need mu from half-filling
      else
          mu_nrg = mu
      end if
      SELECT CASE (nspin)
      CASE (1)
         Call nrg_nspin(P, SU2_sim, H_field, mu_nrg, u, t, om, nspin, dd(1,:), point_double_1, occupancy_nrg, sig(1, :), point_double_2, nmax, lam, nkeep, iflag, iover)
      CASE (2)
         Call nrg_nspin(P, SU2_sim, H_field, mu_nrg, u, t, om, nspin, dd(1, :), dd(2, :), occupancy_nrg, sig(1, :), sig(2, :), nmax, lam, nkeep, iflag, iover)
      CASE DEFAULT
         stop 'error in spin value'
      END SELECT
      !--------------------------------------------------------------------------------------------------------

      ! Calculation of Green function with new Sigma
      Call green_function_1_models

      ! Save Sigma and G for current iteration
      j=100+iter
      write(fj, *) j
      write(fi, '(f8.5)') t
      filename = trim(adjustl(filename_dos))//trim(adjustl(fi))//'.'//trim(adjustl(fj))
      open(Unit = 100+iter, FILE = filename, FORM='FORMATTED')
      if (nspin==1) then
         is = 1
         do i=-nmax,nmax
            write(100+iter, '(x, 7(2x, e15.8))') om(i), dos(is, i), dreal(G(is, i)), dimag(G(is,i)), dreal(sig(is, i)), dimag(sig(is,i))
         end do
      else
         do i=-nmax,nmax
            write(100+iter,'(x,20(2x,e15.8))') om(i), totalDOS(i), dos(1,i), dos(2,i), (dreal(G(is,i)), dimag(G(is,i)), dreal(sig(is,i)), dimag(sig(is,i)), is=1, nspin)
         end do
      end if
      close(100+iter)

      ! calculate total energy
      Call total_energy_1

      ! Search for the chemical potential
      write(output_file,*) 'Occupancy after nrg = ', occupancy_nrg
      Call occupancy_and_mu_search_1

      ! Check convergence
      Call check_convergence
      if ( (N_iter_for_converged>0) .and. (for_exit_loop>(N_iter_for_converged-1)) ) then
         dmft_loop_true = .false.
         write(output_file,'(A19,f12.8,A16)') 'DMFT loop for T = ', t,' is converged!!!'
      end if
      IF ( iter>(NDMFT-1) ) THEN
         dmft_loop_true = .false.
      END IF

      ! calculation of grand potential
      !if ( dmft_loop_true .eq. .false. ) then
         call grand_potential_G
         open( 1011, FILE = 'lnZ_imp', form='formatted')
         read(1011,*) Omega_imp
         close(1011)
         Omega_imp = -t * Omega_imp
         Omega_total = Omega_imp + Omega_lnGk - Omega_lnG
         Entropy = (totalEn - Omega_total - mu*occupancy_total)/t
         ! S_inf =  M { N/M ln[N/M] + (1-N/M) * ln[1-N/M] }, M - number of states, N - total occupancy same as
         ! S_inf =  M*ln[M] -N*ln[N] - (M-N)*ln[M-N] , M - number of states, N - total occupancy
         nM = 2.d0
         Entropy_inf = nM*dlog(nM) - occupancy_total*dlog(occupancy_total) - (nM-occupancy_total)*dlog(nM-occupancy_total)
         write(output_file,'(A69)') " Entropy, Omega_total, Omega_imp, Omega_lnGk, Omega_lnG, Entropy_inf:"
         write(output_file,'(6(2x,f22.16))') Entropy, Omega_total, Omega_imp, Omega_lnGk, Omega_lnG, Entropy_inf
      !end if

      ! write total enegry in output file
      write(output_file,'(x,A22,2x,f23.16,2x,i5,2(2x,f17.10),2x,f23.16)') 'T iter Kin Pot TotalEn', t, iter, Kin, Pot, totalEn

      ! Calculation Green function at new mu
      if ( mu_changed .and. dmft_loop_true .and. ((mod(iter,iter_each_change_mu)).eq.0) ) then
         Call green_function_1_models
      end if

      Call stop_timer( iter_time )
   END DO ! DMFT loop
   !=============================================================================

  !  open(521,file='DMFT_out_data',form='FORMATTED')
  !  if (nspin.eq.1) then
  !     write(521,'(A129)') '            T                       mu               occupancy      kinetic energy          potential energy          total energy'
  !     write(521,'(2(2x,e23.16),2x,f9.5,3(2x,e23.16))')t, mu, occupancy_total, Kin, Pot, totalEn
  !  else
  !     write(521,'(A155)') '            T                  magnetic moment               mu               occupancy      kinetic energy          potential energy          total energy'
  !     write(521,'(3(2x,e23.16),2x,f9.5,3(2x,e23.16))')t, Magnetic_moment, mu, occupancy_total, Kin, Pot, totalEn
  !  end if
  !  close(521)

   Call deallocate_variables_1
end if  ! t_found

Call stop_timer( total_time )
close(output_file)

END PROGRAM DMFT_NRG_1_band_energy
!===========================================================================================