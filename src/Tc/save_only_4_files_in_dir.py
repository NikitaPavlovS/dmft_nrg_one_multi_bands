# -*- coding: utf-8 -*-
import os
import glob
#--------------------------------------------------------------------
#--------------------------------------------------------------------
if __name__ == '__main__':
    #--------------------------------------------------------------------
    # search dirs
    curdir = os.getcwd()
    paths_in_dir = os.listdir(curdir)
    path_to_dirs = []
    for path in paths_in_dir:
       if os.path.isdir(path):
          path_to_dirs.append(path)
    #--------------------------------------------------------------------
    # Do subfolders exist?
    os.chdir(curdir)
    path_to_dirs_with_sub = []
    for path in path_to_dirs:
      if os.path.exists(path):
         path_to_dirs_with_sub.append(path)
         paths_in_dir = os.listdir(path)
         for path_two in paths_in_dir:
           full_path=os.path.join(path,path_two)
           if os.path.isdir(full_path):
              path_to_dirs_with_sub.append(full_path)
    #--------------------------------------------------------------------
    # delete DOS files. Save only 10 last iterations
    for path in path_to_dirs_with_sub:
        os.chdir(curdir)
        os.chdir(path)
        dos_files = sorted(glob.glob('DOS_G*'))
        N_dos_files = len(dos_files)
        if (N_dos_files>0):
           i=0
           for files in dos_files:
              if(((N_dos_files-i)>4)):
                 os.remove(files)
              i=i+1
