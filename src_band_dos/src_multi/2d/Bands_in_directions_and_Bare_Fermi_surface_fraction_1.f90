SUBROUTINE Bands_in_directions_and_Bare_Fermi_surface_fraction

  use energy_module
  use hamiltonian_module_3band
  use parameters_module
!  use impurity_module

IMPLICIT NONE

!============================================
! INTERNAL variables
complex*16,       allocatable :: Hamilton(:,:)
complex*16,       allocatable :: EigenValues(:)
complex*16,       allocatable :: EigenValues_cekord(:)
double precision, allocatable :: Coefficient(:,:)
character*1                             :: compute_eigenvector
complex*16,       allocatable :: EigenVectors_right(:,:)
complex*16,       allocatable :: EigenVectors_right_cekord(:,:)
complex*16,       allocatable :: EigenVectors_left(:,:)
complex*16,       allocatable :: EigenVectors_left_cekord(:,:)
! need for lapack procedures for matrix
complex*16,       allocatable :: WORK(:)      
double precision, allocatable :: RWORK(:)
integer                       :: INFO
!--------------------------------------------------------------------------------
integer                       :: i, j, l, m           
double precision              :: dk             ! k step
double precision, PARAMETER   :: kMin=0.d0
double precision              :: kMax
integer,          PARAMETER   :: MaxK=300
double precision, allocatable :: kxa(:), kya(:) ! mesh of k points
double precision              :: kx, ky         ! variable for symmetric directions in Brilloun zone

logical, parameter            :: Hamilton_with_sim_anti=1
!--------------------------------------------------------------------------------
! for bare Fermi surface
logical                       :: EigenValues_Fermi
double precision              :: tempd_value_previous, tempd_value_now
double precision              :: tempdouble
double precision              :: a_left(1:2), b_right(1:2), c_medium(1:2)
!======================================================================================
ALLOCATE(Hamilton( 1:ndim, 1:ndim))
ALLOCATE(EigenValues(1:ndim))
ALLOCATE(EigenValues_cekord(1:ndim))
ALLOCATE(Coefficient( 1:ndim, 1:ndim ))
ALLOCATE(EigenVectors_right(1:ndim, 1:ndim))
ALLOCATE(EigenVectors_right_cekord(1:ndim, 1:ndim))
ALLOCATE(EigenVectors_left(1:ndim, 1:ndim))
ALLOCATE(EigenVectors_left_cekord(1:ndim, 1:ndim))
ALLOCATE(WORK(1:5*ndim))
ALLOCATE(RWORK(1:2*ndim))
ALLOCATE(kxa(0:MaxK))
ALLOCATE(kya(0:MaxK))

!======================================================================================
kMax=pi

IF (Bands_plot) THEN
!make kx and ky mesh
dk=(kMax - kMin)/MaxK
DO i = 0, MaxK
   kxa(i)= kMin +dk*i
   kya(i)= kMin +dk*i
END DO
!==================
OPEN(Unit = 98, FILE = 'OUT_BANDS.dat', FORM='FORMATTED')
if (FBANDS) then
   compute_eigenvector = 'V'
   if (FBANDS_write) then
      OPEN(Unit = 15, FILE = 'OUT_FBANDS_Ed_Ed.dat', FORM='FORMATTED')
      OPEN(Unit = 16, FILE = 'OUT_FBANDS_Ep1_Ed.dat', FORM='FORMATTED')
      OPEN(Unit = 17, FILE = 'OUT_FBANDS_Ep2_Ed.dat', FORM='FORMATTED')

      OPEN(Unit = 18, FILE = 'OUT_FBANDS_Ed_Ep1.dat', FORM='FORMATTED')
      OPEN(Unit = 19, FILE = 'OUT_FBANDS_Ep1_Ep1.dat', FORM='FORMATTED')
      OPEN(Unit = 20, FILE = 'OUT_FBANDS_Ep2_Ep1.dat', FORM='FORMATTED')

      OPEN(Unit = 21, FILE = 'OUT_FBANDS_Ed_Ep2.dat', FORM='FORMATTED')
      OPEN(Unit = 22, FILE = 'OUT_FBANDS_Ep1_Ep2.dat', FORM='FORMATTED')
      OPEN(Unit = 23, FILE = 'OUT_FBANDS_Ep2_Ep2.dat', FORM='FORMATTED')
   end if
else
    compute_eigenvector = 'N'
end if
!-----------------------------------------------------------------------------------------------------------------------------
!direction (0,0) -- (pi,0)
DO i = 0, MaxK
      kx=kxa(i)
      ky=0.d0
      ! searching Hamilton eigenvalues
      CALL Hamilton_three_band_in_kx_ky_mu( kx, ky, mu, Hamilton)
      CALL ZGEEV( 'N', compute_eigenvector, ndim, Hamilton, ndim, EigenValues, EigenVectors_left, ndim,&
                  EigenVectors_right, ndim, WORK, 5*ndim, RWORK, INFO )
!-----------------------------------------------------------------------------------------------------------------------------

!     EigenVectors_right_cekord = EigenVectors_right
!      CALL Sort_Eigenvalues_with_EigenVectors( ndim, EigenValues, EigenValues_cekord, EigenVectors_right, EigenVectors_right_cekord )
!       call cekord( EigenValues, EigenValues_cekord, EigenVectors_right, EigenVectors_right_cekord, EigenVectors_left, Eigen2Vectors_left_cekord,  ndim )
       call cekord( EigenValues, EigenValues_cekord, EigenVectors_right, EigenVectors_right_cekord,  ndim )
!right
     IF (.NOT.Hamilton_with_sim_anti) THEN
     do j=1, ndim
        EigenVectors_right(2,j) = 1.d0/sqrt(2.d0) * ( EigenVectors_right_cekord(2,j) + EigenVectors_right_cekord(3,j) )
        EigenVectors_right(3,j) = 1.d0/sqrt(2.d0) * ( EigenVectors_right_cekord(2,j) - EigenVectors_right_cekord(3,j) )
     end do
     END IF
!origin
      if (FBANDS) then
         Coefficient(1,:) = (abs(EigenVectors_right_cekord(:,1)))**2
         Coefficient(2,:) = (abs(EigenVectors_right_cekord(:,2)))**2
         Coefficient(3,:) = (abs(EigenVectors_right_cekord(:,3)))**2
      end if

!      if (FBANDS) then
!         Coefficient(1,:) = (abs(EigenVectors_right_cekord(1,:)))**2
!         Coefficient(2,:) = (abs(EigenVectors_right_cekord(2,:)))**2
!         Coefficient(3,:) = (abs(EigenVectors_right_cekord(3,:)))**2
!      end if
!------------------------------------------------------------------------------------------------------------------
      WRITE(98, '(2x,10(2x,F15.7))') kx, dreal(EigenValues_cekord(1)), dreal(EigenValues_cekord(2)), dreal(EigenValues_cekord(3))
      if (FBANDS) then
         if (FBANDS_write) then
            WRITE(15, '(2x,3(2x,F15.7))') kx, dreal(EigenValues_cekord(1)), Coefficient(1,1)
            WRITE(16, '(2x,3(2x,F15.7))') kx, dreal(EigenValues_cekord(2)), Coefficient(1,2)
            WRITE(17, '(2x,3(2x,F15.7))') kx, dreal(EigenValues_cekord(3)), Coefficient(1,3)

            WRITE(18, '(2x,3(2x,F15.7))') kx, dreal(EigenValues_cekord(1)), Coefficient(2,1)
            WRITE(19, '(2x,3(2x,F15.7))') kx, dreal(EigenValues_cekord(2)), Coefficient(2,2)
            WRITE(20, '(2x,3(2x,F15.7))') kx, dreal(EigenValues_cekord(3)), Coefficient(2,3)

            WRITE(21, '(2x,3(2x,F15.7))') kx, dreal(EigenValues_cekord(1)), Coefficient(3,1)
            WRITE(22, '(2x,3(2x,F15.7))') kx, dreal(EigenValues_cekord(2)), Coefficient(3,2)
            WRITE(23, '(2x,3(2x,F15.7))') kx, dreal(EigenValues_cekord(3)), Coefficient(3,3)
         end if

         WRITE(99,*) 'C_Ed  = ', Coefficient(1,1) + Coefficient(1,2) + Coefficient(1,3)
         WRITE(99,*) 'C_Ep1 = ', Coefficient(2,1) + Coefficient(2,2) + Coefficient(2,3)
         WRITE(99,*) 'C_Ep2 = ', Coefficient(3,1) + Coefficient(3,2) + Coefficient(3,3)
      end if

END DO!direction (0,0) -- (pi,0)

!stop 'stop in Bands_in_symmetric_direction after 1 cilce'

   WRITE(98,*)
if (FBANDS_write) then
   WRITE(15,*)
   WRITE(16,*)
   WRITE(17,*)

   WRITE(18,*)
   WRITE(19,*)
   WRITE(20,*)

   WRITE(21,*)
   WRITE(22,*)
   WRITE(23,*)
end if
if (FBANDS) then
WRITE(99,*)
end if

!==============================================================================================================
!direction (pi,0) -- (pi,pi)
DO i = 0, MaxK
      kx=kMax
      ky=kya(i)
      CALL Hamilton_three_band_in_kx_ky_mu( kx, ky, mu, Hamilton)
      CALL ZGEEV( 'N', compute_eigenvector, ndim, Hamilton, ndim, EigenValues, EigenVectors_left, ndim,&
                  EigenVectors_right, ndim, WORK, 5*ndim, RWORK, INFO )
!------------------------------------------------------------------------------------------------------------------

!     EigenVectors_right_cekord = EigenVectors_right
!      CALL Sort_Eigenvalues_with_EigenVectors( ndim, EigenValues, EigenValues_cekord, EigenVectors_right, EigenVectors_right_cekord )
!       call cekord( EigenValues, EigenValues_cekord, EigenVectors_right, EigenVectors_right_cekord, EigenVectors_left, EigenVectors_left_cekord, ndim )
       call cekord( EigenValues, EigenValues_cekord, EigenVectors_right, EigenVectors_right_cekord,  ndim )

     IF (.NOT.Hamilton_with_sim_anti) THEN
     do j=1, ndim
        EigenVectors_right(2,j) = 1.d0/sqrt(2.d0) * ( EigenVectors_right_cekord(2,j) + EigenVectors_right_cekord(3,j) )
        EigenVectors_right(3,j) = 1.d0/sqrt(2.d0) * ( EigenVectors_right_cekord(2,j) - EigenVectors_right_cekord(3,j) )
     end do
     END IF

      if (FBANDS) then
         Coefficient(1,:) = (abs(EigenVectors_right_cekord(:,1)))**2
         Coefficient(2,:) = (abs(EigenVectors_right_cekord(:,2)))**2
         Coefficient(3,:) = (abs(EigenVectors_right_cekord(:,3)))**2
      end if
!------------------------------------------------------------------------------------------------------------------
      WRITE(98, '(2x,10(2x,F15.7))') kMax + ky, dreal(EigenValues_cekord(1)), dreal(EigenValues_cekord(2)), dreal(EigenValues_cekord(3))
      if (FBANDS) then
         if (FBANDS_write) then
            WRITE(15, '(2x,3(2x,F15.7))') kMax + ky, dreal(EigenValues_cekord(1)), Coefficient(1,1)
            WRITE(16, '(2x,3(2x,F15.7))') kMax + ky, dreal(EigenValues_cekord(2)), Coefficient(1,2)
            WRITE(17, '(2x,3(2x,F15.7))') kMax + ky, dreal(EigenValues_cekord(3)), Coefficient(1,3)

            WRITE(18, '(2x,3(2x,F15.7))') kMax + ky, dreal(EigenValues_cekord(1)), Coefficient(2,1)
            WRITE(19, '(2x,3(2x,F15.7))') kMax + ky, dreal(EigenValues_cekord(2)), Coefficient(2,2)
            WRITE(20, '(2x,3(2x,F15.7))') kMax + ky, dreal(EigenValues_cekord(3)), Coefficient(2,3)

            WRITE(21, '(2x,3(2x,F15.7))') kMax + ky, dreal(EigenValues_cekord(1)), Coefficient(3,1)
            WRITE(22, '(2x,3(2x,F15.7))') kMax + ky, dreal(EigenValues_cekord(2)), Coefficient(3,2)
            WRITE(23, '(2x,3(2x,F15.7))') kMax + ky, dreal(EigenValues_cekord(3)), Coefficient(3,3)
         end if

         WRITE(99,*) 'C_Ed  = ', Coefficient(1,1) + Coefficient(1,2) + Coefficient(1,3)
         WRITE(99,*) 'C_Ep1 = ', Coefficient(2,1) + Coefficient(2,2) + Coefficient(2,3)
         WRITE(99,*) 'C_Ep2 = ', Coefficient(3,1) + Coefficient(3,2) + Coefficient(3,3)
      end if

END DO !direction (pi,0) -- (pi,pi)

   WRITE(98,*)
if (FBANDS_write) then
   WRITE(15,*)
   WRITE(16,*)
   WRITE(17,*)

   WRITE(18,*)
   WRITE(19,*)
   WRITE(20,*)

   WRITE(21,*)
   WRITE(22,*)
   WRITE(23,*)
end if
if (FBANDS) then
WRITE(99,*)
end if


!==============================================================================================================
!direction (pi,pi) -- (0,0)
DO i = 0, MaxK
      kx= kMax -dk*i
      ky= kMax -dk*i
      CALL Hamilton_three_band_in_kx_ky_mu( kx, ky, mu, Hamilton)
      CALL ZGEEV( 'N', compute_eigenvector, ndim, Hamilton, ndim, EigenValues, EigenVectors_left, ndim,&
                  EigenVectors_right, ndim, WORK, 5*ndim, RWORK, INFO )
!------------------------------------------------------------------------------------------------------------------

!     EigenVectors_right_cekord = EigenVectors_right
!     do j=1, ndim
!        EigenVectors_right(j,2) = 1.d0/sqrt(2.d0) * ( EigenVectors_right_cekord(j,2) + EigenVectors_right_cekord(j,3) )
!        EigenVectors_right(j,3) = 1.d0/sqrt(2.d0) * ( EigenVectors_right_cekord(j,2) - EigenVectors_right_cekord(j,3) )
!     end do
!      CALL Sort_Eigenvalues_with_EigenVectors( ndim, EigenValues, EigenValues_cekord, EigenVectors_right, EigenVectors_right_cekord )
!       call cekord( EigenValues, EigenValues_cekord, EigenVectors_right, EigenVectors_right_cekord, EigenVectors_left, EigenVectors_left_cekord, ndim )
       call cekord( EigenValues, EigenValues_cekord, EigenVectors_right, EigenVectors_right_cekord,  ndim )

!right
     IF (.NOT.Hamilton_with_sim_anti) THEN
     do j=1, ndim
        EigenVectors_right(2,j) = 1.d0/sqrt(2.d0) * ( EigenVectors_right_cekord(2,j) + EigenVectors_right_cekord(3,j) )
        EigenVectors_right(3,j) = 1.d0/sqrt(2.d0) * ( EigenVectors_right_cekord(2,j) - EigenVectors_right_cekord(3,j) )
     end do
     END IF

      if (FBANDS) then
         Coefficient(1,:) = (abs(EigenVectors_right_cekord(:,1)))**2
         Coefficient(2,:) = (abs(EigenVectors_right_cekord(:,2)))**2
         Coefficient(3,:) = (abs(EigenVectors_right_cekord(:,3)))**2
      end if
!------------------------------------------------------------------------------------------------------------------
      WRITE(98, '(2x,10(2x,F15.7))') 3*kMax - kx, dreal(EigenValues_cekord(1)), dreal(EigenValues_cekord(2)), dreal(EigenValues_cekord(3))
      if (FBANDS) then
         if (FBANDS_write) then
            WRITE(15, '(2x,3(2x,F15.7))') 3*kMax - kx, dreal(EigenValues_cekord(1)), Coefficient(1,1)
            WRITE(16, '(2x,3(2x,F15.7))') 3*kMax - kx, dreal(EigenValues_cekord(2)), Coefficient(1,2)
            WRITE(17, '(2x,3(2x,F15.7))') 3*kMax - kx, dreal(EigenValues_cekord(3)), Coefficient(1,3)

            WRITE(18, '(2x,3(2x,F15.7))') 3*kMax - kx, dreal(EigenValues_cekord(1)), Coefficient(2,1)
            WRITE(19, '(2x,3(2x,F15.7))') 3*kMax - kx, dreal(EigenValues_cekord(2)), Coefficient(2,2)
            WRITE(20, '(2x,3(2x,F15.7))') 3*kMax - kx, dreal(EigenValues_cekord(3)), Coefficient(2,3)

            WRITE(21, '(2x,3(2x,F15.7))') 3*kMax - kx, dreal(EigenValues_cekord(1)), Coefficient(3,1)
            WRITE(22, '(2x,3(2x,F15.7))') 3*kMax - kx, dreal(EigenValues_cekord(2)), Coefficient(3,2)
            WRITE(23, '(2x,3(2x,F15.7))') 3*kMax - kx, dreal(EigenValues_cekord(3)), Coefficient(3,3)
         end if

         WRITE(99,*) 'C_Ed  = ', Coefficient(1,1) + Coefficient(1,2) + Coefficient(1,3)
         WRITE(99,*) 'C_Ep1 = ', Coefficient(2,1) + Coefficient(2,2) + Coefficient(2,3)
         WRITE(99,*) 'C_Ep2 = ', Coefficient(3,1) + Coefficient(3,2) + Coefficient(3,3)
      end if

END DO !direction (pi,pi) -- (0,0)

WRITE(98,*)
CLOSE(98)
if (FBANDS_write) then
   CLOSE(15)
   CLOSE(16)
   CLOSE(17)

   CLOSE(18)
   CLOSE(19)
   CLOSE(20)

   CLOSE(21)
   CLOSE(22)
   CLOSE(23)
end if
END IF !Bands_plot
!======================================================================================
!======================================================================================
IF (Fermi_Surface_without_Interaction) THEN
    tempdouble = 0.d0
    dk=(kMax - kMin)/MaxK
    compute_eigenvector = 'N'
   ! to file OUT_Fermi_surface_bare.dat write bare Fermi surface
   ! search by diagonal half divided methods
   OPEN(Unit = 900, file='OUT_Fermi_surface_bare.dat', form='FORMATTED')
   DO i = 0, 2*MaxK
      j=0
      DO WHILE ((j.eq.0).or.(.NOT.((abs(kMax-ky)<1E-5).or.(abs(kMax-kx)<1E-5))))
         IF (i<MaxK) THEN
            kx= kMin +dk*j
            ky= kMax +dk*(j-i)
            j=j+1
         ELSE
            kx= kMin +dk*((i-MaxK)+j)
            ky= kMin +dk*j
            j=j+1
         END IF
         b_right(1) = kx
         b_right(2) = ky
         write(901,'(2x,2(2x,F15.8))') kx, ky

         CALL Hamilton_three_band_in_kx_ky_mu( kx, ky, mu, Hamilton)
         CALL ZGEEV( 'N', compute_eigenvector, ndim, Hamilton, ndim, EigenValues, EigenVectors_left, ndim,&
                  EigenVectors_right, ndim, WORK, 5*ndim, RWORK, INFO )
         call cekord( EigenValues, EigenValues_cekord, EigenVectors_right, EigenVectors_right_cekord,  ndim )
         
         IF (j.eq.0) THEN
            tempd_value_previous = dreal(EigenValues_cekord(number_of_band_crossing_FL))
            a_left(1) = kx
            a_left(2) = ky
            IF (abs(tempd_value_previous)<1E-6) THEN
                write(900,'(2x,3(2x,F15.8))') kx, ky, tempdouble
            END IF
         ELSE
            tempd_value_now = dreal(EigenValues_cekord(number_of_band_crossing_FL))
            IF (tempd_value_previous*tempd_value_now<0) THEN
               IF (1) THEN
               EigenValues_Fermi=0

               DO WHILE (.NOT.EigenValues_Fermi)
                  c_medium(1) = (a_left(1) + b_right(1))/2.d0
                  c_medium(2) = (a_left(2) + b_right(2))/2.d0
                  CALL Hamilton_three_band_in_kx_ky_mu( c_medium(1), c_medium(2), mu, Hamilton)
                  CALL ZGEEV( 'N', compute_eigenvector, ndim, Hamilton, ndim, EigenValues, EigenVectors_left, ndim,&
                           EigenVectors_right, ndim, WORK, 5*ndim, RWORK, INFO )
                  call cekord( EigenValues, EigenValues_cekord, EigenVectors_right, EigenVectors_right_cekord,  ndim )
                  IF (tempd_value_previous*dreal(EigenValues_cekord(number_of_band_crossing_FL))<0) THEN
                     b_right(1)=c_medium(1)
                     b_right(2)=c_medium(2)
                     !tempd_value_now = dreal(EigenValues_cekord(3))
                  ELSE
                     a_left(1)=c_medium(1)
                     a_left(2)=c_medium(2)
                     tempd_value_previous = dreal(EigenValues_cekord(number_of_band_crossing_FL))
                  END IF
                  IF (abs(dreal(EigenValues_cekord(number_of_band_crossing_FL)))<1E-6) THEN
                     EigenValues_Fermi=1
                     write(900,'(2x,3(2x,F15.8))') c_medium(1), c_medium(2), tempdouble
                  END IF
               END DO
               !------------------------------------------------------------------------------------------------------------------
               ELSE
                  IF (tempd_value_now<tempd_value_previous) THEN
                     write(900,'(2x,3(2x,F15.8))') b_right(1), b_right(2), tempdouble
                  ELSE
                     write(900,'(2x,3(2x,F15.8))') a_left(1), a_left(2), tempdouble
                  END IF
               END IF
               !------------------------------------------------------------------------------------------------------------------
            END IF
            tempd_value_previous = tempd_value_now
         END IF
         a_left(1) = kx
         a_left(2) = ky
      END DO
   END DO
   CLOSE(900)
END IF
!========================================================================================================================================

!stop 'stop in Bands_in_symmetric_direction END'

deallocate( Hamilton, kxa, kya, EigenValues, Coefficient, EigenVectors_right, EigenValues_cekord, EigenVectors_right_cekord, EigenVectors_left, EigenVectors_left_cekord, WORK, RWORK)

END SUBROUTINE Bands_in_directions_and_Bare_Fermi_surface_fraction