subroutine DMFT_loop_one_band

  use energy_module
  use hamiltonian_module_1band
  use parameters_module
  use impurity_module

  implicit none

!----------------------------------------------------------------------------------------------
!Green function
complex*16,       allocatable :: Gtmp(:,:) ! integrated Green function for px, py, and total d+px+py
! Integer variables for circles
integer                       :: i, j, is
double precision              :: tmpreal
double precision              :: point_double_1,point_double_2
integer                       :: number_of_file_for_DOS, number_of_file_for_dd, number_of_file_for_SIG
complex*16,       allocatable :: z(:)
double precision, allocatable :: b(:),c(:)
!----------------------------------------------------------------------------------------------

if (start_iter<=NDMFT) then
allocate(Gtmp(1:nspin,-nmax:nmax))
point_double_1 = 0.d0
point_double_2 = 0.d0

DO iter=start_iter,NDMFT

 number_of_file_for_SIG = 10+iter
 if (.not.(INIT_dd.and.(iter.eq.start_iter))) then
 ! calculate DOS if not (start from DOS and first iteration)
   if ((nspin==2).and.(H_field>1E-6)) then
      sig(1,:) = sig(1,:) - dcmplx(H_field/2.d0, 0.d0)
      sig(2,:) = sig(2,:) + dcmplx(H_field/2.d0, 0.d0)
   end if
   number_of_file_for_DOS = 100+iter
   number_of_file_for_dd = 200+iter
   IF (Pseudogap) THEN
      SELECT CASE (Method_of_Green_Function_calculation)
      CASE (1)
         do i=-nmax,nmax
            Call GreenFunction_of_E_Sigma_mu_fraction_PG( om(i), sig(:,i), mu, Gtmp(:,i))
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,i))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,i))
            !dd(i)=dimag(sig(i)) - dimag(G(i))/((dreal(G(i)))**2+(dimag(G(i)))**2)
            !----------------------------------------------------
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
            !write(250+iter, '(2x,7(2x,f15.8))') om(i), dd(i), dreal(G(i)), dimag(G(i)), dreal(sig(i)), dimag(sig(i)), dos(i)
         end do
      !----------------------------------------------------
      CASE (2)
         do i=-nmax,nmax
            !Call GreenFunction_of_E_Sigma_mu_Sigma_fraction_PG( om(i), sig(:,i), mu, Gtmp(:,i))
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,i))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,i))
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
         end do
      !----------------------------------------------------
      CASE (3)
         do i=-nmax,nmax
            !Call GreenFunction_of_E_Sigma_mu_Eigen_system_PG( om(i), sig(:,i), mu, Gtmp(:,i))
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,i))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,i))
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
         end do
      !----------------------------------------------------
      CASE DEFAULT
            stop 'error in selection of Green function method'
      END SELECT
   ELSE
      SELECT CASE (Method_of_Green_Function_calculation)
      !----------------------------------------------------
      CASE (0)
         do i=-nmax,nmax
          if (1) then
            allocate(z(1:nspin),b(1:nspin),c(1:nspin))
            Gtmp(:,i) = (0.d0,0.d0)
            do j=-nmax,nmax
             do is=1, nspin
                   c(is) = (dos_0(is,j+1) - dos_0(is,j))/(om(j+1)-om(j))
                   b(is) = dos_0(is,j) - c(is)*om(j)
                   z(is) = dcmplx(om(i)+mu,delta)-sig(is,i)
                   Gtmp(is,i) = Gtmp(is,i) - c(is)*(om(j+1)-om(j)) - (b(is) + c(is)*z(is)) * cdlog(1.d0 - (om(j+1)-om(j))/(z(is) - om(j)))
             end do
            end do
            deallocate(z,b,c)
          else
            Gtmp(:,i) = (0.d0,0.d0)
            do j=-nmax,nmax-1
             do is=1, nspin
                Gtmp(is,i) = Gtmp(is,i) + ( dos_0(is,j+1)/(dcmplx(om(i)-om(j+1),delta)-sig(is,j+1)) - dos_0(is,j)/(dcmplx(om(i)-om(j),delta)-sig(is,j)) )*(om(j+1)-om(j))/2.d0
             end do
            end do
            Gtmp(is,i) = Gtmp(is,i) + ( dos_0(is,-nmax)/(dcmplx(om(i)-om(-nmax),delta)-sig(is,-nmax)) - dos_0(is,nmax)/(dcmplx(om(i)-om(nmax),delta)-sig(is,nmax)) )*(om(nmax)-om(nmax-1))/2.d0
          end if
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,i))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,i))
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
         end do
      !----------------------------------------------------
      CASE (1)
         do i=-nmax,nmax
            Call GreenFunction_of_E_Sigma_mu_fraction_noPG( om(i), sig(:,i), mu, Gtmp(:,i))
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,i))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,i))
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
         end do
      !----------------------------------------------------
      CASE (2)
         do i=-nmax,nmax
            !Call GreenFunction_of_E_Sigma_mu_Sigma_fraction_noPG( om(i), sig(:,i), mu, Gtmp(:,i))
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,i))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,i))
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
         end do
      !----------------------------------------------------
      CASE (3)
         do i=-nmax,nmax
            !Call GreenFunction_of_E_Sigma_mu_Eigen_system_noPG( om(i), sig(:,i), mu, Gtmp(:,i))
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,i))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,i))
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
         end do
      !----------------------------------------------------
      CASE DEFAULT
            stop 'error in selection of Green function method'
      END SELECT
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
!    IF (const_mesh) THEN
!       integral_of_DOS_d = (dos(-nmax)+dos(0))*dE/2.d0
!       integral_of_DOS_total = (totalDOS(-nmax)+totalDOS(0))*dE/2.d0
!       do i=-nmax,0
!          integral_of_DOS_d = integral_of_DOS_d + (dos(i)+dos(i+1))*dE/2.d0        !(om(i+1)-om(i))/2
!          integral_of_DOS_total = integral_of_DOS_total + (totalDOS(i)+totalDOS(i+1))*dE/2.d0        !(om(i+1)-om(i))/2
!       end do
!       write(91,*) 'integral_ot_G do E=0', integral_of_DOS_total
!       write(91,*) 'integral_ot_Gd do E=0', integral_of_DOS_d
!    END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   ! Calculate spectral function and Fermi surface for this iteration
   SELECT CASE (Method_of_Green_Function_calculation)
   CASE (0)
      IF (Spectral_Function_plot) THEN
         CALL Spectral_Function_in_symmetric_direction_fraction
      END IF
      IF (Fermi_surface_plot) THEN
         CALL Fermi_surface_fraction
      END IF
   CASE (1)
      IF (Spectral_Function_plot) THEN
         CALL Spectral_Function_in_symmetric_direction_fraction
      END IF
      IF (Fermi_surface_plot) THEN
         CALL Fermi_surface_fraction
      END IF
   CASE (2)
      IF (Spectral_Function_plot) THEN
!          CALL Spectral_Function_in_symmetric_direction_Sigma_fraction( iter, Spectral_Function_plot, 0,&
!                              om, MaxK_point_for_SF, sig, mu, t, 0, Spectral_Function_maximum_low_cutting)
      END IF
      IF (Fermi_surface_plot) THEN
!          CALL Fermi_surface_Sigma_fraction( iter, om, sig, mu, t, Energy_shift, Apply_Hand_Sigma_value, Hand_Sigma_value )
      END IF
   CASE (3)
      IF (Spectral_Function_plot) THEN
!          CALL Spectral_Function_in_symmetric_direction_Eigen_system( iter, Spectral_Function_plot, 0, &
!                              om, MaxK_point_for_SF, sig, mu, t, 0, Spectral_Function_maximum_low_cutting)
      END IF
      IF (Fermi_surface_plot) THEN
!          CALL Fermi_surface_Eigen_system( iter, om, sig, mu, t, Energy_shift, Apply_Hand_Sigma_value, Hand_Sigma_value )
      END IF
   CASE DEFAULT
        stop 'error in selection of Green function method in DMFT for SF and FS'
   END SELECT

   write(*,*)'Lattice problem mu=',mu
 end if !INIT_dd
   !---------------------------------------------------------------------------------------------------------------------------------------
   !Do an NRG run
   SELECT CASE (nspin)
   CASE (1)
      Call nrg_nspin(P,SU2_sim,H_field,mu,u,t,om,nspin,dd(1,:),point_double_1,occ,sig(1,:),point_double_2,nmax,lam,nkeep,iflag,iover)
   CASE (2)
      Call nrg_nspin(P,SU2_sim,H_field,mu,u,t,om,nspin,dd(1,:),dd(2,:),occ,sig(1,:),sig(2,:),nmax,lam,nkeep,iflag,iover)
   CASE DEFAULT
      stop 'error in spin value'
   END SELECT
   !----------------------------------------------------
   sig(:,:) = sig(:,:) + dcmplx(0.d0,-delta)
   !----------------------------------------------------
   !Save sig for current iteration
   do i=-nmax,nmax
      write(number_of_file_for_SIG,'(x,5(2x,f22.15))') om(i), (dreal(sig(is,i)),dimag(sig(is,i)),is=1,nspin)
   end do
   !---------------------------------------------------------------------------------------------------------------------------------------
   !Search for the chemical potential for given qval
   IF (((iter.ne.start_iter).or.INIT_dd).and.variable_mu) THEN
      Call occupancy_and_mu_search_one_band
   END IF

!   stop 'stop after first iteration of DMFT'
END DO !DMFT loop

deallocate(Gtmp)
end if

end subroutine DMFT_loop_one_band
