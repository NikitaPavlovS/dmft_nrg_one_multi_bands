SUBROUTINE Hamilton_n_band_in_kx_ky_mu( kx, ky, mu, Hamilton)

  USE hamiltonian_module_nband
  IMPLICIT NONE

double precision,    intent(in) :: kx, ky
double precision,    intent(in) :: mu
complex*16,         intent(out) :: Hamilton( 1:ndim, 1:ndim)
!===============================================================

      Hamilton(1,1) = dcmplx( Ed1 + 2.d0*tdd*(Cos(kx) + Cos(ky)) - mu, 0.d0)
      Hamilton(1,2) = dcmplx( 0.d0, 0.d0)
      Hamilton(1,3) = dcmplx( 0.d0, 0.d0)
      Hamilton(1,4) = dcmplx( 0.d0, 0.d0)
      Hamilton(1,5) = dcmplx( 0.d0, 0.d0)
      
      Hamilton(2,1) = dcmplx( 0.d0, 0.d0)
      Hamilton(2,2) = dcmplx( Ed2 + 2.d0*tdd*(Cos(kx) + Cos(ky)) - mu, 0.d0)
      Hamilton(2,3) = dcmplx( 0.d0, 0.d0)
      Hamilton(2,4) = dcmplx( 0.d0, 0.d0)
      Hamilton(2,5) = dcmplx( 0.d0, 0.d0)
      
      Hamilton(3,1) = dcmplx( 0.d0, 0.d0)
      Hamilton(3,2) = dcmplx( 0.d0, 0.d0)
      Hamilton(3,3) = dcmplx( Ed3 + 2.d0*tdd*(Cos(kx) + Cos(ky)) - mu, 0.d0)
      Hamilton(3,4) = dcmplx( 0.d0, 0.d0)
      Hamilton(3,5) = dcmplx( 0.d0, 0.d0)
      
      Hamilton(4,1) = dcmplx( 0.d0, 0.d0)
      Hamilton(4,2) = dcmplx( 0.d0, 0.d0)
      Hamilton(4,3) = dcmplx( 0.d0, 0.d0)
      Hamilton(4,4) = dcmplx( Ed4 + 2.d0*tdd*(Cos(kx) + Cos(ky)) - mu, 0.d0)
      Hamilton(4,5) = dcmplx( 0.d0, 0.d0)
      
      Hamilton(5,1) = dcmplx( 0.d0, 0.d0)
      Hamilton(5,2) = dcmplx( 0.d0, 0.d0)
      Hamilton(5,3) = dcmplx( 0.d0, 0.d0)
      Hamilton(5,4) = dcmplx( 0.d0, 0.d0)
      Hamilton(5,5) = dcmplx( Ed5 + 2.d0*tdd*(Cos(kx) + Cos(ky)) - mu, 0.d0)

!====================================================


END SUBROUTINE Hamilton_n_band_in_kx_ky_mu
