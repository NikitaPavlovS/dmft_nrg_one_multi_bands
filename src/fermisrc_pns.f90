function fermisrc( dos, e, ne, mu, tol, occup, temp, mumin, mumax)
  use module_integration
  implicit none
!----------------------------------------------------------------------|
!                           Fermi level search                         |
!----------------------------------------------------------------------|
!                                                                      |
!i     dos        - DOS                                                |
!i     e          - energy array                                       |
!i     ne         - number of points                                   |
!i     mu         - old value of Fermi level                           |
!i     occup      - required occupancy                                 |
!i     temp       - temperature                                        |
!i     tol        - tolerance of occuracy search                       |
!                                                                      |
!o     fermisrc   - new value of Fermi level                           |
!                                                                      |
!----------------------------------------------------------------------|
!---> Passed parameters
      integer         , intent(in) :: ne
      double precision, intent(in) :: dos(-ne:ne), e(-ne:ne)
      double precision, intent(in) :: occup, temp, mumin, mumax
      double precision, intent(in) :: mu, tol
      double precision             :: fermisrc

!---> Local parameters
      logical          :: scf
      integer          :: ie, nit, maxit
      double precision :: occup_at_munew, munew, mu1, mu2
      double precision :: tmpd(-ne:ne)
      double precision :: fermi
!----------------------------------------------------------------------
      
      ! Starting values      
      mu1 = mumin
      mu2 = mumax
      munew = 0.d0
      occup_at_munew = 0.d0
      ! Number of iteration of fermi search
      nit = 0
      maxit = 100000

      scf = .true.

      do while ( scf )
        nit = nit + 1
        ! calculate occup_at_munew in point munew = (mu2+mu1)/2
        munew = (mu2+mu1)/2.d0
        do ie=-ne, ne
           tmpd(ie) = dos(ie) * fermi( e(ie), temp, munew )
        end do
        occup_at_munew = integration_simpson( -ne, ne, e(-ne:ne), tmpd(-ne:ne) )
        if (occup_at_munew.gt.occup) then
           mu2 = munew
        else
           mu1 = munew
        endif

        if ( (dabs(occup_at_munew-occup).lt.tol) .or. (nit.eq.maxit) ) scf=.false.
      enddo ! while
      !---If Fermi level still not found      
      if (nit.eq.maxit) stop 'Error while Fermi search'

      fermisrc=munew

end function fermisrc
