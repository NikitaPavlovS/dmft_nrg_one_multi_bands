# -*- coding: utf-8 -*-
# pns 02.2018

import os
import glob
import numpy as np
from subprocess import Popen, PIPE
import re
#--------------------------------------------------------------------
#--------------------------------------------------------------------
def createFolder(path):
    if not os.path.exists(path):
        os.makedirs(path)

def copyFile(pathFrom, pathTo):
    with open(pathFrom, 'rb') as fileFrom, open(pathTo, 'wb') as fileTo:
        fileTo.write(fileFrom.read())


# input file for Tc program
def inputFile_Tc(mu, temperature, U, Delta_arg, H_fied_arg, name_input_file_Tc):
    data = '''\
          mu                      T            U (with sign)   Delta (Sigma_imp=Delta^2*Gii)     h=2*mu_B*H   number Gauss intervals
 %.16fd0   %.16fd0   %fd0          %fd0                 %fd0           1000
 ''' % ( mu, temperature, U, Delta_arg, H_fied_arg )
    with open(name_input_file_Tc, 'w') as f:
        f.write(data)


# input file for Tc program
def inputFile_mu_T_interpol(U, Delta_arg, H_field_arg, where_Tc, name_input_file_Tc):
    data = '''\
        U/2D;    Delta/2D (Sigma_imp=Delta^2*Gii);  h=2*mu_B*H;   p (number of gauss intervals); next (abTc_Tmu_interpol: 0 - Tmin<Tc<Tmax, 1 - Tc<Tmin, 2 - Tc>Tmax)
 %.16f    %.16f    %.16f       1000                          %d
 ''' % ( U, Delta_arg, H_field_arg, where_Tc )
    with open(name_input_file_Tc, 'w') as f:
        f.write(data)


# input file for Tc by dos (Kuchinsk)
def inputFile_Tc_dos_mu( mu, temperature, U, occupancy, Delta_arg, H_fied_arg, name_input_file_Tc ):
    data = '''\
       mu0                        T              U (with sign)   occupancy  number Gauss ne   Delta (Sigma_imp=Delta^2*Gii)   h=2*mu_B*H
 %.16fd0    %.16fd0   %fd0   %fd0     1000           %fd0                       %fd0
 ''' % ( mu, temperature, U, (0.5*occupancy), Delta_arg, H_fied_arg )
    with open( name_input_file_Tc, 'w' ) as f:
        f.write(data)



# input file for run DMFT(NRG) program
def inputFile_DMFT_NRG( new, temperature, mu, **params_kw):
    data = '''\
DMFT PARAMETERS:
     Value of Coloumb interaction                               |     %Fd0
     number of DMFT iterations                                  |     %d
     number of DMFT iterations after tolerance satisfaction     |     %d
     new calculation: 1 - new DMFT, 0 - start form old Sigma    |     %d
TEMPERATURE:
     temperature                                                |     %.16fd0
     tolerance of total energy convergence                      |     1.0D-3
FOR SEARCH LAMBDA:
     precision to search border of N                            |     1.0D-10
     precision to search temperature                            |     1.0D-%d
BAND:
     What is model used: Bethe (1), FCC (2), from DOS(0)        |     %d
     half bandwidth                                             |     0.5d0
     small image shift in Green function                        |     1.0D-10
     parameter of disoder: Delta                                |     0.d0
MAGNETIC PROPERTIES:
     Number of spin: 1 - paramagnetic calc, 2 - magnetic calc   |     %d
     Vaule of magnetic field in eV                              |     %fd0
NRG PARAMETERS:
     Lambda                                                     |     2.d0
     number of NRG states kept                                  |     500
     Particle Hole Symmetry                                     |     N
     Keep NRG mesh                                              |     0
     type of output                                             |     11
CHEMICAL POTENTIAL:
     Variable (T) or Const (F)                                  |     %s
     Use this value of mu for first iter (T), else mu=mu+U/2 (F)|     %s
     vaule of start Chemical Potential                          |     %.16fd0
     What is total occupancy needed in result of calculation ?  |     %fd0
     tolerance of occupancy convergence                         |     1.0D-%d
     What is value of part_of_dmu? (mu=mu+part_of_dmu*delta_mu) |     %fd0
     number of iter at change mu (2 -- even iter change mu)     |     %d
     tolerance of search mu                                     |     1.0D-12
ENERGY MESH:
     Exponential (T) or Linear (F)                              |     T
     if previous F: Maximum Energy in eV (integer value)        |     %d
     1/(step of energy mesh)                                    |     %d
     Value of minimum energy for exp meshEmin_exp_mesh          |     1.0D-4
''' % (params_kw['U'],  params_kw['it'],  params_kw['it_after_tolerance'],  new,  temperature,
       params_kw['precision_of_T'],  params_kw['band_model'],  
       params_kw['n_spin'],   params_kw['H_field'][0],   params_kw['variable_mu'],
       params_kw['start_from_read_mu'],  mu,  params_kw['occupancy'],  params_kw['occupancy_tolerance'],
       params_kw['part_of_dmu'][0],  params_kw['iter_each_change_mu'][0],  params_kw['Emax'],  params_kw['mesh'])
    with open(params_kw['name_input_file'], 'w') as f:
        f.write(data)


# script for run DMFT(NRG) program
def runer(name_script_file, prog_DMFT):
    data = '''\
%s > NRG_out 2> NRG_sys
rm -f *.sve''' % prog_DMFT
    with open(name_script_file, 'w') as f:
        f.write(data)


# calc NRG temperature by needed temperature
# if NRG temperature not found then:
# needed temperature is shifted by T_step*needed_temper
def calc_NRG_T(needed_temper, T_step, **params_kw):
    error = [0]
    temper_found = -1.0
    T_start = needed_temper
    while ( temper_found < 0.0 ):
      p = Popen('%s %d %f 1.0e-%d' %(params_kw['Prog_search_lambda'], params_kw['Emax'],
                 needed_temper, params_kw['precision_of_T']), shell=True, stdout=PIPE, stderr=PIPE)
      p.wait()

      # search T from Prog_search_lambda output data
      if ( os.path.exists( params_kw['Prog_search_lambda_out'] ) ):
        with open( params_kw['Prog_search_lambda_out'], 'r') as f:
          outfile = f.read().split('\n')
          for x in outfile:
            if x.startswith(' Lambda is found'):
              temper_found = float( re.findall('([-\s]\d+[.]\d+\S+\d+)', x)[1] )
      else:
          error[0] = 1
          break

      if ( temper_found < 0.0 ):
          needed_temper = needed_temper + T_step*T_start
    if ( os.path.exists( params_kw['Prog_search_lambda_out'] ) ):
        os.remove(params_kw['Prog_search_lambda_out'])
    return (temper_found, error[0])


# define NRG temperature by needed temperature
def define_NRG_T(needed_temper, **params_kw):
    error = [0]

    os.chdir(params_kw['root_dir'])
    temper_min, error[0] = calc_NRG_T(needed_temper, -0.001, **params_kw)
    temper_max, error[0] = calc_NRG_T(needed_temper, +0.001, **params_kw)

    if (error[0] == 0):
      # search which temperature lower or upper is closer to needed_temper
      if ( abs(temper_min - needed_temper) < abs(temper_max - needed_temper) ):
        temper_found = temper_min
      else:
        temper_found = temper_max
    else:
        temper_found = -1.0

    return( temper_found, error[0] )


# find self consistently mu
def find_results_mu_t_input(name_input_file, name_output_file):
    error = [0]
    temper_s = ['-1.0']
    mu_s = ['0.0']
    search_err = [0,0]
    if (os.path.exists(name_output_file)):
      with open(name_output_file, 'r') as f:
        outfile = f.read().split('\n')
        lines = outfile[-27:]
        for x in lines[::-1]:
          if x.startswith('      Lattice problem mu'):
            mu_s = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
      with open(name_input_file, 'r') as f:
        outfile = f.read().split('\n')
        temper_s = re.findall('([-\s]\d+[.]\d+)', outfile[6])

      # errors in search
      if (mu_s==['0.0'] or mu_s==[]):
          search_err[0] = 1
          mu_s = ['0.0']
      if (temper_s==['-1.0'] or temper_s==[]):
          search_err[1] = 1
          temper_s = ['-1.0']
    else:
         print '----- Error in find_results_mu_t. File  "%s"  does not exist -----' % name_output_file
         error[0] = 1
    if ((search_err[0]==1) or (search_err[1]==1)):
         print '----- Error in find_results_mu_t. mu or temperature not found -----'
         error[0] = 1
    return( float(temper_s[0]), float(mu_s[0]), error[0] )

 
# find self consistently occupancy
def find_results_t_mu_U_n_h_Delta(name_input_file, name_output_file):
    error = [0]
    temper_s = ['-1.0']
    mu_s = ['0.0']
    U_s = ['0.0']
    n_s = ['0.0']
    h_s = ['0.0']
    Delta_s = ['0.0']
    search_err = [ 0, 0, 0, 0, 0, 0 ]
    if (os.path.exists(name_output_file)):
      with open(name_output_file, 'r') as f:
        outfile = f.read().split('\n')
        lines = outfile[-27:]
        temper_s = re.findall('([-\s]\d+[.]\d+\S+\d+)', outfile[-8])
        for x in lines[::-1]:
          if x.startswith('     Total occupancy ='):
            n_s = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
          if x.startswith('      Lattice problem mu'):
            mu_s = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
      with open(name_input_file, 'r') as f:
        outfile = f.read().split('\n')
        U_s = re.findall('([-\s]\d+[.]\d+)', outfile[1])
        h_s = re.findall('([-\s]\d+[.]\d+)', outfile[18])
        if (h_s==[]):
            h_s = re.findall('([-\s]\d+[.])', outfile[18])
        Delta_s = re.findall('([-\s]\d+[.]\d+)', outfile[15])
        if (Delta_s==[]):
            Delta_s = re.findall('([-\s]\d+[.])', outfile[15])

      # errors in search
      if (mu_s==['0.0'] or mu_s==[]):
          search_err[0] = 1
          mu_s = ['0.0']
      if (temper_s==['-1.0'] or temper_s==[]):
          search_err[1] = 1
          temper_s = ['-1.0']
      if (n_s==['0.0'] or n_s==[]):
          search_err[2] = 1
          n_s = ['0.0']
      if (U_s==['0.0'] or U_s==[]):
          search_err[3] = 1
          U_s = ['0.0']
      if (h_s==['0.0'] or h_s==[]):
          search_err[4] = 1
          h_s = ['0.0']
      if (Delta_s==['0.0'] or Delta_s==[]):
          search_err[5] = 1
          Delta_s = ['0.0']
    else:
         print '----- Error in find_results_t_mu_U_n_h_Delta. File  "%s"  does not exist -----' % name_output_file
         error[0] = 1
    if ((search_err[0]==1) or (search_err[1]==1) or (search_err[2]==1) or (search_err[3]==1) or (search_err[4]==1) or (search_err[5]==1)):
         print '----- Error in find_results_t_mu_U_n_h_Delta. t, mu, U, n or Delta not found -----'
         error[0] = 1
    return( float(temper_s[0]), float(mu_s[0]), float(U_s[0]), float(n_s[0]), float(h_s[0]), float(Delta_s[0]), error[0] )


# extract Tc from output
def extract_Tc(name_output_file_Tc):
    error = [0]
    Tc = [-1]
    search_err = 0
    if (os.path.exists(name_output_file_Tc)):
      with open(name_output_file_Tc, 'r') as f:
        outfile = f.read().split('\n')
        Tc = re.findall('([-\s]\d+[.]\d+\S+\d+\Z)', outfile[0])
        if (Tc == [-1]):
           search_err = 1
    else:
        print '----- Error in extract_Tc. File  "%s"  does not exist -----' % name_output_file_Tc
        error[0] = 1
    if (search_err==1):
        print '----- Error in extract_Tc. Tc not found in file %s -----' % name_output_file_Tc
        error[0] = 1
    return( float(Tc[0]), error[0] )


# search critical temperature using mu and T
def search_Tc_using_muT(U_arg, temper_now, Delta_arg, H_field_arg, where_Tc, **params_kw):
    error = [0]

    # softing array of mu and T
    number_of_T_points = len(params_kw['mu_T_Tc_at_U'])
    T_mu_index = np.argsort( [ params_kw['mu_T_Tc_at_U'][i_temp][1] for i_temp in range(0,number_of_T_points) ] )
    data_b = [ [ params_kw['mu_T_Tc_at_U'][x][i] for i in range(2) ]  for x in T_mu_index ]

    # write array of mu and T to DMFT_input_mu_T.dat
    path_Tc_interpol = os.path.join( params_kw['root_dir'], 'DMFT_input_mu_T.dat' )
    with open(path_Tc_interpol, 'w') as f:
      for i_temp in range(0,number_of_T_points):
        if ( data_b[i_temp][1] > 0 ):
            f.write( '%.16f  %.16f\n' %( data_b[i_temp][0], data_b[i_temp][1] ) )

    # run program prog_Tc_interpol for interpolation Tc by mu and T
    os.chdir(params_kw['root_dir'])
    inputFile_mu_T_interpol(U=U_arg, Delta_arg=Delta_arg, H_field_arg=H_field_arg, where_Tc=where_Tc, name_input_file_Tc=params_kw['prog_Tc_interpol_input'])
    p = Popen(params_kw['prog_Tc_interpol'], shell=True, stdout=PIPE, stderr=PIPE)
    p.wait()

    # defining Tc from prog_Tc_interpol output
    prog_Tc_interpol_out = glob.glob( 'DMFT_out_mu_T_interpol_U*' )[0]
    if ( os.path.exists(prog_Tc_interpol_out) ):
      with open(prog_Tc_interpol_out, 'r') as f:
          outfile = f.read().split('\n')
          arrayList = outfile[-2].split()
          temperature = float( arrayList[2] )
    else:
        error[0] = 1
        temperature = -1.0

    return(temperature, error[0])


# Calculation of critical temperature
def calc_Tc(path, **params_kw):
    error = [0]
    Tc_found = 0
    temperature = 0.0

    print '  Calculation of Tc:'
    # search mu and temperature
    os.chdir(params_kw['root_dir'])
    os.chdir(path)
    temper_f, mu_f, U_f, n_f, H_fied_f, Delta_f, error[0] = find_results_t_mu_U_n_h_Delta(name_input_file=params_kw['name_input_file'],
                                                 name_output_file=params_kw['name_output_file'])

    if ( error[0] == 0):
      # calculation of new Tc
      inputFile_Tc( mu=mu_f, temperature=temper_f, U=U_f, Delta_arg=Delta_f,
                    H_fied_arg = H_fied_f, name_input_file_Tc=params_kw['name_input_file_Tc'] )
      p = Popen(params_kw['prog_calc_Tc'], shell=True, stdout=PIPE, stderr=PIPE)
      p.wait()

      # extract Tc from prog_calc_Tc output
      temperature, error[0] = extract_Tc(params_kw['name_output_file_Tc'])   # Tc and errors of extract Tc
      params_kw['mu_T_Tc_at_U'].append( [ mu_f, temper_f, temperature, U_f ] )

      # Can interpolation be used ?
      interpolation_use = False
      number_of_T_points = len(params_kw['mu_T_Tc_at_U'])
      if ( number_of_T_points > 1 ):
        
        for i_cycles in range(number_of_T_points):
            # if one of Tc is zero and other Tc is not zero
            if ( params_kw['mu_T_Tc_at_U'][i_cycles][2] < 1e-20 ):
              for j_cycles in range(number_of_T_points):
                if ( params_kw['mu_T_Tc_at_U'][j_cycles][2] > 1e-20 ):
                   interpolation_use = True

            if ( params_kw['mu_T_Tc_at_U'][i_cycles][2] > params_kw['mu_T_Tc_at_U'][i_cycles][1] ):
              for j_cycles in range(number_of_T_points):
                if ( (j_cycles != i_cycles) and ( params_kw['mu_T_Tc_at_U'][j_cycles][2] < params_kw['mu_T_Tc_at_U'][j_cycles][1] ) ):
                   interpolation_use = True

      # perform interpolation. Seach Tc by mu and T data
      if (interpolation_use):
          print '      !!! interpolation for Tc is using !!!'
          temperature, error[0] = search_Tc_using_muT(U_arg=U_f, temper_now=temper_f, Delta_arg=Delta_f, 
                            H_field_arg=H_fied_f, where_Tc=0, **params_kw)
      else:
        if ( temperature < 1e-20 ):
          temperature = 0.9*temper_f
          print '     !!! Critical temperature is zero !!!'

      # print results
      print '     U = %f  n = %.8f  T = %.16f  mu = %.16f   Tc = %.16f' % ( U_f, n_f, temper_f, mu_f, temperature )
      print '     New temperature is %.16f' %temperature
      if (abs(temper_f - temperature) < 0.001*temperature):
          print '+++++ Critical temperature is found with precision=0.001. Tc = %.16f' %( temperature )
          Tc_found = 1

    os.chdir(params_kw['root_dir'])
    return(temperature, mu_f, Tc_found, error[0])


# Calculation of critical temperature (Kuchinsk)
def calc_Tc_mu_KK(path, **params_kw):
    error = [0]
    Tc_found = 0
    temperature = 0.0

    print '  Calculation of Tc:'
    # search mu and temperature
    os.chdir(params_kw['root_dir'])
    os.chdir(path)
    temper_f, mu_f, U_f, n_f, H_fied_f, Delta_f, error[0] = find_results_t_mu_U_n_h_Delta(name_input_file=params_kw['name_input_file'],
                                                 name_output_file=params_kw['name_output_file'])

    if ( error[0] == 0):
      # Kuchinskii, Kuleeva mu
    #   mu_p = mu_f - 0.5 * U_f
    #   mu_KK = 2.0*mu_p + 0.5 * U_f
      mu_KK = 2.0*mu_f - 0.5 * U_f
      # calculation of new Tc
      inputFile_Tc( mu=mu_KK, temperature=temper_f, U=U_f, Delta_arg=Delta_f,
                    H_fied_arg = H_fied_f, name_input_file_Tc=params_kw['name_input_file_Tc'] )
      p = Popen(params_kw['prog_calc_Tc'], shell=True, stdout=PIPE, stderr=PIPE)
      p.wait()

      # extract Tc from prog_calc_Tc output
      temperature, error[0] = extract_Tc(params_kw['name_output_file_Tc'])   # Tc and errors of extract Tc
      params_kw['mu_T_Tc_at_U'].append( [ mu_KK, temper_f, temperature, U_f ] )

      # Can interpolation be used ?
      interpolation_use = False
      number_of_T_points = len(params_kw['mu_T_Tc_at_U'])
      if ( number_of_T_points > 1 ):
        
        for i_cycles in range(number_of_T_points):
            # if one of Tc is zero and other Tc is not zero
            if ( params_kw['mu_T_Tc_at_U'][i_cycles][2] < 1e-20 ):
              for j_cycles in range(number_of_T_points):
                if ( params_kw['mu_T_Tc_at_U'][j_cycles][2] > 1e-20 ):
                   interpolation_use = True

            if ( params_kw['mu_T_Tc_at_U'][i_cycles][2] > params_kw['mu_T_Tc_at_U'][i_cycles][1] ):
              for j_cycles in range(number_of_T_points):
                if ( (j_cycles != i_cycles) and ( params_kw['mu_T_Tc_at_U'][j_cycles][2] < params_kw['mu_T_Tc_at_U'][j_cycles][1] ) ):
                   interpolation_use = True

      # perform interpolation. Seach Tc by mu and T data
      if (interpolation_use):
          print '      !!! interpolation for Tc is using !!!'
          temperature, error[0] = search_Tc_using_muT(U_arg=U_f, temper_now=temper_f, Delta_arg=Delta_f, 
                            H_field_arg=H_fied_f, where_Tc=0, **params_kw)
      else:
        if ( temperature < 1e-20 ):
          temperature = 0.9*temper_f
          print '     !!! Critical temperature is zero !!!'

      # print results
      print '     U = %f  n = %.8f  T = %.16f  mu = %.16f   Tc = %.16f' % ( U_f, n_f, temper_f, mu_KK, temperature )
      print '     New temperature is %.16f' %temperature
      if (abs(temper_f - temperature) < 0.001*temperature):
          print '+++++ Critical temperature is found with precision=0.001. Tc = %.16f' %( temperature )
          Tc_found = 1

    os.chdir(params_kw['root_dir'])
    return(temperature, mu_f, Tc_found, error[0])


# Calculation of critical temperature (Kuchinsk)
def calc_Tc_dos_mu_KK(path, **params_kw):
    error = [0]
    Tc_found = 0
    temperature = 0.0

    print '  Calculation of Tc:'
    # search mu and temperature
    os.chdir(params_kw['root_dir'])
    os.chdir(path)
    temper_f, mu_f, U_f, n_f, H_fied_f, Delta_f, error[0] = find_results_t_mu_U_n_h_Delta(name_input_file=params_kw['name_input_file'],
                                                 name_output_file=params_kw['name_output_file'])

    if ( error[0] == 0):
      # calculation of new Tc
      inputFile_Tc_dos_mu( mu=mu_f,  temperature=temper_f,  U=U_f,  occupancy=n_f,
                Delta_arg=Delta_f,  H_fied_arg = H_fied_f,  name_input_file_Tc=params_kw['name_input_file_Tc'] )
      p = Popen(params_kw['prog_calc_Tc'], shell=True, stdout=PIPE, stderr=PIPE)
      p.wait()

      # extract Tc from prog_calc_Tc output
      temperature, error[0] = extract_Tc(params_kw['name_output_file_Tc'])   # Tc and errors of extract Tc
      params_kw['mu_T_Tc_at_U'].append( [ mu_f, temper_f, temperature, U_f ] )

      if ( temperature < 1e-20 ):
          # seach Tc by mu and T
          temperature, error[0] = search_Tc_using_muT(U_arg=U_f, temper_now=temper_f, Delta_arg=Delta_f,
                                  H_field_arg=H_fied_f, where_Tc=0, **params_kw)
          print '     Critical temperature is zero'
          print '     T is taken %.16f' % temperature
          print '     U = %f  n = %.8f  T = %.16f  mu = %.16f   Tc = 0.0' % ( U_f, n_f, temper_f, mu_f )
      else:
          print '     New temperature is %.16f' %temperature
          print '     U = %f  n = %.8f  T = %.16f  mu = %.16f   Tc = %.16f' % ( U_f, n_f, temper_f, mu_f, temperature )
          if (abs(temper_f - temperature) < 0.001*temperature):
              print '+++++ Critical temperature is found with precision=0.001. Tc = %.16f' %( temperature )
              Tc_found = 1

    os.chdir(params_kw['root_dir'])
    return(temperature, mu_f, Tc_found, error[0])


# check convergence of DMFT(NRG) calculation
def check_convergence(path_check, **params_kw):
    error = [0]
    converged=0
    search_err = 1
    t_not_found = 0

    print '  Check convergence:'
    os.chdir(params_kw['root_dir'])
    os.chdir(path_check)
    if (os.path.exists(params_kw['name_output_file'])):
      with open(params_kw['name_output_file'], 'r') as f:
        outfile = f.read().split('\n')
        for x in outfile:
          if  x.startswith('  ERROR'):
              error[0] = 1
              search_err = 0
              t_not_found = 1
              print '----- NRG temperature not found -----'
        if (error[0]==0):
          lines = outfile[-27:]
          for x in lines[::-1]:
            if x.startswith(' DMFT loop for T'):
              converged=1
            # for compare current and last calculation by occupancy_convergence
            elif x.startswith(' Occupancy convergence ='):
              tmp = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
              params_kw['occupancy_convergence'].append( float(tmp[0]) )
              search_err = 0
            # for compare current and last calculation by occupancy difference with desired occ
            elif x.startswith(' Occupancy difference with desired occ'):
              tmp = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
              params_kw['occupancy_diff_with_desired_occ'].append( float(tmp[0]) )
              search_err = 0

    else:
        print '----- Error in check_convergence. File  "%s"  does not exist -----' % params_kw['name_output_file']
        error[0] = 1
    if (search_err == 1):
        print '----- Error in check_convergence. Occupancy convergence not found in results -----'
        error[0] = 1
    os.chdir(params_kw['root_dir'])
    return(converged, error[0], t_not_found)


# new DMFT(NRG) calculation with input parameters
def DMFT_new(path_start, temperature, **params_kw):
    print '  new DMFT:'
    os.chdir(params_kw['root_dir'])
    dos_file_start = sorted(glob.glob('%s/DOS_G*' % path_start))
    if (len(dos_file_start)==0):
        print '     Start DMFT'
        createFolder(path_start)
        os.chdir(path_start)
        runer(params_kw['name_script_file'], params_kw['prog_DMFT'])
        inputFile_DMFT_NRG( new=1, temperature=temperature, mu=params_kw['mu_start'], **params_kw )
        p = Popen('bash %s'%params_kw['name_script_file'], shell=True, stdout=PIPE, stderr=PIPE)
        p.wait()
        os.chdir(params_kw['root_dir'])
    else:
        print '----- Warning in DMFT_new.  "DOS files"  exist -----'
        # search value of part_of_dmu  of previous DMFT calculation
        os.chdir(params_kw['root_dir'])
        os.chdir(path_start)
        if (os.path.exists(params_kw['name_input_file'])):
          with open(params_kw['name_input_file'], 'r') as f:
            outfile = f.read().split('\n')
            params_kw['part_of_dmu'][1] = float( re.findall('([-\s]\d+[.]\d+)', outfile[31])[0] )
            print '   part_of_dmu  from  previous DMFT calculation', params_kw['part_of_dmu'][1]
        os.chdir(params_kw['root_dir'])

        #  for part_of_dmu is using  value of part_of_dmu_start  or  value form previous DMFT calculation
        if ( params_kw['part_of_dmu_use_start'] == 'F' ):
            params_kw['part_of_dmu'][0] = params_kw['part_of_dmu'][1]
        # Change  iter_each_change_mu
        if ( (params_kw['part_of_dmu'][0] < 0.005) and (params_kw['iter_each_change_mu'][0] > 4) ):
            print '!!!! Change params[iter_each_change_mu] from %.6f to 4' %( params_kw['iter_each_change_mu'][0] )
            params_kw['iter_each_change_mu'][0] = 4


# continue DMFT(NRG) calculation with T and mu from output
def DMFT_continue(path_continue, **params_kw):
  error = [0]
  os.chdir(params_kw['root_dir'])
  if os.path.exists(path_continue):
    os.chdir(path_continue)
    dos_files = sorted(glob.glob('DOS_G*'))
    N_dos_files = len(dos_files)
    if (N_dos_files>0):
        #--------------------------------------------------------------------
        # search mu and temperature
        temper_f, mu_f, error[0] = find_results_mu_t_input(name_input_file=params_kw['name_input_file'],
                                                     name_output_file=params_kw['name_output_file'])

        #--------------------------------------------------------------------
        # save old file to new dir "path_to_old_data"
        path_to_old_data='001_%s' %N_dos_files
        if os.path.exists(path_to_old_data):
           path_exists = 1
           i=2
           while (path_exists == 1):
              if not os.path.exists(path_to_old_data+'_%s'%i):
                 path_to_old_data = path_to_old_data+'_%s'%i
                 path_exists = 0
              i=i+1
        os.makedirs(path_to_old_data)
        for name_file in params_kw['files_to_save']:
           if os.path.exists(name_file):
              copyFile(name_file,os.path.join(path_to_old_data,name_file))
              os.remove(name_file)

        copyFile(dos_files[-1],'sig.dat')
        index=0
        for files in dos_files:
            index=index+1
            if (N_dos_files-index)<5: # save only last 4 files
               copyFile( files, os.path.join(path_to_old_data,files) )
            os.remove(files)

        #--------------------------------------------------------------------
        # write new input file
        inputFile_DMFT_NRG( new=0, temperature=temper_f, mu=mu_f, **params_kw )
        runer(params_kw['name_script_file'], params_kw['prog_DMFT'])

        #--------------------------------------------------------------------
        # run new calc
        p = Popen('bash %s'%params_kw['name_script_file'], shell=True, stdout=PIPE, stderr=PIPE)
        p.wait()
        #--------------------------------------------------------------------
    else:
        print '----- Error in DMFT_continue.  "DOS files"  do not exist -----'
        error[0] = 1
    os.chdir(params_kw['root_dir'])

  return(error[0])


# continue DMFT(NRG) calculation with T and mu from output 5 iters and check convergence
def run_DMFT(path, temperature, **params_kw):
    error = [0]
    params_kw['occupancy_convergence'] = []
    params_kw['occupancy_diff_with_desired_occ'] = []
    #--------------------------------------------------------------------------------------
    print ''
    print '----------------------------------------'
    print 'Temperature is %.16f' %temperature
    DMFT_new(path, temperature, **params_kw)
    #--------------------------------------------------------------------------------------
    DMFT_conveged, error[0], t_not_found = check_convergence(path, **params_kw)
    if (t_not_found==0):
     for iter in range( 1, params_kw['iters_of_DMFT_continue'] ):
      if (error[0]==0):
        if ( DMFT_conveged == 0 ):
            print ' Start DMFT continue # %d' % iter
            error[0] = DMFT_continue(path, **params_kw)
            DMFT_conveged, error[0], t_not_found  = check_convergence(path, **params_kw)
        else:
            break

        # if in DMFT_continue occupancy convergence greater then in previous DMFT calculation
        # and occupancy difference with desired occupancy is also greater

        if ( params_kw['occupancy_convergence'][iter] > params_kw['occupancy_convergence'][iter-1] ):
          if ( params_kw['occupancy_diff_with_desired_occ'][iter] > params_kw['occupancy_diff_with_desired_occ'][iter-1] ):
            print '!!!! DMFT calculation does not conveged, occupancy convergence of DMFT cycles:'
            print ' ', params_kw['occupancy_convergence']
            print '     occupancy difference with desired occ:'
            print ' ', params_kw['occupancy_diff_with_desired_occ']
            if ( params_kw['part_of_dmu'][0] < 0.005 ):
                small_factor = 0.5
            else:
                small_factor = 0.1
            print '!!!! Change params[part_of_dmu] from %.6f to %.6f' %( params_kw['part_of_dmu'][0], small_factor * params_kw['part_of_dmu'][0] )
            params_kw['part_of_dmu'][0] = small_factor * params_kw['part_of_dmu'][0]

            # Change  iter_each_change_mu
            if ( (params_kw['part_of_dmu'][0] < 0.005) and (params_kw['iter_each_change_mu'][0] > 4) ):
                print '!!!! Change params[iter_each_change_mu] from %.6f to 4' %( params_kw['iter_each_change_mu'][0] )
                params_kw['iter_each_change_mu'][0] = 4

     if ( (DMFT_conveged==1) ):
        print '+++++ DMFT is conveged for T = %.16f'%temperature
     else:
        print '----- DMFT calculation does not conveged for T = %.16f'%temperature

    return( DMFT_conveged, error[0], t_not_found )




# main function which calling external
# for DMFT calculation of Bethe and search temperature of surperconductivity
def DMFT_NRG_T_cycle(T_array, **params_kw):
    params_kw['root_dir'] = os.getcwd()
    temperature_now = temperature_start
    errors = [0]

    for T in T_array:
        params_kw['T'] = T
        params_kw['part_of_dmu'] = [ params_kw['part_of_dmu_start'], 0.0 ]
        print '=================================================='
        print '=====     START DMFT calculation for T = %f' % params_kw['T']
        errors[0] = 0

        #--------------------------------------------------------------------------------------
        # calc NRG temperature by needed temperature
        params_kw['T'], errors[0] = define_NRG_T(params_kw['T'], **params_kw)
        if (errors[0]==0):
            # start DMFT convergence
            path_now = 'D%s_U%s_n%s_H%s_T%s' % ( params_kw['Delta'], params_kw['U'], params_kw['occupancy'], params_kw['H_field'], params_kw['T'] )
            DMFT_conveged, errors[0], t_not_found = run_DMFT( path_now, params_kw['T'], **params_kw )
        print ''
        #--------------------------------------------------------------------------------------
        # print results
        if ( errors[0]==0 ):
            os.chdir(params_kw['root_dir'])
            os.chdir(path_now)
            # print data (critical temperature and other to display)
            temper_f, mu_f, errors[0] = find_results_mu_t_input(name_input_file=params_kw['name_input_file'],name_output_file=params_kw['name_output_file'])
            os.chdir(params_kw['root_dir'])
            print '+++++ for U = %f.  Is Tc found ? Answer: %d.  mu = %.16f  Tc = %.16f' % ( params_kw['U'], Tc_found, mu_f, temperature_now )

        if (errors[0]!=0):
            print '    ERRORS for T = %f' % params_kw['T']
        print ''
        print '======     END calculation for T = %f' % params_kw['T']
        print '=================================================='
        print ''
        print ''






# main function which calling external
# for DMFT calculation of Bethe and search temperature of surperconductivity
def DMFT_Bethe_search_Tc(U_start, U_end, dU, temperature_start, **params_kw):
    params_kw['root_dir'] = os.getcwd()
    temperature_now = temperature_start
    errors = [0]

    U_array = np.arange(U_start, U_end, dU)
    for U in U_array:
        params_kw['U'] = U
        params_kw['mu_T_Tc_at_U'] = []
        params_kw['part_of_dmu'] = [ params_kw['part_of_dmu_start'], 0.0 ]
        print '=================================================='
        print '=====     START DMFT calculation for U = %f' % params_kw['U']
        errors[0] = 0

        #--------------------------------------------------------------------------------------
        # calc NRG temperature by needed temperature
        temperature_now, errors[0] = define_NRG_T(temperature_now, **params_kw)
        if (errors[0]==0):
            # start DMFT convergence
            path_now = 'D%s_U%s_n%s_H%s_T%s' % ( params_kw['Delta'], params_kw['U'], params_kw['occupancy'], params_kw['H_field'], temperature_now )
            DMFT_conveged, errors[0], t_not_found = run_DMFT( path_now, temperature_now, **params_kw )
        if (errors[0]==0):
            # calc new T
            temperature_now, params_kw['mu_start'], Tc_found, errors[0] = calc_Tc( path_now, **params_kw )
        print ''
        #--------------------------------------------------------------------------------------
        if ( errors[0]==0 ):
          if ( (t_not_found==0) and (Tc_found==0) ):
            # Start temperature cycle
            Tc_found = 0
            if ( (DMFT_conveged==1) and (errors[0]==0) ):
              DMFT_conveged_T = 1
              for T_iter in range( 2, params_kw['iters_of_temperature_cycle'] + 1 ):
                params_kw['part_of_dmu'][0] = params_kw['part_of_dmu_start']
                if ( (DMFT_conveged_T==1) and (errors[0]==0) and (Tc_found==0) ):
                  # calc NRG temperature by needed temperature
                  temperature_now, errors[0] = define_NRG_T(temperature_now, **params_kw)
                  # start DMFT convergence
                  path_now = 'D%s_U%s_n%s_H%s_T%s' % ( params_kw['Delta'], params_kw['U'], params_kw['occupancy'], params_kw['H_field'], temperature_now )
                  DMFT_conveged_T, errors[0], t_not_found = run_DMFT( path_now, temperature_now, **params_kw )
                  if (errors[0]==0):
                      # calc new T
                      temperature_now, params_kw['mu_start'], Tc_found, errors[0] = calc_Tc( path_now, **params_kw )
                      print ''

          if (errors[0]==0):
            os.chdir(params_kw['root_dir'])
            os.chdir(path_now)
            # print data (critical temperature and other to display)
            temper_f, mu_f, errors[0] = find_results_mu_t_input(name_input_file=params_kw['name_input_file'],name_output_file=params_kw['name_output_file'])
            os.chdir(params_kw['root_dir'])
            print '+++++ for U = %f.  Is Tc found ? Answer: %d.  mu = %.16f  Tc = %.16f' % ( params_kw['U'], Tc_found, mu_f, temperature_now )
            # print results  mu  T  Tc
            print 'List of     mu                 T                    Tc             U:'
            number_of_T_points = len(params_kw['mu_T_Tc_at_U'])
            # softing array of mu and T
            T_mu_index = np.argsort( [ params_kw['mu_T_Tc_at_U'][i_temp][1] for i_temp in range(0,number_of_T_points) ] )
            data_b = [ [ params_kw['mu_T_Tc_at_U'][x][i] for i in range(4) ]  for x in T_mu_index ]
            for i_temp in range(0,number_of_T_points):
                print( ' %.16f   %.16f   %.16f   %.2f\n' %( data_b[i_temp][0], data_b[i_temp][1], data_b[i_temp][2], data_b[i_temp][3] ) )

        if (errors[0]!=0):
            print '    ERRORS for U = %f' % params_kw['U']
        print ''
        print '======     END calculation for U = %f' % params_kw['U']
        print '=================================================='
        print ''
        print ''


# main function which calling external
# Temperature of surperconductivity is calculated for results in all directory. Strart from root dir.
def calc_Tc_all_dirs_in_root( **params_kw ):
    params_kw['root_dir'] = os.getcwd()
    errors = [0]
    #--------------------------------------------------------------------
    # search dirs
    paths_in_dir = os.listdir(params_kw['root_dir'])
    path_to_dirs = []
    for path in paths_in_dir:
       if os.path.isdir(path):
          path_to_dirs.append(path)
    #--------------------------------------------------------------------
    params_kw['mu_T_Tc_at_U'] = []
    for path in sorted(path_to_dirs):
        print ''
        print 'path %s' % path
        temperature_now, mu_start, Tc_found, errors[0] = calc_Tc( path, **params_kw )

    #--------------------------------------------------------------------
    # print results  mu  T  Tc  U  to file and display
     # softing array of mu and T
    number_of_T_points = len(params_kw['mu_T_Tc_at_U'])
    T_mu_index = np.argsort( [ params_kw['mu_T_Tc_at_U'][i_temp][1] for i_temp in range(0,number_of_T_points) ] )
    data_b = [ [ params_kw['mu_T_Tc_at_U'][x][i] for i in range(4) ]  for x in T_mu_index ]
    #data_b = params_kw['mu_T_Tc_at_U']
    for i_temp in range(0,number_of_T_points):
        file_name_mu_T_Tc = 'DMFT_mu_T_Tc_U%s.dat' % data_b[i_temp][3]
        with open( os.path.join( params_kw['root_dir'], file_name_mu_T_Tc ), 'a') as f:
             f.write( ' %.16f   %.16f   %.16f   %.2f\n' % ( data_b[i_temp][0], data_b[i_temp][1], data_b[i_temp][2], data_b[i_temp][3] ) )










#=========================================================
#======== Kuchinsk mu ====================================
#=========================================================

# main function which calling external
# for DMFT calculation of Bethe and search temperature of surperconductivity
def DMFT_Bethe_search_Tc_mu_KK(U_start, U_end, dU, temperature_start, **params_kw):
    params_kw['root_dir'] = os.getcwd()
    temperature_now = temperature_start
    errors = [0]

    U_array = np.arange(U_start, U_end, dU)
    for U in U_array:
        params_kw['U'] = U
        params_kw['mu_T_Tc_at_U'] = []
        params_kw['part_of_dmu'] = [ params_kw['part_of_dmu_start'], 0.0 ]
        print '=================================================='
        print '=====     START DMFT calculation for U = %f' % params_kw['U']
        errors[0] = 0

        #--------------------------------------------------------------------------------------
        # calc NRG temperature by needed temperature
        temperature_now, errors[0] = define_NRG_T(temperature_now, **params_kw)
        if (errors[0]==0):
            # start DMFT convergence
            path_now = 'D%s_U%s_n%s_H%s_T%s' % ( params_kw['Delta'], params_kw['U'], params_kw['occupancy'], params_kw['H_field'], temperature_now )
            DMFT_conveged, errors[0], t_not_found = run_DMFT( path_now, temperature_now, **params_kw )
        if (errors[0]==0):
            # calc new T
            temperature_now, params_kw['mu_start'], Tc_found, errors[0] = calc_Tc_mu_KK( path_now, **params_kw )
        print ''
        #--------------------------------------------------------------------------------------
        if ( errors[0]==0 ):
         if ( (t_not_found==0) and (Tc_found==0) ):
          # Start temperature cycle
          Tc_found = 0
          if ( (DMFT_conveged==1) and (errors[0]==0) ):
            DMFT_conveged_T = 1
            for T_iter in range( 2, params_kw['iters_of_temperature_cycle'] + 1 ):
              params_kw['part_of_dmu'][0] = params_kw['part_of_dmu_start']
              if ( (DMFT_conveged_T==1) and (errors[0]==0) and (Tc_found==0) ):
                  # calc NRG temperature by needed temperature
                  temperature_now, errors[0] = define_NRG_T(temperature_now, **params_kw)
                  # start DMFT convergence
                  path_now = 'D%s_U%s_n%s_H%s_T%s' % ( params_kw['Delta'], params_kw['U'], params_kw['occupancy'], params_kw['H_field'], temperature_now )
                  DMFT_conveged_T, errors[0], t_not_found = run_DMFT( path_now, temperature_now, **params_kw )
                  if (errors[0]==0):
                      # calc new T
                      temperature_now, params_kw['mu_start'], Tc_found, errors[0] = calc_Tc_mu_KK( path_now, **params_kw )
                      print ''

         if (errors[0]==0):
            os.chdir(params_kw['root_dir'])
            os.chdir(path_now)
            # print data (critical temperature and other to display)
            temper_f, mu_f, errors[0] = find_results_mu_t_input(name_input_file=params_kw['name_input_file'],name_output_file=params_kw['name_output_file'])
            os.chdir(params_kw['root_dir'])
            print '+++++ for U = %f.  Is Tc found ? Answer: %d.  mu = %.16f  Tc = %.16f' % ( params_kw['U'], Tc_found, mu_f, temperature_now )
            # print results  mu  T  Tc
            print 'List of     mu                 T                    Tc             U:'
            number_of_T_points = len(params_kw['mu_T_Tc_at_U'])
            # softing array of mu and T
            T_mu_index = np.argsort( [ params_kw['mu_T_Tc_at_U'][i_temp][1] for i_temp in range(0,number_of_T_points) ] )
            data_b = [ [ params_kw['mu_T_Tc_at_U'][x][i] for i in range(4) ]  for x in T_mu_index ]
            for i_temp in range(0,number_of_T_points):
                print( ' %.16f   %.16f   %.16f   %.2f\n' %( data_b[i_temp][0], data_b[i_temp][1], data_b[i_temp][2], data_b[i_temp][3] ) )

        if (errors[0]!=0):
            print '    ERRORS for U = %f' % params_kw['U']
        print ''
        print '======     END calculation for U = %f' % params_kw['U']
        print '=================================================='
        print ''
        print ''


# main function which calling external
# Temperature of surperconductivity is calculated for results in all directory. Strart from root dir.
def calc_Tc_all_dirs_in_root_mu_KK( **params_kw ):
    params_kw['root_dir'] = os.getcwd()
    errors = [0]
    #--------------------------------------------------------------------
    # search dirs
    paths_in_dir = os.listdir(params_kw['root_dir'])
    path_to_dirs = []
    for path in paths_in_dir:
       if os.path.isdir(path):
          path_to_dirs.append(path)
    #--------------------------------------------------------------------
    params_kw['mu_T_Tc_at_U'] = []
    for path in sorted(path_to_dirs):
        print ''
        print 'path %s' % path
        temperature_now, mu_start, Tc_found, errors[0] = calc_Tc_mu_KK( path, **params_kw )

    #--------------------------------------------------------------------
    # print results  mu  T  Tc  U  to file and display
     # softing array of mu and T
    number_of_T_points = len(params_kw['mu_T_Tc_at_U'])
    T_mu_index = np.argsort( [ params_kw['mu_T_Tc_at_U'][i_temp][1] for i_temp in range(0,number_of_T_points) ] )
    data_b = [ [ params_kw['mu_T_Tc_at_U'][x][i] for i in range(4) ]  for x in T_mu_index ]
    #data_b = params_kw['mu_T_Tc_at_U']
    for i_temp in range(0,number_of_T_points):
        file_name_mu_T_Tc = 'DMFT_mu_T_Tc_U%s.dat' % data_b[i_temp][3]
        with open( os.path.join( params_kw['root_dir'], file_name_mu_T_Tc ), 'a') as f:
             f.write( ' %.16f   %.16f   %.16f   %.2f\n' % ( data_b[i_temp][0], data_b[i_temp][1], data_b[i_temp][2], data_b[i_temp][3] ) )