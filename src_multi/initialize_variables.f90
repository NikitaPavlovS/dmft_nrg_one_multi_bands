subroutine initialize_variables

  use energy_module
  use hamiltonian_module_nband
  use parameters_module
  use impurity_module

  implicit none

double precision, allocatable :: temp_array(:)
integer                       :: nmax_temp
double precision              :: tmpreal
double precision              :: point_double_1,point_double_2
double precision              :: tmp_Im_Sigma(1:nspin), tmp_Re_Sigma(1:nspin)
integer                       :: i, is

!=========================================================================================================================================
nmax_temp = 1000000
allocate(temp_array(-nmax_temp:nmax_temp))

Call Energy_mesh(temp_array, nmax_temp)

allocate(om(-nmax:nmax))
DO i=-nmax,nmax
   om(i) = temp_array(i)
END DO

deallocate(temp_array)

!------------------------------------------------------------------------------------------------------------------------------------------
allocate(dos(1:nspin,-nmax:nmax))
allocate(totalDOS(1:nspin,-nmax:nmax))
allocate(sig(1:nspin,-nmax:nmax))
allocate(dd(1:nspin,-nmax:nmax))
point_double_1 = 0.d0
point_double_2 = 0.d0

!=========================================================================================================================================
! First call to NRG with iflag<0. Inititialize interal parameters like scale,
! actual temperature, etc. Will return the actually used t.
SELECT CASE (nspin)
CASE (1)
   !Call nrg(P,SU2_sim,0.d0,u,t,om,dd(1,:),occ,sig(1,:),nmax,lam,nkeep,-1,iover)
   Call nrg_nspin(P,SU2_sim,H_field,0.d0,u,t,om,nspin,dd(1,:),point_double_1,occ,sig(1,:),point_double_2,nmax,lam,nkeep,-1,iover)
CASE (2)
   Call nrg_nspin(P,SU2_sim,H_field,0.d0,u,t,om,nspin,dd(1,:),dd(2,:),occ,sig(1,:),sig(2,:),nmax,lam,nkeep,-1,iover)
   !Call nrg_2spin(P,SU2_sim,H_field,0.d0,u,t,om,dd(1,:),dd(2,:),occ,sig(1,:),sig(2,:),nmax,lam,nkeep,-1,iover)
       !nrg_2spin(P,SU2_sim,H_field,mu,u,t,om,dd(1,:),dd(2,:),occ,sig(1,:),sig(2,:),nmax,lam,nkeep,iflag,iover)
CASE DEFAULT
     stop 'error in spin value'
END SELECT
write(*,*)'Actual temperature is ',t
!stop 'initial NRG run'

!------------------------------------------------------------------------------------------------------------------------------------------
! Initialize DMFT parameters
dd = 1E-3
!-----------------------------------------------------------------------
!define delta in Green function
!-----------------------------------------------------------------------
! IF ((variable_mu.and.(power_of_delta>3)).and.(start_iter<10).and.(start_iter>0)) THEN
!    value_of_delta(:)     = 0
!    value_of_delta_for_FS = 0
! ELSE
!    value_of_delta(:)     = power_of_delta
!    value_of_delta_for_FS = power_of_delta
! END IF

!------------------------------------------------------------------------------------------------------------------------------------------
! IF need calculate or continue calculation with starting Sigma for Green function
! ESLE Im(Sigma)=-1E-2 and to Re(Sigma) write double counting Edc, defining one of this calculation method
!!!!!!!!!! NEED ADD CASE nmax<nmax_init
IF (INIT_Sigma) THEN
   open(10,file='Start_sigma.dat',form='FORMATTED')
!-----------------------------------
   IF (nmax_init==nmax) THEN
      do i=-nmax_init,nmax_init
         read(10,*) tmpreal,   ( (tmp_Re_Sigma(is), tmp_Im_Sigma(is)), is=1,nspin )
         do is=1, nspin 
            sig(is,i) = dcmplx( tmp_Re_Sigma(is), tmp_Im_Sigma(is) )
         end do
      end do
!-----------------------------------
   ELSE
    IF (nmax_init<nmax) THEN ! write missing value for sig array (analogy if no INIT_Sigma)
      do i=-nmax_init,nmax_init
         read(10,*) tmpreal,   ( (tmp_Re_Sigma(is), tmp_Im_Sigma(is)), is=1,nspin )
         do is=1, nspin 
            sig(is,i) = dcmplx( tmp_Re_Sigma(is), tmp_Im_Sigma(is) )
         end do
      end do
      IF (start_Edc) THEN
         do i=-nmax,-(nmax_init+1)
            sig(:,i) = dcmplx(-start_Edc_value,-delta)
            sig(:,-i) = dcmplx(-start_Edc_value,-delta)
         end do
      ELSE
         IF (Edc_const) THEN
            do i=-nmax,-(nmax_init+1)
               sig(:,i) = dcmplx(-Edc,-delta)
               sig(:,-i) = dcmplx(-Edc,-delta)
            end do
         ELSE
!             IF (hole_occupancy) THEN
!                 do i=-nmax,-(nmax_init+1)
!                   sig(:,i) = dcmplx(  u*(n_d0 - 1.d0/2.d0),-delta) !napisano s usloviem, 4to duro4noe zapolnenie
!                   sig(:,-i) = dcmplx(  u*(n_d0 - 1.d0/2.d0),-delta)
!                 end do
!             ELSE
!                 do i=-nmax,-(nmax_init+1)
!                   sig(:,i) = dcmplx( -u*(n_d0 - 1.d0/2.d0),-delta)
!                   sig(:,-i) = dcmplx( -u*(n_d0 - 1.d0/2.d0),-delta)
!                 end do
!             END IF
         END IF
      END IF
!-----------------------------------
    ELSE  ! nmax_init>nmax
      do i=-nmax_init,-(nmax+1)
         read(10,*)
      end do
      do i=-nmax,nmax
         read(10,*) tmpreal,   ( (tmp_Re_Sigma(is), tmp_Im_Sigma(is)), is=1,nspin )
         do is=1, nspin 
            sig(is,i) = dcmplx( tmp_Re_Sigma(is), tmp_Im_Sigma(is) )
         end do
      end do
!-----------------------------------
    END IF
   END IF
   close(10)
ELSE
!-------------------------------------------------------------------------
  IF (bare_DOS) THEN
     sig = dcmplx(0.d0,-delta)
  ELSE
      IF (start_Edc) THEN
        ! If need start with given value
         sig = dcmplx(-start_Edc_value,-delta)
      ELSE
         IF (Edc_const) THEN
            ! If calculate with given const value
            sig = dcmplx(-Edc,-delta)
         ELSE
!             IF (hole_occupancy) THEN
!                sig = dcmplx(  u*(n_d0 - 1.d0/2.d0),-delta) !napisano s usloviem, 4to duro4noe zapolnenie
!             ELSE
!                sig = dcmplx( -u*(n_d0 - 1.d0/2.d0),-delta) 
!             END IF
         END IF
      END IF
   END IF
END IF

!----------------------------------------------------
! If need continue calculation with same parameters using already calculating DOS
IF (INIT_dd) THEN
   open(10,file='Start_dd.dat',form='FORMATTED')
   IF (nmax_init==nmax) THEN
      do i=-nmax_init,nmax_init
         read(10,*) tmpreal, (dd(is,i),is=1,nspin), (totalDOS(is,i),is=1,nspin)
      end do
   ELSE
    IF (nmax_init<nmax) THEN
      do i=-nmax_init,nmax_init
         read(10,*) tmpreal, (dd(is,i),is=1,nspin), (totalDOS(is,i),is=1,nspin)
      end do
    ELSE  ! nmax_init>nmax
      do i=-nmax_init,-(nmax+1)
         read(10,*)
      end do
      do i=-nmax,nmax
         read(10,*) tmpreal, (dd(is,i),is=1,nspin), (totalDOS(is,i),is=1,nspin)
      end do
    END IF
   END IF
   close(10)
ELSE
   dos = 0.d0
   totalDOS = 0.d0
END IF

!stop 'stop INIT'

end subroutine initialize_variables