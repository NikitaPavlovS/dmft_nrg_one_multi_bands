subroutine read_input_1
  use module_energy
  use module_impurity_1
  use module_parameters
  use module_nrg_variable
implicit none

!----------------------------------------------------------------------------------------------
! needed temporary variables
character*64  :: tmpstring
character*100 :: char_str
integer       :: ios
!----------------------------------------------------------------------------------------------
! READ input DMFT(NRG) parameters
open(10, file='DMFT_INPUT_1_band', form='FORMATTED', iostat=ios, status='old', action='read', position='rewind')
if (ios/=0) then
   stop '   !!! Program stoped. Input file "DMFT_INPUT_1_band" is needed'
end if

write(output_file, '(A75)')'============================ INPUT PARAMETERS ============================'
!DMFT PARAMETERS!--------------------------------------------------------------------------------------------------
read(10, '(A50)')tmpstring
write(output_file,*)tmpstring
read(10,'(A70,D22.15)') char_str, u                 !     Value of Coloumb interaction
write(output_file,'(A70,f10.5)') char_str, u
read(10,'(A70,I3)') char_str, NDMFT                 !     number of DMFT iterations
write(output_file,'(A70,I3)') char_str, NDMFT
read(10,'(A70,I3)') char_str, N_iter_for_converged  !     number of DMFT iterations after tolerance satisfaction
write(output_file,'(A70,I3)') char_str, N_iter_for_converged
read(10,'(A70,I1)') char_str, new_calc              !     "new_calc: 1 - start new DMFT, 0 - read old Sigma"
write(output_file,'(A70,I1)') char_str, new_calc
!TEMPERATURE!------------------------------------------------------------------------------------------------------
read(10,'(A50)')tmpstring
write(output_file,*)tmpstring
read(10,'(A70,D22.15)') char_str, t                 !    temperature
write(output_file,'(A70,E12.5)') char_str, t
read(10,'(A70,D22.15)') char_str, total_energy_tolerance !    Tolerance of total energy convergence
write(output_file,'(A70,E12.5)') char_str, total_energy_tolerance
!FOR SEARCH LAMBDA:
read(10,'(A50)')tmpstring
write(output_file,*)tmpstring
read(10,'(A70,D22.15)') char_str, precision_of_N    !     precision to search border of N  (good value in impurity module)
write(output_file,'(A70,E12.5)') char_str, precision_of_N
read(10,'(A70,D22.15)') char_str, precision_of_T    !     precision to search temperature
write(output_file,'(A70,E12.5)') char_str, precision_of_T
!BAND info!--------------------------------------------------------------------------------------------------
read(10,'(A50)') tmpstring
write(output_file,*) tmpstring
read(10,'(A70,I1)') char_str, model                 !     What is model used: Bethe (1), FCC (2), from DOS(0) ?
write(output_file,'(A70,I1)') char_str, model
read(10,'(A70,D22.15)') char_str, D                 !     half band width
write(output_file,'(A70,f10.5)') char_str, D
read(10,'(A70,D22.15)') char_str, delta             !     small image shift in Green function
write(output_file,'(A70,E12.5)') char_str, delta
read(10,'(A70,D22.15)') char_str, imp               !     disoder
write(output_file,'(A70,E12.5)') char_str, imp
!MAGNETIC PROPERTIES!----------------------------------------------------------------------------------------------
read(10,'(A50)') tmpstring
write(output_file,*) tmpstring
read(10,'(A70,I1)') char_str,  nspin                !     1 - paramagnetic calc, 2 - magnetic calc
write(output_file,'(A70,I1)') char_str,  nspin
read(10,'(A70,D22.15)') char_str, H_field           !     Magnetic field (in eV)
write(output_file,'(A70,E12.5)') char_str, H_field
!NRG PARAMETERS!---------------------------------------------------------------------------------------------------
read(10,'(A50)') tmpstring
write(output_file,*) tmpstring
read(10,'(A70,D22.15)') char_str, lam               !     Lambda
write(output_file,'(A70,f8.5)') char_str, lam
read(10,'(A70,I6)') char_str, nkeep                 !     number of NRG states kept
write(output_file,'(A70,I6)') char_str, nkeep
read(10,'(A70,A1)') char_str, P                     !     Particle Hole Symmetry
write(output_file,'(A70,A1)') char_str, P
read(10,'(A70,I6)') char_str, iover                 !     Keep NRG mesh
write(output_file,'(A70,I6)') char_str, iover
read(10,'(A70,I3)') char_str, iflag                 !     type of output
write(output_file,'(A70,I3)') char_str, iflag
!CHEMICAL POTENTIAL!-----------------------------------------------------------------------------------------------
read(10,'(A50)') tmpstring
write(output_file,*) tmpstring
read(10,'(A70,L1)') char_str, variable_mu           !     Variable (search mu for n_total_need) (T) or Const (F) 
write(output_file,'(A70,L1)') char_str, variable_mu
read(10,'(A70,L1)') char_str, start_from_read_mu    !     start from read mu in this file:   yes (T), not (F)
write(output_file,'(A70,L1)') char_str, start_from_read_mu
read(10,'(A70,D22.15)') char_str, mu                !     vaule of start Chemical Potential
write(output_file,'(A70,D22.15)') char_str, mu
read(10,'(A70,D22.15)') char_str, n_total_need      !     What total occupancy need in result of calculation
write(output_file,'(A70,f9.5)') char_str, n_total_need 
read(10,'(A70,D22.15)') char_str,occupancy_tolerance!     Tolerance of occupancy convergence
write(output_file,'(A70,E12.5)') char_str,occupancy_tolerance
read(10,'(A70,D22.15)') char_str, part_of_dmu        !     mu = mu + part_of_dmu * delta_mu
write(output_file,'(A70,E12.5)') char_str, part_of_dmu
read(10,'(A70,I3)') char_str, iter_each_change_mu   !     number of iter at change mu (2 -- even iter chenge mu)
write(output_file,'(A70,I3)') char_str, iter_each_change_mu
read(10,'(A70,D22.15)') char_str, mu_tolerance      !     Tolerance of mu searching
write(output_file,'(A70,E12.5)') char_str, mu_tolerance
! READ ENERGY MESH!------------------------------------------------------------------------------------------------
read(10,'(A50)') tmpstring
write(output_file,*) tmpstring
read(10,'(A70,L1)') char_str, exp_mesh              !     Exponential (T) or Linear (F) mesh
write(output_file,'(A70,L1)') char_str, exp_mesh
read(10,'(A70,I2)') char_str, Emax                  !     Maximum Energy in eV (integer value)
write(output_file,'(A70,I2)') char_str, Emax
read(10,'(A70,I6)') char_str, factor_of_step        !     1/(energy step) (nmax=Emax*factor_of_step)
write(output_file,'(A70,I6)') char_str, factor_of_step
read(10,'(A70,D22.15)') char_str, Emin_exp_mesh     !     Value of minimum energy for exp mesh
write(output_file,'(A70,E12.5)') char_str, Emin_exp_mesh
close(10)

write(output_file,'(A75)')'=========================================================================='
write(output_file,*)

end subroutine read_input_1
