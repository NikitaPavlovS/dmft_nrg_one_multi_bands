Subroutine GreenFunction_of_E_Sigma_mu_fraction_noPG( E, Sigma, mu, Integ_G)

  use hamiltonian_module_Nband
  use parameters_module
  use Gaussian_points_module

implicit none
!=======================================================================
! Green function variables
      double precision,  intent(in) :: E      ! value of energy point
      double precision,  intent(in) :: mu
      complex*16,        intent(in) :: Sigma(1:nspin,1:ndim)
      complex*16,       intent(out) :: Integ_G(1:nspin,0:ndim)  ! Green function of E integrated over k for d, p1, p2, and all
!=======================================================================
! LOCAL variables
! Hamilton
complex*16,       allocatable :: Hamilton(:,:)
! Green function
complex*16,       allocatable :: GreenF(:,:,:)
complex*16,       allocatable :: Gk(:,:,:,:)        ! integral of first index matrix element of Grean function
! k points array
double precision, allocatable :: kx(:)
double precision, allocatable :: ky(:)
! needed temporary variables
complex*16       :: gs(1:nspin,1:ndim)
integer          :: ix, iy
double precision :: dx
integer          :: is, i
integer          :: kpoints
!=======================================================================
double precision, allocatable :: xs, ys, xm, ym
integer,          allocatable :: lx, ly, jx, jy, jy2
complex*16,       allocatable :: gs1(:,:), gs2(:,:)

kpoints = 2*ni*ngp
ALLOCATE(kx(kpoints))
ALLOCATE(ky(kpoints))
ALLOCATE(Hamilton( 1:ndim, 1:ndim))
ALLOCATE(GreenF( 1:nspin, 1:ndim, 1:ndim))
ALLOCATE(Gk( 1:nspin, 1:ndim, kpoints, kpoints))


!==========================================================================================
IF (linear_integration) THEN
   !dx = 2.d0*pi/kpoints
   dx = pi/kpoints
   DO ix = 1,kpoints
      kx(ix) = -pi+dx*ix
      DO iy = 1,kpoints
         ky(iy) = -pi+dx*iy
         CALL Hamilton_multi_band_in_kx_ky( kx(ix), ky(iy), Hamilton)
         !-------------------------------------------------
         DO is=1, nspin
            CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, Sigma(is,:), GreenF(is,:,:))
            DO i=1, ndim
               Gk(is,i,ix,iy) = GreenF(is,i,i)
            ENDDO
         END DO
      ENDDO
   ENDDO
   !=======integrate  Green function of E(Emin, Emax) po zone========================
   gs=(0.d0,0.d0)
   DO ix = 1,kpoints
      DO iy = 1,kpoints
         DO is=1, nspin
           DO i=1, ndim
              gs(is,i)= gs(is,i) + Gk(is,i,ix,iy)
           ENDDO
         ENDDO
      ENDDO
   ENDDO
   Integ_G(:,0)= (0.d0,0.d0)
   DO is=1, nspin
    DO i=1, ndim
       Integ_G(is,i) = gs(is,i)/(kpoints*kpoints)
       Integ_G(is,0) = Integ_G(is,0) + Integ_G(is,i)
    ENDDO
   END DO
!==========================================================================================
ELSE ! Gausian integration
!==========================================================================================
Call Gaussian_points_and_weights
allocate(xs, ys, xm, ym)
allocate(lx, ly, jx, jy, jy2)
allocate(gs1(1:nspin,1:ndim), gs2(1:nspin,1:ndim))
!-------------------------------------------------
   !dx = 2.d0*pi/ni/2
    dx = pi/ni/2
!   xs = -dx! - pi
   xs = -dx - pi
   Integ_G=(0.d0,0.d0)
!stop 'stop in GreenFunction_of_E_Sigma_mu_fraction_noPG start'
   DO lx = 0,(ni-1)
      xs = xs+2*dx
      DO ix = 1,ngp
         xm = dx*x(ix)
!-------------------------------------------------
         !jx = lx*ngp+1-ix
         jx = (2*lx+1)*ngp+1-ix
         kx(jx) = xs-xm

         ys = -dx - pi
         DO ly = 0,(ni-1)
            ys = ys+2*dx
            DO iy = 1,ngp
               ym = dx*x(iy)
               !jy = ly*ngp+1-iy
               jy = (2*ly+1)*ngp+1-iy
               ky(jy) = ys-ym
               !-------------------------------------------------
               CALL Hamilton_multi_band_in_kx_ky( kx(jx), ky(jy), Hamilton)
               !-------------------------------------------------
               DO is=1, nspin
                  CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, Sigma(is,:), GreenF(is,:,:))
                  DO i=1, ndim
                     Gk(is,i,jx,jy) = GreenF(is,i,i)
                  ENDDO
               END DO
               !-------------------------------------------------

               !jy = ly*ngp+iy
               jy = (2*ly+1)*ngp+iy
               ky(jy) = ys+ym
               !-------------------------------------------------
               CALL Hamilton_multi_band_in_kx_ky( kx(jx), ky(jy), Hamilton)
               !-------------------------------------------------
               DO is=1, nspin
                  CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, Sigma(is,:), GreenF(is,:,:))
                  DO i=1, ndim
                     Gk(is,i,jx,jy) = GreenF(is,i,i)
                  ENDDO
               END DO
               !-------------------------------------------------
            ENDDO ! iy
         ENDDO ! ly
!-------------------------------------------------
         !jx = lx*ngp+ix
         jx = (2*lx+1)*ngp+ix
         kx(jx) = xs+xm

         ys = -dx !- pi
         DO ly = 0,(ni-1)
            ys = ys+2*dx
            DO iy = 1,ngp
               ym = dx*x(iy)
               jy = (2*ly+1)*ngp+1-iy
               ky(jy) = ys-ym
               !-------------------------------------------------
               CALL Hamilton_multi_band_in_kx_ky( kx(jx), ky(jy), Hamilton)
               !-------------------------------------------------
               DO is=1, nspin
                  CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, Sigma(is,:), GreenF(is,:,:))
                  DO i=1, ndim
                     Gk(is,i,jx,jy) = GreenF(is,i,i)
                  ENDDO
               END DO
               !-------------------------------------------------

               jy = (2*ly+1)*ngp+iy
               ky(jy) = ys+ym
               !-------------------------------------------------
               CALL Hamilton_multi_band_in_kx_ky( kx(jx), ky(jy), Hamilton)
               !-------------------------------------------------
               DO is=1, nspin
                  CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, Sigma(is,:), GreenF(is,:,:))
                  DO i=1, ndim
                     Gk(is,i,jx,jy) = GreenF(is,i,i)
                  ENDDO
               END DO
               !-------------------------------------------------
            ENDDO ! iy
         ENDDO ! ly
!-------------------------------------------------
      ENDDO ! ix
   ENDDO ! lx
!stop 'in GreenFunction_of_E_Sigma_mu_fraction_noPG in midle'

!=======integrate  Green function of E(Emin, Emax) po zone========================
   gs=(0.d0,0.d0)
   DO lx=0,(ni-1)
      DO ix=1,ngp
         jx=(2*lx+1)*ngp+1-ix
!-------------------------------------------------
         gs1=(0.d0,0.d0)
         DO ly=0,(ni-1)
            DO iy=1,ngp
               jy = (2*ly+1)*ngp+1-iy
               jy2 = (2*ly+1)*ngp+iy
               DO is=1, nspin
                  DO i=1, ndim
                     gs1(is,i) = gs1(is,i)+w(iy)*(Gk(is,i,jx,jy)+Gk(is,i,jx,jy2))
                  ENDDO
               END DO ! spin
            ENDDO ! iy
         ENDDO ! ly
         gs2 = gs1
!-------------------------------------------------
         jx=(2*lx+1)*ngp+ ix
         gs1=(0.d0,0.d0)
         DO ly=0,(ni-1)
            DO iy=1,ngp
               jy = (2*ly+1)*ngp+1-iy
               jy2 = (2*ly+1)*ngp+iy
               DO is=1, nspin
                  DO i=1, ndim
                     gs1(is,i) = gs1(is,i)+w(iy)*(Gk(is,i,jx,jy)+Gk(is,i,jx,jy2))
                  ENDDO
               END DO ! spin
            ENDDO ! iy
         ENDDO ! ly
         gs2=gs2+gs1
!-------------------------------------------------
         gs=gs+w(ix)*gs2
      ENDDO ! ix
   ENDDO ! lx

Integ_G(:,0)= (0.d0,0.d0)
DO is=1, nspin
 DO i=1, ndim
   Integ_G(is,i) = gs(is,i)*dx*dx/pi/pi
   !Integ_G(is,i) = gs(is,i)*dx*dx/pi/pi/4.d0 !/(2*ni**2)
   Integ_G(is,0) = Integ_G(is,0) + Integ_G(is,i)
 ENDDO
END DO

deallocate(xs, ys, xm, ym)
deallocate(lx, ly, jx, jy, jy2)
deallocate(gs1, gs2)

END IF ! (lenear or not integration)
!==========================================================================================

DEALLOCATE( kx, ky, GreenF, Hamilton, Gk)

!stop 'in GreenFunction_of_E_Sigma_mu_fraction_noPG in end'

End Subroutine GreenFunction_of_E_Sigma_mu_fraction_noPG
