SUBROUTINE Hamilton_n_band_in_kx_ky( kx, ky, Hamilton)

  USE hamiltonian_module_nband
  IMPLICIT NONE

double precision,    intent(in) :: kx, ky
complex*16,         intent(out) :: Hamilton( 1:ndim, 1:ndim)
!===============================================================

      Hamilton(1,1) = dcmplx( 2.d0*tt*( Cos(kx) ), 0.d0 )

!====================================================


END SUBROUTINE Hamilton_n_band_in_kx_ky
