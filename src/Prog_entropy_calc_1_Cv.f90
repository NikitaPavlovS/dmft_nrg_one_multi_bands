program Entropy_calc_1_Cv

  use module_integration

  implicit none
!----------------------------------------------------------------------------------------------
  integer                       :: ios
  character(64)                 :: argname_1, argname_2, filename_in

  double precision              :: tmp_d, nM, occupancy
  double precision              :: dE, Entropy_inf
  integer                       :: i, poinst_of_T

  double precision, allocatable :: temperature(:)
  double precision, allocatable :: Cv(:)
  double precision, allocatable :: Entropy(:)

!========================================================================================================================
!    Input file
   call getarg( 1, argname_1 )
   argname_1 = trim(adjustl(argname_1))
   filename_in = trim(adjustl(argname_1))

   call getarg( 2, argname_2 )
   argname_2 = trim(adjustl(argname_2))
   open( 11, form='formatted', status='scratch', iostat=ios)
   write(11,*) argname_2
   rewind(11)
   read(11,*,iostat=ios) occupancy
   if ( ios /= 0 ) then
      stop 'Give arguments: "file name with Cv" "occupancy"'
   end if
   close(11)


   open( 10, file=filename_in, form='formatted', status='old', iostat=ios, position='rewind' )
   i=0
   do
     read(10,*,iostat=ios) tmp_d
     if( ios /= 0 ) exit
     i=i+1
   end do
   poinst_of_T = i

   allocate(temperature(1:poinst_of_T))
   allocate(Cv(1:poinst_of_T))
   allocate(Entropy(1:poinst_of_T))

   rewind(10)
   do i=1, poinst_of_T
     read(10,*) temperature(i), Cv(i)
   end do
   close(10)

   ! S_inf =  M { N/M ln[N/M] + (1-N/M) * ln[1-N/M] }, M - number of states, N - total occupancy same as
   ! S_inf =  M*ln[M] -N*ln[N] - (M-N)*ln[M-N] , M - number of states, N - total occupancy
   nM = 2.d0
   Entropy_inf = nM*dlog(nM) - occupancy*dlog(occupancy) - (nM-occupancy)*dlog(nM-occupancy)

   do i=1, poinst_of_T
     Cv(i) = Cv(i) / temperature(i)
   end do

   Entropy(poinst_of_T) = Entropy_inf
   do i=1, (poinst_of_T-1)
      Entropy(i) = integration_trapezoidal( i, poinst_of_T, temperature(i:poinst_of_T), Cv(i:poinst_of_T) )
      Entropy(i) = Entropy_inf - Entropy(i)
   end do

   open( 11, file='Etropy_Cv.dat', form='formatted')
   do i=1, poinst_of_T
      write(11,*) temperature(i), Entropy(i), Entropy_inf
   end do
   close(11)


end program Entropy_calc_1_Cv
