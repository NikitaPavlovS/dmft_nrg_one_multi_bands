# -*- coding: utf-8 -*-
"""
Created on Fri Jun 13 2017
by pns
"""

import os
import glob
import numpy as np
import re


def inputFile(pathTo, temper, H, mu):
    data = '''\
TEMPERATURE:
     temperature                                                |     %fd0
MAGNETIC PROPERTIES:
     Number of spin: 1 - paramagnetic calc, 2 - magnetic calc   |     2
     Vaule of magnetic field in eV                              |     %fd0
     vaule of start Chemical Potential                          |     %fd0
ENERGY MESH:
     Exponential (T) or Linear (F)                              |     F
     Maximum Energy in eV (integer value)                       |     10
     1/(step of energy mesh)                                    |     2000
     Value of minimum energy for exp meshEmin_exp_mesh          |     1.0D-8
''' % (temper, H, mu)

    path = os.path.join(pathTo, 'INPUT_total_en')
    with open(path, 'w') as f:
        f.write(data)


def copy_GS(pathFrom, pathTo):
    with open(pathFrom, 'rb') as fileFrom, open(os.path.join(pathTo, 'Input_GS.dat'), 'wb') as fileTo:
        fileTo.write(fileFrom.read())


def find_results_and_recalc(path_to_dirs):
    for current_dir in path_to_dirs:
        path_to_file = os.path.join(current_dir, 'Occupancy_and_mu')
        with open(path_to_file, 'r') as f:
          outfile = f.read().split('\n')
          if (len(outfile)>2):
            data = np.array([])
            temper=['0.0']
            mu=['0.0']
            lines = outfile[-26:]
            for x in lines[::-1]:
              if x.startswith('      Lattice problem mu'):
                 mu = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
            temper = re.findall('([-\s]\d+[.]\d+\S+\d+)', outfile[2])

            data = np.append(data,temper)
            data = np.append(data,mu)
            temper_f=float(data[0])
            mu_f=float(data[1])
            inputFile(current_dir, temper_f, H, mu_f)

            file_with_G = sorted(glob.glob('%s/G_dos*' % current_dir))[-1]
            copy_GS(file_with_G, current_dir)

            os.chdir(current_dir)
            os.system('../../01_total_energy.run > out_tot')
            os.chdir(home_dir)


if __name__ == '__main__':
    H = 0.0001

    home_dir = os.getcwd()

    path_to_dirs = glob.glob('*/*on')
    find_results_and_recalc(path_to_dirs)



