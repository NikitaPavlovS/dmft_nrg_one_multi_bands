SUBROUTINE Spectral_Function_in_symmetric_direction_fraction

  use energy_module
  use hamiltonian_module_1band
  use parameters_module

  IMPLICIT NONE
! EXTERNAL variables
!==========================================================================
! INTERNAL variables
! Green Function variables
complex*16,       allocatable :: Hamilton
complex*16,       allocatable :: GreenF(:,:,:)
double precision, allocatable :: Awk(:,:)
double precision, allocatable :: Awk_2(:,:)
!-------------------------------------------------------------------
! Fermi function variables
logical, PARAMETER            :: Fermineed = 0
double precision              :: fermi !Value of Fermi function
double precision              :: step
double precision              :: ngf
!-------------------------------------------------------------------
! Integer variables for circles
integer                       :: i, j, ik, iE, is
!-------------------------------------------------------------------
! Constant variables
double precision, PARAMETER   :: kMin=0.d0
double precision              :: kMax
integer                       :: MaxK
!-------------------------------------------------------------------
! Temporary variables
double precision, allocatable :: kx(:), ky(:), kz(:)
double precision              :: dk
complex*16                    :: tmpcomplex
double precision              :: tmpdouble
!======================================================================================
MaxK=MaxK_point_for_SF

ALLOCATE(Hamilton)
ALLOCATE(kx(0:4*MaxK))
ALLOCATE(ky(0:4*MaxK))
ALLOCATE(kz(0:4*MaxK))
ALLOCATE(Awk(-nmax:nmax, 0:4*MaxK))

!make kx, ky, kz mesh
kMax=pi
dk=(kMax - kMin)/MaxK
!direction (0,0,0) -- (pi,0,0)
DO i = 0, MaxK
   kx(i) = kMin +dk*i
   ky(i) = 0.d0
   kz(i) = 0.d0
END DO
!direction (pi,0,0) -- (pi,pi,0)
j=0
DO i = MaxK+1, 2*MaxK
   j= j+1
   kx(i) = kMax
   ky(i) = kMin +dk*j
   kz(i) = 0.d0
END DO
!direction (pi,pi,0) -- (pi,pi,pi)
j=0
DO i = 2*MaxK+1, 3*MaxK
   j= j+1
   kx(i) = kMax
   ky(i) = kMax
   kz(i) = kMin +dk*j
END DO
!direction (pi,pi,pi) -- (0,0,0)
j=0
DO i = 3*MaxK+1, 4*MaxK
   j= j+1
   kx(i) = kMax -dk*j
   ky(i) = kx(i)
   kz(i) = kx(i)
END DO

!=========================================================================================================================================
!stop 'spectral function'

Awk(iE,ik) = 0.d0
   DO ik = 0, 4*MaxK
      DO iE = -nmax, nmax
         CALL Hamilton_one_band_in_kx_ky_kz( kx(ik), ky(ik), kz(ik), Hamilton)
         DO is=1, nspin
            Awk(iE,ik) = dimag( 1.d0/(dcmplx( om(iE) + mu, 0.d0) -Hamilton -sig(is,iE) ) )
         END DO
      END DO
   END DO
Awk(:,:) = -1.d0/pi * Awk(:,:) 


!------------------------------------------------------------------------------------------------------------------
IF (Spectral_Function_maximum_plot) THEN
   open(950,file='Spectral_Function_maximum.dat',form='FORMATTED')
   DO ik = 0, 4*MaxK
!       tmpdouble = 300*DBLE(i)/DBLE(MaxK)
      tmpdouble = 300.d0*dble(ik)
      tmpdouble = tmpdouble/dble(MaxK)
      DO iE = -(nmax-1), (nmax-1)
         IF ((Awk(iE,ik)>Awk(iE-1,ik)).and.(Awk(iE,ik)>Awk(iE+1,ik)).and.(Awk(iE,ik)>Spectral_Function_maximum_low_cutting)) THEN
              write(950,*) tmpdouble, om(iE), Awk(iE,ik)
              write(950,*)
!         ELSE
!              write(950,*) i, om(iE), Spectral_Function_maximum_low_cutting
         END IF
      END DO
   !stop 'spectral function'
   END DO
   close(950)
END IF

!stop 'spectral function'
!------------------------------------------------------------------------------------------------------------------
IF (Fermineed) THEN
   ALLOCATE(Awk_2(-nmax:nmax, 0:4*MaxK))
   DO ik = 0, 4*MaxK
      ngf=0.d0
      DO iE = -nmax, nmax
         Awk_2(iE,ik)=Awk(iE,ik)*fermi(om(iE),t,0.d0)
         if (iE.ne.(3*MaxK)) then
            ngf=ngf+Awk_2(iE,ik)*(om(iE+1)-om(iE))
         else
            ngf=ngf+Awk_2(iE,ik)*(om(iE)-om(iE-1))
         endif
      END DO
      Awk_2(:,ik)=Awk_2(:,ik)/ngf
   END DO
   deALLOCATE(Awk_2)
END IF

!------------------------------------------------------------------------------------------------------------------
IF (Spectral_Function_plot) THEN
   step = 0.5
      DO ik = 0, 4*MaxK
         tmpdouble = 300.d0*dble(ik)
         tmpdouble = tmpdouble/dble(MaxK)
         DO iE = -nmax, nmax
            write(301,*) tmpdouble, om(iE), Awk(iE,ik)
            !write(401,'(255f20.15)') om(iE), Awk(iE,ik) + ik*step
         END DO
         write(301,*)
         !write(401,*)
      END DO
END IF

deallocate( Hamilton, GreenF, kx, ky, Awk )

END SUBROUTINE Spectral_Function_in_symmetric_direction_fraction