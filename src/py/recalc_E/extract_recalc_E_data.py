#-*- coding:utf-8 -*-
###############################################################################
import re
import glob
import os
import numpy as np
###############################################################################

def find_results_from_new_E(b):
    data_b = np.array([[], [], [], [], []])
    for path in b:
        f=open(path, 'r')
        lines = f.readlines()
        arrayList = []
        arrayList.extend(lines[1].split())

        temper=[]
        temper=np.append(temper,float(arrayList[0]))
        moment=[]
        moment=np.append(moment,float(arrayList[1]))
        occu=[]
        occu=np.append(occu,float(arrayList[2]))
        mu=[]
        mu=np.append(mu,float(arrayList[3]))
        energy=[]
        energy=np.append(energy,float(arrayList[4]))
        data_b = np.append(data_b, [temper, moment, occu, mu, energy], axis=1)
        f.close()

    q = np.argsort(data_b[0])
    data_b = [[data_b[i][x] for x in q] for i in range(5)]
    data_b = [[float(i) for i in data_b[x]] for x in range(5)]
    return data_b


if __name__ == '__main__':
    ##########################################################################
    # data format: 		data[0] Temperature
    #				data[1] Magnetic moment
    #				data[2] Occupancy
    #				data[3] Chemical potential
    #				data[4] Energy
    ##########################################################################
    files_to_remove = ['out_tot','INPUT_total_en','Input_GS.dat']
    path_to_dir=os.path.join(os.getcwd(),'hfield_on')
    if not os.path.isfile('dataON.dat'):
       d_on = sorted(glob.glob('*/*on/Total_energy_new.dat'))
       dataON = find_results_from_new_E(d_on)
       np.savetxt('dataON.dat', np.transpose(dataON))
       d_remove = sorted(glob.glob('*/*on'))
       for path in d_remove:
         for file_name in files_to_remove:
           all_path=os.path.join(path,file_name)
           if os.path.isfile(all_path):
              os.remove(all_path)