program Entropy_calc

   implicit none
!----------------------------------------------------------------------------------------------
  integer                       :: ios
  character(64)                 :: argname_1, argname_2, filename_in

  double precision              :: tmp_d, nM, occupancy
  double precision              :: dE, Entropy_inf
  integer                       :: i, poinst_of_T

  double precision, allocatable :: temp(:)
  double precision, allocatable :: E_total(:)
  double precision, allocatable :: Entropy(:)

!========================================================================================================================
!    Input file
   call getarg( 1, argname_1 )
   argname_1 = trim(adjustl(argname_1))
   filename_in = trim(adjustl(argname_1))

   call getarg( 2, argname_2 )
   argname_2 = trim(adjustl(argname_2))
   open( 11, form='formatted', status='scratch', iostat=ios)
   write(11,*) argname_2
   rewind(11)
   read(11,*,iostat=ios) occupancy
   if ( ios /= 0 ) then
      stop 'Give arguments: "file_name" "occupancy"'
   end if
   close(11)


   open( 10, file=filename_in, form='formatted', status='old', iostat=ios, position='rewind' )
   i=0
   do
     read(10,*,iostat=ios) tmp_d
     if( ios /= 0 ) exit
     i=i+1
   end do
   poinst_of_T = i

   allocate(temp(poinst_of_T))
   allocate(E_total(poinst_of_T))
   allocate(Entropy(poinst_of_T))

   rewind(10)
   do i=1, poinst_of_T
     read(10,*) temp(i), tmp_d, tmp_d, tmp_d, E_total(i)
   end do
   close(10)

   ! S_inf =  M { N/M ln[N/M] + (1-N/M) * ln[1-N/M] }, M - number of states, N - total occupancy same as
   ! S_inf =  M*ln[M] -N*ln[N] - (M-N)*ln[M-N] , M - number of states, N - total occupancy
   nM = 2.d0
   Entropy_inf = nM*dlog(nM) - occupancy*dlog(occupancy) - (nM-occupancy)*dlog(nM-occupancy)

   Entropy(:) = 0.d0
   do i=poinst_of_T-1, 2,-1
      dE = E_total(i+1) - E_total(i)
      Entropy(i) = Entropy(i+1) + 0.5d0 * ( 1.d0/temp(i+1) + 1.d0/temp(i) )*dE
   end do

   do i=1, poinst_of_T
      Entropy(i) = Entropy_inf - Entropy(i)
   end do

   open( 11, file='Etropy.dat', form='formatted')
   do i=1, poinst_of_T
      write(11,*) temp(i), Entropy(i), Entropy_inf
   end do
   write(11,*) temp(poinst_of_T), Entropy_inf
   close(11)


end program Entropy_calc
