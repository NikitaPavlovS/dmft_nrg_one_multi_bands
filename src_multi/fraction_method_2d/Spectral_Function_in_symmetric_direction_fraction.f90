SUBROUTINE Spectral_Function_in_symmetric_direction_fraction

  use energy_module
  use hamiltonian_module_nband
  use parameters_module
  use impurity_module

  IMPLICIT NONE
! EXTERNAL variables
!==========================================================================
! INTERNAL variables
double precision              :: Pseudogap_maximum_low_cutting
! Green Function variables
complex*16,       allocatable :: Hamilton(:,:)
complex*16,       allocatable :: GreenF(:,:,:)
double precision, allocatable :: Awk(:,:)
double precision, allocatable :: Awk_2(:,:)
!-------------------------------------------------------------------
!PSEUDOGAP
complex*16,       allocatable :: PG_in_direction(:,:)
double precision              :: Energy_k                    ! Eigenvalue in (kx,ky)
double precision              :: Energy_k_shifted            ! Eigenvalue in (kx+pi,ky+pi)
double precision              :: Velocity, Velocity_shifted  ! velocity in (kx,ky) and in (kx+pi,ky+pi)
double precision, allocatable :: Image_part_of_PG_in_direction(:,:)
!-------------------------------------------------------------------
! Fermi function variables
logical, PARAMETER            :: Fermineed = 0
double precision              :: fermi !Value of Fermi function
double precision              :: step
double precision              :: ngf
!-------------------------------------------------------------------
! Integer variables for circles
integer                       :: i, j, ik, iE, is
!-------------------------------------------------------------------
! Constant variables
double precision, PARAMETER   :: kMin=0.d0
double precision              :: kMax
integer                       :: MaxK
!-------------------------------------------------------------------
! Temporary variables
double precision, allocatable :: kx(:), ky(:)
double precision              :: dk
complex*16                    :: tmpcomplex
double precision              :: tmpdouble
!======================================================================================
MaxK=MaxK_point_for_SF

ALLOCATE(kx(0:3*MaxK))
ALLOCATE(ky(0:3*MaxK))

!make kx and ky mesh
kMax=pi
dk=(kMax - kMin)/MaxK
!direction (0,0) -- (pi,0)
DO i = 0, MaxK
   kx(i)= kMin +dk*i
   ky(i)=0.d0
END DO
!direction (pi,0) -- (pi,pi)
j=0
DO i = MaxK+1, 2*MaxK
      j= j+1
      kx(i) = kMax
      ky(i) = kMin +dk*j
END DO
!napravlenie (pi,pi) -- (0,0)
j=0
DO i = 2*MaxK+1, 3*MaxK
      j= j+1
      kx(i) = kMax -dk*j
      ky(i) = kMax -dk*j
END DO

!=========================================================================================================================================
!stop 'spectral function'
ALLOCATE(GreenF( 1:nspin, 1:ndim, 1:ndim))
ALLOCATE(Hamilton( 1:ndim, 1:ndim))
IF (Pseudogap) THEN
   allocate(PG_in_direction(-nmax:nmax, 0:3*MaxK))
END IF


! IF (Quasiparticle_bands_location) THEN
!    open(952,file='Spectral_Function_Quasibands_location.dat',form='FORMATTED')
!    DO ik = 0, 3*MaxK
!       tmpdouble = 300.d0*dble(ik)
!       tmpdouble = tmpdouble/dble(MaxK)
!       CALL Hamilton_n_band_in_kx_ky( kx(ik), ky(ik), Hamilton)
!       DO is=1, nspin
!          tmpdouble_2(is)= om(-nmax) + mu - dreal((Hamilton + sig(is,-nmax)))
!       END DO
!       DO iE = -nmax+1, nmax
!          DO is=1, nspin
!             tmpdouble_1(is)= om(iE) + mu - dreal((Hamilton + sig(is,iE)))
!             IF (tmpdouble_1(is)*tmpdouble_2(is)<0) THEN
!                write(952,*) tmpdouble, ((om(iE)+om(iE-1))/2)
!                write(952,*)
!             END IF
!          END DO
!          tmpdouble_2(:) = tmpdouble_1(:)
!       END DO
!    END DO
! 
!    IF (Pseudogap) THEN
!       DO ik = 0, 3*MaxK
!          tmpdouble = 300.d0*dble(ik)
!          tmpdouble = tmpdouble/dble(MaxK)
!          CALL Hamilton_n_band_in_kx_ky( kx(ik), ky(ik), Hamilton)
! 
!          CALL Spectrum_and_Velocity_in_kx_ky_through_G_fraction( Hamilton, om(-nmax), kx(ik), ky(ik), mu, pi, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
!          DO is=1, nspin
!             CALL fract(ww, KP, om(-nmax), Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG_in_direction(iE,ik), sig(is,iE), comb)
!             !tmpdouble_2(is)= om(-nmax) + mu - dreal((Hamilton + sig(is,-nmax)))
!          END DO
! 
!          DO iE = -nmax+1, nmax
!             CALL Spectrum_and_Velocity_in_kx_ky_through_G_fraction( Hamilton, om(iE), kx(ik), ky(ik), mu, pi, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
!             DO is=1, nspin
!                CALL fract(ww, KP, om(iE), Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG_in_direction(iE,ik), sig(is,iE), comb)
!                !CALL GreenFunction_of_Hamiltonian_fraction( om(iE), mu, Hamilton, sig(is,iE)+PG_in_direction(iE,ik), GreenF(is,:,:))
!                !tmpdouble_1(is)= om(-nmax) + mu - dreal((Hamilton + sig(is,-nmax)))
!                IF (tmpdouble_1(is)*tmpdouble_2(is)<0) THEN
!                   write(952,*) tmpdouble, ((om(iE)+om(iE-1))/2)
!                   write(952,*)
!                END IF
!             END DO
!          END DO
!       END DO
!    ELSE
!       DO ik = 0, 3*MaxK
!          tmpdouble = 300.d0*dble(ik)
!          tmpdouble = tmpdouble/dble(MaxK)
!          CALL Hamilton_n_band_in_kx_ky( kx(ik), ky(ik), Hamilton)
! 
!          DO is=1, nspin
!             !tmpdouble_2(is)= om(-nmax) + mu - dreal((Hamilton + sig(is,-nmax)))
!          END DO
!          DO iE = -nmax+1, nmax
!             DO is=1, nspin
!                !CALL GreenFunction_of_Hamiltonian_fraction( om(iE), mu, Hamilton, sig(is,iE), GreenF(is,:,:))
!                !tmpdouble_1(is)= om(-nmax) + mu - dreal((Hamilton + sig(is,-nmax)))
!                IF (tmpdouble_1(is)*tmpdouble_2(is)<0) THEN
!                   write(952,*) tmpdouble, ((om(iE)+om(iE-1))/2)
!                   write(952,*)
!                END IF
!             END DO
!          END DO
!       END DO
!    END IF
! 
!    close(952)
! END IF


IF (Spectral_Function_plot.OR.Spectral_Function_maximum_plot) THEN

   ALLOCATE(Awk(-nmax:nmax, 0:3*MaxK))
   Awk(iE,ik) = 0.d0

   IF (Pseudogap) THEN
      DO ik = 0, 3*MaxK
         CALL Hamilton_n_band_in_kx_ky( kx(ik), ky(ik), Hamilton)
         DO iE = -nmax, nmax
            CALL Spectrum_and_Velocity_in_kx_ky_through_G_fraction( Hamilton, om(iE), kx(ik), ky(ik), mu, pi, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
            DO is=1, nspin
               CALL fract(ww, KP, om(iE), Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG_in_direction(iE,ik), sig(is,iE), comb)
               CALL GreenFunction_of_Hamiltonian_fraction( om(iE), mu, Hamilton, sig(is,iE)+PG_in_direction(iE,ik), GreenF(is,:,:))
               DO i=1, ndim
                  Awk(iE,ik) = Awk(iE,ik) + dimag(GreenF(is,i,i))
               ENDDO
            END DO
         END DO
      !stop 'spectral function'
      END DO
   ELSE
      DO ik = 0, 3*MaxK
         CALL Hamilton_n_band_in_kx_ky( kx(ik), ky(ik), Hamilton)
         DO iE = -nmax, nmax
            DO is=1, nspin
               CALL GreenFunction_of_Hamiltonian_fraction( om(iE), mu, Hamilton, sig(is,iE), GreenF(is,:,:))
               DO i=1, ndim
                  Awk(iE,ik) = Awk(iE,ik) + dimag(GreenF(is,i,i))
               ENDDO
            END DO
         END DO
      END DO
   END IF
   Awk(:,:) = -1.d0/pi * Awk(:,:)

END IF

deallocate( Hamilton, GreenF, kx, ky )


!------------------------------------------------------------------------------------------------------------------
IF (Spectral_Function_maximum_plot) THEN
   IF (Pseudogap) THEN
       allocate(Image_part_of_PG_in_direction(-nmax:nmax, 0:3*MaxK))
       Pseudogap_maximum_low_cutting = 0.01d0
       Image_part_of_PG_in_direction(:,:) = - dimag(PG_in_direction(:,:))
      open(951,file='Pseudogap_maximum.dat',form='FORMATTED')
   END IF
   open(950,file='Spectral_Function_maximum.dat',form='FORMATTED')
   DO ik = 0, 3*MaxK
!       tmpdouble = 300*DBLE(i)/DBLE(MaxK)
      tmpdouble = 300.d0*dble(ik)
      tmpdouble = tmpdouble/dble(MaxK)
      DO iE = -(nmax-1), (nmax-1)
         IF ((Awk(iE,ik)>Awk(iE-1,ik)).and.(Awk(iE,ik)>Awk(iE+1,ik)).and.(Awk(iE,ik)>Spectral_Function_maximum_low_cutting)) THEN
              write(950,*) tmpdouble, om(iE), Awk(iE,ik)
              write(950,*)
!         ELSE
!              write(950,*) i, om(iE), Spectral_Function_maximum_low_cutting
         END IF
            IF (Pseudogap.and.(Image_part_of_PG_in_direction(iE,ik)>Image_part_of_PG_in_direction(iE-1,ik)).and.(Image_part_of_PG_in_direction(iE,ik)>Image_part_of_PG_in_direction(iE+1,ik)).and.(Image_part_of_PG_in_direction(iE,i)>Pseudogap_maximum_low_cutting)) THEN
                 write(951,*) tmpdouble, om(iE), Image_part_of_PG_in_direction(iE,ik)
                 !write(951,*)
            END IF
      END DO
   !stop 'spectral function'
   END DO
   close(950)
   IF (Pseudogap) THEN
      close(951)
      deallocate( Image_part_of_PG_in_direction )
   END IF
END IF

!stop 'spectral function'
!------------------------------------------------------------------------------------------------------------------
IF (Fermineed) THEN
   ALLOCATE(Awk_2(-nmax:nmax, 0:3*MaxK))
   DO ik = 0, 3*MaxK
      ngf=0.d0
      DO iE = -nmax, nmax
         Awk_2(iE,ik)=Awk(iE,ik)*fermi(om(iE),t,0.d0)
         if (iE.ne.(3*MaxK)) then
            ngf=ngf+Awk_2(iE,ik)*(om(iE+1)-om(iE))
         else
            ngf=ngf+Awk_2(iE,ik)*(om(iE)-om(iE-1))
         endif
      END DO
      Awk_2(:,ik)=Awk_2(:,ik)/ngf
   END DO
   deALLOCATE(Awk_2)
END IF

!------------------------------------------------------------------------------------------------------------------
IF (Spectral_Function_plot) THEN
   step = 0.5
   IF (Pseudogap) THEN
      DO ik = 0, 3*MaxK
         tmpdouble = 300.d0*dble(ik)
         tmpdouble = tmpdouble/dble(MaxK)
         DO iE = -nmax, nmax
            write(300+iter,*) tmpdouble, om(iE), Awk(iE,ik)
            ! write(400+iter,'(255f20.15)') om(iE), Awk(iE,ik) + i*step
            write(500+iter,'(255f20.15)') om(iE), dreal(PG_in_direction(iE,ik)) + ik*step*0.1
            write(600+iter,'(255f20.15)') om(iE), -dimag(PG_in_direction(iE,ik)) + ik*step*0.1
            write(700+iter,*) i, om(iE), -dimag(PG_in_direction(iE,ik))
      !      write(11,'(255f20.15)') om(iE), Awk_2(iE,ik) + (ik-1)*step
      !      write(12,*) i, om(iE), Awk_2(iE,ik)
         END DO
         write(300+iter,*)
         ! write(400+iter,*)
         write(500+iter,*)
         write(600+iter,*)
         write(700+iter,*)
      !   write(11,*)
      !   write(12,*)
      END DO

      deallocate( PG_in_direction )

   ELSE
      DO ik = 0, 3*MaxK
         tmpdouble = 300.d0*dble(ik)
         tmpdouble = tmpdouble/dble(MaxK)
         DO iE = -nmax, nmax
            write(300+iter,*) tmpdouble, om(iE), Awk(iE,ik)
            ! write(400+iter,'(255f20.15)') om(iE), Awk(iE,ik) + ik*step
         END DO
         write(300+iter,*)
         ! write(400+iter,*)
      END DO
   END IF
END IF

IF (Spectral_Function_plot.OR.Spectral_Function_maximum_plot) THEN
   deallocate( Awk )
END IF

!    IF (iter>NDMFT) THEN
!       DO ik = 0, 3*MaxK
!          write(900+iter,*) i*300/MaxK, Awk(0,ik)
!       END DO
!    END IF


END SUBROUTINE Spectral_Function_in_symmetric_direction_fraction