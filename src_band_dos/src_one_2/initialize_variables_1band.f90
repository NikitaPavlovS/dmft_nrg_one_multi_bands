subroutine initialize_variables_1band

  use energy_module
  use hamiltonian_module_1band
  use parameters_module

  implicit none

double precision, allocatable :: temp_array(:)
integer                       :: nmax_temp
double precision              :: tmpreal
double precision              :: tmp_Im_Sigma(1:nspin), tmp_Re_Sigma(1:nspin)
integer                       :: i, is

!=========================================================================================================================================
nmax_temp = 100000
allocate(temp_array(-nmax_temp:nmax_temp))

Call Energy_mesh(temp_array, nmax_temp)

allocate(om(-nmax:nmax))
DO i=-nmax,nmax
   om(i) = temp_array(i)
END DO

deallocate(temp_array)

!------------------------------------------------------------------------------------------------------------------------------------------
allocate(dos(1:nspin,-nmax:nmax))
allocate(dos_0(1:nspin,-nmax:nmax))
allocate(totalDOS(-nmax:nmax))
allocate(sig(1:nspin,-nmax:nmax))


IF (INIT_Sigma) THEN
   open(10,file='Start_sigma.dat',form='FORMATTED')
   !-----------------------------------
   IF (nmax_init==nmax) THEN
      do i=-nmax_init,nmax_init
         read(10,*) tmpreal,   ( (tmp_Re_Sigma(is), tmp_Im_Sigma(is)), is=1,nspin )
         do is=1, nspin 
            sig(is,i) = dcmplx( tmp_Re_Sigma(is), tmp_Im_Sigma(is) )+dcmplx(0.d0,-delta)
         end do
      end do
   !-----------------------------------
   ELSE
    IF (nmax_init<nmax) THEN ! write missing value for sig array (analogy if no INIT_Sigma)
      do i=-nmax_init,nmax_init
         read(10,*) tmpreal,   ( (tmp_Re_Sigma(is), tmp_Im_Sigma(is)), is=1,nspin )
         do is=1, nspin 
            sig(is,i) = dcmplx( tmp_Re_Sigma(is), tmp_Im_Sigma(is) )+dcmplx(0.d0,-delta)
         end do
      end do
      do i=-nmax,-(nmax_init+1)
            sig(:,i) = dcmplx(0.d0,-delta)
            sig(:,-i) = dcmplx(0.d0,-delta)
      end do
    !-----------------------------------
    ELSE  ! nmax_init>nmax
      do i=-nmax_init,-(nmax+1)
         read(10,*)
      end do
      do i=-nmax,nmax
         read(10,*) tmpreal,   ( (tmp_Re_Sigma(is), tmp_Im_Sigma(is)), is=1,nspin )
         do is=1, nspin 
            sig(is,i) = dcmplx( tmp_Re_Sigma(is), tmp_Im_Sigma(is) )+dcmplx(0.d0,-delta)
         end do
      end do
    !-----------------------------------
    END IF
   END IF
   close(10)
ELSE
!-------------------------------------------------------------------------
  sig = dcmplx(0.d0,-delta)
END IF

!----------------------------------------------------
   dos = 0.d0
   totalDOS = 0.d0

IF (INIT_dos) THEN
   open(10,file='Start_dos.dat',form='FORMATTED')
   IF (nmax_init==nmax) THEN
      do i=-nmax_init,nmax_init
         read(10,*) tmpreal, (dos_0(is,i),is=1,nspin), totalDOS(i)
      end do
   ELSE
    IF (nmax_init<nmax) THEN
      do i=-nmax_init,nmax_init
         read(10,*) tmpreal, (dos_0(is,i),is=1,nspin), totalDOS(i)
      end do
    ELSE  ! nmax_init>nmax
      do i=-nmax_init,-(nmax+1)
         read(10,*)
      end do
      do i=-nmax,nmax
         read(10,*) tmpreal, (dos_0(is,i),is=1,nspin), totalDOS(i)
      end do
    END IF
   END IF
   close(10)
   dos = dos_0
END IF

!stop 'stop INIT'

end subroutine initialize_variables_1band