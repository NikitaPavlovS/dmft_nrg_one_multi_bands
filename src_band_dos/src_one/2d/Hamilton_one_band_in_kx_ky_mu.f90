SUBROUTINE Hamilton_one_band_in_kx_ky_mu(  kx, ky, mu, Hamilton)

  USE hamiltonian_module_1band
  IMPLICIT NONE

double precision,    intent(in) :: kx, ky  ! proekcia volnovogo vektora na x and y
double precision,    intent(in) :: mu
complex*16,         intent(out) :: Hamilton
!===============================================================

      Hamilton = dcmplx( (kx+ky)/6.d0 -mu  , 0.d0)


END SUBROUTINE Hamilton_one_band_in_kx_ky_mu
