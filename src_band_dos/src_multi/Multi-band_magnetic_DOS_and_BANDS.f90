! Simple Fortran program to demonstrate the use of the NRG-Fortran interface

program Multi_band_magnetic_DOS_and_BANDS

  use energy_module
  use hamiltonian_module_Nband
  use parameters_module

implicit none

complex*16, allocatable :: Gtmp(:,:)
integer                 :: i, j, is

!=========================================================================================================================================
Call read_input_Nband
!=========================================================================================================================================
Call initialize_variables
!=========================================================================================================================================
!stop 'stop init'

!=========================================================================================================================================
SELECT CASE (Method_of_Green_Function_calculation)
!======================================================================================
CASE (1)
   IF (DOS_plot) THEN
      if ((nspin==2).and.(H_field>1E-6)) then
         sig(1,:,:) = sig(1,:,:) - dcmplx(H_field/2.d0, 0.d0)
         sig(2,:,:) = sig(2,:,:) + dcmplx(H_field/2.d0, 0.d0)
      end if
      allocate(Gtmp(1:nspin,0:ndim))
      OPEN(Unit = 14, file='DOS.dat', form='FORMATTED')
         do i=-nmax,nmax
            Call GreenFunction_of_E_Sigma_mu_fraction_noPG( om(i), sig(:,:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,1))
            totalDOS(:,i) = -1.d0/pi*dimag(Gtmp(:,0))
            !----------------------------------------------------
            write(14,'(2x,100(2x,f20.15))') om(i), ( (( -1.d0/pi*dimag(Gtmp(is,j)) ),is=1,nspin),j=1,ndim ), (totalDOS(is,i),is=1,nspin)
         end do
      close(14)
      Call occupancy_and_mu_search
      deallocate(Gtmp)
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Bands_plot.or.Fermi_Surface_without_Interaction) THEN
      CALL Bands_in_directions_and_Bare_Fermi_surface_fraction
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Spectral_Function_plot.OR.Spectral_Function_maximum_plot) THEN
      CALL Spectral_Function_in_symmetric_direction_fraction
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
!======================================================================================
CASE (2)

!======================================================================================
CASE (3)

!======================================================================================
CASE DEFAULT
     stop 'error in selection of Green function method'
END SELECT
!==========================================================================================================================================
IF (Write_HK) THEN
   Call Write_Hamiltonian_HK
END IF
!==========================================================================================================================================

deallocate( dos, om, totalDOS, sig)

end program Multi_band_magnetic_DOS_and_BANDS
