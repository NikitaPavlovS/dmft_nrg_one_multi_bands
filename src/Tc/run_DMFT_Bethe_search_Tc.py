# -*- coding: utf-8 -*-
# pns 07.2017

from lib_for_DMFT_NRG import *

p={}

# What is model used: Bethe (1), FCC (2), from DOS(0)
# !!! main parameter !!!
p['band_model'] = 1
p['n_spin'] = 1
p['H_field'] = [0.0]

#---- INTERACTION ----
U_start = -0.9
U_end = -1.15
dU = -0.1

#---- OCCUPANCY ----
# What is total occupancy needed in result of calculation ?
p['occupancy'] = 0.5
# tolerance of occupancy convergence in expression 1.0D-%d
p['occupancy_tolerance'] = 4

#---- DMFT convergence ----
# number of DMFT iterations
p['it'] = 40
# number of DMFT iterations after tolerance satisfaction
p['it_after_tolerance'] = 10
# number of different DMFT calculation with one temperature
p['iters_of_DMFT_continue'] = 5

#---- TEMPERATURE ----
temperature_start = 0.1
# number of different temperatures for DMFT calculation
p['iters_of_temperature_cycle'] = 4
# precision to search temperature 1.0E-%d
p['precision_of_T'] = 2

#---- CHEMICAL POTENTIAL ----
p['variable_mu'] = 'T'
p['start_from_read_mu'] = 'F'
p['mu_start'] = -0.494812223907229
# What is value of part_of_dmu? (mu=mu+part_of_dmu*delta_mu)
p['part_of_dmu_start'] = 0.1
p['part_of_dmu_use_start'] = 'F'  # T: if calculation exists, the value above is using, F: ... isn't using
p['iter_each_change_mu'] = [6]

#---- ENERGY MESH ----
# Maximum Energy in eV (integer value) 
p['Emax'] = 5
# 1 eV/(step of energy mesh)
p['mesh'] = 100

#---- disoder for Bethe ----
p['Delta'] = 0.0


#--------------------------------------------------------------------
#---- DMFT NRG program files ----
p['prog_DMFT'] = 'DMFT_1_band_9.run'
p['name_script_file'] = 'DMFT_RUN.sh'
p['name_input_file'] = 'DMFT_INPUT_1_band'
p['name_output_file'] = 'DMFT_out'
p['files_to_save'] = [p['name_input_file'],p['name_output_file'],'NRG_out',p['name_script_file'],'sig.dat']

#---- program files for critical temperature calculation ----
p['prog_calc_Tc'] = 'DMFT_Bethe_Tc_mu.run'
p['name_input_file_Tc'] = 'DMFT_input_Tc.dat'
p['name_output_file_Tc'] = 'DMFT_out_Tc.dat'

#---- program for search critical temperature using mu and T ----
p['prog_Tc_interpol'] = 'Tc_muT_interpol.run'
p['prog_Tc_interpol_input'] = 'DMFT_input_mu_T_interpol.dat'

#---- program search lambda ----
p['Prog_search_lambda'] = 'Prog_search_lambda.run'
p['Prog_search_lambda_out'] = 'NRG_T_out.dat'
#--------------------------------------------------------------------

DMFT_Bethe_search_Tc(U_start=U_start, U_end=U_end, dU=dU, temperature_start=temperature_start, **p)
