subroutine DMFT_loop_multi_band

  use module_energy
  use module_hamiltonian_nband
  use module_parameters
  use module_impurity
  use module_nrg_variable

  implicit none

!----------------------------------------------------------------------------------------------
!Green function
complex*16,       allocatable :: Gtmp(:,:) ! integrated Green function for px, py, and total d+px+py
! Integer variables for circles
integer                       :: i, j, is
double precision              :: tmpreal
double precision              :: point_double_1,point_double_2
integer                       :: number_of_file_for_DOS, number_of_file_for_dd, number_of_file_for_SIG
!----------------------------------------------------------------------------------------------

allocate(Gtmp(1:nspin,0:ndim))
point_double_1 = 0.d0
point_double_2 = 0.d0

DO iter=start_iter,NDMFT

 number_of_file_for_SIG = 10+iter
 if (.not.(INIT_dd.and.(iter.eq.start_iter))) then
 ! calculate DOS if not (start from DOS and first iteration)
   if ((nspin==2).and.(H_field>1E-6)) then
      sig(1,:) = sig(1,:) - dcmplx(H_field/2.d0, 0.d0)
      sig(2,:) = sig(2,:) + dcmplx(H_field/2.d0, 0.d0)
   end if
   number_of_file_for_DOS = 100+iter
   number_of_file_for_dd = 200+iter
   IF (Pseudogap) THEN
      SELECT CASE (Method_of_Green_Function_calculation)
      CASE (1)
         do i=-nmax,nmax
            Call GreenFunction_of_E_Sigma_mu_fraction_PG( om(i), sig(:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,1))
            totalDOS(:,i) = -1.d0/pi*dimag(Gtmp(:,0))
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,1))
            !dd(i)=dimag(sig(i)) - dimag(G(i))/((dreal(G(i)))**2+(dimag(G(i)))**2)
            !----------------------------------------------------
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( (( -1.d0/pi*dimag(Gtmp(is,j)) ),is=1,nspin),j=1,ndim ), (totalDOS(is,i),is=1,nspin)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), (totalDOS(is,i),is=1,nspin)
            !write(250+iter, '(2x,7(2x,f15.8))') om(i), dd(i), dreal(G(i)), dimag(G(i)), dreal(sig(i)), dimag(sig(i)), dos(i)
         end do
      !----------------------------------------------------
      CASE (2)
         do i=-nmax,nmax
            !Call GreenFunction_of_E_Sigma_mu_Sigma_fraction_PG( om(i), sig(:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,1))
            totalDOS(:,i) = -1.d0/pi*dimag(Gtmp(:,0))
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,1))
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( (( -1.d0/pi*dimag(Gtmp(is,j)) ),is=1,nspin),j=1,ndim ), (totalDOS(is,i),is=1,nspin)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), (totalDOS(is,i),is=1,nspin)
         end do
      !----------------------------------------------------
      CASE (3)
         do i=-nmax,nmax
            !Call GreenFunction_of_E_Sigma_mu_Eigen_system_PG( om(i), sig(:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,1))
            totalDOS(:,i) = -1.d0/pi*dimag(Gtmp(:,0))
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,1))
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( (( -1.d0/pi*dimag(Gtmp(is,j)) ),is=1,nspin),j=1,ndim ), (totalDOS(is,i),is=1,nspin)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), (totalDOS(is,i),is=1,nspin)
         end do
      !----------------------------------------------------
      CASE DEFAULT
            stop 'error in selection of Green function method'
      END SELECT
   ELSE
      SELECT CASE (Method_of_Green_Function_calculation)
      CASE (1)
         do i=-nmax,nmax
            Call GreenFunction_of_E_Sigma_mu_fraction_noPG( om(i), sig(:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,1))
            totalDOS(:,i) = -1.d0/pi*dimag(Gtmp(:,0))
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,1))
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( (( -1.d0/pi*dimag(Gtmp(is,j)) ),is=1,nspin),j=1,ndim ), (totalDOS(is,i),is=1,nspin)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), (totalDOS(is,i),is=1,nspin)
         end do
      !----------------------------------------------------
      CASE (2)
         do i=-nmax,nmax
            !Call GreenFunction_of_E_Sigma_mu_Sigma_fraction_noPG( om(i), sig(:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,1))
            totalDOS(:,i) = -1.d0/pi*dimag(Gtmp(:,0))
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,1))
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( (( -1.d0/pi*dimag(Gtmp(is,j)) ),is=1,nspin),j=1,ndim ), (totalDOS(is,i),is=1,nspin)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), (totalDOS(is,i),is=1,nspin)
         end do
      !----------------------------------------------------
      CASE (3)
         do i=-nmax,nmax
            !Call GreenFunction_of_E_Sigma_mu_Eigen_system_noPG( om(i), sig(:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,1))
            totalDOS(:,i) = -1.d0/pi*dimag(Gtmp(:,0))
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:,1))
            write(number_of_file_for_DOS, '(2x,100(2x,f20.15))') om(i), ( (( -1.d0/pi*dimag(Gtmp(is,j)) ),is=1,nspin),j=1,ndim ), (totalDOS(is,i),is=1,nspin)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), (totalDOS(is,i),is=1,nspin)
         end do
      !----------------------------------------------------
      CASE DEFAULT
            stop 'error in selection of Green function method'
      END SELECT
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
!    IF (const_mesh) THEN
!       integral_of_DOS_d = (dos(-nmax)+dos(0))*dE/2.d0
!       integral_of_DOS_total = (totalDOS(-nmax)+totalDOS(0))*dE/2.d0
!       do i=-nmax,0
!          integral_of_DOS_d = integral_of_DOS_d + (dos(i)+dos(i+1))*dE/2.d0        !(om(i+1)-om(i))/2
!          integral_of_DOS_total = integral_of_DOS_total + (totalDOS(i)+totalDOS(i+1))*dE/2.d0        !(om(i+1)-om(i))/2
!       end do
!       write(91,*) 'integral_ot_G do E=0', integral_of_DOS_total
!       write(91,*) 'integral_ot_Gd do E=0', integral_of_DOS_d
!    END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   ! Calculate spectral function and Fermi surface for this iteration
   SELECT CASE (Method_of_Green_Function_calculation)
   CASE (1)
      IF (Spectral_Function_plot) THEN
         CALL Spectral_Function_in_symmetric_direction_fraction
      END IF
      IF (Fermi_surface_plot) THEN
         CALL Fermi_surface_fraction
      END IF
   CASE (2)
      IF (Spectral_Function_plot) THEN
!          CALL Spectral_Function_in_symmetric_direction_Sigma_fraction( iter, Spectral_Function_plot, 0,&
!                              om, MaxK_point_for_SF, sig, mu, t, 0, Spectral_Function_maximum_low_cutting)
      END IF
      IF (Fermi_surface_plot) THEN
!          CALL Fermi_surface_Sigma_fraction( iter, om, sig, mu, t, Energy_shift, Apply_Hand_Sigma_value, Hand_Sigma_value )
      END IF
   CASE (3)
      IF (Spectral_Function_plot) THEN
!          CALL Spectral_Function_in_symmetric_direction_Eigen_system( iter, Spectral_Function_plot, 0, &
!                              om, MaxK_point_for_SF, sig, mu, t, 0, Spectral_Function_maximum_low_cutting)
      END IF
      IF (Fermi_surface_plot) THEN
!          CALL Fermi_surface_Eigen_system( iter, om, sig, mu, t, Energy_shift, Apply_Hand_Sigma_value, Hand_Sigma_value )
      END IF
   CASE DEFAULT
        stop 'error in selection of Green function method in DMFT for SF and FS'
   END SELECT

   write(*,*)'Lattice problem mu=',mu
 end if !INIT_dd
   !---------------------------------------------------------------------------------------------------------------------------------------
   !Do an NRG run
   SELECT CASE (nspin)
   CASE (1)
      !Call nrg(P,SU2_sim,mu,u,t,om,dd(1,:),occ,sig(1,:),nmax,lam,nkeep,iflag,iover)
      Call nrg_nspin(P,SU2_sim,H_field,mu,u,t,om,nspin,dd(1,:),point_double_1,occ,sig(1,:),point_double_2,nmax,lam,nkeep,iflag,iover)
   CASE (2)
      Call nrg_nspin(P,SU2_sim,H_field,mu,u,t,om,nspin,dd(1,:),dd(2,:),occ,sig(1,:),sig(2,:),nmax,lam,nkeep,iflag,iover)
      !Call nrg_2spin(P,SU2_sim,H_field,mu,u,t,om,dd(1,:),dd(2,:),occ,sig(1,:),sig(2,:),nmax,lam,nkeep,iflag,iover)
   CASE DEFAULT
      stop 'error in spin value'
   END SELECT
   !----------------------------------------------------
   !   Edc apply for Sigma
   IF (Edc_const) THEN
      sig = sig + dcmplx(-Edc,0.d0)
   ELSE
!       IF (hole_occupancy) THEN
!          sig = sig + dcmplx(  u*(n_d0 - 1.d0/2.d0),0.d0) !napisano s usloviem, 4to duro4noe zapolnenie
!       ELSE
!          sig = sig + dcmplx( -u*(n_d0 - 1.d0/2.d0),0.d0) 
!       END IF
   END IF
   !----------------------------------------------------
   !Save sig for current iteration
   do i=-nmax,nmax
      write(number_of_file_for_SIG,'(x,5(2x,f22.15))') om(i), (dreal(sig(is,i)),dimag(sig(is,i)),is=1,nspin)
   end do
   !---------------------------------------------------------------------------------------------------------------------------------------
   !Search for the chemical potential for given qval
   IF (((iter.ne.start_iter).or.INIT_dd).and.variable_mu) THEN
      Call occupancy_and_mu_search
   END IF

!   stop 'stop after first iteration of DMFT'
END DO !DMFT loop

deallocate(Gtmp)


end subroutine DMFT_loop_multi_band
