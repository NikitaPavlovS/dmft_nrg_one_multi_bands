! pns 2016
! Search Lambda value
! finding lambda for given t0. Lambda is taken in the interval 1.95 - 2.05
! The program looks for where the N_nrg is changing.
! Then program calculates the temperature at each border of the N_nrg.
! If the N_nrg is constant, then the temperature is monotonous.
! Hence the program searches in the interval from the maximum to the minimum value of temperature.

FUNCTION search_lambda(t0, t_found)
  use module_nrg_variable, only : precision_of_N, precision_of_T
  use module_parameters, only   : output_file
implicit none

double precision              :: search_lambda
double precision, intent(in)  :: t0
logical,          intent(out) :: t_found

! local var
integer          :: i, N_nrg, N_left, N_right, number_of_N, N, N_min, N_max, sum_i

double precision :: N_temp, t_nrg, N_nrg_d, temp
double precision :: L_left, L_right, left, right, lambda
double precision, allocatable :: L1(:), L2(:), t_max(:), t_min(:)

double precision :: delta_N

logical          :: search_true

!===============================================================================================

   L_left = 1.95d0
   L_right = 2.05d0

   N_left = N_nrg(t0, L_left)   ! Nmax
   N_right = N_nrg(t0, L_right) ! Nmin

   write(output_file,*) '+++++++++++++++++++   Start function search Lambda   +++++++++++++++++'
   write(output_file,*) 'search Lambda for T = ', t0
   write(output_file,'(A13,i3,A1,i3,A1)') ' N value is (', N_left,':', N_right, ')'
   number_of_N = N_left - N_right

   allocate(L1(0:number_of_N),L2(0:number_of_N))
   allocate(t_min(0:number_of_N),t_max(0:number_of_N))
   L1 = L_left
   L2 = L_right
   L1(0) = L_left
   L2(number_of_N) = L_right

   t_max(0) = t_nrg(t0, L_left)
   t_min(number_of_N) = t_nrg(t0, L_right)

   ! ==================================================================
   ! search intervals of N_nrg
   N_min = 0

   if (number_of_N>0) then
      ! right border of interval with N=const 
      do i=0, number_of_N-1
         N=N_left-i
         left = L_left
         right = L2(i)
         search_true=.true.
         do while (search_true)
            lambda = 0.5d0 * (left + right)
            N_temp = N_nrg_d(t0, lambda)
            delta_N = N_temp-dble(N)
            if ( delta_N>0.d0 ) then
               left=lambda
            else
               right=lambda
            end if

            if ( (delta_N<precision_of_N) .and. (delta_N>0.0d0) ) then
               L2(i) = lambda
               t_min(i) = t_nrg(t0, L2(i))
               write(output_file,'(A29,i3,A4,f14.10)') ' found rifht boder of Lambda(',N,') = ', L2(i)
               search_true=.false.
            end if
         end do

         if ( t_nrg(t0,L2(i)) < t0) then
            N_min = N
         end if
      end do

      ! left border of interval with N=const 
      do i=1, number_of_N
         N=N_left-i+1
         left = L_left
         right = L2(i)
         search_true=.true.
         do while (search_true)
            lambda = 0.5d0 * (left + right)
            N_temp = N_nrg_d(t0, lambda)
            delta_N = dble(N)-N_temp
            if ( delta_N<0.d0 ) then
               left=lambda
            else
               right=lambda
            end if

            if ( (delta_N<precision_of_N) .and. (delta_N>0.0d0) ) then
               L1(i) = lambda
               t_max(i) = t_nrg(t0, L1(i))
               write(output_file,'(A28,i3,A4,f14.10)') ' found left boder of Lambda(',N-1,') = ', L1(i)
               search_true=.false.
            end if
         end do
      end do
   end if

   if (N_min<0.001) then
      N_min = N_right
   end if

   write(output_file,*) 
   do i=0, number_of_N
      write(output_file,*) 'interval ', i
      write(output_file,'(A18,i3,1x,i3)') ' N_left, N_right = ', N_nrg(t0, L1(i)), N_nrg(t0, L2(i)) 
      write(output_file,'(A10,f14.10,1x,f14.10)') ' L1, L2 = ', L1(i), L2(i)
      write(output_file,'(A16,f14.10,1x,f14.10)') ' max T, min T = ', t_max(i), t_min(i)
      write(output_file,*) 
   end do
   write(output_file,*)  'N_min =', N_min

   ! ==================================================================
   ! search Lambda
   sum_i = 0
   search_cycle: DO i=0, (N_left-N_min)

      ! if condition below is true then t0 lay in t_max(i),t_min(i). => Else Lambda don't search for this "i"
      if (.not.( ((t0-t_max(i))>precision_of_T*t0) .or. ((t_min(i)-t0)>precision_of_T*t0) ) ) then
         sum_i = sum_i +1
         left = L1(i)
         right = L2(i)
         search_true = .true.
         do while (search_true)
            lambda = 0.5d0 * (left+right)
            temp = t_nrg(t0, lambda)
            if ( (temp-t0)>0 ) then
               left=lambda
            else
               right=lambda
            end if
            if ( abs(temp-t0)<precision_of_T*t0 ) then
               search_lambda = lambda
               exit search_cycle
               search_true = .false.
            end if
         end do
      end if

   END DO search_cycle

   if (sum_i<0.01) then
      write(output_file,'(A88,e15.8)') ' ERROR: Needed temperature did not find in Lambda interval [1.95:2.05] with precision =', precision_of_T
      search_lambda = 2.d0
      t_found = .false.
   else
      write(output_file,'(A21,f20.16,A7,f20.16)')' Lambda is found. L =', search_lambda, '   T = ', temp
      t_found = .true.
   end if

   deallocate(L1,L2, t_min,t_max)

   write(output_file,*) '+++++++++++++++++++   End function search Lambda   +++++++++++++++++++'
   write(output_file,*)

END FUNCTION search_lambda
!===============================================================================================


!===============================================================================================
!                         NRG functions
function N_nrg(tem, lambda)   !   decreasing function of lambda
   use module_energy, only : Emax
   implicit none
   integer           :: N_nrg
   double precision  :: tem, lambda
   N_nrg = dint( 2.d0 - 2.d0*dlog( ( 2.d0*tem/dble(Emax) )/( 1.d0+1.d0/lambda ) )/dlog(lambda) )
end function N_nrg

function N_nrg_d(tem, lambda)   !   decreasing function of lambda
   use module_energy, only : Emax
   implicit none
   double precision  :: N_nrg_d
   double precision  :: tem, lambda
   N_nrg_d = 2.d0 - 2.d0*dlog( ( 2.d0*tem/dble(Emax) )/( 1.d0+1.d0/lambda ) )/dlog(lambda) 
end function N_nrg_d

function t_nrg(tem, lambda)   !   decreasing function of lambda
   use module_energy, only : Emax
   implicit none
   double precision  :: t_nrg
   integer           :: N_nrg
   double precision  :: tem, lambda
   t_nrg = 0.5d0*(1.d0+1.d0/lambda)*dble(Emax)*lambda**((2.d0-N_nrg(tem, lambda))/2.d0)
end function t_nrg
!===============================================================================================