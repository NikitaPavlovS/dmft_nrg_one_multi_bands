SUBROUTINE Spectrum_and_Velocity_in_kx_ky_kz( Hamilton_in_kx_ky_kz, kx, ky, kz, DeltaXY,&
                                           Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)

   use hamiltonian_module_1band

implicit none

! calculate spectrum for interacting state by fomula Ek=(E-1)/G_dd(E,k)
! EXTERNAL variables
double precision,  intent(in) :: Hamilton_in_kx_ky_kz
double precision,  intent(in) :: kx, ky, kz
double precision,  intent(in) :: DeltaXY          ! value of shifting for kx and ky
! Pseudogap variables
double precision, intent(out) :: Energy_k         ! Eigenvalue in (kx,ky)
double precision, intent(out) :: Energy_k_shifted ! Eigenvalue in (kx+pi,ky+pi)
double precision, intent(out) :: Velocity         ! velocity in (kx,ky)
double precision, intent(out) :: Velocity_shifted ! velocity in (kx+pi,ky+pi)
!===============================================
! INTERNAL variables
complex*16,       allocatable :: Hamilton

double precision              :: dk
double precision              :: Energy_k_kx, Energy_k_ky, Energy_k_kz ! Eigenvalue in (kx+0.01,ky), (kx,ky+0.01)
double precision              :: Velocity_x, Velocity_y, Velocity_z
!=========================================================================================

ALLOCATE(Hamilton)
!==========================================================================================================================================

   dk =0.01d0
!    CALL Hamilton_one_band_in_kx_ky_kz( kx, ky, kz, Hamilton)
   Energy_k = Hamilton_in_kx_ky_kz
   CALL Hamilton_one_band_in_kx_ky_kz( kx+dk, ky, kz, Hamilton)
   Energy_k_kx = Hamilton
   CALL Hamilton_one_band_in_kx_ky_kz( kx, ky+dk, kz, Hamilton)
   Energy_k_ky = Hamilton
   CALL Hamilton_one_band_in_kx_ky_kz( kx, ky, kz+dk, Hamilton)
   Energy_k_kz = Hamilton
!------------------------------------------------------------------------------------------------------------------------------------------
   Velocity_x = (Energy_k-Energy_k_kx)/dk
   Velocity_y = (Energy_k-Energy_k_ky)/dk
   Velocity_z = (Energy_k-Energy_k_kz)/dk
   Velocity = abs(Velocity_x) + abs(Velocity_y) + abs(Velocity_z)
!==========================================================================================================================================
   CALL Hamilton_one_band_in_kx_ky_kz( kx+DeltaXY, ky+DeltaXY, kz+DeltaXY, Hamilton)
   Energy_k_shifted =  Hamilton
   CALL Hamilton_one_band_in_kx_ky_kz( kx+dk+DeltaXY, ky+DeltaXY, kz+DeltaXY, Hamilton)
   Energy_k_kx = Hamilton
   CALL Hamilton_one_band_in_kx_ky_kz( kx+DeltaXY, ky+dk+DeltaXY, kz+DeltaXY, Hamilton)
   Energy_k_ky = Hamilton
   CALL Hamilton_one_band_in_kx_ky_kz( kx+DeltaXY, ky+DeltaXY, kz+dk+DeltaXY, Hamilton)
   Energy_k_kz = Hamilton
!------------------------------------------------------------------------------------------------------------------------------------------
   Velocity_x = (Energy_k_shifted-Energy_k_kx)/dk
   Velocity_y = (Energy_k_shifted-Energy_k_ky)/dk
   Velocity_z = (Energy_k_shifted-Energy_k_kz)/dk
   Velocity_shifted = abs(Velocity_x) + abs(Velocity_y) + abs(Velocity_z)
!==========================================================================================================================================
deallocate( Hamilton)

END SUBROUTINE Spectrum_and_Velocity_in_kx_ky_kz