subroutine total_energy_1

  use module_energy
  use module_impurity_1
  use module_parameters
  use module_integration

   implicit none

integer                       :: i, is
double precision              :: tmpd
double precision              :: fermi, FE
double precision, allocatable :: unK(:), unP(:)

   !=============================================================================
   ! Calculation of Total Energy
   allocate(unK(-nmax:nmax))
   allocate(unP(-nmax:nmax))
   unK(:) = 0.d0
   unP(:) = 0.d0

   if (nspin==1) then
      is = 1
      do i = -nmax, nmax
         FE = fermi( om(i), t, 0.d0 )
         unK(i) = FE*dimag( (om(i)-sig(is,i))*G(is,i) )
         unP(i) = FE*0.5d0*dimag( sig(is,i)*G(is,i) )
      end do
   else
      do i = -nmax, nmax
         FE = fermi( om(i), t, 0.d0 )
         unK(i) = FE* dimag( (om(i)+mu-sig(1,i))*G(1,i) + (om(i)+mu-sig(2,i))*G(2,i) )
         unP(i) = FE*0.5d0* dimag( sig(1,i)*G(1,i) + sig(2,i)*G(2,i))
      end do
   end if

   Kin = integration_simpson( -nmax, nmax, om(-nmax:nmax), unK(-nmax:nmax) )
   Pot = integration_simpson( -nmax, nmax, om(-nmax:nmax), unP(-nmax:nmax) )
 
   if (nspin==1) then
      Kin = -2.d0 * Kin/pi
      Pot = -2.d0 * Pot/pi
   else
      Kin = - Kin/pi
      Pot = - Pot/pi
   end if
   totalEn = Kin + Pot

   deallocate(unK, unP)

end subroutine total_energy_1
