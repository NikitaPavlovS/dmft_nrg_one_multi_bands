SUBROUTINE Hamilton_n_band_in_kx_ky( kx, ky, Hamilton)

  USE hamiltonian_module_nband
  IMPLICIT NONE

double precision,    intent(in) :: kx, ky
complex*16,         intent(out) :: Hamilton( 1:ndim, 1:ndim)
!===============================================================

      Hamilton(1,1) = dcmplx( Ed1 + 2.d0*tdd*(Cos(kx) + Cos(ky)), 0.d0)
      Hamilton(1,2) = dcmplx( 0.d0, 0.d0)
      Hamilton(1,3) = dcmplx( 0.d0, 0.d0)
      Hamilton(2,1) = dcmplx( 0.d0, 0.d0)
      Hamilton(2,2) = dcmplx( Ed2 + 2.d0*tdd*(Cos(kx) + Cos(ky)), 0.d0)
      Hamilton(2,3) = dcmplx( 0.d0, 0.d0)
      Hamilton(3,1) = dcmplx( 0.d0, 0.d0)
      Hamilton(3,2) = dcmplx( 0.d0, 0.d0)
      Hamilton(3,3) = dcmplx( Ed3 + 2.d0*tdd*(Cos(kx) + Cos(ky)), 0.d0)

!====================================================


END SUBROUTINE Hamilton_n_band_in_kx_ky
