! Simple Fortran program to demonstrate the use of the NRG-Fortran interface

program call_nrg
implicit none

! This is the maximal size of the fields, it must be set to a value
! accomodating the possible size of the fields. The value should be OK,
! but a test is necessary for actual applications 
integer,parameter :: nmax=500
double precision :: a
double precision :: b
double precision :: emax=5.   ! шкала энергий (-5,5)
double precision :: emin=1e-4 !ближайшая к нулю точка по энергии


!NRG needed variables

double precision :: om(-nmax:nmax) ! energy mesh
				   ! The code assumes a symmetric energy mesh,
 				   ! The first value !=0 will be used as energy scale.
double precision :: dd(-nmax:nmax) ! hybridization function -1/piIm(1/G+Sigma)
double precision :: t ! temperature in eV
                      !(later t defines number of NRG iteration N
                      ! (lam)^(-N/2)~=t
double precision :: lam ! NRG logarithmic discretization parameter
double precision :: u ! Coulomb interaction
double precision :: mu ! chemical potential
double precision :: occ ! occupancy ???
complex*16       :: sig(-nmax:nmax)! sig contains on exit of NRG the self-energy

integer          :: nkeep ! the number of states kept in the NRG iteration
		          ! nkeep=800 typical for one band problem
integer          :: iflag ! iflag=-1 initialize NRG computation
			  ! 	     (do once before perform NRG)
			  ! iflag=0 will produce no output
                          ! 0<iflag<10 will produce some output
                          ! iflag>10 will correct for acausal self-energies
			  !          (setting Im(Sig) to some small negative value)
			  ! see more details in in NRG_for_Fortran.C
integer          :: iover ! iover=0 on NRG exit keeps your energy mesh
			  !        self-energy interpolated (Hermite spline) to the mesh
    			  ! iover=1 Use final NRG mesh 
character*1      :: P     ! 'P' - particle-hole symmetry forced
			  ! For those familiar with the SIAM:
			  ! mu=-eps, where eps is the local energy of the SIAM.
			  ! Thus, mu=U/2 corresponds to particle-hole symmetry.
			  ! 'N' - une can use your mu
!character*1      :: S     ! 'S' - SU2 symmetry of the interaction term is forced

!DMFT variables

complex*16       :: G(-nmax:nmax)  ! Green function
complex*16       :: G_old(-nmax:nmax) ! Green function from previos DMFT iteration
double precision :: dos(-nmax:nmax),fermisrc

double precision :: qval ! desired occupancy
complex*16       :: PG
integer          :: i,iter
integer          :: NDMFT ! number of DMFT iteration
character*20     :: tmpstring
integer          :: new_culc     ! 1 - new calculation, 0 - read sig from sig.dat
double precision :: omsig,resig,imsig,tmp

! Set frequency and hybridization to some values.
! For DMFT, these can be read in from data sets.

b=log(emax/emin)/(nmax-1)
a=emin*exp(-b)

do i=-nmax,nmax
   dd(i)=1e-2
if (i.EQ.0) then
   om(i)=0e0
else
if(i.lt.0) then
   om(i)=-a*exp(-b*i)
else 
   om(i)=a*exp(b*i)
end if
end if
end do
    
   
! READ input DMFT(NRG) parameters
open(10,file='INPUT_DMFT',form='FORMATTED')
read(10,*)tmpstring
read(10,*)u,qval,t,NDMFT
write(*,*)u,qval,t,NDMFT
read(10,*)tmpstring
read(10,*)P
write(*,*)P
read(10,*)tmpstring
read(10,*)lam,nkeep
write(*,*)lam,nkeep
read(10,*)tmpstring
read(10,*)iover,iflag
write(*,*)iover,iflag
read(10,*)tmpstring
read(10,*)new_culc
write(*,*)new_culc
close(10)

!stop 'read'

mu=u/2+0.2405*u

! First call to NRG with iflag<0. Inititialize interal parameters like scale,
! actual temperature, etc. Will return the actually used t.

call nrg(P,'S',0.0,u,t,om,dd,occ,sig,nmax,lam,nkeep,-1,iover)
write(*,*)'Actual temperature is ',t

if(new_culc.eq.0) then
  open(10,file='sig.dat',form='FORMATTED')
  write(*,*)'Started new calculation with old Sigma'
  do i=-nmax,nmax
    read(10,*)omsig,tmp,tmp,tmp,tmp,resig,imsig
    sig(i)=dcmplx(resig,imsig)
    write(*,*)omsig,sig(i)
  end do
  sig=sig-mu
close(10)
endif



!stop 'initial NRG run'

! Initialize DMFT parameters
G(:)=(0.0,0.d0)
G_old(:)=(0.0,0.d0)
dos=0


!DMFT loop

do iter=1,NDMFT

!Lattice problem

do i=-nmax,nmax
   G(i)=PG(om(i),sig(i),mu-u/2.,G_old(i))
   dos(i)=-1.d0/3.14195*dimag(G(i))
!   write(99,*)om(i),dos(i)
   dd(i)=dimag(sig(i)+1.d0/G(i))
   G_old(i)=G(i)
!   write(23,*)om(i),dd(i)
end do
!write(99,*)
!write(23,*)

! Search for the chemical potential for given qval
!mu=u/2+fermisrc(dos,om,nmax,mu,qval,t,om(-nmax),om(nmax))

write(*,*)'Lattice problem mu=',mu

! Do an NRG run.
call nrg(P,'S',mu,u,t,om,dd,occ,sig,nmax,lam,nkeep,iflag,iover)

write(*,*)nmax

! Save sig and G for current iteration
do i=-nmax,nmax
!write(*,*)'2'

 write(10+iter,'(x,7(2x,e13.5))')om(i),dreal(G(i)),dimag(G(i)),&
                                      -1/3.14159*dimag(G(i)),0,&
                                      dreal(sig(i)),dimag(sig(i))
end do
sig=sig-mu

write(*,*)'Occ=',occ

enddo !DMFT loop

end program
