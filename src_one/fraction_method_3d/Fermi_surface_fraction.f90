SUBROUTINE Fermi_surface_fraction

  use energy_module
  use hamiltonian_module_1band
  use parameters_module
  use impurity_module

  IMPLICIT NONE

!-------------------------------------------------------------------
! INTERNAL variables
complex*16,       allocatable :: Hamilton
double precision, allocatable :: Awk_at_FL(:,:)
!double precision, allocatable :: Awk_at_FL_2(:,:)
complex*16                    :: PG

integer                       :: i, is, ix,iy,iz
double precision              :: dk
double precision, PARAMETER   :: kMin=0.d0
double precision              :: kMax
integer,            PARAMETER :: MaxK=1000

integer                       :: Number_of_elemet_in_Emesh_for_shift=0

double precision              :: Energy_k(1:3)               ! eigenvalue in (kx,ky), (kx+0.01,ky), (kx,ky+0.01)
double precision              :: Energy_k_shifted(1:3)       ! eigenvalue in (kx+pi,ky+pi), (kx+pi+0.01,ky+pi), (kx+pi,ky+pi+0.01)
double precision              :: Velocity, Velocity_shifted  ! velocity in (kx,ky) and in (kx+pi,ky+pi)

double precision, allocatable :: kx(:), ky(:), kz(:)

integer                       :: number_of_file

!EXTERNAL zgeev

ALLOCATE(Hamilton)
ALLOCATE(Awk_at_FL( 0:MaxK, 0:MaxK))
!ALLOCATE(Awk_at_FL_2(-nmax:nmax, 0:MaxK))
ALLOCATE(kx(0:MaxK))
ALLOCATE(ky(0:MaxK))

!make kx and ky mesh
kMax=pi
dk=(kMax - kMin)/MaxK
DO i = 0, MaxK
   kx(i)= kMin +dk*i
   ky(i)= kx(i)
   kz(i)= kx(i)
END DO
! search element number for energy array to do Energy shift in Fermi surface calculation
IF (abs(Energy_shift)>1E-6) THEN
   DO i = -nmax, (nmax-1)
      IF ((Energy_shift>=om(i)).and.(Energy_shift<om(i+1))) THEN
         Number_of_elemet_in_Emesh_for_shift = i
         !write(*,*) ' i = ', i
         !write(*,*) ' Number_of_elemet_in_Emesh_for_shift = ', Number_of_elemet_in_Emesh_for_shift
         !write(*,*) ' Energy_shift = ', Energy_shift
         !stop 'stop in Fermi surface in Number_of_elemet_in_Emesh_for_shift definition right'
      END IF
   END DO
   IF (Number_of_elemet_in_Emesh_for_shift.eq.0) THEN
       stop 'stop in Fermi surface in Number_of_elemet_in_Emesh_for_shift definition'
   END IF
END IF
!stop 'stop in Fermi surface'
!=========================================================================================================================================
Awk_at_FL = 0.d0
iz = 0
IF (Apply_Hand_Sigma_value) THEN
   !------------------------------------------------------------------------------------------------------------------
   IF (Pseudogap) THEN
      DO ix = 0, MaxK
         DO iy = 0, MaxK
            CALL Hamilton_one_band_in_kx_ky_kz( kx(ix), ky(iy), kz(iz), Hamilton)
            CALL Spectrum_and_Velocity_in_kx_ky_kz( Hamilton, om(Number_of_elemet_in_Emesh_for_shift), kx(ix), ky(iy), kz(iz), pi, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
            DO is=1, nspin
               CALL fract(ww, KP, om(Number_of_elemet_in_Emesh_for_shift), Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, sig(is,Number_of_elemet_in_Emesh_for_shift), comb)
               Awk_at_FL(ix,iy) = dimag( 1.d0/(dcmplx( om(Number_of_elemet_in_Emesh_for_shift) + mu, 0.d0) -Hamilton -PG -dcmplx(dreal(sig(is,Number_of_elemet_in_Emesh_for_shift)),-Hand_Sigma_value) ) )
            END DO
            !stop 'stop mu in FS'
         END DO
      END DO
   !------------------------------------------------------------------------------------------------------------------
   ELSE
      DO ix = 0, MaxK
         DO iy = 0, MaxK
            CALL Hamilton_one_band_in_kx_ky_kz( kx(ix), ky(iy), kz(iz), Hamilton)
            DO is=1, nspin
               Awk_at_FL(ix,iy) = dimag( 1.d0/(dcmplx( om(Number_of_elemet_in_Emesh_for_shift) + mu, 0.d0) - Hamilton - dcmplx(dreal(sig(is,Number_of_elemet_in_Emesh_for_shift)),-Hand_Sigma_value) ) )
            END DO
         END DO
      END DO
   END IF
   !------------------------------------------------------------------------------------------------------------------
ELSE
   !------------------------------------------------------------------------------------------------------------------
   IF (Pseudogap) THEN
      DO ix = 0, MaxK
         DO iy = 0, MaxK
            CALL Hamilton_one_band_in_kx_ky_kz( kx(ix), ky(iy), kz(iz), Hamilton)
            CALL Spectrum_and_Velocity_in_kx_ky_kz( Hamilton, om(Number_of_elemet_in_Emesh_for_shift), kx(ix), ky(iy), kz(iz), pi, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
            DO is=1, nspin
               CALL fract(ww, KP, om(Number_of_elemet_in_Emesh_for_shift), Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, sig(is,Number_of_elemet_in_Emesh_for_shift), comb)
               Awk_at_FL(ix,iy) = dimag( 1.d0/(dcmplx( om(Number_of_elemet_in_Emesh_for_shift) + mu, 0.d0) -Hamilton -PG -sig(is,Number_of_elemet_in_Emesh_for_shift) ) )
            END DO
         END DO
      END DO
   !------------------------------------------------------------------------------------------------------------------
   ELSE
      DO ix = 0, MaxK
         DO iy = 0, MaxK
            CALL Hamilton_one_band_in_kx_ky_kz( kx(ix), ky(iy), kz(iz), Hamilton)
            DO is=1, nspin
               Awk_at_FL(ix,iy) = dimag( 1.d0/(dcmplx( om(Number_of_elemet_in_Emesh_for_shift) + mu, 0.d0) -Hamilton -sig(is,Number_of_elemet_in_Emesh_for_shift) ) )
            END DO
         END DO
      END DO
   END IF
   !------------------------------------------------------------------------------------------------------------------
END IF
Awk_at_FL(:,:) = -1.d0/pi * Awk_at_FL(:,:)
!------------------------------------------------------------------------------------------------------------------
if (iter<NDMFT) then
   number_of_file = 800+iter
   DO ix = 0, MaxK
      DO iy = 0, MaxK
         write(number_of_file,'(2x,3(2x,F15.8))') kx(ix), ky(iy), Awk_at_FL(ix,iy)
      END DO
      write(number_of_file,*)
   END DO
else
   OPEN(Unit = 100, file='OUT_Fermi_surface.dat', form='FORMATTED')
   DO ix = 0, MaxK
   DO iy = 0, MaxK
      write(100,'(2x,3(2x,F15.8))') kx(ix), ky(iy), Awk_at_FL(ix,iy)
   END DO
   write(100,*)
   END DO
   CLOSE(100)
end if

deallocate( Hamilton, kx, ky, Awk_at_FL)

END SUBROUTINE Fermi_surface_fraction