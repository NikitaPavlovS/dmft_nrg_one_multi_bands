SUBROUTINE Spectrum_and_Velocity_in_kx_ky( Hamilton_in_kx_ky, kx, ky, DeltaXY,&
                                           Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)

   use hamiltonian_module_1band

implicit none

! calculate spectrum for interacting state by fomula Ek=(E-1)/G_dd(E,k)
! EXTERNAL variables
double precision,  intent(in) :: Hamilton_in_kx_ky
double precision,  intent(in) :: kx, ky
double precision,  intent(in) :: DeltaXY          ! value of shifting for kx and ky
! Pseudogap variables
double precision, intent(out) :: Energy_k         ! Eigenvalue in (kx,ky)
double precision, intent(out) :: Energy_k_shifted ! Eigenvalue in (kx+pi,ky+pi)
double precision, intent(out) :: Velocity         ! velocity in (kx,ky)
double precision, intent(out) :: Velocity_shifted ! velocity in (kx+pi,ky+pi)
!===============================================
! INTERNAL variables
complex*16,       allocatable :: Hamilton

double precision              :: dk
double precision              :: Energy_k_kx, Energy_k_ky ! Eigenvalue in (kx+0.01,ky), (kx,ky+0.01)
double precision              :: Velocity_x, Velocity_y
!=========================================================================================

ALLOCATE(Hamilton)
!==========================================================================================================================================

   dk =0.01d0
!    CALL Hamilton_in_kx_ky_tpd1_no_delta( kx, ky, Hamilton)
   Energy_k = Hamilton_in_kx_ky
   CALL Hamilton_one_band_in_kx_ky( kx+dk, ky, Hamilton)
   Energy_k_kx = Hamilton
   CALL Hamilton_one_band_in_kx_ky( kx, ky+dk, Hamilton)
   Energy_k_ky = Hamilton
!------------------------------------------------------------------------------------------------------------------------------------------
   Velocity_x = (Energy_k-Energy_k_kx)/dk
   Velocity_y = (Energy_k-Energy_k_ky)/dk
   Velocity = abs(Velocity_x) + abs(Velocity_y)
!==========================================================================================================================================
   CALL Hamilton_one_band_in_kx_ky( kx+DeltaXY, ky+DeltaXY, Hamilton)
   Energy_k_shifted =  Hamilton
   CALL Hamilton_one_band_in_kx_ky( kx+dk+DeltaXY, ky+DeltaXY, Hamilton)
   Energy_k_kx = Hamilton
   CALL Hamilton_one_band_in_kx_ky( kx+DeltaXY, ky+dk+DeltaXY, Hamilton)
   Energy_k_ky = Hamilton
!------------------------------------------------------------------------------------------------------------------------------------------
   Velocity_x = (Energy_k_shifted-Energy_k_kx)/dk
   Velocity_y = (Energy_k_shifted-Energy_k_ky)/dk
   Velocity_shifted = abs(Velocity_x) + abs(Velocity_y)
!==========================================================================================================================================
deallocate( Hamilton)

END SUBROUTINE Spectrum_and_Velocity_in_kx_ky