SUBROUTINE Bands_in_directions_and_Bare_Fermi_surface_fraction

  use energy_module
  use hamiltonian_module_nband
  use parameters_module
!  use impurity_module

IMPLICIT NONE

!============================================
! INTERNAL variables
complex*16,       allocatable :: Hamilton(:,:)
complex*16,       allocatable :: EigenValues(:)
double precision, allocatable :: Coefficient(:,:)
 character*1                  :: compute_eigenvector
complex*16,       allocatable :: EigenVectors_right(:,:)
complex*16,       allocatable :: EigenVectors_left(:,:)
complex*16,       allocatable :: EigenValues_cekord(:)
complex*16,       allocatable :: EigenVectors_right_cekord(:,:)
! need for lapack procedures for matrix
complex*16,       allocatable :: WORK(:)      
double precision, allocatable :: RWORK(:)
integer                       :: INFO
!--------------------------------------------------------------------------------
integer                       :: i, j, l, m, ik
double precision              :: dk             ! k step
double precision, PARAMETER   :: kMin=0.d0
double precision              :: kMax
integer,          PARAMETER   :: MaxK=200
double precision              :: kx, ky         ! variable for symmetric directions in Brilloun zone
double precision, allocatable :: kpoint_for_bands(:)
double precision, allocatable :: kxa(:), kya(:) ! mesh of k points
 character(64)                :: string_1, string_2

!logical, parameter            :: Hamilton_with_sim_anti=1
!--------------------------------------------------------------------------------
! for bare Fermi surface
logical                       :: EigenValues_Fermi
double precision              :: value_prev, value_now
double precision              :: a_left(1:2), b_right(1:2), c_medium(1:2)
!======================================================================================
ALLOCATE(Hamilton( 1:ndim, 1:ndim))
ALLOCATE(EigenValues(1:ndim))
ALLOCATE(EigenVectors_right(1:ndim, 1:ndim))
ALLOCATE(EigenVectors_left(1:ndim, 1:ndim))
ALLOCATE(WORK(1:5*ndim))
ALLOCATE(RWORK(1:2*ndim))
ALLOCATE(EigenValues_cekord(1:ndim))
ALLOCATE(EigenVectors_right_cekord(1:ndim, 1:ndim))
!======================================================================================

kMax=pi

IF (Bands_plot) THEN
  ALLOCATE(kxa(0:3*MaxK))
  ALLOCATE(kya(0:3*MaxK))
  ALLOCATE(kpoint_for_bands(0:3*MaxK))
  !make kx and ky mesh
  dk=(kMax - kMin)/MaxK
  !direction (0,0) -- (pi,0)
  do ik = 0, MaxK
     kxa(ik) = kMin +dk*ik
     kya(ik) = 0.d0
     kpoint_for_bands(ik) = kxa(ik)
  end do
  !direction (pi,0) -- (pi,pi)
  do ik = MaxK, 2*MaxK-1
  	kxa(ik) = kMax
  	kya(ik) = kxa(ik-MaxK)
  	kpoint_for_bands(ik) = kMax+kya(ik)
  end do
  !direction (pi,pi) -- (0,0)
  do ik = 2*MaxK, 3*MaxK
  	kxa(ik) = kxa(3*MaxK-ik)
  	kya(ik) = kxa(ik)
  	kpoint_for_bands(ik) = 2.d0*kMax+dsqrt(2.d0)*kxa(ik-2*MaxK)
  end do
  
  OPEN(Unit = 98, FILE = 'bands.dat', FORM='FORMATTED')
  OPEN(Unit = 97, FILE = 'bands_kpoints.dat', FORM='FORMATTED')
  if (FBANDS) then
     OPEN(Unit = 99, FILE = 'bands_check_probability.dat', FORM='FORMATTED')
     ALLOCATE(Coefficient( 1:ndim, 1:ndim ))
     compute_eigenvector = 'V'
     do i=1, ndim
        do j=1, ndim
           write(string_1, '(I2)') i
           write(string_2, '(I2)') j
           string_2 = 'band_'//trim(adjustl(string_1))//'_state_'//trim(adjustl(string_2))//'.dat'
           OPEN(Unit = 20+i+ndim*(j-1), FILE = string_2, FORM='FORMATTED')
       end do
     end do
  else
      compute_eigenvector = 'N'
  end if
  !==============================================================================================================
  DO ik = 0, 3*MaxK
        write(97,'(2(2x,F10.5))') kxa(ik), kya(ik)
  	! searching Hamilton eigenvalues
  	CALL Hamilton_n_band_in_kx_ky_mu( kxa(ik), kya(ik), mu, Hamilton(1:ndim, 1:ndim))
  	CALL ZGEEV( 'N', compute_eigenvector, ndim, Hamilton, ndim, EigenValues(1:ndim),&
                    EigenVectors_left(1:ndim, 1:ndim), ndim, EigenVectors_right(1:ndim, 1:ndim),&
                    ndim, WORK(1:5*ndim), 5*ndim, RWORK(1:2*ndim), INFO )
        ! sorting EigenValues
        CALL cekord( EigenValues(1:ndim), EigenValues_cekord(1:ndim), EigenVectors_right(1:ndim, 1:ndim), EigenVectors_right_cekord(1:ndim, 1:ndim),  ndim )
  	write(98, '(100(2x,F15.8))') kpoint_for_bands(ik), (dreal(EigenValues_cekord(i)), i=1,ndim)
  	if (FBANDS) then
           do i=1, ndim
  	      do j=1, ndim
                 Coefficient(i,j) = (abs(EigenVectors_right_cekord(j,i)))**2
  		 write(20+i+ndim*(j-1), '(3(2x,F15.8))') kpoint_for_bands(ik), dreal(EigenValues_cekord(i)), 0.1*Coefficient(i,j)
  	      end do
  	   end do
  	   do i=1, ndim
  	      write(99,'(A33,I2,A3,F12.8)') 'sum of state probability in band ',i,' = ', sum(Coefficient(i,1:ndim))
  	   end do
  	end if
  END DO!
  
  deallocate( kxa, kya)
  if (FBANDS) then
     deallocate( Coefficient)
!     do i=1, ndim
!       do j=1, ndim
!  	  write(20+i+ndim*(j-1), *)
!       end do
!     end do
!     write(99,*)
     close(99)
  end if
!  write(98,*)
  close(97)
  close(98)
  
END IF !Bands_plot
!======================================================================================
!======================================================================================
IF (Fermi_Surface_without_Interaction) THEN
    dk=(kMax - kMin)/MaxK
    compute_eigenvector = 'N'
   ! to file OUT_Fermi_surface_bare.dat write bare Fermi surface
   ! search by diagonal half divided methods
   OPEN(Unit = 900, file='OUT_Fermi_surface_bare.dat', form='FORMATTED')
   DO i = 0, 2*MaxK
      j=0
      DO WHILE ((j.eq.0).or.(.NOT.((abs(kMax-ky)<1E-5).or.(abs(kMax-kx)<1E-5))))
         IF (i<MaxK) THEN
            kx= kMin +dk*j
            ky= kMax +dk*(j-i)
            j=j+1
         ELSE
            kx= kMin +dk*((i-MaxK)+j)
            ky= kMin +dk*j
            j=j+1
         END IF
         b_right(1) = kx
         b_right(2) = ky
         write(901,'(2x,2(2x,F15.8))') kx, ky

         CALL Hamilton_n_band_in_kx_ky_mu( kx, ky, mu, Hamilton(1:ndim, 1:ndim))
         CALL ZGEEV( 'N', compute_eigenvector, ndim, Hamilton, ndim, EigenValues(1:ndim),&
                    EigenVectors_left(1:ndim, 1:ndim), ndim, EigenVectors_right(1:ndim, 1:ndim),&
                    ndim, WORK(1:5*ndim), 5*ndim, RWORK(1:2*ndim), INFO )
         ! sorting EigenValues
         CALL cekord( EigenValues(1:ndim), EigenValues_cekord(1:ndim), EigenVectors_right(1:ndim, 1:ndim), EigenVectors_right_cekord(1:ndim, 1:ndim),  ndim )
         
         IF (j.eq.0) THEN
            value_prev = dreal(EigenValues_cekord(number_of_band_crossing_FL))
            a_left(1) = kx
            a_left(2) = ky
            IF (abs(value_prev)<1E-6) THEN
                write(900,'(2(2x,F15.8))') kx, ky
            END IF
         ELSE
            value_now = dreal(EigenValues_cekord(number_of_band_crossing_FL))
            IF (value_prev*value_now<0) THEN
              IF (1) THEN
               EigenValues_Fermi=0

               DO WHILE (.NOT.EigenValues_Fermi)
                  c_medium(1) = (a_left(1) + b_right(1))/2.d0
                  c_medium(2) = (a_left(2) + b_right(2))/2.d0
                  CALL Hamilton_n_band_in_kx_ky_mu( c_medium(1), c_medium(2), mu, Hamilton(1:ndim, 1:ndim))
                  CALL ZGEEV( 'N', compute_eigenvector, ndim, Hamilton, ndim, EigenValues(1:ndim),&
                            EigenVectors_left(1:ndim, 1:ndim), ndim, EigenVectors_right(1:ndim, 1:ndim),&
                            ndim, WORK(1:5*ndim), 5*ndim, RWORK(1:2*ndim), INFO )
                  CALL cekord( EigenValues(1:ndim), EigenValues_cekord(1:ndim), EigenVectors_right(1:ndim, 1:ndim), EigenVectors_right_cekord(1:ndim, 1:ndim),  ndim )
                  IF (value_prev*dreal(EigenValues_cekord(number_of_band_crossing_FL))<0) THEN
                     b_right(1)=c_medium(1)
                     b_right(2)=c_medium(2)
                  ELSE
                     a_left(1)=c_medium(1)
                     a_left(2)=c_medium(2)
                     value_prev = dreal(EigenValues_cekord(number_of_band_crossing_FL))
                  END IF
                  IF (abs(dreal(EigenValues_cekord(number_of_band_crossing_FL)))<1E-6) THEN
                     EigenValues_Fermi=1
                     write(900,'(2(2x,F15.8))') c_medium(1), c_medium(2)
                  END IF
               END DO
              !------------------------------------------------------------------------------------------------------------------
              ELSE
                  IF (value_now<value_prev) THEN
                     write(900,'(2(2x,F15.8))') b_right(1), b_right(2)
                  ELSE
                     write(900,'(2(2x,F15.8))') a_left(1), a_left(2)
                  END IF
              END IF
              !------------------------------------------------------------------------------------------------------------------
            END IF
            value_prev = value_now
         END IF
         a_left(1) = kx
         a_left(2) = ky
      END DO
   END DO
   CLOSE(900)
END IF
!========================================================================================================================================

!stop 'stop in Bands_in_symmetric_direction END'

deallocate( Hamilton, EigenValues, EigenVectors_right, EigenVectors_left, WORK, RWORK)
deallocate( EigenValues_cekord, EigenVectors_right_cekord)

END SUBROUTINE Bands_in_directions_and_Bare_Fermi_surface_fraction


!------------------------------------------------------------------------------------------------------------------
!     IF (.NOT.Hamilton_with_sim_anti) THEN ! for three band
!     do j=1, ndim
!        EigenVectors_right(2,j) = 1.d0/sqrt(2.d0) * ( EigenVectors_right_cekord(2,j) + EigenVectors_right_cekord(3,j) )
!        EigenVectors_right(3,j) = 1.d0/sqrt(2.d0) * ( EigenVectors_right_cekord(2,j) - EigenVectors_right_cekord(3,j) )
!     end do
!     END IF
!------------------------------------------------------------------------------------------------------------------
