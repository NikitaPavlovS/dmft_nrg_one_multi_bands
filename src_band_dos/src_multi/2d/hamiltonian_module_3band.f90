module hamiltonian_module_Nband

  implicit none
  public
!----------------------------------------------------------------------------------------------
! Hamilton variables
integer,          save :: ndim = 3   ! dimension of Hamilton
double precision, save :: Ed, Ep     ! energy parameters
double precision, save :: tdd, tpd, tpd1, tpd2, tpp, tpp1 ! hopping parameters
double precision, save :: delta=0.005d0               ! delta for calculate Green function (small image part)
!----------------------------------------------------------------------------------------------
!Green function
double precision, allocatable, save :: dos(:,:)       ! -1/pi ImG      (spin and energy)
double precision, allocatable, save :: totalDOS(:,:)  ! total dos as energy mesh  (spin and energy)
complex*16,       allocatable, save :: sig(:,:,:) ! Sigma (spin,ndim,iE)
!----------------------------------------------------------------------------------------------

end module hamiltonian_module_Nband
