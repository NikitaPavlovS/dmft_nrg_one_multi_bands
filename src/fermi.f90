!---------------------------------------------------------------------|
!                                                                     |
! Value of Fermi distribution in energy point ee for temperature temp |
!                                                                     |
!---------------------------------------------------------------------|

double precision function fermi(ee,tev,mu)
    implicit none
!---> Passed parameters
      double precision,intent(in):: tev,ee,mu
!---> Local parameters
      double precision:: dco,beta

  if ( tev>1E-20 ) then 
      dco = 200.d0*tev
      beta = 1.d0/tev

      if ( (ee-mu) .gt. dco ) then
        fermi = 0.d0
      else
        if ( (ee-mu) .lt. (-1.d0*dco) ) then
          fermi = 1.d0
        else
          fermi = 1.d0/( dexp(beta*(ee-mu)) + 1.d0 )
        end if
      end if
  else
      if ((ee-mu)>0.0) then
        fermi = 0.d0
      else
        fermi = 1.d0
      end if
  end if
       
end function fermi
