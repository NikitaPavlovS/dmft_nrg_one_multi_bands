! Simple Fortran program to demonstrate the use of the NRG-Fortran interface

program One_band_magnetic_DOS_and_BANDS

  use energy_module
  use hamiltonian_module_1band
  use parameters_module

implicit none

complex*16, allocatable :: Gtmp(:,:)
integer                 :: i,j, is

complex*16,       allocatable :: z(:)
double precision, allocatable :: b(:),c(:)
!----------------------------------------------------------------------------------------------

!=========================================================================================================================================
Call read_input_1band
!=========================================================================================================================================
Call initialize_variables_1band
!=========================================================================================================================================
!stop 'stop init'

!=========================================================================================================================================
SELECT CASE (Method_of_Green_Function_calculation)
!======================================================================================
CASE (0)
   IF (DOS_plot) THEN
      if ((nspin==2).and.(H_field>1E-6)) then
         sig(1,:) = sig(1,:) - dcmplx(H_field/2.d0, 0.d0)
         sig(2,:) = sig(2,:) + dcmplx(H_field/2.d0, 0.d0)
      end if
      allocate(Gtmp(1:nspin,-nmax:nmax))
      OPEN(Unit = 14, file='DOS.dat', form='FORMATTED')
         do i=-nmax,nmax
          if (1) then
            allocate(z(1:nspin),b(1:nspin),c(1:nspin))
            Gtmp(:,i) = (0.d0,0.d0)
            do j=-nmax,nmax
             do is=1, nspin
                   c(is) = (dos_0(is,j+1) - dos_0(is,j))/(om(j+1)-om(j))
                   b(is) = dos_0(is,j) - c(is)*om(j)
                   z(is) = dcmplx(om(i)+mu,delta)-sig(is,i)
                   Gtmp(is,i) = Gtmp(is,i) - c(is)*(om(j+1)-om(j)) - (b(is) + c(is)*z(is)) * cdlog(1.d0 - (om(j+1)-om(j))/(z(is) - om(j)))
             end do
            end do
            deallocate(z,b,c)
          else
            Gtmp(:,i) = (0.d0,0.d0)
            do j=-nmax,nmax-1
             do is=1, nspin
                Gtmp(is,i) = Gtmp(is,i) + ( dos_0(is,j+1)/(dcmplx(om(i)-om(j+1),delta)-sig(is,j+1)) - dos_0(is,j)/(dcmplx(om(i)-om(j),delta)-sig(is,j)) )*(om(j+1)-om(j))/2.d0
             end do
            end do
            Gtmp(is,i) = Gtmp(is,i) + ( dos_0(is,-nmax)/(dcmplx(om(i)-om(-nmax),delta)-sig(is,-nmax)) - dos_0(is,nmax)/(dcmplx(om(i)-om(nmax),delta)-sig(is,nmax)) )*(om(nmax)-om(nmax-1))/2.d0
          end if
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,i))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            write(14,'(2x,100(2x,f20.15))') om(i), (dos(is,i),is=1,nspin), totalDOS(i)
         end do
      close(14)
      Call occupancy_and_mu_search_one_band
      deallocate(Gtmp)
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Bands_plot.or.Fermi_Surface_without_Interaction) THEN
      CALL Bands_in_directions_and_Bare_Fermi_surface_fraction
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Spectral_Function_plot.OR.Spectral_Function_maximum_plot) THEN
      CALL Spectral_Function_in_symmetric_direction_fraction
   END IF
!----------------------------------------------------
CASE (1)
   IF (DOS_plot) THEN
      if ((nspin==2).and.(H_field>1E-6)) then
         sig(1,:) = sig(1,:) - dcmplx(H_field/2.d0, 0.d0)
         sig(2,:) = sig(2,:) + dcmplx(H_field/2.d0, 0.d0)
      end if
      allocate(Gtmp(1:nspin,-nmax:nmax))
      OPEN(Unit = 14, file='DOS.dat', form='FORMATTED')
         do i=-nmax,nmax
            Call GreenFunction_of_E_Sigma_mu_fraction( om(i), sig(:,i), mu, Gtmp(:,i))
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:,i))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            write(14,'(2x,100(2x,f20.15))') om(i), (dos(is,i),is=1,nspin), totalDOS(i)
         end do
      close(14)
      Call occupancy_and_mu_search_one_band
      deallocate(Gtmp)
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Bands_plot.or.Fermi_Surface_without_Interaction) THEN
      CALL Bands_in_directions_and_Bare_Fermi_surface_fraction
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Spectral_Function_plot.OR.Spectral_Function_maximum_plot) THEN
      CALL Spectral_Function_in_symmetric_direction_fraction
   END IF
!======================================================================================
CASE DEFAULT
     stop 'error in selection of Green function method after DMFT'
END SELECT
!======================================================================================

!==========================================================================================================================================

deallocate(dos,dos_0, om, totalDOS, sig)

end program One_band_magnetic_DOS_and_BANDS
