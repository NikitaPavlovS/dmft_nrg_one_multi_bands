module module_impurity_n

  implicit none
  public

double precision,              save :: t     ! temperature in eV
                                             !(later t defines number of NRG iteration N
                                             ! (lam)^(-N/2)~=t

!--------------------------------------------------------------------------------------------
complex*16,       allocatable, save :: G(:,:,:)      ! Green function
complex*16,       allocatable, save :: sig(:,:)      ! sig contains on  of NRG the self-energy
double precision, allocatable, save :: dd(:,:)       ! hybridization function -1/piIm(1/G+Sigma)
double precision, allocatable, save :: dos(:,:)      ! -1/pi ImG      (spin and energy)
double precision, allocatable, save :: totalDOS(:)   ! total dos as energy mesh  (spin and energy)
double precision, allocatable, save :: dos_0(:)

double precision,              save :: delta=0.005d0  ! delta for calculate Green function (small image part)
! Occupancy
double precision, allocatable, save :: occupancy_d(:)
double precision,              save :: occupancy_total
double precision,              save :: Magnetic_moment

!----------------------------------------------------------------------------------------------
!Double-counting variables
logical,                       save :: hole_occupancy=.false. ! define sing before Edc by calculation through n_d
double precision,              save :: Edc = 0.d0             ! Value of Double Counting energy
logical,                       save :: Edc_const = .true.     ! Constant value (T) or Calculated from occupancy (F)
logical,                       save :: start_Edc = .true.     ! need Start from Other Edc:   yes (T), not (F)
double precision,              save :: start_Edc_value = 0.d0 ! Value of starting Double Counting energy

end module module_impurity_n
