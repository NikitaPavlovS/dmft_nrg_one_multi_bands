# -*- coding: utf-8 -*-
# pns 07.2017

from lib_for_DMFT_NRG import *

p={}

# What is model used: Bethe (1), FCC (2), from DOS(0)
# !!! main parameter !!!
p['band_model'] = 1
p['n_spin'] = 1
p['H_field'] = [0.0]

#---- INTERACTION ----
T_arrya = []

#---- OCCUPANCY ----
# What is total occupancy needed in result of calculation ?
p['occupancy'] = 0.5
# tolerance of occupancy convergence in expression 1.0D-%d
p['occupancy_tolerance'] = 4

#---- DMFT convergence ----
# number of DMFT iterations
p['it'] = 40
# number of DMFT iterations after tolerance satisfaction
p['it_after_tolerance'] = 10
# number of different DMFT calculation with one temperature
p['iters_of_DMFT_continue'] = 5

#---- CHEMICAL POTENTIAL ----
p['mu_start'] = -0.494812223907229


#--------------------------------------------------------------------
#---- DMFT NRG program files ----
p['prog_DMFT'] = 'DMFT_1_band_9.run'
p['name_script_file'] = 'DMFT_RUN.sh'
p['name_input_file_1'] = 'DMFT_INPUT_1_band'
p['name_input_file_2'] = 'DMFT_INPUT_1_band'
p['name_output_file'] = 'DMFT_out'
p['files_to_save'] = [p['name_input_file'],p['name_output_file'],'NRG_out',p['name_script_file'],'sig.dat']

#--------------------------------------------------------------------

DMFT_QMC_T_cycle(U_start=U_start, U_end=U_end, dU=dU, temperature_start=temperature_start, **p)
