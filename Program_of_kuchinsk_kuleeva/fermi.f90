!      double precision function fermi(ee,temp,mu)
      double precision function fermi(ee,tev,mu)
      implicit none
!---------------------------------------------------------------------|
!                                                                     |
! Value of Fermi distribution in energy point ee for temperature temp |
!                                                                     |
!---------------------------------------------------------------------|

!---> Passed parameters
      double precision,intent(inout):: tev,ee,mu
!---> Local parameters
      double precision:: dco,beta

!      tev = 8.617346815d-5*temp
      dco=200.d0*tev
      beta=1.d0/tev

      if((ee-mu).gt.dco) then
        fermi=0.d0
        else if((ee-mu).lt.-1.d0*dco) then
          fermi=1.d0
          else
            fermi=1.d0/(dexp(beta*(ee-mu))+1.d0)
       end if
       
       end
