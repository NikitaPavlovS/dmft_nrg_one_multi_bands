module module_energy

  implicit none
  public

!----------------------------------------------------------------------------------------------
!Energy mesh variables
integer,                       save :: nmax      ! 0.5*number of energy mesh points
double precision, allocatable, save :: om(:)     ! energy mesh
                                                 ! The code assumes a symmetric energy mesh,
                                                 ! The first value !=0 will be used as energy scale.
logical,          save :: exp_mesh = .false.     ! Linear (T) or Exponential (F)
integer,          save :: Emax = 10              ! if previous F: Maximum Energy in eV (integer value)
integer,          save :: factor_of_step = 100   ! 1/(energy step)
double precision, save :: Emin_exp_mesh = 1E-8   ! Value of minimum energy for exp mesh
!----------------------------------------------------------------------------------------------
! Chemical potential
double precision, save :: mu = 0.d0              ! chemical potential
logical,          save :: variable_mu = .false.  ! Variable => search mu for given n_total_need
double precision, save :: mu_tolerance = 1E-4    ! If variable_mu, what the smallest change in mu
logical,          save :: start_from_read_mu = .false.
double precision, save :: n_total_need = 1.d0    ! What total occupancy need in result of calculation
double precision, save :: part_of_dmu = 1.d0      ! mu = mu + part_of_dmu * delta_mu
integer,          save :: iter_each_change_mu    ! number of iter at change mu (2 -- even iter chenge mu)

logical,          save :: mu_changed=.true.

end module module_energy