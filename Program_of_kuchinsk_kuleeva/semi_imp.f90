!=================================================
      FUNCTION PG(E,Eii,mu,Gii)
      implicit none

! in ! E : real frequencies
! in ! Eii : Self-energy
! in ! mu : Chemical potential
! in ! D : polu shirina zoni
! out! Gii : local Green Function

! Passed variables
      double precision,     intent(in)  :: E
      double precision,     intent(in)  :: mu
      complex*16                        :: Eii
      complex*16,           intent(in)  :: Gii
      complex*16                        :: PG

! Local variables
      double precision              :: D,imp
      complex*16                    :: a,Et
      character*8                   :: tmpstring

!---Read the HMLT prefix------------------------------------

      open(10,file='INPUT_PG',form='formatted')

      read(10,*) tmpstring
      read(10,*) D,imp
!      write(6,*) D,imp
!-------------------------------------------------

!      write(*,*)'semiimp mu=',mu
      Et=E+mu-Eii-imp*imp*Gii+dcmplx(0.d0,1.d-10)

      a=(Et+SQRT(Et*Et-D*D))/D
      IF (DIMAG(a).GT.0.) a=1/a

      PG=2.d0*a/D

!      write(99,*)PG,imp*Gii,imp,D,Gii

!-------------------------------------------------
      close(10)
!-------------------------------------------------
      end FUNCTION PG
!=================================================


