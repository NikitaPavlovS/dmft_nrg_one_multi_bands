subroutine initialize_variables

  use energy_module
  use hamiltonian_module_Nband
  use parameters_module

  implicit none

double precision, allocatable :: temp_array(:)
integer                       :: nmax_temp
double precision              :: tmpreal
double precision              :: tmp_Im_Sigma(1:nspin), tmp_Re_Sigma(1:nspin)
integer                       :: i, is, in
  character(64) :: file_name

!=========================================================================================================================================
nmax_temp = 100000
allocate(temp_array(-nmax_temp:nmax_temp))

Call Energy_mesh(temp_array, nmax_temp)

allocate(om(-nmax:nmax))
DO i=-nmax,nmax
   om(i) = temp_array(i)
END DO

deallocate(temp_array)

!------------------------------------------------------------------------------------------------------------------------------------------
allocate(dos(1:nspin,-nmax:nmax))
allocate(totalDOS(1:nspin,-nmax:nmax))
allocate(sig(1:nspin,1:ndim,-nmax:nmax))

IF (INIT_Sigma) THEN
  do in=1, ndim
   write(file_name,*) in
   file_name='Start_sigma_'//trim(adjustl(file_name))//trim(adjustl(file_name))//'.dat'
   open(10,file=file_name,form='FORMATTED')
!-----------------------------------
   IF (nmax_init==nmax) THEN
      do i=-nmax_init,nmax_init
         read(10,*) tmpreal,   ( (tmp_Re_Sigma(is), tmp_Im_Sigma(is)), is=1,nspin )
         do is=1, nspin 
            sig(is,in,i) = dcmplx( tmp_Re_Sigma(is), tmp_Im_Sigma(is) )
         end do
      end do
!-----------------------------------
   ELSE
    IF (nmax_init<nmax) THEN ! write missing value for sig array (analogy if no INIT_Sigma)
      do i=-nmax_init,nmax_init
         read(10,*) tmpreal,   ( (tmp_Re_Sigma(is), tmp_Im_Sigma(is)), is=1,nspin )
         do is=1, nspin 
            sig(is,in,i) = dcmplx( tmp_Re_Sigma(is), tmp_Im_Sigma(is) )
         end do
      end do
      do i=-nmax,-(nmax_init+1)
         sig(:,in,i) = dcmplx(0.d0,0.d0)
         sig(:,in,-i) = dcmplx(0.d0,0.d0)
      end do
!-----------------------------------
    ELSE  ! nmax_init>nmax
      do i=-nmax_init,-(nmax+1)
         read(10,*)
      end do
      do i=-nmax,nmax
         read(10,*) tmpreal,   ( (tmp_Re_Sigma(is), tmp_Im_Sigma(is)), is=1,nspin )
         do is=1, nspin 
            sig(is,in,i) = dcmplx( tmp_Re_Sigma(is), tmp_Im_Sigma(is) )
         end do
      end do
!-----------------------------------
    END IF
   END IF
   close(10)
 end do
ELSE
   sig = dcmplx(0.d0,0.d0)
END IF

!----------------------------------------------------
   dos = 0.d0
   totalDOS = 0.d0

!stop 'stop INIT'

end subroutine initialize_variables