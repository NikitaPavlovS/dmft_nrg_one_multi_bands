SUBROUTINE Hamilton_one_band_in_kx_ky_kz_mu(  kx, ky, kz, mu, Hamilton)

  USE hamiltonian_module_1band
  IMPLICIT NONE

double precision,    intent(in) :: kx, ky, kz
double precision,    intent(in) :: mu
complex*16,         intent(out) :: Hamilton
!===============================================================
Hamilton = dcmplx(-mu + 2.d0*tt*( Cos(kx) + Cos(ky) + Cos(kz) ) + 4.d0*t1*(Cos(kx)*Cos(ky) + Cos(kx)*Cos(kz) + Cos(ky)*Cos(kz) ), 0.d0)

END SUBROUTINE Hamilton_one_band_in_kx_ky_kz_mu
