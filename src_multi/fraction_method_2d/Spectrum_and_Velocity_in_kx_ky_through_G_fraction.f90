SUBROUTINE Spectrum_and_Velocity_in_kx_ky_through_G_fraction( Hamilton_in_kx_ky, E, kx, ky, mu, DeltaXY,&
                                                              Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)

   use hamiltonian_module_nband

implicit none

! calculate spectrum for interacting state by fomula Ek=(E-1)/G_dd(E,k)
! EXTERNAL variables
double precision,  intent(in) :: Hamilton_in_kx_ky(1:ndim,1:ndim)
double precision,  intent(in) :: E
double precision,  intent(in) :: kx, ky

double precision,  intent(in) :: mu               ! chemical potential
double precision,  intent(in) :: DeltaXY          ! value of shifting for kx and ky
! Pseudogap variables
double precision, intent(out) :: Energy_k         ! Eigenvalue in (kx,ky)
double precision, intent(out) :: Energy_k_shifted ! Eigenvalue in (kx+pi,ky+pi)
double precision, intent(out) :: Velocity         ! velocity in (kx,ky)
double precision, intent(out) :: Velocity_shifted ! velocity in (kx+pi,ky+pi)
!===============================================
! INTERNAL variables
complex*16,       allocatable :: Hamilton(:,:)
complex*16,       allocatable :: GreenF(:,:)

double precision              :: dk
double precision              :: Energy_k_kx, Energy_k_ky ! Eigenvalue in (kx+0.01,ky), (kx,ky+0.01)
double precision              :: Velocity_x, Velocity_y
!=========================================================================================

ALLOCATE(Hamilton( 1:ndim, 1:ndim))
ALLOCATE(GreenF( 1:ndim, 1:ndim))
   dk =0.01d0
!==========================================================================================================================================
!    CALL Hamilton_n_band_in_kx_ky( kx, ky, Hamilton)
   CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton_in_kx_ky, dcmplx(0.d0,0.d0), GreenF)
   Energy_k = dreal(E-1.d0/GreenF(1,1))

   CALL Hamilton_n_band_in_kx_ky( kx+dk, ky, Hamilton)
   CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, dcmplx(0.d0,0.d0), GreenF)
   Energy_k_kx = dreal(E-1.d0/GreenF(1,1))

   CALL Hamilton_n_band_in_kx_ky( kx, ky+dk, Hamilton)
   CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, dcmplx(0.d0,0.d0), GreenF)
   Energy_k_ky = dreal(E-1.d0/GreenF(1,1))
!------------------------------------------------------------------------------------------------------------------------------------------
   Velocity_x = (Energy_k-Energy_k_kx)/dk
   Velocity_y = (Energy_k-Energy_k_ky)/dk
   Velocity = abs(Velocity_x) + abs(Velocity_y)
!==========================================================================================================================================
   CALL Hamilton_n_band_in_kx_ky( kx+DeltaXY, ky+DeltaXY, Hamilton)
   CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, dcmplx(0.d0,0.d0), GreenF)
   Energy_k_shifted = dreal(E-1.d0/GreenF(1,1))

   CALL Hamilton_n_band_in_kx_ky( kx+dk+DeltaXY, ky+DeltaXY, Hamilton)
   CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, dcmplx(0.d0,0.d0), GreenF)
   Energy_k_kx = dreal(E-1.d0/GreenF(1,1))

   CALL Hamilton_n_band_in_kx_ky( kx+DeltaXY, ky+dk+DeltaXY, Hamilton)
   CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, dcmplx(0.d0,0.d0), GreenF)
   Energy_k_ky = dreal(E-1.d0/GreenF(1,1))
!------------------------------------------------------------------------------------------------------------------------------------------
   Velocity_x = (Energy_k_shifted-Energy_k_kx)/dk
   Velocity_y = (Energy_k_shifted-Energy_k_ky)/dk
   Velocity_shifted = abs(Velocity_x) + abs(Velocity_y)
!==========================================================================================================================================
deallocate( Hamilton, GreenF)

END SUBROUTINE Spectrum_and_Velocity_in_kx_ky_through_G_fraction