﻿PROGRAM Prog_heat_capacity_1

  use module_energy
  use module_impurity_1
  use module_parameters

implicit none

double precision, allocatable :: resig(:), imsig(:), reG(:), imG(:)

double precision, allocatable :: temp_array(:)
double precision	            :: tmpd
integer                       :: i, is
integer                       :: nmax_temp, tmp_i

character(64)                 :: argname_1, argname_2

!=============================================================================
   call getarg( 1, argname_1 )
   call getarg( 2, argname_2 )
   argname_1 = trim(adjustl(argname_1))
   tmp_i = len( trim(adjustl(argname_1)) )
   if (tmp_i>0) then
      open( 211, form='formatted', status='scratch')
      write(211,*) argname_1
      rewind(211)
      read(211,*) t
      close(211)
   else
      stop 'For program "heat_capacity_1": "T" and "mu" is needed'
   end if

   argname_2 = trim(adjustl(argname_2))
   tmp_i = len( trim(adjustl(argname_2)) )
   if (tmp_i>0) then
      open( 211, form='formatted', status='scratch')
      write(211,*) argname_2
      rewind(211)
      read(211,*) mu
      close(211)
   else
      stop 'For "Prog_heat_capacity_1.run": "T" and "mu" is needed'
   end if

   open(unit = output_file, FILE = 'Cv_out', FORM='FORMATTED')
   call read_input_1
   close(output_file,status='DELETE')
   !=============================================================================
   ! Create energy mesh and determine value of nmax
   nmax_temp = 100000
   allocate(temp_array(-nmax_temp:nmax_temp))
   Call Energy_mesh(temp_array, nmax_temp)
   allocate(om(-nmax:nmax))
   DO i=-nmax,nmax
      om(i) = temp_array(i)
   END DO
   deallocate(temp_array)

   !=============================================================================
   allocate(G(1:nspin,-nmax:nmax))
   allocate(sig(1:nspin,-nmax:nmax))
   allocate(dos(1:nspin,-nmax:nmax))
   allocate(totalDOS(-nmax:nmax))
   allocate(reG(1:nspin))
   allocate(imG(1:nspin))
   allocate(resig(1:nspin))
   allocate(imsig(1:nspin))
   !=============================================================================

   open(10,file='Input_GS.dat',form='FORMATTED')
   if (nspin==1) then
      is = 1
      do i=-nmax,nmax
         read(10,*)om(i), dos(is, i), reG(is), imG(is), resig(is), imsig(is)
         G(is,i)=dcmplx(reG(is),imG(is))
         sig(is,i)=dcmplx(resig(is),imsig(is))
      end do
   else
      do i=-nmax,nmax
         read(10,*)om(i), totalDOS(i), dos(1,i), dos(2,i), (reG(is), imG(is), resig(is), imsig(is), is=1, nspin)
         do is=1, nspin
            G(is,i)=dcmplx(reG(is),imG(is))
            sig(is,i)=dcmplx(resig(is),imsig(is))
         end do
      end do
   end if
   close(10)

   ! Calculation of heat capacity
   call heat_capacity_1

   open(output_file,file='heat_capacity.dat',form='FORMATTED')
   write(output_file,'( 2(2x,f15.10) )') t, Cv
   close(output_file)

   deallocate(G,om)
   deallocate(sig)
   deallocate(reG,imG,resig,imsig)
   deallocate(dos, totalDOS)

END PROGRAM Prog_heat_capacity_1
