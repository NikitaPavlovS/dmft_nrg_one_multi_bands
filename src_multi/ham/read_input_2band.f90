subroutine read_input_nband

  use energy_module
  use hamiltonian_module_nband
  use parameters_module
  use impurity_module

  implicit none

!----------------------------------------------------------------------------------------------
! needed temporary variables
character*20                  :: tmpstring
character*100                 :: char_str
!----------------------------------------------------------------------------------------------

! READ input DMFT(NRG) parameters
open(10,file='INPUT_multi-band',form='formatted')
!DMFT PARAMETERS!----------------------------------------------------------------------------------------------
read(10,*) tmpstring
read(10,'(A70,D5.2)') char_str, u                   !     Value of Coloumb interaction
read(10,'(A70,D15.7)') char_str, t                  !     intended Temperature
read(10,'(A70,I3)') char_str, NDMFT                 !     number of DMFT iterations 
!NRG PARAMETERS!----------------------------------------------------------------------------------------------
read(10,*) tmpstring
read(10,'(A70,D15.7)') char_str, qval               !     occupancy
read(10,'(A70,A1)') char_str, P                     !     Particle Hole Symmetry
read(10,'(A70,D15.7)') char_str, lam                !     Lambda
read(10,'(A70,I4)') char_str, nkeep                 !     number of NRG states kept
read(10,'(A70,I3)') char_str, iover                 !     Keep NRG mesh
read(10,'(A70,I3)') char_str, iflag                 !     type of output
!ENERGY MESH!----------------------------------------------------------------------------------------------
read(10,*) tmpstring
read(10,'(A70,L1)') char_str, Linear_mesh           !     Linear (T) or Exponential (F)
read(10,'(A70,L1)') char_str, const_mesh            !     if Linear:   constant mesh (T) or variable (F)
read(10,'(A70,L1)') char_str, mesh_near_zero        !     if Varible:  all DOS near zero (T) or not (F)
read(10,'(A70,L1)') char_str, const_with_exp_near_zero !  if Linear: near zero (-0.3:0.3) exp mesh (T) or linear (F)
read(10,'(A70,I2)') char_str, Emax                  !     if previous F: Maximum Energy in eV (integer value)
read(10,'(A70,I6)') char_str, factor_of_step        !     1/(energy step)
read(10,'(A70,D15.7)') char_str, Emin_exp_mesh      !     Value of minimum energy for exp mesh
read(10,'(A70,I6)') char_str, nmax_exp_mesh         !     Value of points for exp mesh (one value for exp mesh near zero (-0.3:0.3) or full exp)
read(10,'(A70,I6)') char_str, npoint_1_03           !     Value of points in energy interval (-1.0:-0.3) and (0.3,1.0) for const mesh (const_with_exp_near_zero=F)
read(10,'(A70,I6)') char_str, npoint_03_0           !     Value of points in energy interval (-0.3:0.3) for const mesh (const_with_exp_near_zero=F)
!ENERGY PARAMETERS!----------------------------------------------------------------------------------------------
read(10,*) tmpstring
read(10,*) tmpstring
read(10,*) Ed1,     Ed2                             !    Energy parameters
read(10,*) tmpstring
read(10,*) tmpstring
read(10,*) tdd                                      !    Hopping parameters
!CHEMICAL POTENTIAL!----------------------------------------------------------------------------------------------
read(10,*) tmpstring
read(10,'(A70,L1)') char_str, variable_mu           !     Variable (T) or Const (F) 
read(10,'(A70,L1)') char_str, mu_search_for_diel    !     dielectric wanted in the result:   yes (T), not (F)
read(10,'(A70,D15.7)') char_str, mu                 !     vaule of start Chemical Potential
read(10,'(A70,D15.7)') char_str, n_total_need       !     What total occupancy need in result of calculation
!MAGNETIC PROPERTIES!----------------------------------------------------------------------------------------------
read(10,*) tmpstring
read(10,'(A70,I1)') char_str,  nspin                !     1 - paramagnetic calc, 2 - magnetic calc
read(10,'(A70,D20.15)') char_str, H_field           !     Magnetic field (in eV)
!PSEUDOGAP!----------------------------------------------------------------------------------------------
read(10,*) tmpstring
read(10,'(A70,L1)') char_str, Pseudogap             !     not including (F), with including PG (T)
read(10,'(A70,D15.7)') char_str, WW                 !     Delta^2 
read(10,'(A70,D15.7)') char_str, KP                 !     inverse Correlation Length in lattace constant units  
read(10,*) tmpstring
read(10,'(A70,I1)') char_str, comb                  !     Combinatorics: Incommensurate (1), Commensurate (2), Pines (3)
read(10,'(A70,I3)') char_str, nf                    !     Number of recursive steps 
!INITIAL DATA!----------------------------------------------------------------------------------------------
read(10,*) tmpstring
read(10,'(A92,I2)') char_str, start_iter            !     Circle of DMFT from which start
read(10,*) tmpstring
read(10,'(A92,L1)') char_str, INIT_Sigma            !     Start from Sigma (Start_sigma.dat, files -- fort.11-...):   yes (T), not (F)
read(10,'(A92,L1)') char_str, INIT_dd               !     Start from DOS (Start_dd.dat, files -- fort.201-...):   yes (T), not (F)
read(10,*) tmpstring
read(10,'(A92,I6)') char_str, nmax_init             !     integer value of energy points number (see out file of previous calc)
!CALCULATING DATA!----------------------------------------------------------------------------------------------
read(10,*) tmpstring
read(10,'(A84,I1)') char_str,  Method_of_Green_Function_calculation !    Method of Green function calculation:
read(10,*) tmpstring                                                           ! CASE (1): G=(E-H+mu-Sigma)^(-1);
read(10,*) tmpstring                                                           ! CASE (2) G=(E-H)^(-1);
read(10,*) tmpstring                                                           ! CASE (3) Gij(E)=Sum_m EigenVectors_right( i, m) * EigenVectors_left( m, j) / (E-EigenValues(m)+delta)
read(10,'(A84,L1)') char_str, linear_integration                    !     Integration: Linear mesh for k (T) or Gaussian points (F)
read(10,'(A84,I4)') char_str, ni                    !     Number of intervals in 1 direction  of integration
read(10,*) tmpstring       !     BANDS
read(10,'(A84,L1)') char_str, Bands_plot             !     need:   yes (T), not (F)
read(10,'(A84,L1)') char_str, FBANDS                 !     need Fatbands and coefficient of probability:   yes (T), not (F)
read(10,'(A84,L1)') char_str, FBANDS_write           !     write FatBands to Out Files:   yes (T), not (F)
read(10,*) tmpstring       !     Spectral Function:
read(10,'(A84,L1)') char_str, Spectral_Function_plot !     Calculate data file for plot:   yes (T), not (F)
read(10,'(A84,I5)') char_str,  MaxK_point_for_SF     !     Value of point in one direction of Briluin zone
read(10,'(A84,L1)') char_str, Spectral_Function_maximum_plot !     Calculate maximums of Spectral Function
read(10,'(A84,D15.7)') char_str, Spectral_Function_maximum_low_cutting
read(10,*) tmpstring       !     FERMI SURFACE:
read(10,'(A84,L1)') char_str, Fermi_surface_plot     !     Calculate data file for plot:   yes (T), not (F)
read(10,'(A84,D15.7)') char_str, Energy_shift        !     Energy shift from Fermi level, if need calculate Fermi surface not in Fermi level
read(10,'(A84,L1)') char_str, Apply_Hand_Sigma_value !     If need calc Fermi surface with Sigma=(ReSigma,Hand_Sigma_value)
read(10,'(A84,D15.7)') char_str, Hand_Sigma_value    !     value of Image part of Sigma for manualy blurring Fermi surface
read(10,'(A84,L1)') char_str, Fermi_Surface_without_Interaction !     Calculate bare Fermi surface:   yes (T), not (F)
read(10,'(A84,I1)') char_str, number_of_band_crossing_FL
read(10,*) tmpstring
read(10,*) tmpstring       !     DOS:
read(10,'(A84,L1)') char_str, bare_DOS                 ! in start Sigma=(0,-delta) => calculate not interacting DOS (next must T and no one of DMFT cilces)
read(10,'(A84,L1)') char_str, DOS_plot_after_DMFT_loop !     Calculate ending file (OUT_Green_image_DOS):   yes (T), not (F)
!DOUBLE COUNING!----------------------------------------------------------------------------------------------
read(10,*) tmpstring
read(10,'(A82,L1)') char_str, hole_occupancy           !     Occupancy: hole (T) or electron (F) define sing before Edc (T=>+) (F=>-)
read(10,'(A82,L1)') char_str, Edc_const                !     Constant value (T) or Calculated from occupancy (F)
read(10,'(A82,D15.7)') char_str, Edc                   !     Value of constant Double Counting energy
read(10,'(A82,L1)') char_str, start_Edc                !     need Start from Other Edc:   yes (T), not (F)
read(10,'(A82,D15.7)') char_str, start_Edc_value       !     Value of starting Double Counting energy
!HAMILTON!----------------------------------------------------------------------------------------------
read(10,*) tmpstring
read(10,'(A82,D15.7)') char_str, delta
close(10)

end subroutine read_input_nband