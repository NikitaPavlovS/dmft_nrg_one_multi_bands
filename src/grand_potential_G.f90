subroutine grand_potential_G
  use module_impurity_1
  use module_energy
  use module_DOS_analitical
  use module_integration
  use module_parameters

implicit none

double precision, allocatable      :: fx1(:),fx2(:), integral(:)
double precision                   :: iks,igrek, potential_1, potential_2
integer                            :: is,i,j,omega,epsilon
complex*16                         :: Gk_tmp
!double precision, allocatable      :: e(:)

!   allocate(e(-nmax:nmax))
   allocate(fx1(-nmax:nmax))
   allocate(fx2(-nmax:nmax))
   allocate(integral(1:nspin))

! grand potential of Gk = Int_{k,omega,spin} G_{k,spin}(omega)
   Call DOS_0_FCC
   Do is=1, nspin
!$OMP PARALLEL DO PRIVATE(igrek,iks,fx1)
      Do omega = -nmax,nmax
         Do epsilon  = -nmax,nmax
            Gk_tmp = 1/( om(epsilon) - om(omega) + mu - sig(is,epsilon) - (1.5d0 - is)* dcmplx(H_field,0.d0) ) 
            igrek=dimag(Gk_tmp)
            iks = dreal(Gk_tmp)
            fx1(epsilon) = dos_0(omega) * dtanh( om(epsilon)/(2.d0*t) ) *datan2( igrek, iks )
         End do
         fx2(omega)=integration_simpson(-nmax, nmax, om(-nmax:nmax), fx1(-nmax:nmax))
      End do
!$OMP END PARALLEL DO
      integral(is) = integration_simpson(-nmax, nmax, om(-nmax:nmax), fx2(-nmax:nmax))
   End do
!   deallocate(e)
   Omega_lnGk = (1/(2*pi))*(integral(1)+integral(2))

! grand potential of G = Int_{omega,spin} G_{spin}(omega)
! local G
   Do is=1, nspin
      Do epsilon = -nmax, nmax
         igrek= dimag(G(is,epsilon))
         iks= dreal(G(is,epsilon))
         fx1(epsilon) = dtanh(om(epsilon)/(2.d0*t))*datan2(igrek,iks)
      End do
      integral(is) = integration_simpson(-nmax, nmax, om(-nmax:nmax), fx1(-nmax:nmax))
   End do
   Omega_lnG = (1/(2*pi))*(integral(1)+integral(2))

   deallocate(fx1)
   deallocate(fx2)
   deallocate(integral)

end subroutine grand_potential_G






