module module_hamiltonian_nband

  implicit none
  public
!----------------------------------------------------------------------------------------------
! Hamilton variables
integer,          save :: ndim = 3   ! dimension of Hamilton
double precision, save :: Ed, Ep     ! energy parameters
double precision, save :: tdd, tpd, tpd1, tpd2, tpp, tpp1 ! hopping parameters


end module module_hamiltonian_nband