module energy_module

  implicit none
  public

!----------------------------------------------------------------------------------------------
!Energy mesh variables
integer,                       save :: nmax     ! number of 1/2 points of energy mesh
double precision, allocatable, save :: om(:)    ! energy mesh
                                              ! The code assumes a symmetric energy mesh,
                                              ! The first value !=0 will be used as energy scale.
logical,          save :: Linear_mesh = .true.               ! Linear (T) or Exponential (F)
logical,          save :: const_mesh = .false.               ! if Linear:   constant mesh (T) or variable (F)
logical,          save :: mesh_near_zero = .false.           ! if Varible:  all DOS near zero (T) or not (F)
logical,          save :: const_with_exp_near_zero = .false. ! if Linear: near zero (-0.3:0.3) exp mesh (T) or linear (F)
!integer,          save :: Emax = 20                          ! if previous F: Maximum Energy in eV (integer value)
double precision, save :: Emax = 10.d0                       ! if previous F: Maximum Energy in eV (integer value)
integer,          save :: factor_of_step = 50   ! 1/(energy step)
double precision, save :: Emin_exp_mesh = 1E-8  ! Value of minimum energy for exp mesh
integer,          save :: nmax_exp_mesh = 10000 ! Value of points for exp mesh (one value for exp mesh near zero (-0.3:0.3) or full exp)
integer,          save :: npoint_1_03 = 35      ! Value of points in energy interval (-1.0:-0.3) and (0.3,1.0) for const mesh (const_with_exp_near_zero=F)
integer,          save :: npoint_03_0 = 15      ! Value of points in energy interval (-0.3:0.3) for const mesh (const_with_exp_near_zero=F)
integer,          save :: nmax_init = 1000      ! integer value of energy points number (see out file of previous calc)
!----------------------------------------------------------------------------------------------
! Chemical potential
double precision, save :: mu = 0.d0            ! chemical potential
logical,          save :: variable_mu          ! Variable == search mu for given n_total_need
logical,          save :: mu_search_for_diel   ! dielectric wanted in the result:   yes (T), not (F) (NOW, NOT work for dielectric)
double precision, save :: n_total_need         ! What total occupancy need in result of calculation
double precision, save :: t                    ! temperature for fermi function


end module energy_module
