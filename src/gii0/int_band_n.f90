! ��������� �� � "�����" DOS �� �����
      program int_band
      implicit none
!-------------------------------------------------     
      integer                       :: ns,nSP,ne
      integer                       :: m
      double precision, allocatable :: spe(:),dos(:)
	  double precision, allocatable :: spcoefdos(:),tmp(:)
      double precision              :: E,Emax,dE,gamma,SPmin,SPmax,mu
      double precision, parameter   :: pi=3.14159265358979
	  complex*16                    :: Eii,LG,Gii
	  complex*16,       parameter   :: i=(0.d0,1.d0)
      character*8                   :: tmpstring



!---Read "bare" DOS from file -----------------
        open(88,file='dos0.dat',status='unknown')
        ns=0
        do while (.true.)
          read(88,*,END=100)
          ns=ns+1
        enddo
100     continue
        close(88)




! Temporary allocate
	    allocate(spe(ns))
        allocate(dos(ns))
        allocate(spcoefdos(ns))
	  open(88,file='dos0.dat',status='unknown')
        do m=1,ns
          read(88,*) spe(m),dos(m)
        enddo
      close(88)
	  SPmin=spe(1)
	  SPmax=spe(ns)

!---> Make spline coefficients

      allocate(tmp(ns))
      call spline(spe,dos,ns,0.d0,0.d0,spcoefdos,tmp)
      deallocate(tmp)

!=================================================

	  write(*,*) 'input gamma, numb int SP'
	  read(*,*) gamma, nSP
	  Eii=-gamma*i
	  mu=0.0
	  Emax=1.0
	  ne=1000
	  open(1, file = 'N(E)_prob.dat')
      dE=2.*Emax/ne
	  E=-Emax
	   do m=0,ne
	   Gii=LG(E,Eii,mu,spe,dos,spcoefdos,ns,SPmin,SPmax,nSP)
	   write(1,*) E,-DIMAG(Gii)/pi
	   E=E+dE
       enddo



	  deallocate(spe)
      deallocate(dos,spcoefdos)
      end program int_band
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>




!=================================================
      FUNCTION LG(E,Eii,mu,spe,dos,spcoefdos,ns,SPmin,SPmax,nSP)
      implicit none
!-------------------------------------------------     
      integer                       :: ns,nSP
      integer                       :: v,j
      double precision				:: spe(ns),dos(ns)
	  double precision				:: spcoefdos(ns)
      integer,          parameter   :: ngp=50         ! number of Gaussian points
      double precision              :: x(ngp),w(ngp)  ! Gaussian points, weights
      double precision              :: E,SP,SPmin,SPmax,mu,dossp
      double precision              :: xc,xs,s1
      double precision, parameter   :: pi=3.14159265358979
	  complex*16                    :: s,Eii,Et,LG
	  complex*16,       parameter   :: i=(0.d0,1.d0)


! Gaussian points and weights table ------------
      x(1)= 1.562898442154299E-002
      w(1)= 3.125542345386337E-002
      x(2)= 4.687168242159154E-002
      w(2)= 3.122488425484935E-002
      x(3)= 7.806858281343654E-002
      w(3)= 3.116383569620989E-002
      x(4)= 1.091892035800610E-001
      w(4)= 3.107233742756650E-002
      x(5)= 1.402031372361140E-001
      w(5)= 3.095047885049099E-002
      x(6)= 1.710800805386033E-001
      w(6)= 3.079837903115258E-002
      x(7)= 2.017898640957359E-001
      w(7)= 3.061618658398045E-002
      x(8)= 2.323024818449740E-001
      w(8)= 3.040407952645482E-002
      x(9)= 2.625881203715034E-001
      w(9)= 3.016226510516913E-002
      x(10)=2.926171880384719E-001
      w(10)=2.989097959333285E-002
      x(11)=3.223603439005291E-001
      w(11)=2.959048805991265E-002
      x(12)=3.517885263724217E-001
      w(12)=2.926108411063827E-002
      x(13)=3.808729816246298E-001
      w(13)=2.890308960112521E-002
      x(14)=4.095852916783015E-001
      w(14)=2.851685432239509E-002
      x(15)=4.378974021720317E-001
      w(15)=2.810275565910117E-002
      x(16)=4.657816497733581E-001
      w(16)=2.766119822079239E-002
      x(17)=4.932107892081911E-001
      w(17)=2.719261344657690E-002
      x(18)=5.201580198817632E-001
      w(18)=2.669745918357095E-002
      x(19)=5.465970120650944E-001
      w(19)=2.617621923954569E-002
      x(20)=5.725019326213813E-001
      w(20)=2.562940291020812E-002
      x(21)=5.978474702471789E-001
      w(21)=2.505754448157957E-002
      x(22)=6.226088602037079E-001
      w(22)=2.446120270795706E-002
      x(23)=6.467619085141294E-001
      w(23)=2.384096026596818E-002
      x(24)=6.702830156031411E-001
      w(24)=2.319742318525411E-002
      x(25)=6.931491993558020E-001
      w(25)=2.253122025633627E-002
      x(26)=7.153381175730565E-001
      w(26)=2.184300241624740E-002
      x(27)=7.368280898020207E-001
      w(27)=2.113344211252765E-002
      x(28)=7.575981185197073E-001
      w(28)=2.040323264620945E-002
      x(29)=7.776279096494956E-001
      w(29)=1.965308749443529E-002
      x(30)=7.968978923903145E-001
      w(30)=1.888373961337491E-002
      x(31)=8.153892383391763E-001
      w(31)=1.809594072212811E-002
      x(32)=8.330838798884008E-001
      w(32)=1.729046056832359E-002
      x(33)=8.499645278795913E-001
      w(33)=1.646808617614519E-002
      x(34)=8.660146884971647E-001
      w(34)=1.562962107754599E-002
      x(35)=8.812186793850185E-001
      w(35)=1.477588452744131E-002
      x(36)=8.955616449707269E-001
      w(36)=1.390771070371879E-002
      x(37)=9.090295709825296E-001
      w(37)=1.302594789297155E-002
      x(38)=9.216092981453339E-001
      w(38)=1.213145766297949E-002
      x(39)=9.332885350430795E-001
      w(39)=1.122511402318599E-002
      x(40)=9.440558701362559E-001
      w(40)=1.030780257486898E-002
      x(41)=9.539007829254917E-001
      w(41)=9.380419653694464E-003
      x(42)=9.628136542558154E-001
      w(42)=8.443871469669000E-003
      x(43)=9.707857757637063E-001
      w(43)=7.499073255464720E-003
      x(44)=9.778093584869184E-001
      w(44)=6.546948450845286E-003
      x(45)=9.838775407060570E-001
      w(45)=5.588428003865505E-003
      x(46)=9.889843952429918E-001
      w(46)=4.624450063422116E-003
      x(47)=9.931249370374435E-001
      w(47)=3.655961201326339E-003
      x(48)=9.962951347331251E-001
      w(48)=2.683925371553477E-003
      x(49)=9.984919506395959E-001
      w(49)=1.709392653517994E-003
      x(50)=9.997137267734412E-001
      w(50)=7.346344905057997E-004
! end Gaussian points and weights table ------------

!======== integral from SPmin to SPmax===
      Et=E+mu-Eii
	  xc=(SPmax-SPmin)/2./nSP
	  xs=SPmin-xc
	     IF (ABS(IMAG(Et)).GT.0.01) THEN
      s=(0.d0,0.d0)
	   DO v=1,nSP
      xs=xs+2.*xc

	   DO j=1,ngp
	  SP=xs+xc*x(j)
      call splint(spe,dos,spcoefdos,ns,SP,dossp)
      s=s+w(j)*dossp/(Et-SP)

      SP=xs-xc*x(j)
      call splint(spe,dos,spcoefdos,ns,SP,dossp)
      s=s+w(j)*dossp/(Et-SP)
	   ENDDO
	   ENDDO
	  LG=s*xc
	     
		 ELSE
	  SP=DREAL(Et)
	  LG=(0.d0,0.d0)
	  IF (SP.GT.SPmin.and.SP.LT.SPmax) THEN
      call splint(spe,dos,spcoefdos,ns,SP,dossp)
	  LG=-i*pi*dossp
	  ENDIF
	  s1=0.d0
	   DO v=1,nSP
      xs=xs+2.*xc

	   DO j=1,ngp
	  SP=xs+xc*x(j)
      call splint(spe,dos,spcoefdos,ns,SP,dossp)
      s1=s1+w(j)*dossp/(DREAL(Et)-SP)

      SP=xs-xc*x(j)
      call splint(spe,dos,spcoefdos,ns,SP,dossp)
      s1=s1+w(j)*dossp/(DREAL(Et)-SP)
	   ENDDO
	   ENDDO
	  LG=LG+s1*xc
	     ENDIF

!======== integral from Emin to Emax===
     end FUNCTION LG

!=================================================



      subroutine spline( x,y,n,yp1,ypn,y2,u )
!|----------------------------------------------------------------------------|
!|                                                                            |
!|             This subrutine make spline coefficients                        |
!|                                                                            |
!|  Inputs :                                                                  |
!|i    x - set of points where function is defined                            |
!|i    y - set of function value                                              |
!|i    yp1,ypn - value of left end right second derivative of function        |
!|i    n - number of points from which spline coefficients are made           |
!|  Outputs :                                                                 |
!|o    y2 - spline coefficients                                               |
!|r Remarks                                                                   |
!|r    u - auxiliary dimenshion                                               |
!|                                                                            |
!|----------------------------------------------------------------------------|
      implicit none
      integer          :: n,i,k
      double precision :: x(n),y(n),y2(n),u(n)
      double precision :: p,qn,sig,un,yp1,ypn

      if( yp1.gt..99d30 )then
        y2(1) = 0.d0
        u(1)  = 0.d0
       else
        y2(1) = -5.d-1
        u(1)  = ( 3.d0 / ( x(2)-x(1) ) )*( (y(2)-y(1) ) /&
                ( x(2)-x(1) ) - yp1 )
      endif

      do i = 2,n-1
        sig = ( x(i)-x(i-1) ) / ( x(i+1)-x(i-1) )
        p   = sig*y2(i-1) + 2.d0
        y2(i) = ( sig-1.d0 ) / p
        u(i)  = ( 6.d0*( ( y(i+1)-y(i) ) / ( x(i+1)-x(i) ) -&
                ( y(i)-y(i-1) ) / ( x(i)-x(i-1) ) ) /&
                ( x(i+1)-x(i-1) ) - sig*u(i-1) ) / p
      enddo

      if( ypn.gt..99d30 )then
        qn = 0.d0
        un = 0.d0
       else
        qn = 5.d-1
        un = ( 3.d0 / ( x(n)-x(n-1) ) )*( ypn - ( y(n)-y(n-1) ) /&
             ( x(n)-x(n-1) ) )
      endif

      y2(n) = ( un - qn*u(n-1) ) / ( qn*y2(n-1) + 1.d0 )

      do k = n-1,1,-1
        y2(k) = y2(k)*y2(k+1) + u(k)
      enddo

      end subroutine spline

!=================================================
      subroutine splint( xa,ya,y2a,n,x,y )
!|----------------------------------------------------------------------------|
!|                                                                            |
!|             This subrutine make spline for real function                   |
!|                                                                            |
!|  Inputs :                                                                  |
!|i    xa - set of points where function is defined                           |
!|i    ya - set of function value                                             |
!|i    y2a - spline coefficients                                              |
!|i    n - number of points from which spline coefficients are made           |
!|i    x - point where is necessary to calculate new value of function        |
!|  Outputs :                                                                 |
!|o    y - value of function in point x                                       |
!|                                                                            |
!|----------------------------------------------------------------------------|
      implicit none

      integer          :: n,k,khi,klo
	  double precision :: xa(n)
	  double precision :: y2a(n),ya(n)
	  double precision :: a,b,x,h
	  double precision :: y

      klo=1
      khi=n

1     if( khi-klo.gt.1 )then
        k = ( khi + klo ) / 2
        if( xa(k).gt.x )then
          khi = k
         else
          klo = k
        endif
        goto 1
      endif

      h = xa(khi) - xa(klo)
      if( h.eq.0.d0 ) stop 'bad xa input in splint'

      a = ( xa(khi) - x ) / h
      b = ( x - xa(klo) ) / h


      y = a*ya(klo) + b*ya(khi) + ( ( a**3-a )*y2a(klo) +&
          ( b**3-b )*y2a(khi) )*(h**2) / 6.d0

      end subroutine splint
