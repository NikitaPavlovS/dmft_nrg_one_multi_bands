SUBROUTINE GreenFunction_of_E_Sigma_mu_fraction_PG( E, Sigma, mu, Integ_G)

!  use energy_module
  use hamiltonian_module_1band
  use parameters_module
!  use impurity_module
  use Gaussian_points_module

implicit none
!=======================================================================
! Green function variables
      double precision,  intent(in) :: E      ! value of energy point
      double precision,  intent(in) :: mu
      complex*16,        intent(in) :: Sigma(1:nspin)
      complex*16,       intent(out) :: Integ_G(1:nspin)  ! Green function of E integrated over k for d, p1, p2, and all
!=======================================================================
! LOCAL variables
! Hamilton
complex*16,       allocatable :: Hamilton
! Green function
complex*16,       allocatable :: Gk(:,:,:)        ! integral of first index matrix element of Grean function
! k points array
double precision, allocatable :: kx(:)
double precision, allocatable :: ky(:)
! Pseudogap variables
complex*16       :: PG
double precision :: Energy_k                    ! eigenvalue in (kx,ky)
double precision :: Energy_k_shifted            ! eigenvalue in (kx+pi,ky+pi)
double precision :: Velocity, Velocity_shifted  ! velocity in (kx,ky) and in (kx+pi,ky+pi)
! needed temporary variables
complex*16       :: gs(1:nspin)
integer          :: ix, iy
double precision :: dx
integer          :: is, i
integer          :: kpoints
!=======================================================================
double precision, allocatable :: xs, ys, xm, ym
integer,          allocatable :: lx, ly, jx, jy, jy2
complex*16,       allocatable :: gs1(:), gs2(:)

kpoints = 2*ni*ngp
ALLOCATE(kx(kpoints))
ALLOCATE(ky(kpoints))
ALLOCATE(Hamilton)
ALLOCATE(Gk( 1:nspin, kpoints, kpoints))


!==========================================================================================
IF (linear_integration) THEN
   dx = 2.d0*pi/kpoints
   DO ix = 1,kpoints
      kx(ix) = -pi+dx*ix
      DO iy = 1,kpoints
         ky(iy) = -pi+dx*iy
         CALL Hamilton_one_band_in_kx_ky( kx(ix), ky(iy), Hamilton)
         CALL Spectrum_and_Velocity_in_kx_ky( Hamilton, kx(ix), ky(iy), pi, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
         !-------------------------------------------------
         DO is=1, nspin
            CALL fract(ww, KP, E, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, Sigma(is), comb)
            Gk(is,ix,iy) = 1.d0/(dcmplx( E + mu, 0.d0) - Hamilton - Sigma(is)-PG)
         END DO
      ENDDO
   ENDDO
   !=======integrate  Green function of E(Emin, Emax) po zone========================
   gs=(0.d0,0.d0)
   DO ix = 1,kpoints
      DO iy = 1,kpoints
         DO is=1, nspin
            gs(is)= gs(is) + Gk(is,ix,iy)
         ENDDO
      ENDDO
   ENDDO
   DO is=1, nspin
       Integ_G(is) = gs(is)/(kpoints*kpoints)
   END DO
!==========================================================================================
ELSE ! Gausian integration
!==========================================================================================
Call Gaussian_points_and_weights
allocate(xs, ys, xm, ym)
allocate(lx, ly, jx, jy, jy2)
allocate(gs1(1:nspin), gs2(1:nspin))
!-------------------------------------------------
   dx = pi/ni/2
!   xs = -dx! - pi
   xs = -dx - pi
!stop 'in GreenFunction_of_E_Sigma_mu_fraction_PG start'
   DO lx = 0,(ni-1)
      xs = xs+2*dx
      DO ix = 1,ngp
         xm = dx*x(ix)
!-------------------------------------------------
         !jx = lx*ngp+1-ix
         jx = (2*lx+1)*ngp+1-ix
         kx(jx) = xs-xm

         ys = -dx - pi
         DO ly = 0,(ni-1)
            ys = ys+2*dx
            DO iy = 1,ngp
               ym = dx*x(iy)
               !jy = ly*ngp+1-iy
               jy = (2*ly+1)*ngp+1-iy
               ky(jy) = ys-ym
               !-------------------------------------------------
               CALL Hamilton_one_band_in_kx_ky( kx(jx), ky(jy), Hamilton)
               CALL Spectrum_and_Velocity_in_kx_ky( Hamilton, kx(jx), ky(jy), pi, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
               DO is=1, nspin
                  CALL fract(ww, KP, E, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, Sigma(is), comb)
                  Gk(is,ix,iy) = 1.d0/(dcmplx( E + mu, 0.d0) - Hamilton - Sigma(is)-PG)
               END DO
               !-------------------------------------------------

               !jy = ly*ngp+iy
               jy = (2*ly+1)*ngp+iy
               ky(jy) = ys+ym
               !-------------------------------------------------
               CALL Hamilton_one_band_in_kx_ky( kx(jx), ky(jy), Hamilton)
               CALL Spectrum_and_Velocity_in_kx_ky( Hamilton, kx(jx), ky(jy), pi, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
               DO is=1, nspin
                  CALL fract(ww, KP, E, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, Sigma(is), comb)
                  Gk(is,ix,iy) = 1.d0/(dcmplx( E + mu, 0.d0) - Hamilton - Sigma(is)-PG)
               END DO
               !-------------------------------------------------
            ENDDO ! iy
         ENDDO ! ly
!-------------------------------------------------
         !jx = lx*ngp+ix
         jx = (2*lx+1)*ngp+ix
         kx(jx) = xs+xm

         ys = -dx !- pi
         DO ly = 0,(ni-1)
            ys = ys+2*dx
            DO iy = 1,ngp
               ym = dx*x(iy)
               jy = (2*ly+1)*ngp+1-iy
               ky(jy) = ys-ym
               !-------------------------------------------------
               CALL Hamilton_one_band_in_kx_ky( kx(jx), ky(jy), Hamilton)
               CALL Spectrum_and_Velocity_in_kx_ky( Hamilton, kx(jx), ky(jy), pi, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
               DO is=1, nspin
                  CALL fract(ww, KP, E, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, Sigma(is), comb)
                  Gk(is,ix,iy) = 1.d0/(dcmplx( E + mu, 0.d0) - Hamilton - Sigma(is)-PG)
               END DO
               !-------------------------------------------------

               jy = (2*ly+1)*ngp+iy
               ky(jy) = ys+ym
               !-------------------------------------------------
               CALL Hamilton_one_band_in_kx_ky( kx(jx), ky(jy), Hamilton)
               CALL Spectrum_and_Velocity_in_kx_ky( Hamilton, kx(jx), ky(jy), pi, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
               DO is=1, nspin
                  CALL fract(ww, KP, E, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, Sigma(is), comb)
                  Gk(is,ix,iy) = 1.d0/(dcmplx( E + mu, 0.d0) - Hamilton - Sigma(is)-PG)
               END DO
               !-------------------------------------------------
            ENDDO ! iy
         ENDDO ! ly
!-------------------------------------------------
      ENDDO ! ix
   ENDDO ! lx
!stop 'in GreenFunction_of_E_Sigma_mu_fraction_PG in midle'

!=======integrate  Green function of E(Emin, Emax) po zone========================
   gs=(0.d0,0.d0)
   DO lx=0,(ni-1)
      DO ix=1,ngp
         jx=(2*lx+1)*ngp+1-ix
!-------------------------------------------------
         gs1=(0.d0,0.d0)
         DO ly=0,(ni-1)
            DO iy=1,ngp
               jy = (2*ly+1)*ngp+1-iy
               jy2 = (2*ly+1)*ngp+iy
               DO is=1, nspin
                  gs1(is) = gs1(is)+w(iy)*(Gk(is,jx,jy)+Gk(is,jx,jy2))
               END DO ! spin
            ENDDO ! iy
         ENDDO ! ly
         gs2 = gs1
!-------------------------------------------------
         jx=(2*lx+1)*ngp+ ix
         gs1=(0.d0,0.d0)
         DO ly=0,(ni-1)
            DO iy=1,ngp
               jy = (2*ly+1)*ngp+1-iy
               jy2 = (2*ly+1)*ngp+iy
               DO is=1, nspin
                  gs1(is) = gs1(is)+w(iy)*(Gk(is,jx,jy)+Gk(is,jx,jy2))
               END DO ! spin
            ENDDO ! iy
         ENDDO ! ly
         gs2=gs2+gs1
!-------------------------------------------------
         gs=gs+w(ix)*gs2
      ENDDO ! ix
   ENDDO ! lx

DO is=1, nspin
   Integ_G(is) = gs(is)*dx*dx/pi/pi !/(2*ni**2)
END DO

deallocate(xs, ys, xm, ym)
deallocate(lx, ly, jx, jy, jy2)
deallocate(gs1, gs2)

END IF ! (lenear or not integration)
!==========================================================================================

DEALLOCATE( kx, ky, Hamilton, Gk)


END SUBROUTINE GreenFunction_of_E_Sigma_mu_fraction_PG