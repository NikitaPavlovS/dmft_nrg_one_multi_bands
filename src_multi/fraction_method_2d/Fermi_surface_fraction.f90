SUBROUTINE Fermi_surface_fraction

  use energy_module
  use hamiltonian_module_nband
  use parameters_module
  use impurity_module

  IMPLICIT NONE

!-------------------------------------------------------------------
! INTERNAL variables
complex*16,       allocatable :: Hamilton(:,:)
complex*16,       allocatable :: GreenF(:,:,:)
double precision, allocatable :: Awk_at_FL(:,:)
!double precision, allocatable :: Awk_at_FL_2(:,:)
complex*16                    :: PG

integer                       :: i, is, ix,iy
double precision              :: dk
double precision, PARAMETER   :: kMin=0.d0
double precision              :: kMax
integer,            PARAMETER :: MaxK=1000

integer                       :: Number_of_elemet_in_Emesh_for_shift=0

double precision              :: Energy_k                    ! Eigenvalue in (kx,ky)
double precision              :: Energy_k_shifted            ! Eigenvalue in (kx+pi,ky+pi)
double precision              :: Velocity, Velocity_shifted  ! velocity in (kx,ky) and in (kx+pi,ky+pi)

double precision, allocatable :: kx(:), ky(:)

integer                       :: number_of_file

!EXTERNAL zgeev

ALLOCATE(Hamilton( 1:ndim, 1:ndim))
ALLOCATE(GreenF( 1:nspin, 1:ndim, 1:ndim))
ALLOCATE(Awk_at_FL( 0:MaxK, 0:MaxK))
!ALLOCATE(Awk_at_FL_2(-nmax:nmax, 0:MaxK))
ALLOCATE(kx(0:MaxK))
ALLOCATE(ky(0:MaxK))

!make kx and ky mesh
kMax=pi
dk=(kMax - kMin)/MaxK
DO i = 0, MaxK
   kx(i)= kMin +dk*i
   ky(i)= kMin +dk*i
END DO
! search element number for energy array to do Energy shift in Fermi surface calculation
IF (abs(Energy_shift)>1E-6) THEN
   DO i = -nmax, (nmax-1)
      IF ((Energy_shift>=om(i)).and.(Energy_shift<om(i+1))) THEN
         Number_of_elemet_in_Emesh_for_shift = i
         !write(*,*) ' i = ', i
         !write(*,*) ' Number_of_elemet_in_Emesh_for_shift = ', Number_of_elemet_in_Emesh_for_shift
         !write(*,*) ' Energy_shift = ', Energy_shift
         !stop 'stop in Fermi surface in Number_of_elemet_in_Emesh_for_shift definition right'
      END IF
   END DO
   IF (Number_of_elemet_in_Emesh_for_shift.eq.0) THEN
       stop 'stop in Fermi surface in Number_of_elemet_in_Emesh_for_shift definition'
   END IF
END IF
!stop 'stop in Fermi surface'
!=========================================================================================================================================
Awk_at_FL = 0.d0
IF (Apply_Hand_Sigma_value) THEN
   !------------------------------------------------------------------------------------------------------------------
   IF (Pseudogap) THEN
      DO ix = 0, MaxK
         DO iy = 0, MaxK
            CALL Hamilton_n_band_in_kx_ky( kx(ix), ky(iy), Hamilton)
            CALL Spectrum_and_Velocity_in_kx_ky_through_G_fraction( Hamilton, om(Number_of_elemet_in_Emesh_for_shift), kx(ix), ky(iy), mu, pi, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
            DO is=1, nspin
               CALL fract(ww, KP, om(Number_of_elemet_in_Emesh_for_shift), Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, sig(is,Number_of_elemet_in_Emesh_for_shift), comb)
               CALL GreenFunction_of_Hamiltonian_fraction( om(Number_of_elemet_in_Emesh_for_shift), mu, Hamilton, dcmplx(dreal(sig(is,Number_of_elemet_in_Emesh_for_shift)),-Hand_Sigma_value)+PG, GreenF(is,:,:))
               DO i=1, ndim
                  Awk_at_FL(ix,iy) = Awk_at_FL(ix,iy) + dimag(GreenF(is,i,i))
               ENDDO
            END DO
            !stop 'stop mu in FS'
         END DO
      END DO
   !------------------------------------------------------------------------------------------------------------------
   ELSE
      DO ix = 0, MaxK
         DO iy = 0, MaxK
            CALL Hamilton_n_band_in_kx_ky( kx(ix), ky(iy), Hamilton)
            DO is=1, nspin
               CALL GreenFunction_of_Hamiltonian_fraction( om(Number_of_elemet_in_Emesh_for_shift), mu, Hamilton, dcmplx(dreal(sig(is,Number_of_elemet_in_Emesh_for_shift)),-Hand_Sigma_value), GreenF(is,:,:))
               DO i=1, ndim
                  Awk_at_FL(ix,iy) = Awk_at_FL(ix,iy) + dimag(GreenF(is,i,i))
               ENDDO
            END DO
         END DO
      END DO
   END IF
   !------------------------------------------------------------------------------------------------------------------
ELSE
   !------------------------------------------------------------------------------------------------------------------
   IF (Pseudogap) THEN
      DO ix = 0, MaxK
         DO iy = 0, MaxK
            CALL Hamilton_n_band_in_kx_ky( kx(ix), ky(iy), Hamilton)
            CALL Spectrum_and_Velocity_in_kx_ky_through_G_fraction( Hamilton, om(Number_of_elemet_in_Emesh_for_shift), kx(ix), ky(iy), mu, pi, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
            DO is=1, nspin
               CALL fract(ww, KP, om(Number_of_elemet_in_Emesh_for_shift), Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, sig(is,Number_of_elemet_in_Emesh_for_shift), comb)
               CALL GreenFunction_of_Hamiltonian_fraction( om(Number_of_elemet_in_Emesh_for_shift), mu, Hamilton, sig(is,Number_of_elemet_in_Emesh_for_shift)+PG, GreenF(is,:,:))
               DO i=1, ndim
                  Awk_at_FL(ix,iy) = Awk_at_FL(ix,iy) + dimag(GreenF(is,i,i))
               ENDDO
            END DO
         END DO
      END DO
   !------------------------------------------------------------------------------------------------------------------
   ELSE
      DO ix = 0, MaxK
         DO iy = 0, MaxK
            CALL Hamilton_n_band_in_kx_ky( kx(ix), ky(iy), Hamilton)
            DO is=1, nspin
               CALL GreenFunction_of_Hamiltonian_fraction( om(Number_of_elemet_in_Emesh_for_shift), mu, Hamilton, sig(is,Number_of_elemet_in_Emesh_for_shift), GreenF(is,:,:))
               DO i=1, ndim
                  Awk_at_FL(ix,iy) = Awk_at_FL(ix,iy) + dimag(GreenF(is,i,i))
               ENDDO
            END DO
         END DO
      END DO
   END IF
   !------------------------------------------------------------------------------------------------------------------
END IF
Awk_at_FL(:,:) = -1.d0/pi * Awk_at_FL(:,:)
!------------------------------------------------------------------------------------------------------------------
if (iter<NDMFT) then
   number_of_file = 800+iter
   DO ix = 0, MaxK
      DO iy = 0, MaxK
         write(number_of_file,'(2x,3(2x,F15.8))') kx(ix), ky(iy), Awk_at_FL(ix,iy)
      END DO
      write(number_of_file,*)
   END DO
else
   OPEN(Unit = 100, file='OUT_Fermi_surface.dat', form='FORMATTED')
   DO ix = 0, MaxK
   DO iy = 0, MaxK
      write(100,'(2x,3(2x,F15.8))') kx(ix), ky(iy), Awk_at_FL(ix,iy)
   END DO
   write(100,*)
   END DO
   CLOSE(100)
end if

deallocate( Hamilton, GreenF, kx, ky, Awk_at_FL)

END SUBROUTINE Fermi_surface_fraction