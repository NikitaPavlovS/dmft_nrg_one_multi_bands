module module_check_convergence
  implicit none
public check_convergence
 integer, public           :: for_exit_loop = 0
! private
 double precision, private :: tempTot = 1.d0
 double precision, private :: Magnetic_moment_prev = 0.d0
 double precision, private :: moment_convergence = 0.d0
 double precision, private :: moment_array(0:3) = 0.d0
 double precision, private :: occupancy_prev = 0.d0
 double precision, private :: occupancy_convergence
 double precision, private :: occupancy_diff
 logical, private          :: occupancy_converged = .false.
 double precision, private :: total_energy_diff(1:2) = 0.d0
 logical, private          :: two_solutions = .false.
 logical, private          :: SDW_converged = .false.

contains

SUBROUTINE check_convergence
  use module_energy
  use module_impurity_1, only : Magnetic_moment, occupancy_total, nspin
  use module_parameters
  implicit none
integer                    :: tmp_i
double precision           :: tmpd
!=============================================================================
   write(output_file,*) '-----     Check convergence     -----'
   total_energy_diff(2) = total_energy_diff(1)
   total_energy_diff(1) = abs(tempTot - totalEn)
   occupancy_convergence = abs(occupancy_prev - occupancy_total)
   write(output_file,*) 'Occupancy convergence =', occupancy_convergence

   ! convergence and tolerance of magnetic moment -----------------------------------------------------------
   if (nspin.eq.2) then
      tmp_i = mod(iter,4)
      moment_array(tmp_i) = Magnetic_moment
      tmpd = Magnetic_moment_prev*Magnetic_moment
      IF (tmpd>0.0d0) THEN  ! check SDW
         moment_convergence = abs(Magnetic_moment_prev - Magnetic_moment)
         if (abs(Magnetic_moment)>1.0D-5) then
            moment_tolerance = 1.0D-3*abs(Magnetic_moment_prev + Magnetic_moment) 
         else
            moment_tolerance = 2.0D-7
         end if
         ! check two solutions
         IF ( (moment_convergence>moment_tolerance).and.(abs(moment_array(0)-moment_array(2))<moment_tolerance).and.(abs(moment_array(1)-moment_array(3))<moment_tolerance).and.(iter>3) ) THEN
               two_solutions=.true.
               write(output_file,'(A23,e15.8,2x,e15.8)') 'There are two solutions M: ',moment_array(0),moment_array(1)
         END IF
      ELSE  ! case of SDW
         moment_convergence = abs(Magnetic_moment_prev + Magnetic_moment)
         moment_tolerance = 1.0D-2*abs(Magnetic_moment)
         if ( (abs(moment_array(0)-moment_array(2))<moment_tolerance).and.(abs(moment_array(1)-moment_array(3))<moment_tolerance).and.(iter>3) ) then
            SDW_converged=.true.
            write(output_file,'(A22,e15.8,2x,e15.8)') 'There is SDW +-+-+-+- M: ',moment_array(0),moment_array(1)  ! case of SDW
         end if
         if (abs(Magnetic_moment)<1.0D-5) then
            !moment_tolerance = 1.0D-1*abs(Magnetic_moment)
            moment_tolerance = 2.0D-7
         else
            write(output_file,'(A30,e15.8,2x,e15.8,A18,i2)') 'Moment changes sign (SDW) M: ',moment_array(0),moment_array(1),'. for_exit_loop = ', for_exit_loop  ! case of SDW
         end if
      END IF
      write(output_file,*) 'Magnetic moment convergence =', moment_convergence
   end if
   ! --------------------------------------------------------------------------------------------------------

   write(output_file,*) 'Total energy convergence =', total_energy_diff(1)

   ! check convergence of occupancy -------------------------------------------------------------------------
   IF (variable_mu) THEN
      occupancy_diff = abs(occupancy_total-n_total_need)
      write(output_file,*) 'Occupancy difference with desired occ =', occupancy_diff
      if ( occupancy_diff < occupancy_tolerance ) then
          write(output_file,*) '+++++    Occupancy is converged!'
          mu_changed = .false.
          occupancy_converged = .true.
      else
         mu_changed = .true.
         occupancy_converged = .false.
      end if
   ELSE
      if ( occupancy_convergence < occupancy_tolerance ) then
         write(output_file,*) '+++++    Occupancy is converged!'
         occupancy_converged = .true.
      else
         occupancy_converged = .false.
      end if
   END IF
   ! --------------------------------------------------------------------------------------------------------

   ! check convergence of total energy ----------------------------------------------------------------------
   if (occupancy_converged) then
      IF ( total_energy_diff(1) < total_energy_tolerance) THEN
         if ( (nspin.eq.2) .and. ( (moment_convergence < moment_tolerance).or.SDW_converged.or.two_solutions ) ) then
            for_exit_loop = for_exit_loop + 1
            write(output_file,'(A73,i2)') '+++++    Magnetic moment and total energy is converged! for_exit_loop = ', for_exit_loop
         else
            for_exit_loop = for_exit_loop + 1
            write(output_file,'(A53,i2)') '+++++    Total energy is converged! for_exit_loop = ', for_exit_loop
         end if
      ELSE
         if (abs(total_energy_diff(2)-total_energy_diff(1))<(total_energy_tolerance*0.001d0)) then
            for_exit_loop = for_exit_loop + 1
            if (nspin.eq.2) then
               write(output_file,'(A118,i2)') '+++++    Magnetic moment is converged. Total energy is not converged. May be: there is two solutions. for_exit_loop = ', for_exit_loop
            else
               write(output_file,'(A89,i2)') '+++++    Total energy is not converged. May be: there is two solutions. for_exit_loop = ', for_exit_loop
            end if
         end if
      END IF
   end if
   ! --------------------------------------------------------------------------------------------------------

   ! save data for next iteration
   if (nspin.eq.2) Magnetic_moment_prev = Magnetic_moment
   occupancy_prev = occupancy_total
   tempTot = totalEn

   write(output_file,*) '-----     Check convergence     -----'

END SUBROUTINE check_convergence

end module module_check_convergence