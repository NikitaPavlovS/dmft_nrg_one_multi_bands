subroutine occupancy_and_mu_search

  use energy_module
  use hamiltonian_module_nband
  use parameters_module
  use impurity_module

   implicit none

!----------------------------------------------------------------------------------------------
double precision              :: delta_mu                        ! shift for mu
double precision              :: dE                              ! step for integration
double precision              :: integral_of_DOS_d(1:nspin)      ! integral from DOS of interacting state
double precision              :: integral_of_DOS_d_present(1:nspin)
double precision              :: occupancy_d(1:nspin)
double precision              :: integral_of_DOS_total(1:nspin)  ! integral from all state DOS
double precision              :: integral_of_DOS_total_present(1:nspin)
double precision              :: occupancy_total(0:nspin)
!----------------------------------------------------------------------------------------------
double precision              :: tmpreal(1:nspin)
integer                       :: i, is
double precision              :: fermisrc  ! function to search mu for need occupancy
double precision, allocatable :: FullDOS(:)
double precision, allocatable :: E_left, E_right
logical                       :: true_search

!-------------------------------------------------
   integral_of_DOS_d_present = 0.d0
   integral_of_DOS_total_present = 0.d0
  do is=1,nspin
   do i=-nmax,-1
      dE = (om(i+1)-om(i))
      integral_of_DOS_d_present(is) = integral_of_DOS_d_present(is) + (dos(is,i)+dos(is,i+1))*dE/2.d0
      integral_of_DOS_total_present(is) = integral_of_DOS_total_present(is) + (totalDOS(is,i)+totalDOS(is,i+1))*dE/2.d0	  !(om(i+1)-om(i))/2
   end do
   occupancy_total(is) = integral_of_DOS_total_present(is)
   occupancy_d(is) = integral_of_DOS_d_present(is)
  end do
!-------------------------------------------------
  do is=1,nspin
   do i=0,nmax-1
      dE = (om(i+1)-om(i))
      integral_of_DOS_d_present(is) = integral_of_DOS_d_present(is) + (dos(is,i)+dos(is,i+1))*dE/2.d0
      integral_of_DOS_total_present(is) = integral_of_DOS_total_present(is) + (totalDOS(is,i)+totalDOS(is,i+1))*dE/2.d0	  !(om(i+1)-om(i))/2
   end do

   dE = (om(nmax)-om(nmax-1))
   integral_of_DOS_d_present(is) = integral_of_DOS_d_present(is) + (dos(is,-nmax)+dos(is,nmax))*dE/2.d0
   integral_of_DOS_total_present(is) = integral_of_DOS_total_present(is) + (totalDOS(is,-nmax)+totalDOS(is,nmax))*dE/2.d0
   occupancy_total(is) = occupancy_total(is) + ( totalDOS(is,-nmax)*dE + totalDOS(is,0)*(om(0)-om(-1)) )/2.d0
   occupancy_d(is) = occupancy_d(is) + ( dos(is,-nmax)*dE + dos(is,0)*(om(0)-om(-1)) )/2.d0
  end do
!-------------------------------------------------

   do is=1,nspin
      tmpreal(is)= dble(ndim) / integral_of_DOS_total_present(is)
      totalDOS(is,:) = totalDOS(is,:) * tmpreal(is)
      occupancy_total(is) = occupancy_total(is) * tmpreal(is)
      tmpreal(is) = 1.d0 / integral_of_DOS_d_present(is)
      dos(is,:) = dos(is,:) * tmpreal(is)
      occupancy_d(is) = occupancy_d(is) * tmpreal(is)
   end do
!-------------------------------------------------
  do is=1,nspin
   dE = (om(nmax)-om(nmax-1))
   integral_of_DOS_d(is) = (dos(is,-nmax)+dos(is,nmax))*dE/2.d0
   integral_of_DOS_total(is) = (totalDOS(is,-nmax)+totalDOS(is,nmax))*dE/2.d0
   do i=-nmax,nmax-1
      dE = (om(i+1)-om(i))
      integral_of_DOS_d(is) = integral_of_DOS_d(is) + (dos(is,i)+dos(is,i+1))*dE/2.d0
      integral_of_DOS_total(is) = integral_of_DOS_total(is) + (totalDOS(is,i)+totalDOS(is,i+1))*dE/2.d0
   end do
  end do

   write(91,*) 'present integral of Aii =', (integral_of_DOS_total_present(is), is=1,nspin )
   write(91,*) 'present integral of Add =', (integral_of_DOS_d_present(is), is=1,nspin )
   write(91,*) 'must equally total electrons =', (integral_of_DOS_total(is), is=1,nspin )
   write(91,*) 'must equally d electrons =', (integral_of_DOS_d(is), is=1,nspin )
   write(91,*) 'total occupancy =', (occupancy_total(is), is=1,nspin )
   write(91,*) 'occupancy of d =', (occupancy_d(is), is=1,nspin )

   allocate(FullDOS(-nmax:nmax))
   FullDOS = 0.d0
   occupancy_total(0) = 0.d0
   do is=1,nspin
      FullDOS(:) = FullDOS(:) + totalDOS(is,:)
      occupancy_total(0) = occupancy_total(0) + occupancy_total(is)
   end do

   IF ( (FullDOS(0)<0.01d0*dble(ndim*nspin)).or.(FullDOS(0)<0.2d0) ) THEN ! for dielectric
      IF ( mu_search_for_diel.and.(FullDOS(0)<0.001d0*dble(ndim*nspin)) ) THEN
         tmpreal(1) = FullDOS(0)*10.d0
         allocate(E_left,E_right)
         E_left = 0.d0
         E_right = 0.d0
         i=1
         true_search = .true.
         do while(true_search)
            IF (FullDOS(-i)>tmpreal(1)) THEN
               E_left = om(-i)
            END IF
            IF (FullDOS(i)>tmpreal(1)) THEN
               E_right = om(i)
            END IF
            if ((abs(E_left)>1E-6).and.(abs(E_right)>1E-6)) then
               true_search = .false.
            end if
            i=i+1
         end do
         delta_mu = (E_left+E_right)/2.d0
         deallocate(E_left,E_right)
      ELSE
         delta_mu = 0.25d0*fermisrc(FullDOS,om,nmax,mu,n_total_need,t,om(-nmax),om(nmax))
      END IF
   ELSE !for metal
      delta_mu = fermisrc(FullDOS,om,nmax,mu,n_total_need,t,om(-nmax),om(nmax))
   END IF
   deallocate(FullDOS)

   IF (abs(delta_mu)>1E-4) THEN
      mu=mu+delta_mu
   END IF
   write(91,*) 'new mu in iteration (', iter, ')= ', mu
   write(91,*)


end subroutine occupancy_and_mu_search
