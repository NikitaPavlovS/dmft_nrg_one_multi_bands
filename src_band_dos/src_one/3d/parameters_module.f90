module parameters_module

  implicit none
  public

double precision, save :: pi = 4.d0*atan(1.d0)

integer,          save :: nspin = 1
integer,          save :: iter = 1

!DMFT variables
integer,          save :: NDMFT      ! number of DMFT iteration
integer,          save :: start_iter ! Circle of DMFT from which start

!----------------------------------------------------------------------------------------------
! Logical calculation variables
integer,          save :: Method_of_Green_Function_calculation = 1 !    Method of Green function calculation:
                               ! CASE (1): G=(E-H+mu-Sigma)^(-1);
                               ! CASE (2) G=(E-H)^(-1);
                               ! CASE (3) Gij(E)=Sum_m EigenVectors_right( i, m) * EigenVectors_left( m, j) / (E-EigenValues(m)+delta)
! Logical starting variables
logical,          save :: INIT_dd    = .false.               ! Start from DOS (Start_dd.dat, files -- fort.201-...):   yes (T), not (F)
logical,          save :: INIT_Sigma = .false.               ! Start from Sigma (Start_sigma.dat, files -- fort.11-...):   yes (T), not (F)
logical,          save :: bare_DOS = .false.                 ! in start Sigma=(0,-delta) => calculate not interacting DOS
logical,          save :: DOS_plot_after_DMFT_loop = .true.  ! Calculate ending file (OUT_Green_image_DOS):   yes (T), not (F)
logical,          save :: linear_integration = .true.        ! Inegration: Linear mesh for k (T) or Gaussian points (F)
! BANDS
logical,          save :: Bands_plot = .false.               ! calculate BANDS:   yes (T), not (F)
logical,          save :: FBANDS = .false.                   ! need Fatbands and coefficient of probability:   yes (T), not (F)
logical,          save :: FBANDS_write = .false.             ! write FatBands to Out Files:   yes (T), not (F)
! SPECTRAL FUNCTION
logical,          save :: Spectral_Function_plot = .false.   ! Calculate data file for plot SF:   yes (T), not (F)
integer,          save :: MaxK_point_for_SF = 300            ! Value of point in one direction of Briluin zone
logical,          save :: Spectral_Function_maximum_plot = .false.      ! Calculate maximums of Spectral Function
double precision, save :: Spectral_Function_maximum_low_cutting = 0.1d0 ! low boundary for spectral function maximum
!FERMI SURFACE
logical,          save :: Fermi_surface_plot = .false.       ! Calculate data file for plot FS:   yes (T), not (F)
double precision, save :: Energy_shift = 0.d0                ! Energy shift from Fermi level, if need calculate Fermi surface not in Fermi level
logical,          save :: Apply_Hand_Sigma_value = .false.   ! If need calc Fermi surface with Sigma=(ReSigma,Hand_Sigma_value)
double precision, save :: Hand_Sigma_value = 0.d0            ! value of Image part of Sigma for manualy blurring Fermi surface
logical,          save :: Fermi_Surface_without_Interaction = .false. ! Calculate bare Fermi surface:   yes (T), not (F)
integer,          save :: number_of_band_crossing_FL = 3              ! number of band, which cross Fermi level, needed to calculate bare Fermi surface

!----------------------------------------------------------------------------------------------
!PSEUDOGAP
logical,          save :: Pseudogap = .false.         ! pseudogap not included (F), included PG (T)
double precision, save :: WW = 0.d0                   ! Delta^2
double precision, save :: KP = 0.d0                   ! inverse Correlation Length in lattace constant units
integer,          save :: ni = 1                      ! Number of intervals in 1 direction  of integration
integer,          save :: nf = 100                    ! Number of recursive steps in PG procedure
integer,          save :: comb = 3                    ! Combinatorics: Incommensurate (1), Commensurate (2), Pines (3)

!----------------------------------------------------------------------------------------------
!Magnetic properties
double precision, save :: H_field = 0.d0            ! magnetic field in eV
!----------------------------------------------------------------------------------------------



end module parameters_module