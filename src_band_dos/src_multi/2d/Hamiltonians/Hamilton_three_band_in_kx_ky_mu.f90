SUBROUTINE Hamilton_multi_band_in_kx_ky_mu( kx, ky, mu, Hamilton)

  USE hamiltonian_module_Nband
  IMPLICIT NONE

double precision,    intent(in) :: kx, ky
double precision,    intent(in) :: mu
complex*16,         intent(out) :: Hamilton( 1:ndim, 1:ndim)
!===============================================================
double precision                  :: cos_kx, cos_ky, sin_kx2, sin_ky2, temp_x, temp_y
!====================================================
      cos_kx = Cos(kx)
      cos_ky = Cos(ky)
      sin_kx2 = Sin(kx/2.d0)
      sin_ky2 = Sin(ky/2.d0)
      temp_x = 2.d0*(tpd*sin_kx2 + 2.d0*tpd1*sin_kx2*cos_ky+ tpd2*Sin(3.d0*kx/2.d0))
      temp_y = 2.d0*(tpd*sin_ky2 + 2.d0*tpd1*sin_ky2*cos_kx+ tpd2*Sin(3.d0*ky/2.d0))
!------------------------------------------------------------------------------------------------
      Hamilton(1,1) = dcmplx( Ed + 2.d0*tdd*(cos_kx + cos_ky) - mu, 0.d0)
      Hamilton(1,2) = dcmplx( 0.d0, temp_x)
      Hamilton(1,3) = dcmplx( 0.d0, temp_y)
      Hamilton(2,1) = dcmplx( 0.d0, -temp_x)
      Hamilton(2,2) = dcmplx( Ep + 2.d0*tpp1*Cos(2.d0*kx) - mu, 0.d0)
      Hamilton(2,3) = dcmplx( -4.d0*tpp*sin_kx2*sin_ky2, 0.d0)
      Hamilton(3,1) = dcmplx( 0.d0, -temp_y)
      Hamilton(3,2) = Hamilton(2,3)
      Hamilton(3,3) = dcmplx( Ep + 2.d0*tpp1*Cos(2.d0*ky) - mu, 0.d0)
!====================================================


END SUBROUTINE Hamilton_multi_band_in_kx_ky_mu
