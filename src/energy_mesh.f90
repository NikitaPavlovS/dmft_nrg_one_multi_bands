SUBROUTINE Energy_mesh( E, nmax_temp)
  use module_energy
  use module_parameters, only : output_file
implicit none
! energy mesh
! The code assumes a symmetric energy mesh.
!=========================================================================================
! External variables:
      integer,           intent(in) :: nmax_temp
      double precision, intent(out) :: E(-nmax_temp:nmax_temp)
!-------------------------------------------------------------------
! Internal variables:
integer            :: i, j
!-------------------------------------------------------------------
double precision   :: temp_double
!-------------------------------------------------------------------
!for exponential energy mesh
double precision :: a
double precision :: b

 !==================================================================
 ! Exponential energy mesh (-Emax,Emax)
 IF (exp_mesh) THEN
    nmax=Emax*factor_of_step
    b=log(dble(Emax)/Emin_exp_mesh)/dble(nmax-1)
    a=Emin_exp_mesh*exp(-b)
    do i=-nmax,nmax
       if (i.EQ.0) then
          E(i)=0.d0
       else
          if(i.lt.0) then
             E(i)=-a*exp(-b*i)
          else
             E(i)=a*exp(b*i)
          end if
       end if
    end do
 !==================================================================
 ! Linear energy mesh
 ELSE
    nmax=Emax*factor_of_step
    do i=-nmax,nmax
       E(i)=-Emax+dble(nmax+i)/dble(factor_of_step)
    end do
 END IF
 !==================================================================
 write(output_file,*) 'In this calc namber of energy point: nmax=', nmax

END SUBROUTINE Energy_mesh
