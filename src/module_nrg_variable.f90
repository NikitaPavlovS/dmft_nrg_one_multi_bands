module module_nrg_variable

  implicit none
  public

!--------------------------------------------------------------------------------------------
! NRG parameter
double precision,              save :: u     ! Coulomb interaction

double precision,              save :: lam   ! NRG logarithmic discretization parameter
integer,                       save :: nkeep ! the number of states kept in the NRG iteration
                                             ! nkeep=800 typical for one band problem
integer,                       save :: iflag ! iflag=-1 initialize NRG computation
                                             ! (do once before perform NRG)
                                             ! iflag=0 will produce no output
                                             ! 0<iflag<10 will produce some output
                                             ! iflag>10 will correct for acausal self-energies
                                             !          (setting Im(Sig) to some small negative value)
                                             ! see more details in in NRG_for_Fortran.C
integer,                       save :: iover ! iover=0 on NRG exit keeps your energy mesh
                                             !        self-energy interpolated (Hermite spline) to the mesh
                                             ! iover=1 Use final NRG mesh 
character*1,                   save :: P = 'N' ! 'P' - particle-hole symmetry forced
                                             ! For those familiar with the SIAM:
                                             ! mu=-eps, where eps is the local energy of the SIAM.
                                             ! Thus, mu=U/2 corresponds to particle-hole symmetry.
                                             ! 'N' - une can use your mu
character*1,                   save :: SU2_sim = 'N' ! S - 1 spin, other ...
double precision,              save :: occupancy_nrg
!--------------------------------------------------------------------------------------------
! for search Lambda
double precision,              save :: precision_of_N = 1.0D-10 ! precision to search border of N
double precision,              save :: precision_of_T = 1.0D-3  ! precision to search temperature
      
end module module_nrg_variable