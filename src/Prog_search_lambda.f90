PROGRAM Prog_search_lambda
  use module_energy, only       : Emax
  use module_nrg_variable, only : precision_of_N, precision_of_T
  use module_parameters,   only : output_file
implicit none

double precision :: t0
double precision :: search_lambda
logical          :: t_found
double precision :: lam
integer          :: ios
character(64)    :: argname_1, argname_2, argname_3, filename_in

!========================================================================================================================
   !  Needed Emax
   call getarg( 1, argname_1 )
   call getarg( 2, argname_2 )
   call getarg( 3, argname_3 )
   argname_1 = trim(adjustl(argname_1))
   open( 11, form='formatted', status='scratch', iostat=ios)
   write(11,*) argname_1
   rewind(11)
   read(11,*,iostat=ios) Emax
   if ( ios /= 0 ) then
      stop 'Give arguments: "Emax of energy mesh" "needed T" "precision of T"'
   end if
   close(11)
   !  Needed T
   argname_2 = trim(adjustl(argname_2))
   open( 11, form='formatted', status='scratch', iostat=ios)
   write(11,*) argname_2
   rewind(11)
   read(11,*,iostat=ios) t0
   if ( ios /= 0 ) then
      stop 'Give arguments: "needed T" "precision of T"'
   end if
   close(11)
   !  Needed precision of T
   argname_3 = trim(adjustl(argname_3))
   open( 11, form='formatted', status='scratch', iostat=ios)
   write(11,*) argname_3
   rewind(11)
   read(11,*,iostat=ios) precision_of_T
   if ( ios /= 0 ) then
      stop 'Give arguments: "needed T" "precision of T"'
   end if
   close(11)

   ! output_file = 0 for wtite to disply
   open(unit = output_file, FILE = 'NRG_T_out.dat', FORM='FORMATTED')
   lam = search_lambda(t0, t_found)
   close(output_file)

END PROGRAM Prog_search_lambda
