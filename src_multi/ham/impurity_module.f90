module impurity_module

  implicit none
  public

double precision, save :: t          ! temperature in eV
                                       !(later t defines number of NRG iteration N
                                       ! (lam)^(-N/2)~=t
double precision, save :: u          ! Coulomb interaction
!--------------------------------------------------------------------------------------------
!NRG variables
complex*16,       allocatable, save :: sig(:,:) ! sig contains on  of NRG the self-energy
double precision, allocatable, save :: dd(:,:)  ! hybridization function -1/piIm(1/G+Sigma)
double precision,              save :: occ    ! occupancy ???

double precision,              save :: lam   ! NRG logarithmic discretization parameter
integer,                       save :: nkeep ! the number of states kept in the NRG iteration
                                         ! nkeep=800 typical for one band problem
integer,                       save :: iflag  ! iflag=-1 initialize NRG computation
                                         ! (do once before perform NRG)
                                         ! iflag=0 will produce no output
                                         ! 0<iflag<10 will produce some output
                                         ! iflag>10 will correct for acausal self-energies
                                         !          (setting Im(Sig) to some small negative value)
                                         ! see more details in in NRG_for_Fortran.C
integer,                       save :: iover ! iover=0 on NRG exit keeps your energy mesh
                                         !        self-energy interpolated (Hermite spline) to the mesh
                                         ! iover=1 Use final NRG mesh 
character*1,                   save :: P = 'N'       ! 'P' - particle-hole symmetry forced
                                         ! For those familiar with the SIAM:
                                         ! mu=-eps, where eps is the local energy of the SIAM.
                                         ! Thus, mu=U/2 corresponds to particle-hole symmetry.
                                         ! 'N' - une can use your mu
character*1,                   save :: SU2_sim = 'N' ! S - 1 spin, other ...


end module impurity_module