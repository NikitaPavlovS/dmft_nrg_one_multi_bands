module hamiltonian_module_1band

  implicit none
  public
!----------------------------------------------------------------------------------------------
! Hamilton variables
integer,          save :: ndim = 1                    ! dimension of Hamilton
double precision, save :: tt, t1, t2                  ! hopping parameters
double precision, save :: qval                        ! total occupancy of interaction state
double precision, save :: delta=0.005d0               ! delta for calculate Green function (small image part)
!----------------------------------------------------------------------------------------------
!Green function
double precision, allocatable, save :: dos(:,:)       ! -1/pi ImG      (spin and energy)
double precision, allocatable, save :: dos_0(:,:)     ! bare DOS for calc G from DOS (spin and energy)
double precision, allocatable, save :: totalDOS(:)    ! total dos as energy mesh  (spin and energy)
complex*16,       allocatable, save :: sig(:,:)       ! Sigma (spin and energy)
!----------------------------------------------------------------------------------------------

end module hamiltonian_module_1band
