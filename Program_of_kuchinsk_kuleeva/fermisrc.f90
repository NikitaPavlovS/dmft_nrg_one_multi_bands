      function fermisrc(dos,e,nr,mu,qval,temp,mumin,mumax)
      implicit none
!----------------------------------------------------------------------|
!                                                                      |
!                           Fermi level search                         |
!                                                                      |
!----------------------------------------------------------------------|
!                                                                      |
!i     dos        - DOS                                                |
!i     e          - energy array                                       |
!i     nr         - number of points                                   |
!i     mu         - old value of Fermi level                           |
!i     qval       - number of valence electrons                        |
!i     temp       - temperature                                        |
!i     tol        - occuracy of search of Fermi level                  |
!                                                                      |
!o     mu         - new value of Fermi level                           |
!                                                                      |
!----------------------------------------------------------------------|

!---> Passed parameters
      integer         :: ir,nr,nit,ferit
      double precision,intent(in)   :: dos(-nr:nr),e(-nr:nr),qval,temp,mumin,mumax
      double precision,intent(in)   :: mu
      double precision              :: fermisrc
!      integer nr,ferit
!      double precision mu,degen,temp,fermi,qval,elnum
!      double precision dos(nr),e(0:nr)
!---> Local parameters
      logical         :: scf
      double precision:: dosint,occup,nel1,nel2,&
                         tol,nelnew,munew,fermi,mu1,mu2

      
!---> Starting values      
      mu1 = mumin
      mu2 = mumax
      nel1 = 0.d0
      nel2 = 0.d0
      munew = 0.d0
      nelnew = 0.d0
      dosint = 0.d0
      tol=1.d-5
!---> Number of fermi iteration
      nit = 0
      ferit = 10000

!---> Occupancy
      occup = 0.5d0*qval

      scf = .true.

      do while ( scf )
        nit = nit + 1

!---> calculate nelnew in point munew = (mu2+mu1)/2
        munew = (mu2+mu1)/2.d0
        nelnew=0.d0
	do ir = -nr,nr-1
          nelnew = nelnew + &
	  (dos(ir)*fermi(e(ir),temp,munew)+&
	   dos(ir+1)*fermi(e(ir+1),temp,munew))*5.d-1*&
	   (e(ir+1)-e(ir))
!write(*,*)ir,dos(ir),nelnew
        enddo
!stop 'fermisrc'
        if (nelnew.gt.occup) then
           mu2=munew
        else
           mu1=munew
        endif

        if (dabs(nelnew-occup).lt.tol.or.&
!           dabs(mu2-mu1).lt.tol.or.&
           nit.eq.ferit) scf=.false.
write(*,*)'munew=',munew,'  nelnew=',nelnew      
      enddo ! while

      fermisrc=munew

!---If Fermi level still not found      
      if (nit.eq.1000) stop 'Error while Fermi search'
      
      end
