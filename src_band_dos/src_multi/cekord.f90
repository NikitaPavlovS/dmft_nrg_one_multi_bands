      subroutine cekord( ck,cknew,cr,crnew,ndim )
!|--------------------------------------------------------------------|
!|                                                                    |
!|   Sorts complex eigenvalues on its real parts in ascending order   |
!|                                                                    |
!|   Inputs:                                                          |
!i     ndim -  size of arrays                                         |
!|   Inputs/Outputs:                                                  |
!io    ck   -  complex eigenvalues                                    |
!io    cr   -  matrix of right eigenvectors                           |
!|   Works:                                                           |
!w     cknew,crnew                                                    |
!|                                                                    |
!---------------------------------------------------------------------|
      implicit none
!--->  Passed variables      
      integer ndim
      complex*16 ck(ndim),cknew(ndim),cr(ndim,ndim),crnew(ndim,ndim)
!--->  Local variables      
      integer i,j,l,imin,nmin
      double precision dmin
!--->  Intrinsic
      intrinsic dcmplx,dreal      

      nmin = 0 
      do j = 1,ndim 
        
	nmin = nmin + 1
        dmin = dreal( ck(ndim-nmin+1) )
        imin = ndim - nmin + 1
        do i = 1,ndim-nmin
          if( dmin.ge.dreal( ck(i) ) )then
            dmin = dreal( ck(i) )
            imin = i
          end if
        end do  
        
	cknew(nmin) = ck(imin)
        do i = 1,ndim
          crnew(i,nmin) = cr(i,imin)
        end do  
        do i = 1,ndim-nmin
          if( i.ge.imin )then
            ck(i) = ck(i+1)
            do l = 1,ndim
              cr(l,i) = cr(l,i+1)
            end do  
          end if  
        end do
        ck(ndim-nmin+1) = dcmplx(1.d+100,0.d0)
      end do    !    j   
      
      do i = 1,ndim
        ck(i) = cknew(i)
        do j = 1,ndim
          cr(i,j) = crnew(i,j)
        end do
      end do   
       
      end
