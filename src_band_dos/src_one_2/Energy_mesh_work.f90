SUBROUTINE Energy_mesh( E, nmax_temp)

USE energy_module

IMPLICIT NONE
! energy mesh. DESCRIPTION BELOW variables
! The code assumes a symmetric energy mesh,
! The first value !=0 will be used as energy scale.
!=========================================================================================
! External variables:
      integer,           intent(in) :: nmax_temp
      double precision, intent(out) :: E(-nmax_temp:nmax_temp)
!-------------------------------------------------------------------
! Internal variables:
integer            :: i, j
!-------------------------------------------------------------------
integer, parameter :: npoint_10_8=40, npoint_8_1=700 ! number of points for mesh_near_zero
Integer            :: npoint_Emax_10, npoint_Emax_1 ! number of points from E=-Emax to E=-10 and E=-1 eV, step between them 0.01
!integer, parameter :: npoint_1_03=350
double precision   :: temp_double
!-------------------------------------------------------------------
!for exponential energy mesh
double precision :: a
double precision :: b
double precision :: Emax_exp_mesh   !  (-5,5)

!====================================
! IF Linear_mesh:
!      IF const_mesh:
!           linear mash from -15 to 15 with 0.01 step
!-------------------------------------------------------------------
!      ELSE (const_mesh):
!            IF mesh_near_zero:
!                 mesh from -15 to -10 with 0.1 step,  from -10 to -8 with 0.05 step, from -8 to -1 with 0.01 step,
!           ELSE (mesh_near_zero):
!                 mesh from Emax to -1 with 0.01 step
!-------------------------------------------------------------------
! ELSE (Linear_mesh)
!       Exponential energy mesh form -15 to 15 eV with thickness near Fermi level (E=0)
!====================================

!=========================================================================================================================================
!=========================================================================================================================================
!Linear energy mesh
IF (Linear_mesh) THEN
   IF (const_mesh) THEN ! linear mash from -Emax to Emax with 0.01 step
      !==================================================================
      nmax=Emax*factor_of_step
      !==================================================================
      do i=-nmax,nmax
         E(i)=-Emax+dble(nmax+i)/dble(factor_of_step)
      end do
   !=========================================================================================================================================
   ELSE ! const_mesh
       IF (mesh_near_zero) THEN
         ! mesh from -Emax to -10 with 0.1 step,  from -10 to -8 with 0.05 step, from -8 to -1 with 0.01 step,
         ! from -1 to -0.03 with 0.005 step, and from -0.03 to 0.03 with 0.001 step
         !==================================================================
         npoint_Emax_10 = (Emax-10)*10
         nmax=npoint_Emax_10+npoint_10_8+npoint_8_1+npoint_1_03+npoint_03_0
         !==================================================================
         j=0
         temp_double = dble(npoint_Emax_10)/(15.d0-10.d0)
         do i=-nmax,-(nmax-npoint_Emax_10)
            E(i)=-15.d0+dble(j)/temp_double
            j=j+1
         end do
         j=1
         temp_double = dble(npoint_10_8)/(10.d0-8.d0)
         do i=-(nmax-npoint_Emax_10-1),-(nmax-npoint_Emax_10-npoint_10_8)
            E(i)=-10.d0+dble(j)/temp_double
            j=j+1
         end do
         j=1
         temp_double = dble(npoint_8_1)/(8.d0-1.d0)
         do i=-(nmax-npoint_Emax_10-npoint_10_8-1),-(npoint_1_03+npoint_03_0)
            E(i)=-8.d0+dble(j)/temp_double
            j=j+1
         end do
         j=1
         temp_double = dble(npoint_1_03)/(1.d0-0.3d0)
         do i=-(npoint_1_03+npoint_03_0-1),npoint_03_0
            E(i)=-1.d0+dble(j)/temp_double
            j=j+1
         end do
         temp_double = dble(npoint_03_0)/(0.3d0-0.0d0)
         do i=-npoint_03_0,-1
            E(i)=-0.3d0+dble(npoint_03_0+i)/temp_double
         end do
         E(0)=0.d0
         do i=1,npoint_03_0
            E(i)=-0.3d0+dble(npoint_03_0+i)/temp_double
         end do
         j=1
         temp_double = dble(npoint_1_03)/(1.d0-0.3d0)
         do i=npoint_03_0+1,(npoint_1_03+npoint_03_0)
            E(i)=0.3d0+dble(j)/temp_double
            j=j+1
         end do
         j=1
         temp_double = dble(npoint_8_1)/(8.d0-1.d0)
         do i=(npoint_1_03+npoint_03_0+1),(nmax-npoint_Emax_10-npoint_10_8)
            E(i)=1.d0+dble(j)/temp_double
            j=j+1
         end do
         j=1
         temp_double = dble(npoint_10_8)/(10.d0-8.d0)
         do i=(nmax-npoint_Emax_10-npoint_10_8+1),(nmax-npoint_Emax_10)
            E(i)=8.d0+dble(j)/temp_double
            j=j+1
         end do
         j=0
         temp_double = dble(npoint_Emax_10)/(15.d0-10.d0)
         do i=(nmax-npoint_Emax_10),nmax
            E(i)=10.d0+dble(j)/temp_double
            j=j+1
         end do
    !--------------------------------------------------------------------------------------------------------------------------------------
      ELSE ! mesh_near_zero
         ! mash from -Emax to -1 with 0.01 step
         ! from -1 to -0.03 with 0.005 step, and from -0.03 to 0.03 with 0.001 step
         !==================================================================
         IF (const_with_exp_near_zero) THEN
            npoint_Emax_1 = (Emax-1)*factor_of_step
            nmax=npoint_Emax_1 + npoint_1_03 + nmax_exp_mesh
            j=0
            temp_double = dble(npoint_Emax_1)/(dble(Emax)-1.d0)
            do i=-nmax,-(nmax-npoint_Emax_1)
                E(i)=-dble(Emax)+dble(j)/temp_double
                j=j+1
           end do
           !-------------------------------------------------------------------
            j=1
            temp_double = dble(npoint_1_03)/(1.d0-0.3d0)
            do i=-(npoint_1_03+nmax_exp_mesh-1),nmax_exp_mesh
               E(i)=-1.d0+dble(j)/temp_double
               j=j+1
            end do
            Emax_exp_mesh = 0.3d0
            b=log(Emax_exp_mesh/Emin_exp_mesh)/(nmax_exp_mesh-1)
            a=Emin_exp_mesh*exp(-b)
            do i=-nmax_exp_mesh,nmax_exp_mesh
                if (i.EQ.0) then
                   E(i)=0.d0
                else
                   if(i.lt.0) then
                      E(i)=-a*exp(-b*i)
                   else
                      E(i)=a*exp(b*i)
                   end if
               end if
            end do
            j=1
            temp_double = dble(npoint_1_03)/(1.d0-0.3d0)
            do i=nmax_exp_mesh+1,(npoint_1_03+nmax_exp_mesh)
               E(i)=0.3d0+dble(j)/temp_double
               j=j+1
            end do
            j=1
            temp_double = dble(npoint_Emax_1)/(dble(Emax)-1.d0)
            do i=(npoint_1_03+nmax_exp_mesh+1),nmax
               E(i)=1.d0+dble(j)/temp_double
               j=j+1
            end do            
         ELSE !const_with_exp_near_zero=F
            npoint_Emax_1 = (Emax-1)*factor_of_step
            nmax=npoint_Emax_1 + npoint_1_03 + npoint_03_0
            j=0
            temp_double = dble(npoint_Emax_1)/(dble(Emax)-1.d0)
            do i=-nmax,-(nmax-npoint_Emax_1)
                E(i)=-dble(Emax)+dble(j)/temp_double
                j=j+1
           end do
           !-------------------------------------------------------------------
            j=1
            temp_double = dble(npoint_1_03)/(1.d0-0.3d0)
            do i=-(npoint_1_03+npoint_03_0-1),npoint_03_0
               E(i)=-1.d0+dble(j)/temp_double
               j=j+1
            end do
            temp_double = dble(npoint_03_0)/(0.3d0-0.0d0)
            do i=-npoint_03_0,-1
               E(i)=-0.3d0+dble(npoint_03_0+i)/temp_double
            end do
            E(0)=0.d0
            do i=1,npoint_03_0
               E(i)=-0.3d0+dble(npoint_03_0+i)/temp_double
            end do
            j=1
            temp_double = dble(npoint_1_03)/(1.d0-0.3d0)
            do i=npoint_03_0+1,(npoint_1_03+npoint_03_0)
               E(i)=0.3d0+dble(j)/temp_double
               j=j+1
            end do
            j=1
            temp_double = dble(npoint_Emax_1)/(dble(Emax)-1.d0)
            do i=(npoint_1_03+npoint_03_0+1),nmax
               E(i)=1.d0+dble(j)/temp_double
               j=j+1
            end do
        END IF
      END IF ! mesh_near_zero
  END IF ! const_mesh
!=========================================================================================================================================
!=========================================================================================================================================
ELSE ! Linear_mesh
!Exponential energy mesh (-Emax,Emax)
   !==================================================================
   Emax_exp_mesh = dble(Emax)
   nmax = nmax_exp_mesh
   !==================================================================
   b=log(Emax_exp_mesh/Emin_exp_mesh)/dble(nmax-1)
   a=Emin_exp_mesh*exp(-b)
   do i=-nmax,nmax
      if (i.EQ.0) then
         E(i)=0.d0
      else
         if(i.lt.0) then
            E(i)=-a*exp(-b*i)
         else
            E(i)=a*exp(b*i)
         end if
      end if
   end do
END IF
!=========================================================================================================================================
write(*,*) 'In this calc namber of energy point: nmax=', nmax

END SUBROUTINE Energy_mesh