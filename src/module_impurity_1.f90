module module_impurity_1

  implicit none
  public

integer,                       save :: model=2       ! What is model used: Bethe (1), FCC (2), from DOS(0) ?

double precision,              save :: t             ! temperature in eV
                                                     !(later t defines number of NRG iteration N
                                                     ! (lam)^(-N/2)~=t
integer,                       save :: nspin=1
!--------------------------------------------------------------------------------------------
complex*16,       allocatable, save :: G(:,:)        ! Green function
complex*16,       allocatable, save :: G_prev(:,:)        ! Green function
complex*16,       allocatable, save :: sig(:,:)      ! sig contains on  of NRG the self-energy
double precision, allocatable, save :: dd(:,:)       ! hybridization function -1/piIm(1/G+Sigma)
double precision, allocatable, save :: dos(:,:)      ! -1/pi ImG      (spin and energy)
double precision, allocatable, save :: totalDOS(:)   ! total dos as energy mesh  (spin and energy)
double precision, allocatable, save :: dos_0(:)

double precision,              save :: delta=0.005d0  ! delta for calculate Green function (small image part)
! Occupancy
double precision, allocatable, save :: occupancy_d(:)
double precision,              save :: occupancy_total
double precision,              save :: Magnetic_moment

end module module_impurity_1
