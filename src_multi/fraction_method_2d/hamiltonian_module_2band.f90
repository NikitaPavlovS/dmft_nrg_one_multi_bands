module hamiltonian_module_nband

  implicit none
  public
!----------------------------------------------------------------------------------------------
! Hamilton variables
integer,          save :: ndim = 2   ! dimension of Hamilton
double precision, save :: Ed1, Ed2   ! energy parameters
double precision, save :: tdd        ! hopping parameters
double precision, save :: qval                        ! total occupancy of interaction state
double precision, save :: delta=0.005d0               ! delta for calculate Green function (small image part)
!----------------------------------------------------------------------------------------------
!Green function
double precision, allocatable, save :: dos(:,:)       ! -1/pi ImG      (spin and energy)
double precision, allocatable, save :: totalDOS(:,:)  ! total dos as energy mesh  (spin and energy)
!----------------------------------------------------------------------------------------------
!Double-counting variables
logical,          save :: hole_occupancy         ! define sing before Edc by calculation through n_d
double precision, save :: Edc = 0.d0             ! Value of Double Counting energy
logical,          save :: Edc_const = .true.     ! Constant value (T) or Calculated from occupancy (F)
logical,          save :: start_Edc = .true.     ! need Start from Other Edc:   yes (T), not (F)
double precision, save :: start_Edc_value = 0.d0 ! Value of starting Double Counting energy

end module hamiltonian_module_nband