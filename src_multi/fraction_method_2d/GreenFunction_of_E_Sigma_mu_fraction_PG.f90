SUBROUTINE GreenFunction_of_E_Sigma_mu_fraction_PG( E, Sigma, mu, Integ_G)

  use hamiltonian_module_nband
  use parameters_module
  use Gaussian_points_module

implicit none
!=======================================================================
! Green function variables
! global variable
   double precision,  intent(in) :: E      ! value of energy point
   double precision,  intent(in) :: mu
   complex*16,        intent(in) :: Sigma(1:nspin)
   complex*16,       intent(out) :: Integ_G(1:nspin,0:ndim)  ! Green function of E integrated over k for d, p1, p2, and all
!=======================================================================
! LOCAL variables
complex*16,       allocatable :: Hamilton(:,:)
! Green function
complex*16,       allocatable :: GreenF(:,:,:)
   ! k points array
   double precision              :: kx, ky, kMin, kMax
   ! Pseudogap variables
   complex*16       :: PG
   double precision :: Energy_k                    ! Eigenvalue in (kx,ky)
   double precision :: Energy_k_shifted            ! Eigenvalue in (kx+pi,ky+pi)
   double precision :: Velocity, Velocity_shifted  ! velocity in (kx,ky) and in (kx+pi,ky+pi)
   ! needed temporary variables
   complex*16       :: gs(1:nspin,1:ndim), gs2(1:nspin,1:ndim)
   integer          :: ix, iy
   double precision :: dk_ni
   integer          :: is, i
   integer          :: kpoints

   double precision, allocatable :: xs, ys, xm, ym
   integer,          allocatable :: lx, ly, jx, jy, jy2
!=======================================================================
kpoints = 2*ni*ngp
kMax = pi
kMin = -pi
ALLOCATE(Hamilton( 1:ndim, 1:ndim))
ALLOCATE(GreenF( 1:nspin, 1:ndim, 1:ndim))

!=======================================================================
IF (linear_integration) THEN
   gs(1:nspin,1:ndim)=dcmplx(0.d0,0.d0)
   dk_ni = (kMax-kMin)/kpoints
   kx=kMin
   do ix = 1,kpoints
      kx = kx+dk_ni
      ky=kMin
      do iy = 1,kpoints
         ky = ky+dk_ni
  CALL Hamilton_n_band_in_kx_ky( kx, ky, Hamilton)
  CALL Spectrum_and_Velocity_in_kx_ky_through_G_fraction( Hamilton, E, kx, ky, mu, pi,&
                                                          Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
  DO is=1, nspin
    CALL fract(ww, KP, E, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, Sigma(is), comb)
    CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, Sigma(is)+PG, GreenF(is,:,:))
  END DO
         do is=1, nspin
           do i=1, ndim
              gs(is,i)= gs(is,i) + GreenF(is,i,i)
           end do
         end do
      end do
   end do
   Integ_G(1:nspin,0)= (0.d0,0.d0)
   do is=1, nspin
    do i=1, ndim
       Integ_G(is,i) = gs(is,i)/(kpoints*kpoints)
       Integ_G(is,0) = Integ_G(is,0) + Integ_G(is,i)
    end do
   end do
!==========================================================================================
ELSE ! Gausian integration
!==========================================================================================
   Call Gaussian_points_and_weights
   allocate(xs, ys, xm, ym)
   allocate(lx, ly, jx, jy, jy2)
   !-------------------------------------------------
   gs(1:nspin,1:ndim) = dcmplx(0.d0,0.d0)
   dk_ni = (kMax-kMin)/(2.d0*ni)
   xs = kMin - dk_ni
   DO lx = 1, ni
      xs = xs + 2.d0*dk_ni
      DO ix = 1, ngp
         xm = dk_ni*x(ix)

         gs2(1:nspin,1:ndim) = dcmplx(0.d0,0.d0)
         kx = xs - xm
         ys = kMin - dk_ni
         DO ly = 1, ni
            ys = ys + 2.d0*dk_ni
            DO iy = 1, ngp
               ym = dk_ni*x(iy)
               ky = ys - ym
  CALL Hamilton_n_band_in_kx_ky( kx, ky, Hamilton)
  CALL Spectrum_and_Velocity_in_kx_ky_through_G_fraction( Hamilton, E, kx, ky, mu, pi,&
                                                          Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
  DO is=1, nspin
    CALL fract(ww, KP, E, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, Sigma(is), comb)
    CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, Sigma(is)+PG, GreenF(is,:,:))
  END DO
               do i=1, ndim
                  gs2(1:nspin,i)=gs2(1:nspin,i)+w(iy)*GreenF(1:nspin,i,i)
               end do
               ky = ys + ym
  CALL Hamilton_n_band_in_kx_ky( kx, ky, Hamilton)
  CALL Spectrum_and_Velocity_in_kx_ky_through_G_fraction( Hamilton, E, kx, ky, mu, pi,&
                                                          Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
  DO is=1, nspin
    CALL fract(ww, KP, E, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, Sigma(is), comb)
    CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, Sigma(is)+PG, GreenF(is,:,:))
  END DO
               do i=1, ndim
                  gs2(1:nspin,i)=gs2(1:nspin,i)+w(iy)*GreenF(1:nspin,i,i)
               end do
            ENDDO ! iy
         ENDDO ! ly
         do i=1, ndim
            gs(1:nspin,i) = gs(1:nspin,i) + w(ix)* gs2(1:nspin,i)
         end do
      ENDDO ! ix
      DO ix = 1, ngp
         xm = dk_ni*x(ix)

         gs2(1:nspin,1:ndim) = dcmplx(0.d0,0.d0)
         kx = xs + xm
         ys = kMin - dk_ni
         DO ly = 1, ni
            ys = ys + 2.d0*dk_ni
            DO iy = 1, ngp
               ym = dk_ni*x(iy)
               ky = ys - ym
  CALL Hamilton_n_band_in_kx_ky( kx, ky, Hamilton)
  CALL Spectrum_and_Velocity_in_kx_ky_through_G_fraction( Hamilton, E, kx, ky, mu, pi,&
                                                          Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
  DO is=1, nspin
    CALL fract(ww, KP, E, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, Sigma(is), comb)
    CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, Sigma(is)+PG, GreenF(is,:,:))
  END DO
               do i=1, ndim
                  gs2(1:nspin,i)=gs2(1:nspin,i)+w(iy)*GreenF(1:nspin,i,i)
               end do
               ky = ys + ym
  CALL Hamilton_n_band_in_kx_ky( kx, ky, Hamilton)
  CALL Spectrum_and_Velocity_in_kx_ky_through_G_fraction( Hamilton, E, kx, ky, mu, pi,&
                                                          Energy_k, Energy_k_shifted, Velocity, Velocity_shifted)
  DO is=1, nspin
    CALL fract(ww, KP, E, Energy_k, Energy_k_shifted, Velocity, Velocity_shifted, nf, PG, Sigma(is), comb)
    CALL GreenFunction_of_Hamiltonian_fraction( E, mu, Hamilton, Sigma(is)+PG, GreenF(is,:,:))
  END DO
               do i=1, ndim
                  gs2(1:nspin,i)=gs2(1:nspin,i)+w(iy)*GreenF(1:nspin,i,i)
               end do
            ENDDO ! iy
         ENDDO ! ly
         do i=1, ndim
            gs(1:nspin,i) = gs(1:nspin,i) + w(ix)* gs2(1:nspin,i)
         end do
      ENDDO ! ix
   ENDDO ! lx

   Integ_G(:,0)= (0.d0,0.d0)
   DO is=1, nspin
    DO i=1, ndim
      Integ_G(is,i) = gs(is,i)!*dk_ni*dk_ni
      Integ_G(is,0) = Integ_G(is,0) + Integ_G(is,i)
    ENDDO
   END DO

   deallocate(xs, ys, xm, ym)
   deallocate(lx, ly, jx, jy, jy2)

END IF ! (lenear or Gausian integration)

DEALLOCATE( GreenF, Hamilton)
!==========================================================================================

END SUBROUTINE GreenFunction_of_E_Sigma_mu_fraction_PG


