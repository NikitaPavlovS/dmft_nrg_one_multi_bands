SUBROUTINE Hamilton_multi_band_in_kx_ky( kx, ky,Hamilton)

  USE hamiltonian_module_Nband
  IMPLICIT NONE

double precision,    intent(in) :: kx, ky
complex*16,         intent(out) :: Hamilton( 1:ndim, 1:ndim)
!===============================================================
      Hamilton(1,1) = dcmplx( (kx + ky)/6.d0, 0.d0)
      Hamilton(1,2) = dcmplx( tdd, 0.d0)
      Hamilton(2,1) = dcmplx( tdd, 0.d0)
      Hamilton(2,2) = dcmplx( (kx + ky)/6.d0, 0.d0)
!====================================================


END SUBROUTINE Hamilton_multi_band_in_kx_ky
