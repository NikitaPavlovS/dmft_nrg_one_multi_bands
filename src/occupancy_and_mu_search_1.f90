subroutine occupancy_and_mu_search_1

  use module_energy
  use module_impurity_1
  use module_parameters
  use module_integration

   implicit none
!----------------------------------------------------------------------------------------------
double precision              :: delta_mu                   ! shift for mu
double precision              :: dE                         ! step for integration
double precision              :: int_of_DOS_d(1:nspin)      ! integral from DOS of interacting state
double precision              :: int_of_DOS_total
double precision, allocatable :: fermi_function(:)
double precision              :: fermi
!----------------------------------------------------------------------------------------------
integer                       :: i, is
double precision              :: fermisrc  ! function to search mu for need occupancy
!double precision              :: tmpreal(1:nspin)
!integer                       :: om_of_mu
!========================================================================================================================
write(output_file,*)'-----     Occupancy     -----'

allocate(fermi_function(-nmax:nmax))
  do i=-nmax,nmax
     fermi_function(i) = fermi(om(i),t,0.d0)
  end do

IF (nspin==1)  THEN
   !----- capacity calculation
   is = 1
   int_of_DOS_d(is) = integration_simpson( -nmax, nmax, om(-nmax:nmax), dos(is,-nmax:nmax) )
   write(output_file,'(A32,2(2x,F11.8))') ' Capacity of correlated states =', int_of_DOS_d(is)
   !----- occupancy calculation
   occupancy_d(is) = integration_simpson( -nmax, nmax, om(-nmax:nmax), dos(is,-nmax:nmax)*fermi_function(-nmax:nmax) )
   occupancy_total =  2.d0*occupancy_d(is)
   write(output_file,'(A22,2x,E23.16)') '     Total occupancy =', occupancy_total
   write(output_file,'(A37,2x,E23.16)') '     Occupancy of correlated states =', occupancy_d(is)
!========================================================================================================================
ELSE
  !----- capacity calculation
  do is=1,nspin
    int_of_DOS_d(is) = integration_simpson( -nmax, nmax, om(-nmax:nmax), dos(is,-nmax:nmax) )
  end do
  !-------------------------------------------------
  int_of_DOS_total = int_of_DOS_d(1)+int_of_DOS_d(2)
  write(output_file,'(A24,2x,F11.8)') ' Capacity of Total DOS =',  int_of_DOS_total
  write(output_file,'(A32,2(2x,F11.8))') ' Capacity of correlated states =', (int_of_DOS_d(is), is=1,nspin )
  !----- occupancy calculation
  do is=1,nspin
     occupancy_d(is) = integration_simpson( -nmax, nmax, om(-nmax:nmax), dos(is,-nmax:nmax)*fermi_function(-nmax:nmax) )
  end do
  occupancy_total = occupancy_d(1) + occupancy_d(2)
  Magnetic_moment = occupancy_d(1) - occupancy_d(2)
  write(output_file,'(A22,2x,E23.16)') '     Total occupancy =', occupancy_total
  write(output_file,'(A37,2(2x,E23.16))') '     Occupancy of correlated states =', (occupancy_d(is), is=1,nspin )
  write(output_file,'(A22,2x,E23.16)') '     Magnetic moment =', Magnetic_moment
END IF

IF ( variable_mu .and. mu_changed .and. ((mod(iter,iter_each_change_mu)).eq.0) ) THEN
  delta_mu = fermisrc(totalDOS(-nmax:nmax), om(-nmax:nmax), nmax, 0.d0, mu_tolerance, n_total_need, t, om(-nmax), om(nmax))
  write(output_file,*) 'delta mu=', delta_mu
  if (iter.ne.0) then
     mu = mu + part_of_dmu * delta_mu
     write(output_file,*) 'apply delta mu', part_of_dmu*delta_mu
  else
     mu = mu + delta_mu
  end if
  write(output_file,*) 'new mu in iteration (', iter, ')= ', mu

!  if (part_of_dmu) then   ! may be for dielectric state
!    do i=-nmax+1,nmax-1
!       if ( (om(i-1)<delta_mu).and.(delta_mu<om(i+1)) ) then
!          om_of_mu=i
!          exit
!       end if
!    end do
!    tmpreal(1) = totalDOS(0)/totalDOS(om_of_mu)
!    if (tmpreal(1)>1.d0) then
!       tmpreal(1) = 1.d0/tmpreal(1)
!    end if
!    if (tmpreal(1)<0.01d0) then
!       tmpreal(1) = 0.01d0
!    else
!       if ((tmpreal(1)<1.0d0).and.(tmpreal(1)>0.95d0)) then
!          tmpreal(1) = 1.0d0
!       end if
!    end if
!    tmpreal(1) = tmpreal(1)*delta_mu
!    if (abs(tmpreal(1))>mu_tolerance) then
!       mu=mu+tmpreal(1)
!    end if
!  end if
END IF

! IF (iter>NDMFT) then
!    OPEN(Unit = 15, file='OUT_Green_image_DOS_fermi.dat', form='FORMATTED')
!    do i=-nmax,nmax
!       write(15,'(2x,100(2x,f20.15))') om(i), (dos(is,i)*fermi_function(i),is=1,nspin), totalDOS(i)*fermi_function(i)
!    end do
!    close(15)
! END IF

deallocate(fermi_function)
write(output_file,*)'-----     Occupancy     -----'

end subroutine occupancy_and_mu_search_1
