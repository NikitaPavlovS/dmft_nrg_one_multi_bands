module module_DOS_analitical
use module_energy
use module_impurity_1
implicit none
public DOS_0_FCC
 
contains

subroutine DOS_0_FCC
integer             :: i
double precision    :: pi = 4.d0*atan(1.d0)
 
  do i = -nmax,0
     dos_0(i)= 0
  end do 

  do i =  1, nmax 
     dos_0(i) = exp(-0.5d0*sqrt(2.d0)*om(i))/sqrt(pi*om(i)*sqrt(2.d0))
  end do 

  dos_0(0) = exp(-0.5d0*sqrt(2.d0)*0.5d0*(om(0)+om(1)))/sqrt(pi*0.5d0*(om(0)+om(1))*sqrt(2.d0))

end subroutine DOS_0_FCC
 
 
 subroutine DOS_0_Bethe
 use module_parameters
 integer      :: i
  do i = -nmax, nmax
     if (ABS(om(i)) < ABS(D)) then
        dos_0(i) = sqrt(D**2-om(i)**2)*2.d0/(pi*D**2)
     else
        dos_0(i)= 0 
     end if 
  end do
 end subroutine DOS_0_Bethe

end module module_DOS_analitical