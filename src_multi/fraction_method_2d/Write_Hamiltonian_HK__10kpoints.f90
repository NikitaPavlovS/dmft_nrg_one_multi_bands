Subroutine Write_Hamiltonian_HK

  use hamiltonian_module_nband
  use parameters_module
  use Gaussian_points_module

implicit none
!=======================================================================
! LOCAL variables
! Hamilton
complex*16,       allocatable :: Hamilton(:,:)
! k points array
double precision, allocatable :: kx(:)
double precision, allocatable :: ky(:)
double precision, allocatable :: kxyz(:,:)
double precision, allocatable :: wtkp(:)
! needed temporary variables
integer          :: ix, iy
double precision :: dx
integer          :: i, j
integer          :: kpoints
integer          :: tmp_int
!=======================================================================

kpoints = 2*ni*ngp
ALLOCATE(kx(kpoints))
ALLOCATE(ky(kpoints))
ALLOCATE(kxyz(3,kpoints*kpoints))
ALLOCATE(Hamilton( 1:ndim, 1:ndim))
ALLOCATE(wtkp(kpoints*kpoints))

!==========================================================================================
   open( 200, file='HK', form='formatted' )
   write(200,*) 100, ndim
   wtkp(:) = 1.d0
   write(200,*) ( wtkp(i), i=1,100 )

   kxyz(:,:) = 0.d0

   dx = pi/10
   DO ix = 1,10
      kx(ix) = -pi+dx*ix
      DO iy = 1,10
         ky(iy) = -pi+dx*iy
         tmp_int = (ix-1)*10 + iy
         kxyz(1,tmp_int) = kx(ix)
         kxyz(2,tmp_int) = ky(iy)
      ENDDO
   ENDDO

!   write(200,*) ( (kxyz(i,j),i=1,3), j=1,10*10 )

   DO ix = 1,10
      DO iy = 1,10
         CALL Hamilton_n_band_in_kx_ky( kx(ix), ky(iy), Hamilton)
         write(200,*) ( (Hamilton(i,j),j=i,ndim), i=1,ndim )
      ENDDO
   ENDDO
   close(200)

!==========================================================================================

DEALLOCATE( kx, ky, Hamilton, wtkp, kxyz )


End Subroutine Write_Hamiltonian_HK
