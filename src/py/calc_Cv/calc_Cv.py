# -*- coding: utf-8 -*-
"""
Created august 2017
by pns
"""

import os
import glob
import numpy as np
import re


def copy_GS(pathFrom, pathTo):
    with open(pathFrom, 'rb') as fileFrom, open(os.path.join(pathTo, 'Input_GS.dat'), 'wb') as fileTo:
        fileTo.write(fileFrom.read())


def find_results_and_recalc(path_to_dirs):
    for current_dir in path_to_dirs:
        path_to_file = os.path.join(current_dir, 'DMFT_out')
        with open(path_to_file, 'r') as f:
          outfile = f.read().split('\n')
          if (len(outfile)>2):
            data = np.array([])
            temper=['0.0']
            mu=['0.0']
            lines = outfile[-27:]
            for x in lines[::-1]:
              if x.startswith('      Lattice problem mu'):
                 mu = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
            temper = re.findall('([-\s]\d+[.]\d+\S+\d+)', outfile[-8])

            temper_f=float(temper[0])
            mu_f=float(mu[1])
 
            file_with_G = sorted(glob.glob('%s/DOS_G*' % current_dir))[-1]
            copy_GS(file_with_G, current_dir)

            os.chdir(current_dir)
            os.system( '01_heat_capacity.run %f %f' %(temper_f, mu_f) )
            os.chdir(home_dir)


if __name__ == '__main__':

    home_dir = os.getcwd()

    path_to_dirs = glob.glob('*/*on')
    find_results_and_recalc(path_to_dirs)

    path_to_dirs = glob.glob('*/*off')
    find_results_and_recalc(path_to_dirs)

