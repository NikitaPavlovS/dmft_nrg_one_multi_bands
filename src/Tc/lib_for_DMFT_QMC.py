# -*- coding: utf-8 -*-
# pns 02.2018

import os
import glob
import numpy as np
from subprocess import Popen, PIPE
import re
#--------------------------------------------------------------------
#--------------------------------------------------------------------
def createFolder(path):
    if not os.path.exists(path):
        os.makedirs(path)

def copyFile(pathFrom, pathTo):
    with open(pathFrom, 'rb') as fileFrom, open(pathTo, 'wb') as fileTo:
        fileTo.write(fileFrom.read())


# input file for run DMFT(NRG) program
def inputFile_DMFT_NRG( new, temperature, mu, **params_kw):
    data = '''\
DMFT PARAMETERS:
     Value of Coloumb interaction                               |     %Fd0
     number of DMFT iterations                                  |     %d
     number of DMFT iterations after tolerance satisfaction     |     %d
     new calculation: 1 - new DMFT, 0 - start form old Sigma    |     %d
TEMPERATURE:
     temperature                                                |     %.16fd0
     tolerance of total energy convergence                      |     1.0D-3
FOR SEARCH LAMBDA:
     precision to search border of N                            |     1.0D-10
     precision to search temperature                            |     1.0D-%d

''' % (params_kw['U'],  params_kw['it'],  params_kw['it_after_tolerance'],  new,  temperature,
       params_kw['precision_of_T'],  params_kw['band_model'],  
       params_kw['n_spin'],   params_kw['H_field'][0], )
    with open(params_kw['name_input_file_1'], 'w') as f:
        f.write(data)

    data='''\
DMFT PARAMETERS:
     Value of Coloumb interaction                               |     %Fd0
     number of DMFT iterations                                  |     %d
     number of DMFT iterations after tolerance satisfaction     |     %d
     new calculation: 1 - new DMFT, 0 - start form old Sigma    |     %d
TEMPERATURE:
     temperature                                                |     %.16fd0
     tolerance of total energy convergence                      |     1.0D-3
FOR SEARCH LAMBDA:
     precision to search border of N                            |     1.0D-10
     precision to search temperature                            |     1.0D-%d

''' % (params_kw['U'],  params_kw['it'],  params_kw['it_after_tolerance'],  new,  temperature,
       params_kw['precision_of_T'],  params_kw['band_model'],  
       params_kw['n_spin'],   params_kw['H_field'][0], )
    with open(params_kw['name_input_file_2'], 'w') as f:
        f.write(data)


# script for run DMFT(NRG) program
def runer(name_script_file, prog_DMFT):
    data = '''\
mpirun -np 16 %s 2> NRG_sys
''' % prog_DMFT
    with open(name_script_file, 'w') as f:
        f.write(data)


# find self consistently mu
def find_results_mu_t_input(name_input_file, name_output_file):
    error = [0]
    temper_s = ['-1.0']
    mu_s = ['0.0']
    search_err = [0,0]
    if (os.path.exists(name_output_file)):
      with open(name_output_file, 'r') as f:
        outfile = f.read().split('\n')
        lines = outfile[-27:]
        for x in lines[::-1]:
          if x.startswith('      Lattice problem mu'):
            mu_s = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
      with open(name_input_file, 'r') as f:
        outfile = f.read().split('\n')
        temper_s = re.findall('([-\s]\d+[.]\d+)', outfile[6])

      # errors in search
      if (mu_s==['0.0'] or mu_s==[]):
          search_err[0] = 1
          mu_s = ['0.0']
      if (temper_s==['-1.0'] or temper_s==[]):
          search_err[1] = 1
          temper_s = ['-1.0']
    else:
         print '----- Error in find_results_mu_t. File  "%s"  does not exist -----' % name_output_file
         error[0] = 1
    if ((search_err[0]==1) or (search_err[1]==1)):
         print '----- Error in find_results_mu_t. mu or temperature not found -----'
         error[0] = 1
    return( float(temper_s[0]), float(mu_s[0]), error[0] )

 
# find self consistently occupancy
def find_results_t_mu_U_n_h_Delta(name_input_file, name_output_file):
    error = [0]
    temper_s = ['-1.0']
    mu_s = ['0.0']
    U_s = ['0.0']
    n_s = ['0.0']
    h_s = ['0.0']
    Delta_s = ['0.0']
    search_err = [ 0, 0, 0, 0, 0, 0 ]
    if (os.path.exists(name_output_file)):
      with open(name_output_file, 'r') as f:
        outfile = f.read().split('\n')
        lines = outfile[-27:]
        temper_s = re.findall('([-\s]\d+[.]\d+\S+\d+)', outfile[-8])
        for x in lines[::-1]:
          if x.startswith('     Total occupancy ='):
            n_s = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
          if x.startswith('      Lattice problem mu'):
            mu_s = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
      with open(name_input_file, 'r') as f:
        outfile = f.read().split('\n')
        U_s = re.findall('([-\s]\d+[.]\d+)', outfile[1])
        h_s = re.findall('([-\s]\d+[.]\d+)', outfile[18])
        if (h_s==[]):
            h_s = re.findall('([-\s]\d+[.])', outfile[18])
        Delta_s = re.findall('([-\s]\d+[.]\d+)', outfile[15])
        if (Delta_s==[]):
            Delta_s = re.findall('([-\s]\d+[.])', outfile[15])

      # errors in search
      if (mu_s==['0.0'] or mu_s==[]):
          search_err[0] = 1
          mu_s = ['0.0']
      if (temper_s==['-1.0'] or temper_s==[]):
          search_err[1] = 1
          temper_s = ['-1.0']
      if (n_s==['0.0'] or n_s==[]):
          search_err[2] = 1
          n_s = ['0.0']
      if (U_s==['0.0'] or U_s==[]):
          search_err[3] = 1
          U_s = ['0.0']
      if (h_s==['0.0'] or h_s==[]):
          search_err[4] = 1
          h_s = ['0.0']
      if (Delta_s==['0.0'] or Delta_s==[]):
          search_err[5] = 1
          Delta_s = ['0.0']
    else:
         print '----- Error in find_results_t_mu_U_n_h_Delta. File  "%s"  does not exist -----' % name_output_file
         error[0] = 1
    if ((search_err[0]==1) or (search_err[1]==1) or (search_err[2]==1) or (search_err[3]==1) or (search_err[4]==1) or (search_err[5]==1)):
         print '----- Error in find_results_t_mu_U_n_h_Delta. t, mu, U, n or Delta not found -----'
         error[0] = 1
    return( float(temper_s[0]), float(mu_s[0]), float(U_s[0]), float(n_s[0]), float(h_s[0]), float(Delta_s[0]), error[0] )


# check convergence of DMFT(NRG) calculation
def check_convergence(path_check, **params_kw):
    error = [0]
    converged=0
    search_err = 1
    t_not_found = 0

    print '  Check convergence:'
    os.chdir(params_kw['root_dir'])
    os.chdir(path_check)
    if (os.path.exists(params_kw['name_output_file'])):
      with open(params_kw['name_output_file'], 'r') as f:
        outfile = f.read().split('\n')
        for x in outfile:
          if  x.startswith('  ERROR'):
              error[0] = 1
              search_err = 0
              t_not_found = 1
              print '----- NRG temperature not found -----'
        if (error[0]==0):
          lines = outfile[-27:]
          for x in lines[::-1]:
            if x.startswith(' DMFT loop for T'):
              converged=1
            # for compare current and last calculation by occupancy_convergence
            elif x.startswith(' Occupancy convergence ='):
              tmp = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
              params_kw['occupancy_convergence'].append( float(tmp[0]) )
              search_err = 0
            # for compare current and last calculation by occupancy difference with desired occ
            elif x.startswith(' Occupancy difference with desired occ'):
              tmp = re.findall('([-\s]\d+[.]\d+\S+\d+)', x)
              params_kw['occupancy_diff_with_desired_occ'].append( float(tmp[0]) )
              search_err = 0

    else:
        print '----- Error in check_convergence. File  "%s"  does not exist -----' % params_kw['name_output_file']
        error[0] = 1
    if (search_err == 1):
        print '----- Error in check_convergence. Occupancy convergence not found in results -----'
        error[0] = 1
    os.chdir(params_kw['root_dir'])
    return(converged, error[0], t_not_found)


# new DMFT(NRG) calculation with input parameters
def DMFT_new(path_start, temperature, **params_kw):
    print '  new DMFT:'
    os.chdir(params_kw['root_dir'])
    dos_file_start = sorted(glob.glob('%s/DOS_G*' % path_start))
    if (len(dos_file_start)==0):
        print '     Start DMFT'
        createFolder(path_start)
        os.chdir(path_start)
        runer(params_kw['name_script_file'], params_kw['prog_DMFT'])
        inputFile_DMFT_NRG( new=1, temperature=temperature, mu=params_kw['mu_start'], **params_kw )
        p = Popen('bash %s'%params_kw['name_script_file'], shell=True, stdout=PIPE, stderr=PIPE)
        p.wait()
        os.chdir(params_kw['root_dir'])
    else:
        print '----- Warning in DMFT_new.  "DOS files"  exist -----'
        # search value of part_of_dmu  of previous DMFT calculation
        os.chdir(params_kw['root_dir'])
        os.chdir(path_start)
        if (os.path.exists(params_kw['name_input_file'])):
          with open(params_kw['name_input_file'], 'r') as f:
            outfile = f.read().split('\n')
            params_kw['part_of_dmu'][1] = float( re.findall('([-\s]\d+[.]\d+)', outfile[31])[0] )
            print '   part_of_dmu  from  previous DMFT calculation', params_kw['part_of_dmu'][1]
        os.chdir(params_kw['root_dir'])

        #  for part_of_dmu is using  value of part_of_dmu_start  or  value form previous DMFT calculation
        if ( params_kw['part_of_dmu_use_start'] == 'F' ):
            params_kw['part_of_dmu'][0] = params_kw['part_of_dmu'][1]
        # Change  iter_each_change_mu
        if ( (params_kw['part_of_dmu'][0] < 0.005) and (params_kw['iter_each_change_mu'][0] > 4) ):
            print '!!!! Change params[iter_each_change_mu] from %.6f to 4' %( params_kw['iter_each_change_mu'][0] )
            params_kw['iter_each_change_mu'][0] = 4


# continue DMFT(NRG) calculation with T and mu from output
def DMFT_continue(path_continue, **params_kw):
  error = [0]
  os.chdir(params_kw['root_dir'])
  if os.path.exists(path_continue):
    os.chdir(path_continue)
    dos_files = sorted(glob.glob('DOS_G*'))
    N_dos_files = len(dos_files)
    if (N_dos_files>0):
        #--------------------------------------------------------------------
        # search mu and temperature
        temper_f, mu_f, error[0] = find_results_mu_t_input(name_input_file=params_kw['name_input_file'],
                                                     name_output_file=params_kw['name_output_file'])

        #--------------------------------------------------------------------
        # save old file to new dir "path_to_old_data"
        path_to_old_data='001_%s' %N_dos_files
        if os.path.exists(path_to_old_data):
           path_exists = 1
           i=2
           while (path_exists == 1):
              if not os.path.exists(path_to_old_data+'_%s'%i):
                 path_to_old_data = path_to_old_data+'_%s'%i
                 path_exists = 0
              i=i+1
        os.makedirs(path_to_old_data)
        for name_file in params_kw['files_to_save']:
           if os.path.exists(name_file):
              copyFile(name_file,os.path.join(path_to_old_data,name_file))
              os.remove(name_file)

        copyFile(dos_files[-1],'sig.dat')
        index=0
        for files in dos_files:
            index=index+1
            if (N_dos_files-index)<5: # save only last 4 files
               copyFile( files, os.path.join(path_to_old_data,files) )
            os.remove(files)

        #--------------------------------------------------------------------
        # write new input file
        inputFile_DMFT_NRG( new=0, temperature=temper_f, mu=mu_f, **params_kw )
        runer(params_kw['name_script_file'], params_kw['prog_DMFT'])

        #--------------------------------------------------------------------
        # run new calc
        p = Popen('bash %s'%params_kw['name_script_file'], shell=True, stdout=PIPE, stderr=PIPE)
        p.wait()
        #--------------------------------------------------------------------
    else:
        print '----- Error in DMFT_continue.  "DOS files"  do not exist -----'
        error[0] = 1
    os.chdir(params_kw['root_dir'])

  return(error[0])


# continue DMFT(NRG) calculation with T and mu from output 5 iters and check convergence
def run_DMFT(path, temperature, **params_kw):
    error = [0]
    params_kw['occupancy_convergence'] = []
    params_kw['occupancy_diff_with_desired_occ'] = []
    #--------------------------------------------------------------------------------------
    print ''
    print '----------------------------------------'
    DMFT_new(path, temperature, **params_kw)
    #--------------------------------------------------------------------------------------
    DMFT_conveged, error[0], t_not_found = check_convergence(path, **params_kw)
    if (t_not_found==0):
     for iter in range( 1, params_kw['iters_of_DMFT_continue'] ):
      if (error[0]==0):
        if ( DMFT_conveged == 0 ):
            print ' Start DMFT continue # %d' % iter
            error[0] = DMFT_continue(path, **params_kw)
            DMFT_conveged, error[0], t_not_found  = check_convergence(path, **params_kw)
        else:
            break

        # if in DMFT_continue occupancy convergence greater then in previous DMFT calculation
        # and occupancy difference with desired occupancy is also greater

        if ( params_kw['occupancy_convergence'][iter] > params_kw['occupancy_convergence'][iter-1] ):
          if ( params_kw['occupancy_diff_with_desired_occ'][iter] > params_kw['occupancy_diff_with_desired_occ'][iter-1] ):
            print '!!!! DMFT calculation does not conveged, occupancy convergence of DMFT cycles:'
            print ' ', params_kw['occupancy_convergence']
            print '     occupancy difference with desired occ:'
            print ' ', params_kw['occupancy_diff_with_desired_occ']
            if ( params_kw['part_of_dmu'][0] < 0.005 ):
                small_factor = 0.5
            else:
                small_factor = 0.1
            print '!!!! Change params[part_of_dmu] from %.6f to %.6f' %( params_kw['part_of_dmu'][0], small_factor * params_kw['part_of_dmu'][0] )
            params_kw['part_of_dmu'][0] = small_factor * params_kw['part_of_dmu'][0]

            # Change  iter_each_change_mu
            if ( (params_kw['part_of_dmu'][0] < 0.005) and (params_kw['iter_each_change_mu'][0] > 4) ):
                print '!!!! Change params[iter_each_change_mu] from %.6f to 4' %( params_kw['iter_each_change_mu'][0] )
                params_kw['iter_each_change_mu'][0] = 4

     if ( (DMFT_conveged==1) ):
        print '+++++ DMFT is conveged for T = %.16f'%temperature
     else:
        print '----- DMFT calculation does not conveged for T = %.16f'%temperature

    return( DMFT_conveged, error[0], t_not_found )



# main function which calling external
# for DMFT calculation of Bethe and search temperature of surperconductivity
def DMFT_QMC_T_cycle(T_array, **params_kw):
    params_kw['root_dir'] = os.getcwd()
    errors = [0]

    for T in T_array:
        params_kw['T'] = T
        params_kw['part_of_dmu'] = [ params_kw['part_of_dmu_start'], 0.0 ]
        print '=================================================='
        print '=====     START DMFT calculation for T = %f' % params_kw['T']
        errors[0] = 0

        #--------------------------------------------------------------------------------------
        # start DMFT convergence
        path_now = 'U%s_n%s_H%s_T%s' % ( params_kw['U'], params_kw['occupancy'], params_kw['H_field'], params_kw['T'] )
        DMFT_conveged, errors[0], t_not_found = run_DMFT( path_now, params_kw['T'], **params_kw )
        print ''
        #--------------------------------------------------------------------------------------
        # print results
        if ( errors[0]==0 ):
            os.chdir(params_kw['root_dir'])
            os.chdir(path_now)
            # print data (critical temperature and other to display)
            temper_f, mu_f, errors[0] = find_results_mu_t_input(name_input_file=params_kw['name_input_file'],name_output_file=params_kw['name_output_file'])
            os.chdir(params_kw['root_dir'])
            print '+++++ for U = %f.  Is Tc found ? Answer: %d.  mu = %.16f  Tc = %.16f' % ( params_kw['U'], Tc_found, mu_f, temperature_now )

        if (errors[0]!=0):
            print '    ERRORS for T = %f' % params_kw['T']
        print ''
        print '======     END calculation for T = %f' % params_kw['T']
        print '=================================================='
        print ''
        print ''





# main function which calling external
# for DMFT calculation of Bethe and search temperature of surperconductivity
def DMFT_NRG_T_cycle(T_array, **params_kw):
    params_kw['root_dir'] = os.getcwd()
    temperature_now = temperature_start
    errors = [0]

    for T in T_array:
        params_kw['T'] = T
        params_kw['part_of_dmu'] = [ params_kw['part_of_dmu_start'], 0.0 ]
        print '=================================================='
        print '=====     START DMFT calculation for T = %f' % params_kw['T']
        errors[0] = 0

        #--------------------------------------------------------------------------------------
        # calc NRG temperature by needed temperature
        params_kw['T'], errors[0] = define_NRG_T(params_kw['T'], **params_kw)
        if (errors[0]==0):
            # start DMFT convergence
            path_now = 'D%s_U%s_n%s_H%s_T%s' % ( params_kw['Delta'], params_kw['U'], params_kw['occupancy'], params_kw['H_field'], params_kw['T'] )
            DMFT_conveged, errors[0], t_not_found = run_DMFT( path_now, params_kw['T'], **params_kw )
        print ''
        #--------------------------------------------------------------------------------------
        # print results
        if ( errors[0]==0 ):
            os.chdir(params_kw['root_dir'])
            os.chdir(path_now)
            # print data (critical temperature and other to display)
            temper_f, mu_f, errors[0] = find_results_mu_t_input(name_input_file=params_kw['name_input_file'],name_output_file=params_kw['name_output_file'])
            os.chdir(params_kw['root_dir'])
            print '+++++ for U = %f.  Is Tc found ? Answer: %d.  mu = %.16f  Tc = %.16f' % ( params_kw['U'], Tc_found, mu_f, temperature_now )

        if (errors[0]!=0):
            print '    ERRORS for T = %f' % params_kw['T']
        print ''
        print '======     END calculation for T = %f' % params_kw['T']
        print '=================================================='
        print ''
        print ''