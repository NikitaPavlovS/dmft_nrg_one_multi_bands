subroutine heat_capacity_1

  use module_energy
  use module_impurity_1
  use module_parameters
  use module_integration

   implicit none

integer                       :: i, is
double precision              :: tmpd
double precision              :: ch2
double precision, allocatable :: integral(:)

   !=============================================================================
   ! Calculation of Total Energy
   allocate(integral(-nmax:nmax))
   integral(:) = 0.d0
   Cv = 0.d0

   if (nspin==1) then
      is = 1
      do i = -nmax, nmax
         ch2 =  om(i) / (cosh( om(i)/(2.d0*t) ))**2
         integral(i) = ch2*( dimag( (om(i)-sig(is,i))*G(is,i) ) + 0.5d0*dimag( sig(is,i)*G(is,i) ) )
      end do
   else
      do i = -nmax, nmax
         ch2 =  om(i) / (cosh( om(i)/(2.d0*t) ))**2
         integral(i) = ch2*( dimag( (om(i)+mu-sig(1,i))*G(1,i) + (om(i)+mu-sig(2,i))*G(2,i) ) + 0.5d0*dimag( sig(1,i)*G(1,i) + sig(2,i)*G(2,i) ) )
      end do
   end if

   Cv = integration_simpson( -nmax, nmax, om(-nmax:nmax), integral(-nmax:nmax) )
 
   if (nspin==1) then
      Cv = - Cv/( 2.d0*pi * (t**2) )
   else
      Cv = - Cv/( 4.d0*pi * (t**2) )
   end if

   deallocate(integral)

end subroutine heat_capacity_1
