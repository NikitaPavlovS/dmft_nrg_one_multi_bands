subroutine after_DMFT_loop_one_band

  use energy_module
  use hamiltonian_module_1band
  use parameters_module
  use impurity_module

complex*16, allocatable :: Gtmp(:)
integer                 :: i, is
integer                 :: number_of_file_for_dd

number_of_file_for_dd = 200+iter

SELECT CASE (Method_of_Green_Function_calculation)
!======================================================================================
CASE (1)
   IF (DOS_plot_after_DMFT_loop) THEN
      if ((nspin==2).and.(H_field>1E-6)) then
         sig(1,:) = sig(1,:) - dcmplx(H_field/2.d0, 0.d0)
         sig(2,:) = sig(2,:) + dcmplx(H_field/2.d0, 0.d0)
      end if
      allocate(Gtmp(1:nspin))
      OPEN(Unit = 14, file='OUT_Green_image_DOS.dat', form='FORMATTED')
      OPEN(unit = 13, file='OUT_Green_real.dat', form='FORMATTED')
      IF (Pseudogap) THEN
         do i=-nmax,nmax
            Call GreenFunction_of_E_Sigma_mu_fraction_PG( om(i), sig(:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:))
            !----------------------------------------------------
            write(13,'(2x,100(2x,f20.15))') om(i), (dreal(Gtmp(is)),is=1,nspin)
            write(14,'(2x,100(2x,f20.15))') om(i), (dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
         end do
      ELSE
         do i=-nmax,nmax
            Call GreenFunction_of_E_Sigma_mu_fraction_noPG( om(i), sig(:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:))
            !----------------------------------------------------
            write(13,'(2x,100(2x,f20.15))') om(i), (dreal(Gtmp(is)),is=1,nspin)
            write(14,'(2x,100(2x,f20.15))') om(i), (dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
         end do
      END IF
      close(14)
      close(13)
      Call occupancy_and_mu_search_one_band
      deallocate(Gtmp)
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Bands_plot.or.Fermi_Surface_without_Interaction) THEN
      CALL Bands_in_directions_and_Bare_Fermi_surface_fraction
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Spectral_Function_plot.OR.Spectral_Function_maximum_plot) THEN
      CALL Spectral_Function_in_symmetric_direction_fraction
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Fermi_surface_plot) THEN
       CALL Fermi_surface_fraction
   END IF
!======================================================================================
CASE (2)
   IF (DOS_plot_after_DMFT_loop) THEN
      if ((nspin==2).and.(H_field>1E-6)) then
         sig(1,:) = sig(1,:) - dcmplx(H_field/2.d0, 0.d0)
         sig(2,:) = sig(2,:) + dcmplx(H_field/2.d0, 0.d0)
      end if
      allocate(Gtmp(1:nspin))
      OPEN(Unit = 14, file='OUT_Green_image_DOS.dat', form='FORMATTED')
      OPEN(unit = 13, file='OUT_Green_real.dat', form='FORMATTED')
      IF (Pseudogap) THEN
         do i=-nmax,nmax
            !Call GreenFunction_of_E_Sigma_mu_Sigma_fraction_PG( om(i), sig(:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:))
            !----------------------------------------------------
            write(13,'(2x,100(2x,f20.15))') om(i), (dreal(Gtmp(is)),is=1,nspin)
            write(14,'(2x,100(2x,f20.15))') om(i), (dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
         end do
      ELSE
         do i=-nmax,nmax
            !Call GreenFunction_of_E_Sigma_mu_Sigma_fraction_noPG( om(i), sig(:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:))
            !----------------------------------------------------
            write(13,'(2x,100(2x,f20.15))') om(i), (dreal(Gtmp(is)),is=1,nspin)
            write(14,'(2x,100(2x,f20.15))') om(i), (dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
         end do
      END IF
      close(14)
      close(13)
      Call occupancy_and_mu_search_one_band
      deallocate(Gtmp)
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Bands_plot.or.Fermi_Surface_without_Interaction) THEN
!       CALL Bands_in_directions_and_Bare_Fermi_surface_Sigma_fraction( Bands_plot, FBANDS, FBANDS_write,&
!                               Fermi_Surface_without_Interaction, number_of_band_crossing_FL, mu )
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Spectral_Function_plot.OR.Spectral_Function_maximum_plot) THEN
!       CALL Spectral_Function_in_symmetric_direction_Sigma_fraction( iter+1, Spectral_Function_plot, 1,&
!                              om, MaxK_point_for_SF, sig, mu, t, Spectral_Function_maximum_plot, Spectral_Function_maximum_low_cutting)
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Fermi_surface_plot) THEN
!       CALL Fermi_surface_Sigma_fraction( 0, om, sig, mu, t, Energy_shift, Apply_Hand_Sigma_value, Hand_Sigma_value )
   END IF
!======================================================================================
CASE (3)
   IF (DOS_plot_after_DMFT_loop) THEN
      if ((nspin==2).and.(H_field>1E-6)) then
         sig(1,:) = sig(1,:) - dcmplx(H_field/2.d0, 0.d0)
         sig(2,:) = sig(2,:) + dcmplx(H_field/2.d0, 0.d0)
      end if
      allocate(Gtmp(1:nspin))
      OPEN(Unit = 14, file='OUT_Green_image_DOS.dat', form='FORMATTED')
      OPEN(unit = 13, file='OUT_Green_real.dat', form='FORMATTED')
      IF (Pseudogap) THEN
         do i=-nmax,nmax
            !Call GreenFunction_of_E_Sigma_mu_Eigen_system_PG( om(i), sig(:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:))
            !----------------------------------------------------
            write(13,'(2x,100(2x,f20.15))') om(i), (dreal(Gtmp(is)),is=1,nspin)
            write(14,'(2x,100(2x,f20.15))') om(i), (dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
         end do
      ELSE
         do i=-nmax,nmax
            !Call GreenFunction_of_E_Sigma_mu_Eigen_system_noPG( om(i), sig(:,i), mu, Gtmp)
            dos(:,i) = -1.d0/pi*dimag(Gtmp(:))
            totalDOS(i) = 0.d0
            do is=1, nspin
               totalDOS(i) = totalDOS(i)+dos(is,i)
            end do
            dd(:,i)=dimag(sig(:,i)+1.d0/Gtmp(:))
            !----------------------------------------------------
            write(13,'(2x,100(2x,f20.15))') om(i), (dreal(Gtmp(is)),is=1,nspin)
            write(14,'(2x,100(2x,f20.15))') om(i), (dos(is,i),is=1,nspin), totalDOS(i)
            write(number_of_file_for_dd, '(2x,5(2x,f20.15))') om(i), (dd(is,i),is=1,nspin), totalDOS(i)
         end do
      END IF
      close(14)
      close(13)
      Call occupancy_and_mu_search_one_band
      deallocate(Gtmp)
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Bands_plot.or.Fermi_Surface_without_Interaction) THEN
!       CALL Bands_in_directions_and_Bare_Fermi_surface_Eigen_system( Bands_plot, FBANDS, FBANDS_write,&
!                               Fermi_Surface_without_Interaction, number_of_band_crossing_FL, mu )
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Spectral_Function_plot.OR.Spectral_Function_maximum_plot) THEN
!       CALL Spectral_Function_in_symmetric_direction_Eigen_system( iter+1, Spectral_Function_plot, 1,&
!                              om, MaxK_point_for_SF, sig, mu, t, Spectral_Function_maximum_plot, Spectral_Function_maximum_low_cutting)
   END IF
   !---------------------------------------------------------------------------------------------------------------------------------------
   IF (Fermi_surface_plot) THEN
!       CALL Fermi_surface_Eigen_system( 0, om, sig, mu, t, Energy_shift, Apply_Hand_Sigma_value, Hand_Sigma_value )
   END IF
!======================================================================================
CASE DEFAULT
     stop 'error in selection of Green function method after DMFT'
END SELECT
!======================================================================================


end subroutine after_DMFT_loop_one_band
